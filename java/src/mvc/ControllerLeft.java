package mvc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import proteo.Param;

/**
 *
 * @author Venceslas Douillard
 */
public class ControllerLeft {

	public ControllerLeft(Controller ctrl, ViewLeft left, Model model) {
//------LEFT - FILE CHOOSERS--------------------------------------------------------------------------------------------------------------------------------------

		// Input
		left.bInp.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent arg0) {

				FileChooser data = new FileChooser();
				data.setTitle("Choose a set of spectra");
				data.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter filterall = new FileChooser.ExtensionFilter("All spectra", "*.mgf",
						"*.mzxml", "*.mzXML");
				FileChooser.ExtensionFilter filtermgf = new FileChooser.ExtensionFilter("MGF (Mascot General Format)",
						"*.mgf");
				FileChooser.ExtensionFilter filtermzxml = new FileChooser.ExtensionFilter("mzXML", "*.mzxml",
						"*.mzXML");
				data.getExtensionFilters().addAll(filterall, filtermgf, filtermzxml);
				File datum = data.showOpenDialog(Main.getStage());
				if (datum != null) {
					model.propInput.set(datum.getAbsolutePath());
					Controller.lastVisitedDirectory = datum.getParent();
				}
			}
		});

		// Contaminant file
		left.bCont.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent arg0) {

				FileChooser data = new FileChooser();
				data.setTitle("Choose a contaminant file");
				data.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter filterfasta = new FileChooser.ExtensionFilter("Fasta file", "*.fasta");
				data.getExtensionFilters().add(filterfasta);
				File datum = data.showOpenDialog(Main.getStage());
				if (datum != null) {
					model.propCont.set(datum.getAbsolutePath());
					Controller.lastVisitedDirectory = datum.getParent();
				}
			}
		});

		// Database file
		left.bDatab.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent arg0) {

				FileChooser data = new FileChooser();
				data.setTitle("Choose a database");
				data.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter filterfasta = new FileChooser.ExtensionFilter("Fasta file", "*.fasta");
				data.getExtensionFilters().add(filterfasta);
				File datum = data.showOpenDialog(Main.getStage());
				if (datum != null) {
					model.propData.set(datum.getAbsolutePath());
					Controller.lastVisitedDirectory = datum.getParent();
				}
			}
		});

		// Output file
		left.bOut.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent arg0) {

				String contentParam = "";

				// Choosing the file
				FileChooser sChooser = new FileChooser();
				sChooser.setTitle("Choose your output file");
				sChooser.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
				sChooser.getExtensionFilters().add(extFilter);
				File file = sChooser.showSaveDialog(Main.getStage());

				if (file != null) {
					try {
						FileWriter fileWriter = null;
						fileWriter = new FileWriter(file);
						fileWriter.write(contentParam);
						fileWriter.close();
						model.propFeedback.set("File created.");
						model.propOutput.set(file.getAbsolutePath());
						Controller.lastVisitedDirectory = file.getParent();

					} catch (IOException ex) {
						//Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		});

		// Input list of PTM
		left.bInList.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent event) {
				FileChooser data = new FileChooser();
				data.setTitle("Choose a list of PTM");
				data.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter filtertxt = new FileChooser.ExtensionFilter("Text file", "*.txt");
				data.getExtensionFilters().add(filtertxt);
				File datum = data.showOpenDialog(Main.getStage());
				if (datum != null) {
					model.propInList.set(datum.getAbsolutePath());
					Controller.lastVisitedDirectory = datum.getParent();
				}
			}
		});

		// Input results file
		left.bInRes.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent event) {
				FileChooser data = new FileChooser();
				data.setTitle("Choose a result file");
				data.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter filtertxt = new FileChooser.ExtensionFilter("CSV file", "*.csv");
				data.getExtensionFilters().add(filtertxt);
				File datum = data.showOpenDialog(Main.getStage());
				if (datum != null) {
					model.propInRes.set(datum.getAbsolutePath());
					Controller.lastVisitedDirectory = datum.getParent();
				}
			}
		});
	}
}
