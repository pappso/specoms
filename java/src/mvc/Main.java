/* * * * *
 * This software is licensed under the Apache 2 license, quoted below.
 *
 * Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * * * * */

package mvc;

import javafx.application.Application;
import javafx.stage.Stage;
import java.util.logging.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;

/**
 * JavaFX Application of SpecOMS, using MVC design-pattern without FXML.
 *
 * @author Venceslas Douillard
 */

public class Main extends Application {
	protected static final Logger logger = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) {
		// logger.log(Level.INFO, " main arg:" + String.join(" ", args));
		launch(args);
	}

	public static Stage primaryStage = new Stage();

	@Override
	public void start(Stage primaryStage) throws Exception {

		/*
		 * If the application has been launched from the UI, nothing is passed as
		 * argument. The same thing happens when it is launched from the command line.
		 *
		 * The use of the option -c allows the user to launch the application without
		 * the UI.
		 */

		try {
			InputStream configFile = MainWrapper.class.getResourceAsStream("/logging.properties");
			if (configFile != null)
				LogManager.getLogManager().readConfiguration(configFile);
		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
		}
		logger.setLevel(Level.SEVERE); // overrides logging properties
		logger.addHandler(new ConsoleHandler());
		logger.log(Level.INFO, " start ( Stage primaryStage ) ");

		if (!MainWrapper.argument.isEmpty()) {
			if (MainWrapper.argument.equals("-c")) {
				new Model("cmd");
				System.exit(0);
			} else {
				System.out.println("Unrecognized parameter.");
				System.exit(0);
			}
			;
		} else {
			logger.log(Level.INFO, " MainWrapper.argument.isEmpty() ");
			Model model = new Model();
			View view = new View(model);
			Controller control = new Controller(view, model);
			control.startControl();

			primaryStage.setTitle("SpecOMS");
			primaryStage.setScene(view.cadre);
			primaryStage.show();

		}

	}

	public static Stage getStage() {
		return primaryStage;
	}

}
