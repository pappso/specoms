package mvc;

//Desktop loading
import java.awt.Desktop;

//Algorithm parameters
import java.io.File;
import java.io.PrintStream;

/**
 *
 * @author Venceslas Douillard
 */
public class Controller {

	protected Desktop desktop = Desktop.getDesktop();

	protected static String lastVisitedDirectory = "./";

	// Variables used to redirect standard output to a file.
	public PrintStream out;

	public PrintStream outDefault = System.out;
	public PrintStream errDefault = System.err;

	private boolean log = false;

	public void setLog(boolean newBool) {
		log = newBool;
	}

	public boolean keepLogFile() {
		return log;
	}

	protected String logPath = ".\\logfiles\\run.log";
	protected File logFile;

	private View view;
	private Model model;

	public ControllerTop ctrlTop;
	public ControllerLeft ctrlLeft;
	public ControllerContent ctrlContent;

	public Controller(View view, Model model) {

		this.view = view;
		this.model = model;

	}

	public void startControl() {

		ctrlTop = new ControllerTop(this, view.top, model);
		ctrlLeft = new ControllerLeft(this, view.left, model);
		ctrlContent = new ControllerContent(this, view.left, view.content, model);

//------BOT----------------------------------------------------------------------------------------------------------------------------------------------------------

		view.bot.progress.visibleProperty().bind(model.programUI.runningProperty());

	}
}
