package mvc;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 *
 * @author Venceslas Douillard
 */
public class ResultsGraph {
	DecimalFormat df = new DecimalFormat("#.####");
	int nbSpec = 0, nbFilter = 0;
	double deltaX, deltaY, modifiedMassDelta = 0;

	static boolean filtered = false;
	static boolean rightClicked = false;
	static double X_LOW = 0, X_HIGH = 0, Y_LOW = 0, Y_HIGH = 0;

	Stage stageGraph;
	Scene sceneGraph;
	Group root;
	VBox vWrapGraph = new VBox(5);
	Rectangle zoomRect;
	HBox hSuppInfo = new HBox(20);
	HBox hFillerFixed = new HBox();
	HBox hFiller = new HBox();
	TextField pointInfo = new TextField("<Spectrum information on click>");
	Button saveGraph = new Button("Save Graph as PNG");

	NumberAxis xAxis = new NumberAxis();
	NumberAxis yAxis = new NumberAxis();
	ObservableList<ResultsData> orderedList;
	List<BooleanProperty> selectedList;
	ObservableList<ListPTM> dataList;

	ObservableList<ResultsData> myNewList;
	ObservableList<XYChart.Data<Integer, Double>> finalResults = FXCollections.observableArrayList();
	LineChartMarkers<Number, Number> myChart;
	Data<Number, Number> horizontalMarker;
	Text pointLabel = new Text();

	public ResultsGraph(ObservableList<ResultsData> datacolOrd, List<BooleanProperty> selectedList,
			ObservableList<ListPTM> dataList) {

		orderedList = datacolOrd;
		this.selectedList = selectedList;
		this.dataList = dataList;

		myNewList = orderedList;
	}

	public void filterResults(ObservableList<ResultsFilters> filter) {

		nbFilter = 0;
		ObservableSet<ResultsData> newSet = FXCollections.observableSet();
		myNewList = FXCollections.observableArrayList();

		while (nbFilter < filter.size()) {
//    		System.out.println(filter.get(nbFilter).getMinMass()+" "+filter.get(nbFilter).getMaxMass()+
//    				" "+filter.get(nbFilter).getBeforeSpec()+" "+filter.get(nbFilter).getAfterSpec());

			newSet.addAll(orderedList.filtered(line -> (((line.getMassDelta()) > (filter.get(nbFilter).getMinMass()))
					&& ((line.getMassDelta()) < (filter.get(nbFilter).getMaxMass()))
					&& ((line.getBeforeSpec()) > (filter.get(nbFilter).getBeforeSpec()))
					&& ((line.getAfterSpec()) > (filter.get(nbFilter).getAfterSpec())))));
			nbFilter++;

		}

		myNewList.addAll(newSet);

		Collections.sort(myNewList, new Comparator<ResultsData>() {
			public int compare(ResultsData r1, ResultsData r2) {
				return r1.massDelta.getValue().compareTo(r2.massDelta.getValue());
			}
		});

	}

	/**
	 * Launch a Service calculating the graph, in case it takes a few seconds. This
	 * method also handle the display of the graph once it is successfully loaded.
	 */
	public void plotPos() {

		graphCalc.reset();
		graphCalc.start();

		graphCalc.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent e) {

				stageGraph = new Stage();
				root = new Group();
				sceneGraph = new Scene(root, 1200, 850);

				finalResults = (ObservableList<XYChart.Data<Integer, Double>>) e.getSource().getValue();

				myChart = new LineChartMarkers(xAxis, yAxis, FXCollections.observableArrayList(
						new XYChart.Series<Integer, Double>("Curve", FXCollections.observableArrayList(finalResults))));

				setUpChart();
				setUpZoom(zoomRect, myChart);
				setUpPTM();

				hSuppInfo.getChildren().addAll(hFiller, pointInfo, saveGraph);
				vWrapGraph.getChildren().addAll(myChart, hSuppInfo);
				root.getChildren().addAll(vWrapGraph, zoomRect);
				stageGraph.setScene(sceneGraph);
				showStage();
			}
		});

		graphCalc.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent e) {
				ViewAlerts.graphAlert();
			}
		});
	}

	/**
	 * Service using the spectra ordered by mass delta to plot their mass delta. All
	 * the points created are attributed a hoveredLabel object containing all the
	 * info and events. The Service returns an observable list to be integrated in a
	 * chart.
	 * 
	 * @return ObservableList<XYChart.Data>
	 */

	Service<ObservableList<XYChart.Data<Integer, Double>>> graphCalc = new Service<ObservableList<XYChart.Data<Integer, Double>>>() {

		@Override
		protected Task<ObservableList<XYChart.Data<Integer, Double>>> createTask() {
			return new Task<ObservableList<XYChart.Data<Integer, Double>>>() {

				@Override
				protected ObservableList<XYChart.Data<Integer, Double>> call() throws Exception {
					final ObservableList<XYChart.Data<Integer, Double>> dataset = FXCollections.observableArrayList();
					int i = 0;

					X_LOW = 0;
					Y_LOW = 0;
					Y_HIGH = 0;
					X_HIGH = myNewList.size() - 1;

					while (i < myNewList.size()) {

						if (Model.isLogScale()) {
							modifiedMassDelta = myNewList.get(i).getMassDelta() == 0 ? Math.log(0.0000001)
									: Math.log(Math.abs(myNewList.get(i).getMassDelta()));
						} else {
//							modifiedMassDelta = Math.abs(myNewList.get(i).getMassDelta());
							modifiedMassDelta = myNewList.get(i).getMassDelta();
						}

						if (modifiedMassDelta < Y_LOW) {
							Y_LOW = modifiedMassDelta;
						}

						final XYChart.Data<Integer, Double> data = new XYChart.Data<>(i, modifiedMassDelta);
						data.setNode(new hoveredLabel(i + 1, myNewList.get(i)));
						data.getNode().setOpacity(0.0);
						dataset.add(data);

						if (modifiedMassDelta > Y_HIGH) {
							Y_HIGH = modifiedMassDelta;
						}

						i++;

					}
					return dataset;
				}
			};
		}
	};

	/**
	 * Adds all options concerning axes, the chart or the selection rectangle.
	 */
	public void setUpChart() {
		xAxis.setTickLabelsVisible(true);
		xAxis.setLabel("Spectra");
		xAxis.setAutoRanging(false);
		xAxis.setTickUnit(200);
		xAxis.setTickMarkVisible(true);
		xAxis.setMinorTickVisible(false);
		xAxis.setLowerBound(X_LOW);
		xAxis.setUpperBound(X_HIGH);

		if (Model.isLogScale()) {
			yAxis.setLabel("log10(abs(massDelta))");
			yAxis.setAutoRanging(false);
			yAxis.setTickUnit(0.5);
			yAxis.setLowerBound(Y_LOW);
			yAxis.setUpperBound(Y_HIGH);
		} else {
			yAxis.setLabel("massDelta (in Da)");
			yAxis.setAutoRanging(false);
			yAxis.setTickUnit(50);
			yAxis.setLowerBound(Y_LOW);
			yAxis.setUpperBound(Y_HIGH);
		}

		zoomRect = new Rectangle();
		zoomRect.setManaged(false);
		zoomRect.setFill(Color.BURLYWOOD.deriveColor(0, 1, 1, 0.5));

		myChart.setAnimated(false);
		myChart.setVerticalGridLinesVisible(false);
		myChart.setHorizontalGridLinesVisible(false);
		myChart.setLegendVisible(false);
		myChart.setTitle("Mass delta of each spectra, in ascending order");
		myChart.prefWidthProperty().bind(sceneGraph.widthProperty());
		myChart.prefHeightProperty().bind(sceneGraph.heightProperty().multiply(0.95));

		hSuppInfo.setAlignment(Pos.CENTER);
		hSuppInfo.setPadding(new Insets(0, 10, 5, 10));

		hFiller.setPrefWidth(170);

		HBox.setHgrow(pointInfo, Priority.ALWAYS);
		pointInfo.setFocusTraversable(false);
		pointInfo.setEditable(false);
		pointInfo.getStyleClass().add("copyable-label");
		pointInfo.setAlignment(Pos.CENTER);

		saveGraph.setAlignment(Pos.CENTER);
		saveGraph.setMnemonicParsing(true);
		saveGraph.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				myChart.forceFocus();
				WritableImage myPng = myChart.snapshot(new SnapshotParameters(), null);
				myChart.cancelFocus();
				FileChooser fC = new FileChooser();
				fC.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
				fC.getExtensionFilters().add(extFilter);
				File wrapFile = fC.showSaveDialog(stageGraph);
				if (wrapFile != null) {
					try {
						ImageIO.write(SwingFXUtils.fromFXImage(myPng, null), "png", wrapFile);
						Controller.lastVisitedDirectory = wrapFile.getParent();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		});

		stageGraph.setTitle("SpecOMS graphical representation");
		// sceneGraph.getStylesheets().add(getClass().getResource("myStyle.css").toExternalForm());
		sceneGraph.getStylesheets().add("myStyle.css");
	}

	/**
	 * Implements the events needed for zooming. Continuous zooming can be heavy
	 * with a great number of points, therefore its is done by selecting an area.
	 * 
	 * @param rect
	 * @param myChart
	 * 
	 */
	public void setUpZoom(final Rectangle rect, LineChart<Number, Number> myChart) {

		final ObjectProperty<Point2D> mouseAnchor = new SimpleObjectProperty<>();

		myChart.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.isSecondaryButtonDown()) {
					rightClicked = true;
					final NumberAxis xAxis = (NumberAxis) myChart.getXAxis();
					xAxis.setLowerBound(X_LOW);
					xAxis.setUpperBound(X_HIGH);
					final NumberAxis yAxis = (NumberAxis) myChart.getYAxis();
					yAxis.setLowerBound(Y_LOW);
					yAxis.setUpperBound(Y_HIGH);

					rect.setWidth(0);
					rect.setHeight(0);

				} else {
					rightClicked = false;
					mouseAnchor.set(new Point2D(event.getX(), event.getY()));
					rect.setWidth(0);
					rect.setHeight(0);
				}
			}
		});

		myChart.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.isPrimaryButtonDown()) {
					double x = event.getX();
					double y = event.getY();

					rect.setX(Math.min(x, mouseAnchor.get().getX()));
					rect.setY(Math.min(y, mouseAnchor.get().getY()));
					rect.setWidth(Math.abs(x - mouseAnchor.get().getX()));
					rect.setHeight(Math.abs(y - mouseAnchor.get().getY()));
				}
			}
		});

		myChart.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (!rightClicked) {
					doZoom(rect, myChart);
				} else {
					rect.setWidth(0);
					rect.setHeight(0);
				}
			}
		});

	}

	/**
	 * A horizontal line is added for each selected PTM. It is placed at the level
	 * of the corresponding mass delta and associated with a label.
	 */
	public void setUpPTM() {

		for (int i = 0; i < selectedList.size(); i++) {
			if (selectedList.get(i).get() == true) {
				if (Model.isLogScale()) {
					horizontalMarker = new Data<>(0, Math.log(dataList.get(i).getMassDelta()));
				} else {
					horizontalMarker = new Data<>(0, dataList.get(i).getMassDelta());

				}
				myChart.addHorizontalValueMarker(horizontalMarker,
						dataList.get(i).getPTM() + " " + dataList.get(i).getMassDelta());
			}
		}
	}

	/**
	 * Calculates the new area to display for the zoom.
	 * 
	 * @param zoomRect
	 * @param chart
	 */
	private void doZoom(Rectangle zoomRect, LineChart<Number, Number> chart) {
		Point2D zoomTopLeft = new Point2D(zoomRect.getX(), zoomRect.getY());
		Point2D zoomBottomRight = new Point2D(zoomRect.getX() + zoomRect.getWidth(),
				zoomRect.getY() + zoomRect.getHeight());
		final NumberAxis yAxis = (NumberAxis) chart.getYAxis();
		Point2D yAxisInScene = yAxis.localToScene(0, 0);
		final NumberAxis xAxis = (NumberAxis) chart.getXAxis();
		Point2D xAxisInScene = xAxis.localToScene(0, 0);
		double xOffset = zoomTopLeft.getX() - yAxisInScene.getX();
		double yOffset = zoomBottomRight.getY() - xAxisInScene.getY();
		double xAxisScale = xAxis.getScale();
		double yAxisScale = yAxis.getScale();

		// Do nothing when only left clicked
		if (!zoomTopLeft.equals(zoomBottomRight)) {
			xAxis.setLowerBound((int) (xAxis.getLowerBound() + xOffset / xAxisScale));
			if (xAxis.getLowerBound() < 0) {
				xAxis.setLowerBound(0);
			}
			xAxis.setUpperBound((int) (xAxis.getLowerBound() + zoomRect.getWidth() / xAxisScale));
			yAxis.setLowerBound(yAxis.getLowerBound() + yOffset / yAxisScale);
			yAxis.setUpperBound(yAxis.getLowerBound() - zoomRect.getHeight() / yAxisScale);
		}
		zoomRect.setWidth(0);
		zoomRect.setHeight(0);

	}

	/**
	 * Using data from the TableView, allows the user to see the mass delta of a
	 * spectrum. The point will only appear if you hover it. Clicking on it will
	 * display the full info of the spectrum.
	 * 
	 * @extends StackPane
	 */
	private class hoveredLabel extends StackPane {
		hoveredLabel(int nb, ResultsData myResult) {

			double myMassDelta = myResult.getMassDelta();

			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent e) {
					pointLabel.setText("n°" + nb + "\n" + df.format(myMassDelta));
					getChildren().add(pointLabel);
					setOpacity(1);
					pointLabel.setTranslateX(e.getX() + 30);
					pointLabel.setTranslateY(e.getY() + 25);
					pointLabel.setVisible(true);

				}
			});

			setOnMouseExited(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent e) {
					getChildren().clear();
					setOpacity(0);
					pointLabel.setVisible(false);
				}
			});

			setOnMousePressed(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent e) {
					pointInfo.setText(
							myResult.getSpectrumID() + ";" + myResult.getPeptide() + ";" + myResult.getBeforeSpec()
									+ ";" + myResult.getAfterSpec() + ";" + myResult.getMassDeltaLoc() + ";"
									+ myResult.getMassDelta() + ";" + myResult.getAffix() + ";" + myResult.getOrigin());
				}
			});
		}
	}

	public void showStage() {
		stageGraph.show();
	}

}
