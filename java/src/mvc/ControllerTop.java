package mvc;

import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import proteo.Param;

/**
 *
 * @author Venceslas Douillard
 */
public class ControllerTop {

	public ControllerTop(Controller ctrl, ViewTop top, Model model) {

//------TOP------------------------------------------------------------------------------------------------------------------------------------------------------

		/*
		 * Saving parameters The user may want to save his parameters after setting them
		 * up. This method reads all the parameters and saves them in a file, following
		 * the proper formatting.
		 */
		top.saveParamMenuItem.setOnAction(e -> {
			String contentParam = "minimumPeptideLength=" + model.propMinPep.get() + "\n" + "maximumPeptideLength="
					+ model.propMaxPep.get() + "\n" + "threshold=" + model.propThresh.get() + "\n" + "minimumScore="
					+ model.propScore.get() + "\n" + "maxMassesCount=" + model.propMMC.get() + "\n"
					+ "minimumPeptideCharge=" + model.propMinCharg.get() + "\n" + "maximumPeptideCharge="
					+ model.propMaxCharg.get() + "\n" + "numberOfDecimals="
					+ model.propAcc.get().split("\\.")[1].length() + "\n" + "decimalValue="
					+ Integer.parseInt(model.propAcc.get().split("\\.")[1].replaceAll("^0*", "")) + "\n" + "decoyBase="
					+ String.valueOf(model.propDecoy.get()) + "\n" + "shift=" + String.valueOf(model.propShift.get())
					+ "\n" + "missDB=" + model.propMissDatabase.get() + "\n" + "ter=" + model.terToText() + "\n"
					+ "aaCleaved=" + model.propAACleaved.get() + "\n";
			if (model.propAANotCleaved.get().equals("")) {
				contentParam += "aaNotCleaved=<none>\n";
			} else {
				contentParam += "aaNotCleaved=" + model.propAANotCleaved.get() + "\n";
			}
			contentParam += "enzPattern=" + model.propEnzPattern.get() + "\n" + "nbMissCleavage="
					+ model.propMissCleavage.get();

			FileChooser sChooser = new FileChooser();
			sChooser.setInitialDirectory(new File(Controller.lastVisitedDirectory));
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("INI files (*.ini)", "*.ini");
			sChooser.getExtensionFilters().add(extFilter);
			File file = sChooser.showSaveDialog(Main.getStage());

			if (file != null) {
				try {
					FileWriter fileWriter = null;
					fileWriter = new FileWriter(file);
					fileWriter.write(contentParam);
					fileWriter.close();
					model.propFeedback.set("Parameters saved.");
					Controller.lastVisitedDirectory = file.getParent();
				} catch (IOException ex) {
					//Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

		/*
		 * Loading parameters can't be done during a run.
		 */
		top.loadParamMenuItem.disableProperty().bind(model.programUI.runningProperty());

		top.loadParamMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent arg0) {
				FileChooser data = new FileChooser();
				data.setTitle("Choose your configuration file");
				data.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter filtertxt = new FileChooser.ExtensionFilter(".ini", "*.ini");
				data.getExtensionFilters().addAll(filtertxt);
				File datum = data.showOpenDialog(Main.getStage());
				if (datum != null) {
					Param.SetConfigFile(datum.getAbsolutePath());

					Controller.lastVisitedDirectory = datum.getParent();

					Param.setConfiguration();
					model.propAcc.set(String.valueOf(Param.accuracy()));
					model.propMaxPep.set(Param.maxPeptideLength());
					model.propMinPep.set(Param.minPeptideLength());
					model.propDecoy.set(Param.decoy());
					model.propShift.set(Param.shift());
					model.propMinCharg.set(Param.minimalCharge());
					model.propMaxCharg.set(Param.maximalCharge());
					model.propThresh.set(Param.threshold());
					model.propScore.set(Param.minimumScore());
					model.propMMC.set(Param.massFilterSize());
					model.propMissDatabase.set(Param.missDB());
					model.propTer.set(Param.ter());
					model.propAACleaved.set(Param.aaCleaved());
					model.propAANotCleaved.set(Param.aaNotCleaved());
					model.propEnzPattern.set(Param.enzPattern());
					model.propMissCleavage.set(Param.missCleavage());

					model.propFeedback.set("Parameters loaded.");
					Controller.lastVisitedDirectory = datum.getParent();
				}
			}
		});

		top.exitMenuItem.setOnAction(e -> {
			Platform.exit();
		});

		// TOP - Menu Option method
		top.resetMenuItem.setOnAction(e -> {
			model.propAcc.set("0.02");
			model.propMinPep.set(7);
			model.propMaxPep.set(25);
			model.propThresh.set(8);
			model.propScore.set(8);
			model.propDecoy.set(false);
			model.propMinCharg.set(1);
			model.propMaxCharg.set(3);
			model.propMMC.set(50);
			model.propTer.set(false);
			model.propMissCleavage.set("0");
			model.propMissDatabase.set(false);
			model.propAACleaved.setValue("KR");
			model.propAANotCleaved.setValue("<none>");
		});

		// Sets the standard output to a specific log file if selected.
		top.logMenuItem.selectedProperty().addListener((lis, oldv, newv) -> {
			if (newv == true) {
				ctrl.setLog(true);
			} else {
				ctrl.setLog(false);
			}
		});

		// Choosing the log file location.
		top.dirLogMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent arg0) {

				// Choosing the file
				FileChooser sChooser = new FileChooser();
				sChooser.setTitle("Choose your log file");
				sChooser.setInitialDirectory(new File(Controller.lastVisitedDirectory));
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("LOG files (*.log)", "*.log");
				sChooser.getExtensionFilters().add(extFilter);
				ctrl.logFile = sChooser.showSaveDialog(Main.getStage());
				ctrl.logPath = ctrl.logFile.getAbsolutePath();
				model.propFeedback.set("Log file created.");
				Controller.lastVisitedDirectory = ctrl.logFile.getParent();

			}
		});

		/*
		 * TOP - Help & Contact
		 */

		/*
		 * Opens the README.txt file.
		 */
		top.readmeMenuItem.setOnAction(e -> {
			File readme = new File("README.txt");
			if (readme.exists()) {
				try {
					ctrl.desktop.open(readme);
				} catch (IOException ex) {
					//Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
				}
			} else {
				ViewAlerts.readmeAlert();
			}
		});

		/*
		 * Opens the link of the GitHub if README file is not present.
		 */

		ViewAlerts.link.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				try {
					ViewAlerts.closeReadMeAlert();
					Desktop.getDesktop().browse(new URI("https://github.com/matthieu-david/SpecOMS"));
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});

		top.contactMenuItem.setOnAction(e -> {
			ViewAlerts.contactAlert();
		});

	}
}
