package mvc;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

/**
 * Static alert boxes which can be called from anywhere in the program.
 *
 * @author Venceslas Douillard
 */
public class ViewAlerts {

	private static Alert format = new Alert(AlertType.WARNING);
	private static Alert missingInOut = new Alert(AlertType.ERROR);
	private static Alert missingParam = new Alert(AlertType.ERROR);
	private static Alert wrongPath = new Alert(AlertType.ERROR);
	private static Alert contactDial = new Alert(AlertType.INFORMATION);
	private static Alert noReadme = new Alert(AlertType.ERROR);
	private static Alert parsing = new Alert(AlertType.WARNING);
	private static Alert filter = new Alert(AlertType.WARNING);
	private static Alert graph = new Alert(AlertType.WARNING);

	// Allows different text objects to be placed consecutively.
	FlowPane readmeLink = new FlowPane();
	Label presLink = new Label("The README.txt is available on GitHub.");
	static Hyperlink link = new Hyperlink("Do you wish to access this file ?");

	public ViewAlerts() {

		format.setTitle("Format error");
		format.setContentText("Letters and two digit numbers are not allowed.");

		missingInOut.setTitle("Application cannot start.");
		missingInOut.setHeaderText("One or several input and/or output files paths are missing.");
		missingInOut.setContentText("Please fill out all the paths before starting.");

		wrongPath.setTitle("Application cannot start.");
		wrongPath.setHeaderText("One or several paths do not exist.");
		wrongPath.setContentText("Please check if one of the provided files has been deleted or moved.");

		missingParam.setTitle("Application cannot start.");
		missingParam.setHeaderText("One or several parameters do not exist.");
		missingParam.setContentText(
				"Please fill out all the parameters. Default parameters can also be found in the Option menu.");

		contactDial.setTitle("Contact Us");
		contactDial.setHeaderText(
				"Any trouble with our software and you can't find the solution in the provided documentation ?");
		contactDial.setContentText("Contact us at dominique.tessier@inra.fr or matthieu.david@univ-nantes.fr");

		noReadme.setTitle("File cannot be displayed.");
		noReadme.setHeaderText("The README.txt file is not present in the software files.");
		readmeLink.getChildren().addAll(presLink, link);
		noReadme.getDialogPane().setContent(readmeLink);

		parsing.setTitle("Parsing error.");
		parsing.setHeaderText("The mzXML file is not properly formated.");
		parsing.setContentText(
				"The concerned spectrum has been skipped, however results and behavior are not guarenteed anymore.");

		filter.setTitle("Unknown parameters.");
		filter.setHeaderText("One or several parameters are either missing or not properly formatted.");
		filter.setContentText(
				"Uncheck the parameters you do not want to filter and make sure they respect the following formats :\n"
						+ "\t- Minimum mass delta : floating number\n" + "\t- Maximum mass delta : floating number\n"
						+ "\t- Minimum score before SpecFit : integer\n" + "\t- Minimum score after SpecFit : integer");

		graph.setTitle("Plotting error.");
		graph.setHeaderText("The graph cannot be displayed.\n");
		graph.setContentText("This problem usually occurs when the input contains too many spectra.\n"
				+ "Try to filter your input file.");

	}

	public static void formatAlert() {
		format.showAndWait();
	}

	public static void inOutAlert() {
		missingInOut.showAndWait();
	}

	public static void pathAlert() {
		wrongPath.showAndWait();
	}

	public static void paramAlert() {
		missingParam.showAndWait();
	}

	public static void contactAlert() {
		contactDial.showAndWait();
	}

	public static void readmeAlert() {
		noReadme.showAndWait();
	}

	public static void closeReadMeAlert() {
		noReadme.close();
	}

	public static void parsingAlert() {
		parsing.showAndWait();
	}

	public static void filterAlert() {
		filter.showAndWait();
	}

	public static void graphAlert() {
		graph.showAndWait();
	}

}
