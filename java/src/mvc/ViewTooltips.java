package mvc;

import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Venceslas Douillard
 */
public class ViewTooltips {

	public ViewTooltips(View view) {

//----------ENZYMATIC PARAM-----------------------------------------------------------------
		Tooltip tPreset = new Tooltip("Sets the enzyme cleavage parameters to one of six proteases.");
		tPreset.setMaxWidth(300);
		tPreset.setWrapText(true);
		tPreset.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.protease.setTooltip(tPreset);
		view.content.lblEnz.setTooltip(tPreset);

		Tooltip tCleave = new Tooltip("Protein bank will be digested in-silico cutting after these amino acids.");
		tCleave.setMaxWidth(300);
		tCleave.setWrapText(true);
		tCleave.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.tfCleaved.setTooltip(tCleave);

		Tooltip tNotCleave = new Tooltip("Protein bank will be digested in-silico and not cut when these amino acids "
				+ "are following one that should be cleaved.");
		tNotCleave.setMaxWidth(300);
		tNotCleave.setWrapText(true);
		tNotCleave.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.tfNotCleaved.setTooltip(tNotCleave);

		Tooltip tExtrem = new Tooltip("Enzymes can cut before or after certain amino acids. "
				+ "N-terminal corresponds to a cut before the said amino acid\nwhereas C-ter is a cut after the amino acid.");
		tExtrem.setMaxWidth(300);
		tExtrem.setWrapText(true);
		tExtrem.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblExtrem.setTooltip(tExtrem);
		view.content.rbNTer.setTooltip(tExtrem);
		view.content.rbCTer.setTooltip(tExtrem);

		Tooltip tPattern = new Tooltip("This regular expression has two purposes. "
				+ "On the one hand it updates when you use the classic parameters, on the other hand it can be used to directly modify how peptides are generated.");
		tPattern.setMaxWidth(300);
		tPattern.setWrapText(true);
		tPattern.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblPattern.setTooltip(tPattern);
		view.content.tfRegexPattern.setTooltip(tPattern);

		Tooltip tMissDB = new Tooltip(
				"Peptides created by miss-cleavage are either:\n\t- stored in the theoretical spectra database (traditional)- calculated dynamically (post-analysis using mass deltas, which is quicker and should return less false positives.");
		tMissDB.setMaxWidth(300);
		tMissDB.setWrapText(true);
		tMissDB.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.rbStoredMC.setTooltip(tMissDB);
		view.content.rbComputedMC.setTooltip(tMissDB);

		Tooltip tTer = new Tooltip("Choose the cutting side of the enzyme.");
		tTer.setMaxWidth(300);
		tTer.setWrapText(true);
		tTer.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.rbCTer.setTooltip(tTer);
		view.content.rbNTer.setTooltip(tTer);

		Tooltip tMissCleavage = new Tooltip("Number of allowed missed cleavage. "
				+ "Create new peptides simulating the absence of enzyme cleavage, which can potentially match with experimental spectra. "
				+ "Be aware that beyond 2 missed cleavages, peptides must be stored in the database (the mass of prefix and suffix were not calculated)");
		tMissCleavage.setMaxWidth(300);
		tMissCleavage.setWrapText(true);
		tMissCleavage.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.missCleavage.setTooltip(tMissCleavage);
		view.content.lblMissCleavage.setTooltip(tMissCleavage);

//------SPECOMS PARAM-----------------------------------------------------------------------
		Tooltip tRange = new Tooltip("Allowed peptide sizes during in-silico digestion of the protein database.");
		tRange.setMaxWidth(300);
		tRange.setWrapText(true);
		tRange.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblRange.setTooltip(tRange);
		view.content.tfMinPep.setTooltip(tRange);
		view.content.tfMaxPep.setTooltip(tRange);

		Tooltip tCharge = new Tooltip("Allowed charges for a spectrum.");
		tCharge.setMaxWidth(300);
		tCharge.setWrapText(true);
		tCharge.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblCharge.setTooltip(tCharge);
		Tooltip.install(view.content.lblMinCharge, tCharge);
		view.content.tfMinCharge.setTooltip(tCharge);
		Tooltip.install(view.content.lblMaxCharge, tCharge);
		view.content.tfMaxCharge.setTooltip(tCharge);

		Tooltip tMMC = new Tooltip("Spectra with this number of peaks are conserved for analysis.");
		tMMC.setMaxWidth(300);
		tMMC.setWrapText(true);
		tMMC.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblMMC.setTooltip(tMMC);
		view.content.tfMMC.setTooltip(tMMC);
		Tooltip.install(view.content.txtPeaks, tMMC);

		Tooltip tTS = new Tooltip(
				"Minimum number of common peaks between experimental and theoretical spectra for the PTM to be further examined.");
		tTS.setMaxWidth(300);
		tTS.setWrapText(true);
		tTS.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblTS.setTooltip(tTS);

		/*
		 * Tooltips.install is a static method which allows Tooltips for Nodes not
		 * compatible with setTooltip.
		 */

		Tooltip tThresh = new Tooltip(
				"Matching spectra in the result file will all have\na number of common peaks superior or equal to this number.");
		tThresh.setMaxWidth(300);
		tThresh.setWrapText(true);
		tThresh.setTextAlignment(TextAlignment.JUSTIFY);
		Tooltip.install(view.content.lblThresh, tThresh);
		view.content.tfThresh.setTooltip(tThresh);

		Tooltip tScore = new Tooltip("Additional filter to the threshold variable used for graphical interpretation.");
		tScore.setMaxWidth(300);
		tScore.setWrapText(true);
		tScore.setTextAlignment(TextAlignment.JUSTIFY);
		Tooltip.install(view.content.lblScore, tScore);
		view.content.tfScore.setTooltip(tScore);

		Tooltip tDec = new Tooltip("Spectrometer accuracy");
		tDec.setTextAlignment(TextAlignment.JUSTIFY);
		tDec.setMaxWidth(300);
		tDec.setWrapText(true);
		view.content.lblAcc.setTooltip(tDec);
		view.content.tfAcc.setTooltip(tDec);

		Tooltip tDecoy = new Tooltip("Transformation into a database using a peptide reverse method.");
		tDecoy.setMaxWidth(300);
		tDecoy.setWrapText(true);
		tDecoy.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblDecoy.setTooltip(tDecoy);
		view.content.decoy.setTooltip(tDecoy);

		Tooltip tShift = new Tooltip(
				"Specshift optimizes the number of common peaks between a theoretical and an experimental spectrum by shifting them at different points. "
						+ "The solution giving the highest number of common peaks is kept.");
		tShift.setMaxWidth(300);
		tShift.setWrapText(true);
		tShift.setTextAlignment(TextAlignment.JUSTIFY);
		view.content.lblShift.setTooltip(tShift);
		view.content.shift.setTooltip(tShift);
	}
}
