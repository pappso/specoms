package mvc;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Venceslas Douillard
 */
public class ListPTM {

	private StringProperty ptm;
	private DoubleProperty massDelta;

	public ListPTM(String ptm, Double massDelta) {
		this.ptm = new SimpleStringProperty(ptm);
		this.massDelta = new SimpleDoubleProperty(massDelta);
	}

	public ListPTM() {
		this(null, 0.0);
	}

	public final StringProperty ptmProperty() {
		return this.ptm;
	}

	public final String getPTM() {
		return this.ptmProperty().get();
	}

	public final void setPTM(String ptm) {
		this.ptmProperty().set(ptm);
	}

	public final DoubleProperty massDeltaProperty() {
		return this.massDelta;
	}

	public final double getMassDelta() {
		return this.massDeltaProperty().get();
	}

	public final void setMassDelta(final String md) {
		this.massDeltaProperty().set(Double.parseDouble(md.trim()));
	}

}
