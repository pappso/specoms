package mvc;

import javafx.scene.Scene;

//Page setting
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.geometry.Insets;
//Boxes
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Venceslas Douillard
 */
public class View {

	BorderPane fenetre = new BorderPane();
	Scene cadre = new Scene(fenetre, 1100, 750);

	public ViewTooltips tooltips;
	public ViewAlerts alerts;
	public ViewTop top;
	public ViewLeft left;
	public ViewContent content;
	public Model model;
	public ViewBot bot;

	public View(Model model) {

		this.model = model;

		// Order of instanciation matters since some of the parts are dependant of
		// another.
		top = new ViewTop(this);
		left = new ViewLeft(this);
		content = new ViewContent(this);
		bot = new ViewBot(this);
		tooltips = new ViewTooltips(this);
		alerts = new ViewAlerts();

		// Dev Eclipse
		// cadre.getStylesheets().add(getClass().getResource("myStyle.css").toExternalForm()
		// );
		// Production
		cadre.getStylesheets().add("myStyle.css");

//-----BINDINGS & LISTENERS------------------------------------------------------------------------------------------------------------------------------------------------------
		fenetre.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		fenetre.prefWidthProperty().bind(cadre.widthProperty());
		fenetre.prefHeightProperty().bind(cadre.heightProperty());

//------BORDER PANE IMPLEMENTATION------------------------------------------------------------------------------------------------------------------------------------------------------
		fenetre.setTop(top.menubar);
		fenetre.setLeft(left.leftBox);
		fenetre.setCenter(content.tabpane);
		fenetre.setBottom(bot.bottomBox);
	}

}
