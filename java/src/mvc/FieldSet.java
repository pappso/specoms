package mvc;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Venceslas Douillard
 */
public class FieldSet extends StackPane {

	FieldSet(StackPane stack, Label label) {
		getStyleClass().clear();
		getStyleClass().add("field");

		stack.setStyle("-fx-background-color: white;" + "-fx-border: solid;" + "-fx-border-color: black;"
				+ "-fx-padding: 20 20 20 20;");

		// Add Label to StackPane
		label.setStyle("-fx-background-color: white;" + "-fx-padding: 0 10 0 10");
		label.setAlignment(Pos.CENTER_LEFT);
		final double width = label.getLayoutBounds().getWidth();
		// FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();

		// The FontLoader permit to know the size of the label before it is actually
		// computed.
		// FontLoader has been removed since javafx9
		label.translateXProperty().bind(stack.widthProperty().divide(-2).add(width / 2).add(50));
		label.translateYProperty().bind(stack.heightProperty().divide(-2));

		VBox.setMargin(this, new Insets(20));

		getChildren().addAll(stack, label);

	}
}
