package mvc;

//Controls
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;

//Boxes
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.geometry.HPos;
//Layout
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import javafx.util.converter.NumberStringConverter;

/**
 *
 * @author Venceslas Douillard
 */
public class ViewContent {

	/**
	 * Encompasses everything in the first content tab.
	 */
	VBox vContentTab1 = new VBox(5);

	/**
	 * Grows to fill the space between options and Launch button.
	 */
	VBox vBioFill = new VBox();
	HBox hValid = new HBox(5);

	/*
	 * Tabs TAB 1 - Parameters needed for the run. TAB 2 - Allows the user to browse
	 * results file and get a graphical output and/or annotation of the different
	 * spectra.
	 */

	final Tab tab1 = new Tab("Launch SpecOMS");
	final Tab tab2 = new Tab("Analyze results");
	TabPane tabpane = new TabPane();

//------TAB 1 OBJECTS----------------------------------------------------------------------------------------------------------------------------------------------

	GridPane gridClassicOption = new GridPane();
	GridPane gridRegexOption = new GridPane();
	GridPane gridOtherParam = new GridPane();
	ColumnConstraints gridConstraint1 = new ColumnConstraints();
	ColumnConstraints gridConstraint2 = new ColumnConstraints();
	ColumnConstraints gridConstraint3 = new ColumnConstraints();
	ColumnConstraints gridConstraint4 = new ColumnConstraints();

	StackPane stackEnzParam = new StackPane();
	Label labelStackEnz = new Label("Enzymatic parameters");
	VBox vWrapEnz = new VBox(30);
	HBox hProtease = new HBox(5);
	VBox vLeftProtease = new VBox(5);
	HBox hClassicChoice = new HBox(5);
	ToggleGroup groupMode = new ToggleGroup();
	RadioButton rbClassic = new RadioButton("Classic");
	HBox hPreset = new HBox(5);
	/**
	 * Menu which allows to choose an enzyme preset among six different proteases:
	 * trypsin, chymotrypsin, LysC, LysN, GluC, ArgC
	 */
	ComboBox<String> protease = new ComboBox<>();
	Label lblEnz = new Label("Protease");
	HBox hExtrem = new HBox(5);
	ToggleGroup groupCleave = new ToggleGroup();
	Label lblExtrem = new Label("Extremity to cleave");
	RadioButton rbNTer = new RadioButton("N-ter");
	RadioButton rbCTer = new RadioButton("C-ter cleavage");
	HBox hCleave = new HBox(5);
	Label lblCleave = new Label("Cleave after");
	TextField tfCleaved = new TextField("");
	Text txtNoCleave = new Text("not followed by");
	TextField tfNotCleaved = new TextField("");
	Separator sepEnz = new Separator();
	VBox vRightProtease = new VBox(5);
	RadioButton rbRegex = new RadioButton("Regular expression");
	Label lblPattern = new Label("Pattern");
	TextField tfRegexPattern = new TextField("");

	/**
	 * Menu giving the allowed number of missed cleavage.
	 */
	HBox hMiss = new HBox(5);
	Label lblMissCleavage = new Label("Miss-cleavage");
	HBox hWrapMC = new HBox(5);
	Text txtNbMC = new Text("Up to");
	ComboBox<String> missCleavage = new ComboBox<>();
	Label lblMissDB = new Label("\tand\t");
	ToggleGroup groupMiss = new ToggleGroup();
	RadioButton rbStoredMC = new RadioButton("Stored in the database");
	RadioButton rbComputedMC = new RadioButton("Computed after search");

	FieldSet fieldEnz = new FieldSet(stackEnzParam, labelStackEnz);

//	Algorithm parameters

	GridPane gridOMSParam = new GridPane();
	ColumnConstraints oms1 = new ColumnConstraints();
	ColumnConstraints oms2 = new ColumnConstraints();
	ColumnConstraints oms3 = new ColumnConstraints();
	StackPane stackOMSParam = new StackPane();
	Label labelStackOMSParam = new Label("SpecOMS parameters");
	VBox vWrapOMS = new VBox(5);

	// Peptide length
	Label lblRange = new Label("Peptide length after in-silico digestion");
	HBox hRange = new HBox(5);
	TextField tfMinPep;
	Text lblMinPep = new Text("Min\t");
	TextField tfMaxPep;
	Text lblMaxPep = new Text("Max");

	// Spectrum charges
	Label lblCharge = new Label("Peptide charge");
	HBox hCharge = new HBox(5);
	TextField tfMinCharge;
	Text lblMinCharge = new Text("Min\t");
	TextField tfMaxCharge;
	Text lblMaxCharge = new Text("Max");

	// Max Masses Count
	Label lblMMC = new Label("Number of peaks per spectrum");
	HBox hMMC = new HBox(5);
	TextField tfMMC;
	Text txtPeaks = new Text("peaks");

	// Threshold & Score
	Label lblTS = new Label("Minimum number of shared masses for PSM examination");
	HBox hTS = new HBox(5);
	TextField tfThresh;
	Text lblThresh = new Text("Threshold\t");
	TextField tfScore;
	Text lblScore = new Text("Score");

	// Decimals
	Label lblAcc = new Label("Accuracy");
	HBox hAcc = new HBox(5);
	TextField tfAcc;
	Text txtAcc = new Text("Da");

	// Decoy
	Label lblDecoy = new Label("Decoy database");
	CheckBox decoy = new CheckBox();

	// Shift
	Label lblShift = new Label("Improve alignment using SpecFit");
	CheckBox shift = new CheckBox();

	FieldSet fieldOMS = new FieldSet(stackOMSParam, labelStackOMSParam);

	Button bValid = new Button("Start search");
	Button bCancel = new Button("Cancel");

//------TAB 2 OBJECTS----------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Encompasses everything in the second content tab
	 */
	VBox vResultsBox = new VBox(6);

	VBox vPanes = new VBox(6);
	TitledPane tableResultsPane;
	TableView<ResultsData> tableResults = new TableView<>();
	TableColumn<ResultsData, String> col1 = new TableColumn<>("Spectrum ID");
	// Or write directly the name ? TODO
//			    TableColumn<ResultsData, String> col2 = new TableColumn<ResultsData,String>("Protein ID");
	TableColumn<ResultsData, String> col2 = new TableColumn<>("Peptide");
	TableColumn<ResultsData, Number> col3 = new TableColumn<>("Shared Masses");
	TableColumn<ResultsData, Number> col31 = new TableColumn<>("Before");
	TableColumn<ResultsData, Number> col32 = new TableColumn<>("After");
	TableColumn<ResultsData, Number> col4 = new TableColumn<>("Mass delta\nlocation");
	TableColumn<ResultsData, Number> col5 = new TableColumn<>("Mass delta");
	TableColumn<ResultsData, String> col6 = new TableColumn<>("Affix");
	TableColumn<ResultsData, String> col7 = new TableColumn<>("SemiTryptic");
	TableColumn<ResultsData, String> col8 = new TableColumn<>("Origin");

	TitledPane filterModifPane;
	HBox hFilterAndModif = new HBox(5);
	VBox vFilter = new VBox(10);

	HBox hRepresentation = new HBox(5);
	Label lblRepresentation = new Label("Representation");
	ToggleGroup groupRepresentation = new ToggleGroup();
	RadioButton rbLogScale = new RadioButton("Logarithmic");
	RadioButton rbNoLogScale = new RadioButton("Linear");

	TableView<ResultsFilters> filterTable = new TableView<>();
	TableColumn<ResultsFilters, Number> filter1 = new TableColumn<>("Min Mass");
	TableColumn<ResultsFilters, Number> filter2 = new TableColumn<>("Max Mass");
	TableColumn<ResultsFilters, Number> filter3 = new TableColumn<>("Before");
	TableColumn<ResultsFilters, Number> filter4 = new TableColumn<>("After");
	HBox hAddRemove = new HBox(5);
	Button bAddFilter = new Button("Add");
	Button bRemoveFilter = new Button("Remove");

	Separator sep = new Separator();

	VBox vModif = new VBox(5);
	TableView<ListPTM> listPTMTable = new TableView<>();
	TableColumn<ListPTM, Boolean> listCol1 = new TableColumn<>("Selected");
	TableColumn<ListPTM, String> listCol2 = new TableColumn<>("PTM");
	TableColumn<ListPTM, Number> listCol3 = new TableColumn<>("Mass delta");

	HBox hWrapModif = new HBox(5);
	CheckBox cbSelectAll = new CheckBox();
	Label lblSelectAll = new Label("Select All");
	HBox hFillModif = new HBox();
	Button bAddLine = new Button("Load PTM list");

	HBox hLoadRes = new HBox(5);
	Button bLoadRes = new Button("Load SpecOMS file");
	Button bDisplay = new Button("Display graphic");

	public ViewContent(View view) {

		/*
		 * Overriding the replaceText method limits the user possibilties to write. The
		 * user can only write single numbers. Overriding the paste method prevents from
		 * unknown parameters.
		 */
		tfAcc = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^,\\.0-9]")) {
					super.replaceText(start, end, text);
				}
			}

			@Override
			public void paste() {
			};

		};

		tfMinPep = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^0-9]")) {
					super.replaceText(start, end, text);
				}
			}

			@Override
			public void paste() {
			};
		};

		tfMaxPep = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^0-9]")) {
					super.replaceText(start, end, text);
				}
			}

			@Override
			public void paste() {
			};
		};

		tfMinCharge = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^1-9]")) {
					if (start != 1) {
						super.replaceText(start, end, text);
					} else {
						ViewAlerts.formatAlert();
					}
				} else {
					ViewAlerts.formatAlert();
				}
			}

			@Override
			public void paste() {
			};
		};

		tfMaxCharge = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^1-9]")) {
					if (start != 1) {
						super.replaceText(start, end, text);
					} else {
						ViewAlerts.formatAlert();
					}
				} else {
					ViewAlerts.formatAlert();
				}
			}

			@Override
			public void paste() {
			};
		};

		tfThresh = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^0-9]")) {
					super.replaceText(start, end, text);
				}
			}

			@Override
			public void paste() {
			};
		};

		tfScore = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^0-9]")) {
					super.replaceText(start, end, text);
				}
			}

			@Override
			public void paste() {
			};
		};

		tfMMC = new TextField() {
			@Override
			public void replaceText(int start, int end, String text) {
				if (!text.matches("[^0-9]")) {
					super.replaceText(start, end, text);
				}
			}

			@Override
			public void paste() {
			};
		};

//------BINDING & LISTENERS------------------------------------------------------------------------------------------------------------------------------------------------------

		/**
		 * Used for binding between the property of the settings' controls and the
		 * intermediate parameters of the model. Textfields give a String as a result
		 * whereas the properties needed are integers (typed as Numbers in Properties).
		 */
		NumberStringConverter nbToString = new NumberStringConverter();

		// Update the UI controls through variables in the model
		tfAcc.textProperty().bindBidirectional(view.model.propAcc);
		tfAcc.textProperty().addListener((obs, oldv, newv) -> {
			// Get rid of the comma-point confusion by dynamically removing commas.
			if (newv.contains(",")) {
				tfAcc.setText(tfAcc.getText().replaceAll(",", "."));
			}
			// Allows only numbers with 0 as Integer, one decimal separator and as many
			// decimals as needed.
			if (!tfAcc.getText().matches("0\\.[0-9]*")) {
				tfAcc.setText(oldv);
				System.out.println("There already is a decimal/a number.");
			}
		});
		tfAcc.setText("0.02");
		tfMinCharge.textProperty().bindBidirectional(view.model.propMinCharg, nbToString);
		tfMaxCharge.textProperty().bindBidirectional(view.model.propMaxCharg, nbToString);
		tfMinPep.textProperty().bindBidirectional(view.model.propMinPep, nbToString);
		tfMaxPep.textProperty().bindBidirectional(view.model.propMaxPep, nbToString);
//	range.lowValueProperty().bindBidirectional(view.model.propMinPep);
//	range.highValueProperty().bindBidirectional(view.model.propMaxPep);
		decoy.selectedProperty().bindBidirectional(view.model.propDecoy);
		shift.selectedProperty().bindBidirectional(view.model.propShift);
		tfThresh.textProperty().bindBidirectional(view.model.propThresh, nbToString);
		tfScore.textProperty().bindBidirectional(view.model.propScore, nbToString);
		tfMMC.textProperty().bindBidirectional(view.model.propMMC, nbToString);

		tfCleaved.textProperty().bindBidirectional(view.model.propAACleaved);
		tfNotCleaved.textProperty().bindBidirectional(view.model.propAANotCleaved);
		rbNTer.selectedProperty().bindBidirectional(view.model.propTer);
		protease.valueProperty().bindBidirectional(view.model.propPreset);
		tfRegexPattern.textProperty().bindBidirectional(view.model.propEnzPattern);

		missCleavage.valueProperty().bindBidirectional(view.model.propMissCleavage);
		rbStoredMC.selectedProperty().bindBidirectional(view.model.propMissDatabase);

		rbLogScale.selectedProperty().bindBidirectional(Model.propGraphLog);
		filterTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelec, newSelec) -> {
			if (newSelec != null) {
				bRemoveFilter.setDisable(false);
			} else {
				bRemoveFilter.setDisable(true);
			}
		});

//------PAGE SETTING-------------------------------------------------------------------------------------------------------------------------------------------------------------

		gridConstraint2.setPrefWidth(100);
		gridConstraint2.setHalignment(HPos.RIGHT);
		gridClassicOption.getColumnConstraints().addAll(gridConstraint1, gridConstraint2, gridConstraint3,
				gridConstraint4);
		gridClassicOption.setHgap(15);
		gridClassicOption.setVgap(5);
		gridRegexOption.getColumnConstraints().addAll(gridConstraint1, gridConstraint2, gridConstraint3,
				gridConstraint4);
		gridRegexOption.setHgap(15);
		gridRegexOption.setVgap(5);
		gridOtherParam.getColumnConstraints().addAll(gridConstraint1, gridConstraint2, gridConstraint3,
				gridConstraint4);
		gridOtherParam.setHgap(15);
		gridOtherParam.setVgap(5);

		gridOMSParam.getColumnConstraints().addAll(oms1, oms2, oms3);
		oms2.setPrefWidth(200);
		/*
		 * JDK-8145496 : not possible to align to the right and wrap the text at the
		 * same time.
		 */
		oms3.setHalignment(HPos.LEFT);
		gridOMSParam.setHgap(30);
		gridOMSParam.setVgap(10);
		hCleave.setAlignment(Pos.CENTER_LEFT);

		/*
		 * TAB 1
		 */

		VBox.setVgrow(vBioFill, Priority.ALWAYS);
		GridPane.setVgrow(gridOMSParam, Priority.ALWAYS);
		HBox.setHgrow(vWrapOMS, Priority.ALWAYS);
		hProtease.setAlignment(Pos.CENTER);
		gridOMSParam.setAlignment(Pos.CENTER);
		fieldEnz.setMinWidth(750);
		fieldOMS.setMinWidth(750);

		// Left part of the enzymatic parameters
		rbClassic.setToggleGroup(groupMode);
		rbRegex.setToggleGroup(groupMode);
		rbClassic.setSelected(true);
		tfRegexPattern.setPrefWidth(100);
		hPreset.setAlignment(Pos.CENTER);
		hExtrem.setAlignment(Pos.CENTER);
		hCleave.setAlignment(Pos.CENTER);
		rbNTer.setToggleGroup(groupCleave);
		rbCTer.setToggleGroup(groupCleave);
		protease.getItems().addAll("<Preset>", "Trypsin", "Chymotrypsin", "LysC", "LysN", "AspN", "GluC", "ArgC");
		protease.setValue("<Preset>");
		rbCTer.setSelected(true);

		// Separator
		sepEnz.setOrientation(Orientation.VERTICAL);
		VBox.setVgrow(sepEnz, Priority.ALWAYS);

		// Right part of the enzymatic parameter
		tfCleaved.setPrefWidth(100);
		tfNotCleaved.setPrefWidth(100);

		// Other parameters
		gridOtherParam.setAlignment(Pos.CENTER);
		hWrapMC.setAlignment(Pos.CENTER);
		missCleavage.getItems().addAll("0", "1", "2", "3", "4");
		rbStoredMC.setToggleGroup(groupMiss);
		rbComputedMC.setToggleGroup(groupMiss);
		rbStoredMC.setSelected(false);

		// SpecOMS parameters
//		lblRange.setWrapText(true); lblCharge.setWrapText(true); lblMMC.setWrapText(true); lblTS.setWrapText(true); lblShift.setWrapText(true);
		tfAcc.setMaxWidth(60);
		tfMinPep.setMaxWidth(60);
		tfMaxPep.setMaxWidth(60);
		tfMinCharge.setMaxWidth(60);
		tfMaxCharge.setMaxWidth(60);
		tfThresh.setMaxWidth(60);
		tfScore.setMaxWidth(60);
		tfMMC.setMaxWidth(60);
		tfAcc.setAlignment(Pos.CENTER);
		tfMinPep.setAlignment(Pos.CENTER);
		tfMaxPep.setAlignment(Pos.CENTER);
		tfMinCharge.setAlignment(Pos.CENTER);
		tfMaxCharge.setAlignment(Pos.CENTER);
		tfThresh.setAlignment(Pos.CENTER);
		tfScore.setAlignment(Pos.CENTER);
		tfMMC.setAlignment(Pos.CENTER);
		hRange.setAlignment(Pos.CENTER_LEFT);
		hCharge.setAlignment(Pos.CENTER_LEFT);
		hTS.setAlignment(Pos.CENTER_LEFT);
		hMMC.setAlignment(Pos.CENTER_LEFT);
		hAcc.setAlignment(Pos.CENTER_LEFT);

		hValid.setAlignment(Pos.BOTTOM_RIGHT);
		bValid.setAlignment(Pos.BOTTOM_RIGHT);
		bCancel.setAlignment(Pos.BOTTOM_RIGHT);
		bCancel.setVisible(false);

		/*
		 * TAB 2
		 */

		vResultsBox.setPadding(new Insets(20, 10, 10, 10));
		VBox.setVgrow(vResultsBox, Priority.ALWAYS);

		VBox.setVgrow(vPanes, Priority.ALWAYS);

		tableResultsPane = new TitledPane("Result file", tableResults);
		tableResults.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		tableResults.setPlaceholder(new Label(
				"Please select and load a result file from the left panel.\nClick display to obtain a graphical view. Parameters can be modified below."));

		filterModifPane = new TitledPane("Display parameters", hFilterAndModif);
		HBox.setHgrow(hFilterAndModif, Priority.ALWAYS);
		vFilter.setAlignment(Pos.CENTER);
		filterTable.setEditable(true);
		filterTable.setPlaceholder(new Label("You may add restriction on the data displayed in the graph."));
		bRemoveFilter.setDisable(true);

		sep.setOrientation(Orientation.VERTICAL);

		HBox.setHgrow(vModif, Priority.ALWAYS);
		vModif.setAlignment(Pos.CENTER_RIGHT);
		listPTMTable.setEditable(true);
		listPTMTable.setSortPolicy(null);
		listPTMTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		listPTMTable.setPlaceholder(new Label(
				"You may load a list of PTM from the left panel.\nIf so, the graphical representation will display thresholds\nfor every selected PTM in the list."));
		cbSelectAll.setDisable(true);
		lblSelectAll.setDisable(true);
		hWrapModif.setAlignment(Pos.CENTER);
		HBox.setHgrow(hFillModif, Priority.ALWAYS);

		hLoadRes.setAlignment(Pos.BOTTOM_RIGHT);
		bDisplay.setDisable(true);

//------IMPLEMENTATION------------------------------------------------------------------------------------------------------------------------------------------------------
		tab1.setClosable(false);
		tab1.setContent(vContentTab1);
		tab2.setClosable(false);
		tab2.setContent(vResultsBox);
		tabpane.getTabs().setAll(tab1, tab2);

		/*
		 * TAB1
		 */

		hExtrem.getChildren().addAll(rbNTer, rbCTer);
		hCleave.getChildren().addAll(tfCleaved, txtNoCleave, tfNotCleaved);
		vLeftProtease.getChildren().add(gridClassicOption);

		vRightProtease.getChildren().add(gridRegexOption);
		hProtease.getChildren().addAll(vLeftProtease, sepEnz, vRightProtease);

		hRange.getChildren().addAll(tfMinPep, lblMinPep, tfMaxPep, lblMaxPep);
		hCharge.getChildren().addAll(tfMinCharge, lblMinCharge, tfMaxCharge, lblMaxCharge);
		hMMC.getChildren().addAll(tfMMC, txtPeaks);
		hAcc.getChildren().addAll(tfAcc, txtAcc);
		hTS.getChildren().addAll(tfThresh, lblThresh, tfScore, lblScore);

		hWrapMC.getChildren().addAll(txtNbMC, missCleavage, lblMissDB, rbStoredMC, rbComputedMC);
		gridOtherParam.add(new Text(""), 0, 0);
		gridOtherParam.add(lblMissCleavage, 1, 0);
		gridOtherParam.add(hWrapMC, 2, 0, 4, 1);

		vWrapEnz.getChildren().addAll(hProtease, gridOtherParam);
		stackEnzParam.getChildren().add(vWrapEnz);

		vWrapOMS.getChildren().add(gridOMSParam);
		stackOMSParam.getChildren().add(vWrapOMS);

		gridClassicOption.add(rbClassic, 0, 0, 2, 1);
		gridClassicOption.add(lblEnz, 1, 1);
		gridClassicOption.add(protease, 2, 1, 2, 1);
		gridClassicOption.add(lblExtrem, 0, 2, 2, 1);
		gridClassicOption.add(hExtrem, 2, 2);
		gridClassicOption.add(lblCleave, 1, 3);
		gridClassicOption.add(hCleave, 2, 3, 2, 1);

		gridRegexOption.add(rbRegex, 0, 0, 2, 1);
		gridRegexOption.add(new Text(""), 0, 1, 4, 1);
		gridRegexOption.add(new Text(""), 0, 2);
		gridRegexOption.add(lblPattern, 1, 2);
		gridRegexOption.add(tfRegexPattern, 2, 2);

		gridOMSParam.add(lblRange, 0, 0, 2, 1);
		gridOMSParam.add(hRange, 2, 0, 3, 1);
		gridOMSParam.add(lblCharge, 0, 1, 2, 1);
		gridOMSParam.add(hCharge, 2, 1, 3, 1);
		gridOMSParam.add(lblMMC, 0, 2, 2, 1);
		gridOMSParam.add(hMMC, 2, 2, 2, 1);
		gridOMSParam.add(lblTS, 0, 3, 2, 1);
		gridOMSParam.add(hTS, 2, 3, 3, 1);
		gridOMSParam.add(lblAcc, 0, 4, 2, 1);
		gridOMSParam.add(hAcc, 2, 4, 2, 1);
		gridOMSParam.add(lblDecoy, 0, 5, 2, 1);
		;
		gridOMSParam.add(decoy, 2, 5);
		gridOMSParam.add(lblShift, 0, 6, 2, 1);
		gridOMSParam.add(shift, 2, 6);

		hValid.getChildren().addAll(bValid, bCancel, new Text(""));
		vContentTab1.getChildren().addAll(fieldEnz, fieldOMS, hValid);

		/*
		 * TAB2
		 */
		rbLogScale.setToggleGroup(groupRepresentation);
		rbNoLogScale.setToggleGroup(groupRepresentation);
		rbNoLogScale.setSelected(true);
		hRepresentation.getChildren().addAll(lblRepresentation, rbLogScale, rbNoLogScale);

		hAddRemove.getChildren().addAll(bAddFilter, bRemoveFilter);

		hWrapModif.getChildren().addAll(cbSelectAll, lblSelectAll, hFillModif, bAddLine);
		filterTable.getColumns().addAll(filter1, filter2, filter3, filter4);
		vFilter.getChildren().addAll(hRepresentation, filterTable, hAddRemove);
		listPTMTable.getColumns().addAll(listCol1, listCol2, listCol3);
		vModif.getChildren().addAll(listPTMTable, hWrapModif);
		hFilterAndModif.getChildren().addAll(vFilter, sep, vModif);

		tableResults.getColumns().addAll(col1, col2, col3, col4, col5, col6, col7, col8);
		col3.getColumns().addAll(col31, col32);

		hLoadRes.getChildren().addAll(bLoadRes, bDisplay);
		vPanes.getChildren().addAll(tableResultsPane, filterModifPane);
		vResultsBox.getChildren().addAll(vPanes, hLoadRes);

	}
}
