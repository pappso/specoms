package mvc;

import java.util.logging.Logger;
import java.util.logging.LogManager;
import java.util.logging.Level;
import java.util.logging.ConsoleHandler;
import java.io.InputStream;
import java.io.IOException;

/**
 * Wrapper calling the main program in order to set Vsync to false, otherwise it
 * would be set after launching the program, therefore being useless. Vsync
 * should be disabled because progress bar animations are causing the UI to lag
 * after resizing the window. Setting prism.order=sw also solves the problem.
 * This wrapper class is the equivalent of running "java -Dprism.vsync=false
 * -jar SpecOMS.jar"
 *
 * Other options are needed for good performance.
 *
 * @author Venceslas Douillard
 */
public class MainWrapper {

	//protected static final Logger logger = Logger.getLogger(Model.class.getName());

	static {
		System.setProperty("java.util.logging.ConsoleHandler.level", "SEVERE");
	}

	/*
	 * Possible argument written by the user are passed to the main program through
	 * this variable, otherwise they would no tbe available.
	 *
	 */
	public static String argument = "";

	public static void main(String[] args) throws Exception {

		try {
			InputStream configFile = MainWrapper.class.getResourceAsStream("/logging.properties");
			if (configFile != null) {
				LogManager.getLogManager().readConfiguration(configFile);
			}
		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
		}

		// logger.setLevel(Level.SEVERE); // overrides logging properties
		//logger.addHandler(new ConsoleHandler());
		//logger.log(Level.INFO, " MainWrapper arg:");
		System.setProperty("prism.vsync", "false"); // this one has to stay here for visual consistency
		if (args.length > 0) {
			argument = args[0];
		}
		Main.launch(Main.class);
	}

}
