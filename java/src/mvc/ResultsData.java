package mvc;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class describe the data in the results file. When the data is read, it
 * is collected in an ObservableList of this class and used to write the table.
 * 
 * @author Venceslas Douillard
 */
public class ResultsData {

	StringProperty spectrumID = new SimpleStringProperty();
	StringProperty peptide = new SimpleStringProperty();
	IntegerProperty beforeSpec = new SimpleIntegerProperty();
	IntegerProperty afterSpec = new SimpleIntegerProperty();
	IntegerProperty massDeltaLoc = new SimpleIntegerProperty();
	DoubleProperty massDelta = new SimpleDoubleProperty();
	StringProperty affix = new SimpleStringProperty();
	StringProperty stryptic = new SimpleStringProperty();
	StringProperty origin = new SimpleStringProperty();

	// TODO : if you want to have access to the protein identifier
	// StringProperty proteinID = new SimpleStringProperty();

	public final StringProperty spectrumIDProperty() {
		return this.spectrumID;
	}

	public final String getSpectrumID() {
		return this.spectrumIDProperty().get();
	}

	public final void setSpectrumID(final String spectrumID) {
		this.spectrumIDProperty().set(spectrumID);
	}

	public final StringProperty originProperty() {
		return this.origin;
	}

	public final String getOrigin() {
		return this.originProperty().get();
	}

	public final void setOrigin(final String ori) {
		this.originProperty().set(ori);
	}

	public final IntegerProperty massDeltaLocProperty() {
		return this.massDeltaLoc;
	}

	public final int getMassDeltaLoc() {
		return this.massDeltaLocProperty().get();
	}

	public final void setMassDeltaLoc(final String loc) {
		this.massDeltaLocProperty().set(Integer.parseInt(loc.trim()));
	}

	public final StringProperty strypticProperty() {
		return this.stryptic;
	}

	public final String getStryptic() {
		return this.strypticProperty().get();
	}

	public final void setStryptic(final String st) {
		this.strypticProperty().set(st);
	}

//    public final StringProperty proteinIDProperty() {
//        return this.proteinID;
//    }
//
//    public final String getProteinID() {
//        return this.proteinIDProperty().get();
//    }
//
//    public final void setProteinID(final String proteinID) {
//        this.proteinIDProperty().set(proteinID);
//    }

	public final StringProperty peptideProperty() {
		return this.peptide;
	}

	public final String getPeptide() {
		return this.peptideProperty().get();
	}

	public final void setPeptide(final java.lang.String peptide) {
		this.peptideProperty().set(peptide);
	}

	public final IntegerProperty beforeSpecProperty() {
		return this.beforeSpec;
	}

	public final int getBeforeSpec() {
		return this.beforeSpecProperty().get();
	}

	public final void setBeforeSpec(final String before) {
		this.beforeSpecProperty().set(Integer.parseInt(before.trim()));
	}

	public final IntegerProperty afterSpecProperty() {
		return this.afterSpec;
	}

	public final int getAfterSpec() {
		return this.afterSpecProperty().get();
	}

	public final void setAfterSpec(final String after) {
		this.afterSpecProperty().set(Integer.parseInt(after.trim()));
	}

	public final DoubleProperty massDeltaProperty() {
		return this.massDelta;
	}

	public final double getMassDelta() {
		return this.massDeltaProperty().get();
	}

	public final void setMassDelta(final String delta) {
		this.massDeltaProperty().set(Double.parseDouble(delta.trim()));
	}

	public final StringProperty affixProperty() {
		return this.affix;
	}

	public final String getAffix() {
		return this.affixProperty().get();
	}

	public final void setAffix(final String affix) {
		this.affixProperty().set(affix.trim());
	}

}