package mvc;

//Output
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

//Display
import org.controlsfx.control.Notifications;

import algorithms.bucketClustering.BucketClustering;
import algorithms.fptree.FPTAlgorithm;
import algorithms.fptree.FPTree;
import algorithms.fptree.extractor.FPTExtractor;
import biodata.Peptide;
import biodata.Protein;
import biodata.Spectrum;
import filters.FilterWrapper;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
//Properties needed for listening and binding
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
//Multi-threading
import javafx.concurrent.Task;
//Algorithm classes
import proteo.Param;
import proteo.Peptides;
import proteo.Proteins;
import proteo.Spectra;

/**
 *
 * @author Venceslas Douillard
 */
public class Model {
	protected static final Logger logger = Logger.getLogger(Model.class.getName());
	/*
	 * Default values read by the view. Properties can be used for binding and
	 * listening.
	 */
	public SimpleStringProperty propFeedback = new SimpleStringProperty("");
	public SimpleStringProperty propInput = new SimpleStringProperty("");
	public SimpleStringProperty propCont = new SimpleStringProperty("");
	public SimpleStringProperty propData = new SimpleStringProperty("");
	public SimpleBooleanProperty propDbFormat = new SimpleBooleanProperty(false);
	public SimpleStringProperty propOutput = new SimpleStringProperty("");

	public DoubleProperty propProgress = new SimpleDoubleProperty(0);

	public SimpleStringProperty propAcc = new SimpleStringProperty("0.02");
	public SimpleIntegerProperty propMinPep = new SimpleIntegerProperty(7);
	public SimpleIntegerProperty propMaxPep = new SimpleIntegerProperty(25);
	public SimpleIntegerProperty propMinCharg = new SimpleIntegerProperty(1);
	public SimpleIntegerProperty propMaxCharg = new SimpleIntegerProperty(3);
	public SimpleBooleanProperty propDecoy = new SimpleBooleanProperty(false);
	public SimpleBooleanProperty propShift = new SimpleBooleanProperty(true);
	public SimpleIntegerProperty propThresh = new SimpleIntegerProperty(8);
	public SimpleIntegerProperty propScore = new SimpleIntegerProperty(8);
	public SimpleIntegerProperty propMMC = new SimpleIntegerProperty(50);

	public SimpleStringProperty propEnzPattern = new SimpleStringProperty(".*?[KR]");
	public SimpleStringProperty propMissCleavage = new SimpleStringProperty("0");
	public SimpleBooleanProperty propMissDatabase = new SimpleBooleanProperty(false);

	/**
	 * Simulate the binary choice between N or C cleavage. It makes the binding
	 * easier for the radiobuttons.
	 */
	public SimpleBooleanProperty propTer = new SimpleBooleanProperty(false);

	public SimpleStringProperty propAACleaved = new SimpleStringProperty("KR");
	public SimpleStringProperty propAANotCleaved = new SimpleStringProperty("");
	public SimpleStringProperty propPreset = new SimpleStringProperty("Trypsin");
	public SimpleStringProperty propInRes = new SimpleStringProperty("");
	public SimpleStringProperty propInList = new SimpleStringProperty("");

	public static SimpleBooleanProperty propGraphLog = new SimpleBooleanProperty(true);

	public static boolean uiCheck = true;
	ObservableList<ResultsData> datacolNotOrd;
	ObservableList<ResultsData> datacolOrd;
	ObservableList<ListPTM> dataList;
	List<BooleanProperty> selectedList = new ArrayList<>();
	ResultsGraph chart;
	ObservableList<ResultsFilters> dataFilters = FXCollections.observableArrayList();

	public Model() {
	}

	/*
	 * This constructor is only called from the command line, it does not need the
	 * view to be called and it gets all its parameters from files.
	 */
	public Model(String cmd) {
		logger.log(Level.INFO, " Model(String cmd) 1 ");
		uiCheck = false;

		long startTime = System.nanoTime();

		Proteins proteinsList = new Proteins();
		Peptides peptidesList = new Peptides();
		Spectra spectraList = new Spectra();

		BucketClustering bins = null;
		FPTAlgorithm algo = null;
		FPTree tree = null;

		System.setProperty("file.encoding", "UTF-8");
		System.out.println("Reading configuration");
		Param.setConfiguration();
		Param.setInputOutput();

		initializeSpectra(proteinsList, peptidesList, spectraList);

		System.out.println("Clustering " + ((spectraList.numberOfSpectra()) + " spectra..."));
		bins = BucketClustering.clusterSpectra(spectraList.get());

		System.out.println("Building SpecTrees...");
		algo = FPTAlgorithm.useFastAlgorithm(bins.spectraCount());
		tree = FPTree.buildTree(bins, algo);

		System.out.println("Running SpecXtract and SpecShift...");

		tree.extract(spectraList.get(), Param.threshold(), spectraList.limit(), Param.resultsFile());

		saveParameters();

		System.out.print("\007");
		System.out.println("Done.");
		System.out.flush();
		resetSpec();

		long estimatedTime = System.nanoTime() - startTime;
		estimatedTime = TimeUnit.NANOSECONDS.toSeconds(estimatedTime);
		System.out.println();
		System.out.println();
		System.out.println("Dataset analyzed in " + estimatedTime + " seconds.");

	}

	/*
	 * According to the search mode, initializeSpectra creates the lists of
	 * theoretical and experimental spectra. more = projection experimental spectra
	 * vs theoretical spectra mode = peptides_exact theoretical spectra vs
	 * theoretical spectra without accuracy taken in account more =
	 * peptides_duplicated theoretical spectra vs theoretical spectra with accuracy
	 * (peaks duplicated in the buckets)
	 * 
	 */

	private void initializeSpectra(Proteins proteinsList, Peptides peptidesList, Spectra spectraList) {

		Param.massModificationOnAA();

		if (Param.search_mode().contentEquals("peptides_exact")) {
			// Accuracy required to fit with the accuracy given by deltaM
			Param.setAcc(0.0001);
			Param.setAccFac(4);
			Param.setAccuracyValue(0);
		}

		if (Param.search_mode().equals("projection")) {
			logger.log(Level.INFO, " Model(String cmd) projection ");

			proteinsList.addProteins();
			if (!Param.contaminants().equals("")) {
				proteinsList.addContaminants();
			}
			if (Param.decoy()) {
				proteinsList.addDecoy();
			}

			peptidesList.digest(proteinsList);
			System.out.println("Nombre de peptides:" + peptidesList.getPeptidesList().size());
			spectraList.build(peptidesList);
			spectraList.addExp();

			// System.out.println("FINAL CONTENT");
			// spectraList.display();

			spectraList.prepare();

			if (Param.protein_information()) {
				Proteins.write_protein_reference_table();
			}

		}

		if (Param.search_mode().equals("peptides_exact") || Param.search_mode().equals("peptides_duplicated")) {

			logger.log(Level.INFO, " Model(String cmd) addProteins ");
			proteinsList.addProteins();

			if (!Param.contaminants().equals("")) {
				proteinsList.addContaminants();
			}
			/*
			 * if (Param.decoy()) { proteinsList.addDecoy(); }
			 */

			ObjectOpenHashSet<String> tempList = new ObjectOpenHashSet<>();

			// tempList contains all the digested peptides that will be compared
			peptidesList.digest(proteinsList, tempList);

			// peptidesList.display();

			spectraList.build(peptidesList);

			// spectraList.display();
			// System.out.println("------------------");

			if (Param.search_mode().equals("peptides_exact")) {
				spectraList.addNonDuplicatedPeptides(tempList);
			} else {
				spectraList.addDuplicatedPeptides(tempList);
			}
			tempList = null;

			// System.out.println("FINAL CONTENT");
			// spectraList.display();

			spectraList.prepare();

		}

		if (Param.search_mode().equals("spectra")) {

			logger.log(Level.INFO, " Model(String cmd) spectra ");
			spectraList.addNonDuplicatedSpectra();

			spectraList.addExp();
			spectraList.prepare();
		}
		return;
	}

	/*
	 * Using a service to launch the algorithm allows to create a specific thread
	 * for both the UI and the task we are executing. Additionnaly, services can be
	 * called several times whereas simple threads are unique.
	 *
	 * This service is executed from the UI only, the command line does not need the
	 * same type of feedback and configuration.
	 */
	Service<Void> programUI = new Service<Void>() {

		@Override
		protected Task<Void> createTask() {

			Task<Void> taskModel = new Task<Void>() {
				@Override
				protected Void call() throws Exception {

					logger.log(Level.INFO, "FPTExtractor");
					/*
					 * Beginning instructions Resetting of progress and listeners.
					 */
					FPTExtractor.progressZero();

					FPTExtractor.getProgress().addListener((obs, oldprog, newprog) -> {
						updateProgress(FPTExtractor.getProgress().get(), 1.0);
					});

					FPTExtractor.getCheckCancel().addListener((obs, oldv, newv) -> {
						if (FPTExtractor.getCheckCancel().get() == true) {
							updateProgress(0, 1.0);
						}
					});

					logger.log(Level.INFO, "Reading configuration");
					/*
					 * Main instructions Calculation using input and providing the input file.
					 */

					// Executed in the Application Thread
					propFeedback.set("Reading configuration");
					Proteins proteinsList = new Proteins();
					Peptides peptidesList = new Peptides();
					Spectra spectraList = new Spectra();

					BucketClustering bins = null;
					FPTAlgorithm algo = null;
					FPTree tree = null;

					initializeSpectra(proteinsList, peptidesList, spectraList);

					propFeedback.set("Clustering " + ((spectraList.numberOfSpectra()) + " spectra..."));

					bins = BucketClustering.clusterSpectra(spectraList.get());

					propFeedback.set("Building SpecTrees...");

					algo = FPTAlgorithm.useFastAlgorithm(bins.spectraCount());
					tree = FPTree.buildTree(bins, algo);

					propFeedback.set("Running SpecXtract and SpecShift...");
					tree.extract(spectraList.get(), Param.threshold(), spectraList.limit(), Param.resultsFile());
					saveParameters();

					/*
					 * End instructions Resetting of the peptides, spectra and proteins.
					 */

					logger.log(Level.INFO, "FPTExtractor end");
					propFeedback.set("Reading configuration");
					System.out.print("\007\n");
					propFeedback.set("Done.");
					endMsg();
					System.out.flush();

					resetSpec();
					return null;

				}
			};
			taskModel.setOnFailed(evt -> {
				System.out.println(evt.getEventType().toString());
			});

			return taskModel;
		}
	};

	/*
	 * Depending on the size of the file, loading the result file can take a few
	 * seconds. In order not to slow the UI or freeze it, we load the file in a
	 * Service.
	 *
	 */
	Service<Void> results = new Service<Void>() {

		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {

				@Override
				protected Void call() throws Exception {

					logger.log(Level.INFO, "Collection<ResultsData>");
					Collection<ResultsData> list = Files.readAllLines(new File(propInRes.get()).toPath()).stream()
							.skip(1) // Skipping the header
							.filter(line -> !line.equals(";;;;;;;"))
					// Added one ";" TODO
//	                        .filter(line -> !line.equals(";;;;;;;;"))
							.map(line -> {
								ResultsData rd = new ResultsData();
								String[] datacol = line.trim().split(";");
								rd.setSpectrumID(datacol[0]);
								rd.setPeptide(datacol[1]);
								rd.setBeforeSpec(datacol[2]);
								rd.setAfterSpec(datacol[3]);
								rd.setMassDeltaLoc(datacol[4]);
								rd.setMassDelta(datacol[5]);
								rd.setAffix(datacol[6]);
								rd.setStryptic(datacol[7]);
								rd.setOrigin(datacol[9]);
								// rd.setProteinID(datacol[]);
								return rd;
							}).collect(Collectors.toList());

					datacolNotOrd = FXCollections.observableArrayList(list);

					List maListe = new ArrayList(list);
					Collections.sort(maListe, new Comparator<ResultsData>() {
						public int compare(ResultsData r1, ResultsData r2) {
							return r1.massDelta.getValue().compareTo(r2.massDelta.getValue());
						}
					});
					datacolOrd = FXCollections.observableArrayList(maListe);

					return null;
				}
			};
		}
	};

	Service<Void> PTMs = new Service<Void>() {

		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {

				@Override
				protected Void call() throws Exception {

					logger.log(Level.INFO, "Collection<ListPTM>");
					Collection<ListPTM> list = Files.readAllLines(new File(propInList.get()).toPath()).stream()
							.map(line -> {
								String[] datacol = line.split(";");
								ListPTM lp = new ListPTM();
								lp.setPTM(datacol[0]);
								lp.setMassDelta(datacol[1]);
								return lp;
							}).collect(Collectors.toList());

					dataList = FXCollections.observableArrayList(list);
					return null;
				}
			};
		}
	};

	public void loadFilters() {
		ResultsFilters rf = new ResultsFilters(this);
		rf.chooseFilters();
	}

	public void addFilters(ResultsFilters filter) {
		dataFilters.add(filter);
	}

	/**
	 * Transform the boolean into N-ter or C-ter for the parameters file.
	 * 
	 * @return N or C
	 */
	public String terToText() {
		if (Param.ter())
			return "N";
		else
			return "C";
	}

	public static boolean isLogScale() {
		return propGraphLog.get();
	}

	/**
	 * Checks if the program has been called from the UI.
	 */

	public static boolean isUI() {
		return uiCheck;
	}

	/**
	 * reset() and start() methods can be changed by a restart() method.
	 */
	public void startProgram(Service<Void> myService) {
		logger.log(Level.INFO, "startProgram reset", "startProgram reset");
		myService.reset();
		logger.log(Level.INFO, "startProgram start", "startProgram start");
		myService.start();
		logger.log(Level.INFO, "startProgram", "startProgram");
	}

	/**
	 * Returning the data for the controller which needs it to fill the table.
	 */
	public ObservableList<ResultsData> getTable() {
		return datacolNotOrd;
	}

	public ObservableList<ResultsFilters> getFilterTable() {
		return dataFilters;
	}

	public ObservableList<ListPTM> getListTable() {
		return dataList;
	}

	/**
	 * The cancelled state of the service is not sufficient to stop the program. A
	 * cancel check has been placed in the loop counting for the greater part of the
	 * runtime. This only set the boolean property of the check to true.
	 */
	public void cancelProgram() {
		FPTExtractor.cancelProgram();
	}

	/**
	 * Every variable necessary for the algorithm are reset, otherwise the user
	 * would not be able to run it twice in a row. (it would crash during SpecTrees.
	 *
	 */
	public static void resetSpec() {
		Protein.reset();
		Peptide.reset();
		Spectrum.reset();
	}

	/**
	 * Notification is part of the UI and therefore cannot be called within the
	 * Service without a Platform.runLater().
	 */
	public void endMsg() {
		Platform.runLater(() -> {
			programUI.setOnSucceeded(e -> {
				// System.out.println(e.toString());
				Notifications.create().title("Job done !").text("Your run has successfully ended.").showConfirm();
			});
			programUI.setOnFailed(e -> {
				// System.out.println("Failure");
				Notifications.create().title("There was a problem.").text("Your job has abruptly ended.").showError();
			});
		});
	}

	/**
	 * Differs from the saveParameters() in the controller since the user can't
	 * choose the path of the configuration file, it serves as a reminder of the
	 * parameters used for each result file.
	 *
	 * "\\." two backslashes are used, one to escape the special character "." of
	 * regex the second to escape the first backslash.
	 */
	public void saveParameters() {
		String out = Param.resultsFile();
		String[] splittedOut = out.split("\\.");
		splittedOut[splittedOut.length - 1] = "ini";
		File file = new File(String.join(".", splittedOut));

		StringBuffer contentParam = new StringBuffer(5096);
		contentParam.append("decoyBase=" + String.valueOf(Param.decoy()) + "\n" + "missDB=" + Param.missDB() + "\n"
				+ "nbMissCleavage=" + Param.missCleavage() + "\n" + "minimumPeptideLength=" + Param.minPeptideLength()
				+ "\n" + "maximumPeptideLength=" + Param.maxPeptideLength() + "\n" + "proteins="
				+ Param.protein_information() + "\n");
		contentParam.append("enzPattern=" + Param.enzPattern() + "\n" + "modification=" + Param.getMassModification()
				+ "\n" + "\n");
		contentParam.append("maxMassesCount=" + Param.massFilterSize() + "\n" + "minimumPeptideCharge="
				+ Param.minimalCharge() + "\n" + "maximumPeptideCharge=" + Param.maximalCharge() + "\n"
				+ "numberOfDecimals=" + Param.accuracyDec() + "\n" + "decimalValue=" + Param.accuracyValue() + "\n"
				+ "parentIonAccuracy=" + Param.getMc_tolerance() + "\n" + "\n");
//				+ "accuracy="+Param.accuracy()+"\n"
		contentParam.append("parentRangeFilter=" + Param.ParentFilter_range() + "\n" + "lowRangeFilter="
				+ Param.LowFilter_threshold() + "\n" + "parentNeutralLossFilter=" + Param.ParentFilter_range() + "\n"
				+ "dynamicRangeMaxFilter=" + Param.DynamicRangeFilter_max() + "\n" + "dynamicRangeMinFilter="
				+ Param.DynamicRangeFilter_min() + "\n" + "highPassFilter=" + Param.HighPass() + "\n" + "deisotoping="
				+ Param.Deisotoping() + "\n" + "windowMaxFilter=" + Param.WindowFilter_max() + "\n"
				+ "windowSizeFilter=" + Param.WindowFilterSize() + "\n" + "\n" + "single_match=" + Param.singleMatch()
				+ "\n" + "searchMode=" + Param.search_mode() + "\n" + "threshold=" + Param.threshold() + "\n"
				+ "minimumScore=" + Param.minimumScore() + "\n" + "shift=" + String.valueOf(Param.shift()) + "\n"
				+ "minMassDelta=" + Param.minMassDelta() + "\n" + "maxMassDelta=" + Param.maxMassDelta() + "\n" + "\n"
				+ "MC_PRIORITY=" + Param.getMC_PRIORITY() + "\n" + "ST_PRIORITY=" + Param.getST_PRIORITY() + "\n"
				+ "SHIFT_PRIORITY=" + Param.getSHIFT_PRIORITY() + "\n" + "EXACT_DELTA_PRIORITY="
				+ Param.getEXACT_DELTA_PRIORITY() + "\n" + "\n");
		contentParam.append("databaseFile=" + Param.database_file() + "\n" + "contaminantsFile=" + Param.contam_file()
				+ "\n" + "spectraFile=" + Param.exp_file());

		if (file != null) {
			try {
				FileWriter fileWriter = null;
				fileWriter = new FileWriter(file);
				fileWriter.write(contentParam.toString());
				fileWriter.close();
				propFeedback.set("Parameters saved.");
			} catch (Exception ex) {
				System.out.println(ex.toString());
				Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

}
