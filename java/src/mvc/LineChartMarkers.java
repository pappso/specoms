package mvc;

import java.util.Objects;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ValueAxis;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class LineChartMarkers<X, Y> extends LineChart<X, Y> {

	// data defining horizontal markers, xValues are ignored
	private ObservableList<Data<X, Y>> horizontalMarkers;

	public LineChartMarkers(Axis<X> xAxis, Axis<Y> yAxis) {

		super(xAxis, yAxis);
		// a list that notifies on change of the yValue property
		horizontalMarkers = FXCollections.observableArrayList(d -> new Observable[] { d.YValueProperty() });
		// listen to list changes and re-plot
		horizontalMarkers.addListener((InvalidationListener) observable -> layoutPlotChildren());

	}

	public LineChartMarkers(Axis<X> xAxis, Axis<Y> yAxis, ObservableList<Series<X, Y>> data) {

		super(xAxis, yAxis, data);
		horizontalMarkers = FXCollections.observableArrayList(d -> new Observable[] { d.YValueProperty() });
		horizontalMarkers.addListener((InvalidationListener) observable -> layoutPlotChildren());

	}

	/**
	 * Add horizontal value marker. The marker's Y value is used to plot a
	 * horizontal line across the plot area, its X value is ignored.
	 * 
	 * @param marker must not be null.
	 */
	public void addHorizontalValueMarker(Data<X, Y> marker, String label) {
		Objects.requireNonNull(marker, "the marker must not be null");
		if (horizontalMarkers.contains(marker))
			return;
		Group wrap = new Group();
		Label description = new Label(label);
		Line line = new Line(0, 0, 100, 0);
		line.getStyleClass().add("line");
		wrap.getStyleClass().add("hideMarker");

		line.setStrokeWidth(1.5);

		line.setOnMouseEntered(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				description.setVisible(true);
				line.setStroke(Color.RED);
			}
		});

		line.setOnMouseExited(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				description.setVisible(false);
				line.setStroke(Color.BLACK);
			}
		});

		wrap.getChildren().addAll(line, description);
		marker.setNode(wrap);
		getPlotChildren().add(wrap);
		horizontalMarkers.add(marker);

	}

	public void forceFocus() {
		int i;
		for (i = 0; i < horizontalMarkers.size(); i++) {
			horizontalMarkers.get(i).getNode().getStyleClass().removeAll("hideMarker");
			horizontalMarkers.get(i).getNode().getStyleClass().add("displayMarker");
		}
	}

	public void cancelFocus() {
		int i;
		for (i = 0; i < horizontalMarkers.size(); i++) {
			horizontalMarkers.get(i).getNode().getStyleClass().removeAll("displayMarker");
			horizontalMarkers.get(i).getNode().getStyleClass().add("hideMarker");
		}
	}

	/**
	 * Overridden to layout the value markers.
	 */
	@Override
	protected void layoutPlotChildren() {
		super.layoutPlotChildren();
		for (Data<X, Y> horizontalMarker : horizontalMarkers) {
			double lower = ((ValueAxis) getXAxis()).getLowerBound();
			X lowerX = getXAxis().toRealValue(lower);
			double upper = ((ValueAxis) getXAxis()).getUpperBound();
			X upperX = getXAxis().toRealValue(upper);
			Group group = (Group) horizontalMarker.getNode();
			Line line = (Line) group.getChildren().get(0);
			Label label = (Label) group.getChildren().get(1);

			line.setStartX(getXAxis().getDisplayPosition(lowerX));
			line.setEndX(getXAxis().getDisplayPosition(upperX));
			line.setStartY(getYAxis().getDisplayPosition(horizontalMarker.getYValue()));
			line.setEndY(line.getStartY());

			label.setLayoutY(line.getStartY());

		}
	}
}