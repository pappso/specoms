package mvc;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This class describe the data in the results file. When the data is read, it
 * is collected in an ObservableList of this class and used to write the table.
 * 
 * @author Venceslas Douillard
 */
public class ResultsFilters {

	public Model model;

	private Stage stageFilter;
	private VBox wrapFilter;
	private Scene sceneFilter;

	private DoubleProperty minMass;
	private DoubleProperty maxMass;
	private IntegerProperty beforeSpec;
	private IntegerProperty afterSpec;

	public ResultsFilters(Model model) {
		this.model = model;
	}

	public ResultsFilters(double min, double max, int bef, int aft) {
		minMass = new SimpleDoubleProperty(min);
		maxMass = new SimpleDoubleProperty(max);
		beforeSpec = new SimpleIntegerProperty(bef);
		afterSpec = new SimpleIntegerProperty(aft);
	}

	public void chooseFilters() {
		stageFilter = new Stage();
		wrapFilter = new VBox(5);
		sceneFilter = new Scene(wrapFilter, 300, 200);

		stageFilter.setTitle("Choose your filter");
		stageFilter.setResizable(false);
		;

		wrapFilter.setPadding(new Insets(20));

		// Mass delta range
		HBox hMassDelta1 = new HBox(5);
		CheckBox cbMinMassDelta = new CheckBox();
		Label lblMinMassDelta = new Label("Minimum mass delta");
		HBox hFilterFill1 = new HBox();
		TextField tfMinMassDelta = new TextField();
		HBox hMassDelta2 = new HBox(5);
		CheckBox cbMaxMassDelta = new CheckBox();
		Label lblMaxMassDelta = new Label("Maximum mass delta");
		HBox hFilterFill2 = new HBox();
		TextField tfMaxMassDelta = new TextField();
		// Minimum score before and after SpecFit
		HBox hScoreBefore = new HBox(5);
		CheckBox cbScoreBefore = new CheckBox();
		Label lblScoreBefore = new Label("Minimum score before SpecFit");
		HBox hFilterFill3 = new HBox();
		TextField tfScoreBefore = new TextField();
		HBox hScoreAfter = new HBox(5);
		CheckBox cbScoreAfter = new CheckBox();
		Label lblScoreAfter = new Label("Maximum score after SpecFit");
		HBox hFilterFill4 = new HBox();
		TextField tfScoreAfter = new TextField();

		Button bAddObjFilter = new Button("Add this filter");

		bAddObjFilter.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (((cbMinMassDelta.isSelected()) && (!tfMinMassDelta.getText().matches("-?[0-9]+(\\.[0-9]+)?")))
						|| ((cbMaxMassDelta.isSelected())
								&& (!tfMaxMassDelta.getText().matches("-?[0-9]+(\\.[0-9]+)?")))
						|| ((cbScoreBefore.isSelected()) && (!tfScoreBefore.getText().matches("[0-9]+")))
						|| ((cbScoreAfter.isSelected()) && (!tfScoreAfter.getText().matches("[0-9]+")))) {

					ViewAlerts.filterAlert();

				} else {

					/*
					 * Ternary operator only uses the value in the TextField if the checkbox is
					 * selected. If not, infinity numbers are used to avoid restriction during the
					 * filtering part.
					 */
					model.addFilters(new ResultsFilters(
							(cbMinMassDelta.isSelected() ? Double.parseDouble(tfMinMassDelta.getText())
									: (Double.NEGATIVE_INFINITY)),
							(cbMaxMassDelta.isSelected() ? Double.parseDouble(tfMaxMassDelta.getText())
									: (Double.POSITIVE_INFINITY)),
							(cbScoreBefore.isSelected() ? Integer.parseInt(tfScoreBefore.getText())
									: (Integer.MIN_VALUE)),
							(cbScoreAfter.isSelected() ? Integer.parseInt(tfScoreAfter.getText())
									: (Integer.MIN_VALUE))));

				}
			}
		});

		tfMinMassDelta.setMaxWidth(50);
		tfMaxMassDelta.setMaxWidth(50);
		tfScoreBefore.setMaxWidth(50);
		tfScoreAfter.setMaxWidth(50);
		tfMinMassDelta.setAlignment(Pos.CENTER);
		tfMaxMassDelta.setAlignment(Pos.CENTER);
		tfScoreBefore.setAlignment(Pos.CENTER);
		tfScoreAfter.setAlignment(Pos.CENTER);
		HBox.setHgrow(hFilterFill1, Priority.ALWAYS);
		HBox.setHgrow(hFilterFill2, Priority.ALWAYS);
		HBox.setHgrow(hFilterFill3, Priority.ALWAYS);
		HBox.setHgrow(hFilterFill4, Priority.ALWAYS);

		tfMinMassDelta.disableProperty().bind(cbMinMassDelta.selectedProperty().not());
		tfMaxMassDelta.disableProperty().bind(cbMaxMassDelta.selectedProperty().not());
		tfScoreBefore.disableProperty().bind(cbScoreBefore.selectedProperty().not());
		tfScoreAfter.disableProperty().bind(cbScoreAfter.selectedProperty().not());

		hMassDelta1.getChildren().addAll(cbMinMassDelta, lblMinMassDelta, hFilterFill1, tfMinMassDelta);
		hMassDelta2.getChildren().addAll(cbMaxMassDelta, lblMaxMassDelta, hFilterFill2, tfMaxMassDelta);
		hScoreBefore.getChildren().addAll(cbScoreBefore, lblScoreBefore, hFilterFill3, tfScoreBefore);
		hScoreAfter.getChildren().addAll(cbScoreAfter, lblScoreAfter, hFilterFill4, tfScoreAfter);

		wrapFilter.getChildren().addAll(hMassDelta1, hMassDelta2, hScoreBefore, hScoreAfter, bAddObjFilter);

		stageFilter.setScene(sceneFilter);
		stageFilter.show();
	}

	public final DoubleProperty minMass() {
		return this.minMass;
	}

	public final double getMinMass() {
		return this.minMass().get();
	}

	public final void setMinMass(double mass) {
		this.minMass().set(mass);
	}

	public final DoubleProperty maxMass() {
		return this.maxMass;
	}

	public final double getMaxMass() {
		return this.maxMass().get();
	}

	public final void setMaxMass(double mass) {
		this.maxMass().set(mass);
	}

	public final IntegerProperty beforeSpecProperty() {
		return this.beforeSpec;
	}

	public final int getBeforeSpec() {
		return this.beforeSpecProperty().get();
	}

	public final void setBeforeSpec(int before) {
		this.beforeSpecProperty().set(before);
	}

	public final IntegerProperty afterSpecProperty() {
		return this.afterSpec;
	}

	public final int getAfterSpec() {
		return this.afterSpecProperty().get();
	}

	public final void setAfterSpec(int after) {
		this.afterSpecProperty().set(after);
	}
}