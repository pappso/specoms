package mvc;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Venceslas Douillard
 */
public class ViewBot {

//------BOTTOM--------------------------------------------------------------------------

	// Encompasses every item in the bottom box.
	HBox bottomBox = new HBox();

	// Fills the empty space on the left of the bottom line
	HBox hFillBot1 = new HBox();
	HBox hFeedb = new HBox();
	HBox hProgress = new HBox();

	/*
	 * Progress Bar Visible only during an on-going run of the program. It should be
	 * complementary to text feedback.
	 */
	ProgressBar progress = new ProgressBar();

	/*
	 * Feedback text Giving information about the steps of the program.
	 */
	Text feedb = new Text();

	public ViewBot(View view) {

//-----BINDINGS & LISTENERS--------------------------------------------------------------------------
		progress.progressProperty().unbind();
		progress.progressProperty().bind(view.model.programUI.progressProperty());
		feedb.textProperty().bind(view.model.propFeedback);

//------BOTTOM PAGE SETTING--------------------------------------------------------------
		bottomBox.prefWidthProperty().bind(view.cadre.widthProperty());
		bottomBox.prefHeightProperty().bind(view.top.menubar.heightProperty());
		CornerRadii botCorner = new CornerRadii(10, 10, 0, 0, 0, 0, 0, 0, false, false, false, false, false, false,
				false, false);

		hFeedb.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
		hProgress.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, botCorner, Insets.EMPTY)));
		hFillBot1.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		hProgress.setAlignment(Pos.CENTER);
		hFeedb.setAlignment(Pos.CENTER_RIGHT);

		hProgress.prefWidthProperty().bind(view.fenetre.widthProperty().divide(3));
		hFeedb.prefWidthProperty().bind(view.fenetre.widthProperty().divide(3));
		hFillBot1.prefWidthProperty().bind(view.fenetre.widthProperty().divide(3));

		progress.setVisible(false);

//------BOTTOM---------------------------------------------------------------------------
		hFeedb.getChildren().add(feedb);
		hProgress.getChildren().add(progress);
		bottomBox.getChildren().addAll(hFillBot1, hProgress, hFeedb);
	}
}
