package mvc;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author Venceslas Douillard
 */
public class ViewLeft {

	/*
	 * TAB 1 - Parameters
	 */

	/**
	 * Encompasses every element of the left part of the UI.
	 */
	VBox leftBox = new VBox(5);

	HBox hFillLeft = new HBox();
	HBox hLabelInp = new HBox();
	HBox hInp = new HBox(10);
	HBox hLabelCont = new HBox();
	HBox hCont = new HBox(10);
	HBox hLabelDatab = new HBox();
	HBox hDatab = new HBox(10);
	HBox hDBFormat = new HBox(20);
	HBox hLabelOut = new HBox();
	HBox hOut = new HBox(10);
	HBox hBShowInp = new HBox();

	HBox hLabelInRes = new HBox();
	HBox hInRes = new HBox(10);
	HBox hLabelInList = new HBox();
	HBox hInList = new HBox(10);

	/*
	 * UI Control Controllers which permit the user to load his input files as well
	 * as choosing the output directory.
	 */
	Text head = new Text("Type of analysis & Inputs/Output");

	Label lblInp = new Label("Choose your input spectra file :");
	TextField inp = new TextField("");
	Button bInp = new Button("...");

	Label blank1 = new Label("");

	Label lblCont = new Label("Choose your contaminant file :");
	TextField cont = new TextField("");
	Button bCont = new Button("...");

	Label blank2 = new Label("");

	Label lblDatab = new Label("Choose your protein database :");
	TextField datab = new TextField("");
	Button bDatab = new Button("...");

	Label ldbFormat = new Label("Fasta style >db|acc|name... ");
	CheckBox dbFormat = new CheckBox();

	Label blank3 = new Label("");

	Label lblOut = new Label("Choose your output file :");
	TextField out = new TextField("");
	Button bOut = new Button("...");

	/*
	 * TAB 2 - Results
	 */

	Label lblInList = new Label("If needed, choose a list of PTM :");
	TextField inList = new TextField();
	Button bInList = new Button("...");

	Label lblInRes = new Label("Choose your SpecOMS result file");
	TextField inRes = new TextField();
	Button bInRes = new Button("...");

	public ViewLeft(View view) {

//------BINDING & LISTENERS-------------------------------------------------------------

		inp.textProperty().bindBidirectional(view.model.propInput);
		cont.textProperty().bindBidirectional(view.model.propCont);
		datab.textProperty().bindBidirectional(view.model.propData);
		dbFormat.selectedProperty().bindBidirectional(view.model.propDbFormat);
		out.textProperty().bindBidirectional(view.model.propOutput);

		inRes.textProperty().bindBidirectional(view.model.propInRes);
		inList.textProperty().bindBidirectional(view.model.propInList);

//------LEFT PAGE SETTING---------------------------------------------------------------

		// Gives an ID to be read by the CSS file.
		leftBox.setId("leftBox");
//		leftBox.prefWidthProperty().bind(view.fenetre.widthProperty().multiply(0.3));
		DragResizer.makeResizable(leftBox);
//		leftBox.prefHeightProperty().bind(view.fenetre.heightProperty().multiply(0.5));

		hFillLeft.setPrefHeight(20);

		HBox.setHgrow(inp, Priority.ALWAYS);
		HBox.setHgrow(cont, Priority.ALWAYS);
		HBox.setHgrow(datab, Priority.ALWAYS);
		HBox.setHgrow(out, Priority.ALWAYS);

		inp.setPromptText("MGF or mzXML");
		cont.setPromptText("FASTA file");
		datab.setPromptText("FASTA file");
		out.setPromptText("CSV file");

		HBox.setHgrow(inRes, Priority.ALWAYS);
		HBox.setHgrow(inList, Priority.ALWAYS);
		inRes.setPromptText("CSV file");
		inList.setPromptText("TXT file");

//------LEFT------------------------------------------------------------------------------------------------------------------------------------------------------

		/*
		 * TAB 1
		 */

		hLabelInp.getChildren().add(lblInp);
		hLabelCont.getChildren().add(lblCont);
		hLabelDatab.getChildren().add(lblDatab);
		hLabelOut.getChildren().add(lblOut);

		hInp.getChildren().addAll(inp, bInp);
		hCont.getChildren().addAll(cont, bCont);
		hDatab.getChildren().addAll(datab, bDatab);
		hDBFormat.getChildren().addAll(ldbFormat, dbFormat);
		hOut.getChildren().addAll(out, bOut);

		/*
		 * TAB 2
		 */
		hLabelInRes.getChildren().add(lblInRes);
		hInRes.getChildren().addAll(inRes, bInRes);
		hLabelInList.getChildren().add(lblInList);
		hInList.getChildren().addAll(inList, bInList);

	}

	/*
	 * /!\ Implementaiton is written in the ControllerContent class because the left
	 * part is dynamically modified when changing the tab.
	 */
}
