package mvc;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import proteo.Param;

/**
 *
 * @author Venceslas Douillard
 */
public class ControllerContent {

	ViewContent content;
	Model model;

	public ControllerContent(Controller ctrl, ViewLeft left, ViewContent content, Model model) {
//------CONTENT------------------------------------------------------------------------------------------------------------------------------------------------------
		this.content = content;
		this.model = model;
		/*
		 * TAB 1
		 */

		/*
		 * Change the left panel based on the selected tab. clear method avoids adding
		 * children to the same box.
		 */
		content.tabpane.getSelectionModel().selectedItemProperty().addListener((obs, oldv, newv) -> {

			if (newv.getText().equals("Launch SpecOMS")) {
				left.leftBox.getChildren().clear();
				left.leftBox.getChildren().addAll(left.hLabelInp, left.hInp, left.blank1, left.hLabelCont, left.hCont,
						left.blank2, left.hLabelDatab, left.hDatab, left.hDBFormat, left.blank3, left.hLabelOut,
						left.hOut, left.hFillLeft);
			} else if (newv.getText().equals("Analyze results")) {
				left.leftBox.getChildren().clear();
				left.leftBox.getChildren().addAll(left.hLabelInRes, left.hInRes, left.hLabelInList, left.hInList);
				left.leftBox.setMinWidth(left.leftBox.getMinWidth());
			}
		});

		/*
		 * Workaround to see the first tab when launching the application. Without it,
		 * the first tab is displayed but has no children nodes. They only appear after
		 * a first modification. In order to avoid writing the getChildren() of the
		 * first tab, we select another tab then switch back to the first.
		 */
		content.tabpane.getSelectionModel().select(content.tab2);
		content.tabpane.getSelectionModel().select(content.tab1);

		// Another workaround
		content.rbRegex.setSelected(true);
		content.rbClassic.setSelected(true);
		content.rbStoredMC.setSelected(true);
		content.rbComputedMC.setSelected(true);

		/*
		 * ENZYME PATTERN
		 */
		/*
		 * <none> is used in the parameter file for the absence of exception in order to
		 * avoid an empty string which would block the HashMap while reading the file.
		 */

		content.tfCleaved.textProperty().addListener((obs, oldv, newv) -> {
			updateRegex();
		});

		content.tfNotCleaved.textProperty().addListener((obs, oldv, newv) -> {
			if (newv.equals("<none>")) {
				content.tfNotCleaved.setText("");
			}
			updateRegex();
		});

		/*
		 * Checking if parameters have been loaded and whether the extremity to cut has
		 * been changed.
		 */
		content.rbNTer.selectedProperty().addListener((obs, oldv, newv) -> {
			if (newv.equals(true)) {
				content.rbCTer.setSelected(false);
			} else {
				content.rbCTer.setSelected(true);
			}
			updateRegex();
		});

		/*
		 * Same thing with the miss cleavage database option.
		 */
		content.rbStoredMC.selectedProperty().addListener((obs, oldv, newv) -> {
			if (newv.equals(true))
				content.rbComputedMC.setSelected(false);
			else
				content.rbComputedMC.setSelected(true);
		});

		/*
		 * Enzyme presets based on the Mascot software. Platform.runLater() pauses
		 * briefly the application, allowing the instructions above to execute properly.
		 */

		content.protease.getSelectionModel().selectedItemProperty().addListener((obs, oldv, newv) -> {
			switch (newv) {
			case "Trypsin":
				content.tfCleaved.setText("KR");
				content.tfNotCleaved.setText("P");
				content.rbNTer.setSelected(false);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "Chymotrypsin":
				content.tfCleaved.setText("FYWL");
				content.tfNotCleaved.setText("P");
				content.rbNTer.setSelected(false);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "LysC":
				content.tfCleaved.setText("K");
				content.tfNotCleaved.setText("P");
				content.rbNTer.setSelected(false);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "LysN":
				content.tfCleaved.setText("K");
				content.tfNotCleaved.setText("");
				content.rbNTer.setSelected(true);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "AspN":
				content.tfCleaved.setText("CD");
				content.tfNotCleaved.setText("");
				content.rbNTer.setSelected(true);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "GluC":
				content.tfCleaved.setText("DE");
				content.tfNotCleaved.setText("");
				content.rbNTer.setSelected(false);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "ArgC":
				content.tfCleaved.setText("R");
				content.tfNotCleaved.setText("P");
				content.rbNTer.setSelected(false);
				Platform.runLater(() -> content.protease.setValue("<Preset>"));
				break;
			case "<Preset>":
				break;
			default:
				System.out.println("Invalid request.");
				break;
			}
			updateRegex();
		});

		content.rbRegex.selectedProperty().addListener((obs, oldv, newv) -> {
			content.tfCleaved.setDisable(newv);
			content.tfNotCleaved.setDisable(newv);
			content.protease.setDisable(newv);
			content.rbNTer.setDisable(newv);
			content.rbCTer.setDisable(newv);

			content.tfRegexPattern.setDisable(!newv);
		});

		// Disable and display buttons following the launching of the algorithm.
		content.bValid.disableProperty().bind(model.programUI.runningProperty());
		content.bCancel.visibleProperty().bind(model.programUI.runningProperty());

		/*
		 * Verification of all the text fields and paths before setting the parameters
		 * and launching the algorithm.
		 */
		content.bValid.setOnAction(e -> {
			if ((left.inp.getText().equals("")) || (left.cont.getText().equals("")) || (left.datab.getText().equals(""))
					|| (left.out.getText().equals(""))) {

				ViewAlerts.inOutAlert();

			} else if (!(new File(left.inp.getText()).exists()) || !(new File(left.cont.getText()).exists())
					|| !(new File(left.datab.getText()).exists()) || !(new File(left.out.getText()).exists())) {

				ViewAlerts.pathAlert();

			} else if ((content.tfAcc.getText().equals("")) || (content.tfAcc.getText().matches("0\\.0?$"))
					|| (content.tfThresh.getText().equals("")) || (content.tfMinPep.getText().equals(""))
					|| (content.tfMaxPep.getText().equals("")) || (content.tfScore.getText().equals(""))
					|| (content.tfMinCharge.getText().equals("")) || (content.tfMaxCharge.getText().equals(""))
					|| (content.tfCleaved.getText().equals("")) || (content.tfRegexPattern.getText().equals(""))) {

				ViewAlerts.paramAlert();

			} else {

				Param.setExpData(left.inp.getText());
				Param.setCont(left.cont.getText());
				Param.setDatab(left.datab.getText());
				Param.setUniprotFormat(left.dbFormat.isSelected());
				Param.setOutput(left.out.getText());
				Param.setProteinsFileOutput();
				/*
				 * Always set the decimal value before the accuracy factor because the latter
				 * also sets the tolerance which needs the decimal value.
				 */
				Param.setAccuracyValue(Integer.parseInt(content.tfAcc.getText().split("\\.")[1].replaceAll("^0*", "")));
				Param.setAccFac(content.tfAcc.getText().split("\\.")[1].length());
				Param.setAcc(Double.parseDouble(content.tfAcc.getText()));
				Param.setMinPep(Integer.parseInt(content.tfMinPep.getText()));
				Param.setMaxPep(Integer.parseInt(content.tfMaxPep.getText()));
				Param.setThresh(Integer.parseInt(content.tfThresh.getText()));
				Param.setScore(Integer.parseInt(content.tfScore.getText()));
				Param.setMinCharg(Integer.parseInt(content.tfMinCharge.getText()));
				Param.setMaxCharg(Integer.parseInt(content.tfMaxCharge.getText()));
				Param.setDecoy(content.decoy.isSelected());
				Param.setShift(content.shift.isSelected());
				Param.setMaxMassCount(Integer.parseInt(content.tfMMC.getText()));

				updateRegex();
				Param.setEnzPattern(content.tfRegexPattern.getText());
				Param.setMissCleavage(content.missCleavage.getValue());
				Param.setMissDB(content.rbStoredMC.isSelected());

				if (ctrl.keepLogFile()) {
					ctrl.logFile = new File(ctrl.logPath);

					try {
						if (!ctrl.logFile.exists()) {
							new File(".\\logfiles\\").mkdir();
						}
						if (ctrl.logFile.createNewFile()) {
							ctrl.out = new PrintStream(
									new BufferedOutputStream(new FileOutputStream(ctrl.logFile, true)), true);
						}
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					;

					System.setOut(ctrl.out);
					System.setErr(ctrl.errDefault);

				} else {
					System.setOut(ctrl.outDefault);
					System.setErr(ctrl.errDefault);

				}
				model.startProgram(model.programUI);
			}
		});

		/*
		 * The cancel() method only put the service in the CANCELLED state, therefore we
		 * need to break a loop in the FTPExtractor which is called in the Model with
		 * the cancelProgram() method.
		 */
		content.bCancel.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				model.programUI.cancel();
				model.cancelProgram();
			}
		});

		/*
		 * TAB 2
		 */
		content.listPTMTable.itemsProperty().addListener((obs, oldv, newv) -> {
			if (Bindings.isEmpty(content.listPTMTable.getItems()).get()) {
				content.cbSelectAll.setDisable(true);
				content.lblSelectAll.setDisable(true);
			} else {
				content.cbSelectAll.setDisable(false);
				content.lblSelectAll.setDisable(false);
			}
		});

		content.cbSelectAll.selectedProperty().addListener((obs, oldSelec, newSelec) -> {
			for (BooleanProperty bool : model.selectedList) {
				bool.set(newSelec);
			}
		});

		content.bAddLine.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {

				model.startProgram(model.PTMs);

				model.PTMs.setOnSucceeded(e -> {

					/*
					 * Adding checkboxes in every row.
					 */

					// This callback allows the checkbox in the column to access selectedRowList (or
					// more
					// exactly, the boolean property it contains
					Callback<Integer, ObservableValue<Boolean>> selectedStateSelectColumn = new Callback<Integer, ObservableValue<Boolean>>() {

						// index in this context reference the table cell index
						@Override
						public ObservableValue<Boolean> call(Integer index) {
							return model.selectedList.get(index);
						}
					};

					for (ListPTM l : model.dataList) {
						model.selectedList.add(new SimpleBooleanProperty());
					}

					content.listCol1.setCellValueFactory(
							new Callback<CellDataFeatures<ListPTM, Boolean>, ObservableValue<Boolean>>() {
								@Override
								public ObservableValue<Boolean> call(CellDataFeatures<ListPTM, Boolean> cdf) {
									TableView<ListPTM> tblView = cdf.getTableView();
									ListPTM rowData = cdf.getValue();

									int rowIndex = tblView.getItems().indexOf(rowData);
									return model.selectedList.get(rowIndex);
								}
							});

					content.listCol1.setCellFactory(CheckBoxTableCell.forTableColumn(selectedStateSelectColumn));
					content.listCol2.setCellValueFactory(data -> data.getValue().ptmProperty());
					content.listCol3.setCellValueFactory(data -> data.getValue().massDeltaProperty());

					content.listPTMTable.setItems(model.getListTable());

				});
			}
		});

		/*
		 * The display button is disabled when nothing is written in the table and
		 * enabled when something is written.
		 */
		content.tableResults.itemsProperty().addListener((obs, oldv, newv) -> {
			if (Bindings.isEmpty(content.tableResults.getItems()).get()) {
				content.bDisplay.setDisable(true);
			} else {
				content.bDisplay.setDisable(false);
			}
		});

		content.bLoadRes.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (left.inRes.getText().equals("")) {

					ViewAlerts.inOutAlert();

				} else if (!(new File(left.inRes.getText()).exists())) {

					ViewAlerts.pathAlert();

				} else {

					model.startProgram(model.results);

					model.results.setOnSucceeded(e -> {
						content.col1.setCellValueFactory(data -> data.getValue().spectrumIDProperty());
						content.col2.setCellValueFactory(data -> data.getValue().peptideProperty());
						content.col31.setCellValueFactory(data -> data.getValue().beforeSpecProperty());
						content.col32.setCellValueFactory(data -> data.getValue().afterSpecProperty());
						content.col4.setCellValueFactory(data -> data.getValue().massDeltaLocProperty());
						content.col5.setCellValueFactory(data -> data.getValue().massDeltaProperty());
						content.col6.setCellValueFactory(data -> data.getValue().affixProperty());
						content.col7.setCellValueFactory(data -> data.getValue().strypticProperty());
						content.col8.setCellValueFactory(data -> data.getValue().originProperty());
						content.tableResults.setItems(model.getTable());
					});
				}
			}
		});

		content.bDisplay.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				ResultsGraph myGraph = new ResultsGraph(model.datacolOrd, model.selectedList, model.dataList);
				if (!content.filterTable.getItems().isEmpty()) {
					myGraph.filterResults(model.dataFilters);
				}
				myGraph.plotPos();
			}
		});

		content.bAddFilter.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				model.loadFilters();
				content.filter1.setCellValueFactory(data -> data.getValue().minMass());
				content.filter2.setCellValueFactory(data -> data.getValue().maxMass());
				content.filter3.setCellValueFactory(data -> data.getValue().beforeSpecProperty());
				content.filter4.setCellValueFactory(data -> data.getValue().afterSpecProperty());

				content.filterTable.setItems(model.getFilterTable());
			}
		});

		content.bRemoveFilter.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				model.dataFilters.remove(content.filterTable.getSelectionModel().getSelectedIndex());
			}
		});

	}

	/*
	 * Called after each modification to dynamically change the regex and therefore
	 * be able to save it.
	 */

	public void updateRegex() {
		/*
		 * Write the correct regex, with trypsin respectively : C-ter .*?[KR] C-ter not
		 * followed by P .*?[KR](?!P) N-ter [KR].*?(?=[KR]|$) N-ter not followed by P
		 * [KR](?![P]).*?(?=[KR](?![P])|$)
		 */
		if (content.rbClassic.isSelected() == true) {
			if (content.rbNTer.isSelected() == true) {
				if (content.tfNotCleaved.getText().equals("")) {
					model.propEnzPattern.set(
							("[" + content.tfCleaved.getText() + "].*?(?=[" + content.tfCleaved.getText() + "]|$)"));
				} else {
					model.propEnzPattern.set(
							"[" + content.tfCleaved.getText() + "](?![" + content.tfNotCleaved.getText() + "]).*?(?=["
									+ content.tfCleaved.getText() + "](?![" + content.tfNotCleaved.getText() + "])|$)");
				}
			} else {
				if (content.tfNotCleaved.getText().equals("")) {
					model.propEnzPattern.set(".*?[" + content.tfCleaved.getText() + "]");
				} else {
					model.propEnzPattern.set(
							".*?[" + content.tfCleaved.getText() + "](?![" + content.tfNotCleaved.getText() + "])");
				}
			}
		} else {
			model.propEnzPattern.set(content.tfRegexPattern.getText());
		}
	}
}