package mvc;

import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

/**
 *
 * @author Venceslas Douillard
 */
public class ViewTop {

//------TOP-----------------------------------------------------------------------------
	MenuBar menubar = new MenuBar();

	Menu fileMenu = new Menu("File");
	MenuItem saveParamMenuItem = new MenuItem("Save parameters");
	MenuItem loadParamMenuItem = new MenuItem("Load parameters");
	MenuItem exitMenuItem = new MenuItem("Exit");

	Menu optMenu = new Menu("Options");
	MenuItem resetMenuItem = new MenuItem("Default values");
	CheckMenuItem logMenuItem = new CheckMenuItem("Keep log file");
	MenuItem dirLogMenuItem = new MenuItem("Choose log file directory");

	Menu helpMenu = new Menu("Help");
	MenuItem readmeMenuItem = new MenuItem("Open README.txt");
	MenuItem contactMenuItem = new MenuItem("Contact us");

	public ViewTop(View view) {

//-----PAGE SETTING------------------------------------------------------------------
		menubar.prefWidthProperty().bind(view.cadre.widthProperty());

//------IMPLEMENTATION------------------------------------------------------------------------------
		fileMenu.getItems().addAll(saveParamMenuItem, loadParamMenuItem, exitMenuItem);
		helpMenu.getItems().addAll(readmeMenuItem, contactMenuItem);
		optMenu.getItems().addAll(resetMenuItem, logMenuItem, dirLogMenuItem);
		menubar.getMenus().addAll(fileMenu, optMenu, helpMenu);

//-----SHORTCUTS-----------------------------------------------------------------------
		saveParamMenuItem.setMnemonicParsing(true);
		saveParamMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN));
		loadParamMenuItem.setMnemonicParsing(true);
		loadParamMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHORTCUT_DOWN));
	}

}
