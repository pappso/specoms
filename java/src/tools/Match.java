/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package tools;

import biodata.Peptide;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import proteo.Param;
import proteo.Peptides;
import proteo.Spectra;

// v�rifier le bon fonctionnment de cette classe (compare results to non regression set)

public class Match {

	int first;

	IntArrayList second;
	IntArrayList similarity;
	IntArrayList massDelta;
	DoubleArrayList accDelta;

	protected Match() {
	}

	public Match(int fir, int sim, int pep, int delta, double totDelta) {
		first = fir;
		second = new IntArrayList();
		similarity = new IntArrayList();
		massDelta = new IntArrayList();
		accDelta = new DoubleArrayList();
		second.add(pep);
		similarity.add(sim);
		massDelta.add(delta);
		accDelta.add(totDelta);
	}

	// public Match create(int first){ Match m = new Match(); return m; }

	public void removeCandidate() {
		int pos = second.size() - 1;
		second.remove(pos);
		similarity.remove(pos);
		massDelta.remove(pos);
		accDelta.remove(pos);
	}

	public void addCandidate(int sec, int sim, int delta, double totDelta) {
		second.add(sec);
		similarity.add(sim);
		massDelta.add(delta);
		accDelta.add(totDelta);
	}

	public int size() {
		return second.size();
	}

	public int second(int cpt) {
		return second.getInt(cpt);
	}

	public int similarity(int cpt) {
		return similarity.getInt(cpt);
	}

	public double accurateDelta(int cpt) {
		return accDelta.getDouble(cpt);
	}

	public void clear() {
		second = new IntArrayList();
		similarity = new IntArrayList();
		massDelta = new IntArrayList();
		accDelta = new DoubleArrayList();
	}

	public int first() {
		return first;
	}

	public int massDelta(int pos) {
		return massDelta.getInt(pos);
	}

	public int number_of_candidates() {
		return second.size();
	}

	String get_writable_peptide_candidate(int cpt, int limit) {
		StringBuilder candidate_to_write = new StringBuilder();

		candidate_to_write.append(Spectra.get_spectra_name(first));
		candidate_to_write.append(";");
		candidate_to_write.append(Spectra.get_spectra_name(second.getInt(cpt)));
		candidate_to_write.append(";");
		candidate_to_write.append(similarity.getInt(cpt));
		candidate_to_write.append(";");
		// candidate_to_write.append(massDelta.getInt(cpt));
		// candidate_to_write.append(";");
		candidate_to_write.append(accDelta.getDouble(cpt));
		candidate_to_write.append(";");

		// second.getInt() identifies the theoretical spectrum
		if (Param.protein_information()) {
			Peptide p = Spectra.get_peptide(second.getInt(cpt));
			candidate_to_write.append(";");
			candidate_to_write.append(Peptides.get_references_as_string(p.identifier()));
			candidate_to_write.append(";");
			switch (Peptides.getOrigin(second.getInt(cpt))) {
			case -1:
				candidate_to_write.append("Contaminant");
				break;
			case 0:
				candidate_to_write.append("Decoy");
				break;
			case 1:
				candidate_to_write.append("Target");
				break;
			case 2:
				candidate_to_write.append("Decoy and (Target or Contaminant)");
				break;
			case 3:
				candidate_to_write.append("Target and Contaminant");
				break;
			}

		}

		// System.out.println(candidate_to_write.toString());

		return candidate_to_write.toString();
	}

	String get_writable_spectra_candidate(int cpt, int limit) {
		StringBuilder candidate_to_write = new StringBuilder();

		candidate_to_write.append(Spectra.get_spectra_name(first - limit));
		candidate_to_write.append(";");
		candidate_to_write.append(Spectra.get_spectra_name(second.getInt(cpt)));
		candidate_to_write.append(";");
		candidate_to_write.append(similarity.getInt(cpt));
		candidate_to_write.append(";");
		candidate_to_write.append(massDelta.getInt(cpt));
		candidate_to_write.append(";");
		candidate_to_write.append(accDelta.getDouble(cpt));
		candidate_to_write.append(";");

		// System.out.println(candidate_to_write.toString());

		return candidate_to_write.toString();
	}
}
