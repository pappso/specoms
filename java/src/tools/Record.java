/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package tools;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import biodata.Peptide;
import proteo.Param;
import proteo.Peptides;

public class Record {
	//private static final Logger LOGGER = Logger.getLogger(Record.class.getName());

	private StringBuilder to_write = new StringBuilder();

	private int peptide_index = -1;
	private String spectrum_name;
	private int origin;
	private String peptide_sequence = "";
	private int base_peaks = -1;
	private int best_peaks = -1;
	private int delta_location = -1;
	private double accurate_delta = 0.0;

	private String affix = "";
	private int cutSide = 0;

	private int priority_lock = -1;
	private boolean lock = false;
	private int peptide_origin = -1;

	protected Record() {
	}

	public static Record create_empty_record(String spectrum, int origin) {
		Record new_record = new Record();
		new_record.spectrum_name = spectrum;
		new_record.origin = origin;
		return new_record;
	}

	public boolean is_a_worse_solution(int new_threshold) {
		return (new_threshold >= best_peaks);
	}

	public void lock() {
		this.lock = true;
	}

	public void lock(int a) {
		this.lock = true;
		this.priority_lock = a;
	}

	public boolean isLocked() {
		return this.lock;
	}

	public int lockPriority() {
		return this.priority_lock;
	}

	public void fill_record(int index, String peptide, int origin, int base_score, int best_score, int location,
			double accurate, int priority, int cut_side, String affixes) {
		this.peptide_index = index;
		this.peptide_sequence = peptide;
		this.peptide_origin = origin;
		this.base_peaks = base_score;
		this.best_peaks = best_score;
		this.delta_location = location;
		this.accurate_delta = accurate;
		this.affix = affixes;
		this.cutSide = cut_side;
		this.priority_lock = priority;
	}

	// MODIFICATION DT
	public void replace(Record new_solution) {

		if (new_solution.priority_lock > this.priority_lock) {
			copy(new_solution);
		} else {
			if (new_solution.priority_lock == this.priority_lock && new_solution.best_peaks > this.best_peaks) {
				copy(new_solution);
			} else {
				// The simplest solution is preferred rather than random
				if (new_solution.priority_lock == this.priority_lock && new_solution.best_peaks == this.best_peaks
						&& (Math.abs(new_solution.accurate_delta) < Math.abs(this.accurate_delta))) {
					copy(new_solution);
				}
			}
		}

	}

	public void copy(Record new_solution) {

//        if (!Param.singleMatch()) { DataWriter.writeReplacedResult(this.format_to_write()); }

		this.peptide_index = new_solution.peptide_index;
		this.peptide_sequence = new_solution.peptide_sequence;
		this.peptide_origin = new_solution.peptide_origin;
		this.base_peaks = new_solution.base_peaks;
		this.best_peaks = new_solution.best_peaks;
		this.delta_location = new_solution.delta_location;
		this.accurate_delta = new_solution.accurate_delta;
		this.affix = new_solution.affix;
		this.cutSide = new_solution.cutSide;
		this.priority_lock = new_solution.priority_lock;
		this.origin = new_solution.origin;

	}

	public String format_to_write() {

		//LOGGER.log(Level.INFO, String.valueOf(accurate_delta));

		double roundedDelta = accurate_delta;
		roundedDelta = Math.round(roundedDelta * 10000);
		roundedDelta = roundedDelta / 10000;
		//LOGGER.log(Level.INFO, String.valueOf(roundedDelta));

		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
		DecimalFormat df = new DecimalFormat("#", otherSymbols);
		df.setMinimumIntegerDigits(1);
		df.setMaximumFractionDigits(8);

		to_write.setLength(0);
		to_write.append(spectrum_name);
		to_write.append(";");
		to_write.append(peptide_sequence);
		to_write.append(";");
		to_write.append(base_peaks);
		to_write.append(";");
		to_write.append(best_peaks);
		to_write.append(";");
		to_write.append(delta_location);
		to_write.append(";");
		to_write.append(df.format(roundedDelta));
		to_write.append(";");
		to_write.append(affix);
		to_write.append(";");

		if (this.cutSide == -1) {
			to_write.append("Left");
		} else {
			if (this.cutSide == 1) {
				to_write.append("Right");
			} else {
				to_write.append(" ");
			}
		}
		if (Param.protein_information()) {
			to_write.append(";");
			if (Param.uniprotFormat() == true) {
				to_write.append(Peptides.getUniprotRef(peptide_index));
			} else {
				to_write.append(Peptides.get_references_as_string(peptide_index));
			}
		}
		to_write.append(";");
		to_write.append(Peptide.getOrigin(peptide_origin));

		if (Param.search_mode().equals("peptides_exact") || Param.search_mode().equals("peptides_duplicated")) {
			to_write.append(";");
			to_write.append(Peptide.getOrigin(origin));
		}

		return to_write.toString();
	}

	public String spectrum_name() {
		return this.spectrum_name;
	}

}
