/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import proteo.Param;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author E092585L
 */
public class DataWriter {

	private static boolean open;
	private static FileWriter writer;
	private static BufferedWriter buffer;
	private static PrintWriter output = null;

	// private static Object2ObjectOpenHashMap<String, Match> table = new
	// Object2ObjectOpenHashMap<>();

	public static void write_peptides_results(int limit) {
		Match pair;
		for (Object2ObjectOpenHashMap.Entry<String, Match> iter : RecordHandler.getTable().object2ObjectEntrySet()) {
			// table.object2ObjectEntrySet() ){
			pair = iter.getValue();
			for (int cpt = 0; cpt < pair.number_of_candidates(); ++cpt) {
				try {
					buffer.write(pair.get_writable_peptide_candidate(cpt, (limit))); // limit + 1 ?
					buffer.newLine();
				} catch (Exception e) {
					e.printStackTrace(System.out);
				}
			}
		}
	}

	public static void write_spectra_results(int limit) {
		Match pair;
		for (Object2ObjectOpenHashMap.Entry<String, Match> iter : RecordHandler.getTable().object2ObjectEntrySet()) {
			pair = iter.getValue();
			for (int cpt = 0; cpt < pair.number_of_candidates(); ++cpt) {
				try {
					buffer.write(pair.get_writable_spectra_candidate(cpt, (limit))); // limit +1
					buffer.newLine();
				} catch (Exception e) {
					e.printStackTrace(System.out);
				}
			}
		}
	}

	// wtf is that for ?
	public static void clean_data_buffer() {
		RecordHandler.cleanTable();
	}

	static void write_record(Record best_solution) {
		// s.write_record(best_solution);

		try {

			buffer.write(best_solution.format_to_write());
			buffer.newLine();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			System.exit(-1);
		}
	}

	public static void close() {
		if (open) {
			output.close();
		}
	}

	public static void open(String file) {
		try {
			writer = new FileWriter(file, true);
			buffer = new BufferedWriter(writer);
			output = new PrintWriter(buffer);
			open = true;
			// TODO :::: add charge error !!!
			// if ((Param.search_mode().equals("projection") )|| (Param.search_mode().equals
			// ( "peptides_exact" )))
			buffer.write(
					"spectrumID;peptide;sharedMassesBeforeSpecFit;sharedMassesAfterSpecFit;massDeltaLocation;observedMassDelta;affix;cutSide;;proteinsOrigin");
//            else if ((Param.search_mode().equals("peptides_duplicated")) || (Param.search_mode().equals("peptides_exact")))
//            	  buffer.write("peptide1;peptide2;sharedMassesBeforeSpecFit;observedMassDelta;;proteinsOrigin");
			buffer.newLine();
		} catch (Exception ex) {
			open = false;
			System.out.println(ex.toString());
			System.exit(-1);
		}
	}

	// what's htat one ?
	public void init() {
		RecordHandler.cleanTable();
	}

	public static void erasePrevious(String file) {
		try {
			writer = new FileWriter(file);
			buffer = new BufferedWriter(writer);
			output = new PrintWriter(buffer);
		} catch (Exception ex) {
			System.out.println(ex.toString());
		} finally {
			output.close();
		}
	}

	public static void reopen(String file) {
		try {
			writer = new FileWriter(file, true);
			buffer = new BufferedWriter(writer);
			output = new PrintWriter(buffer);
			open = true;
			// spectra = s;
		} catch (Exception ex) {
			open = false;
			System.out.println(ex.toString());
		}
	}

	static void writeReplacedResult(String result) {
		try {
			buffer.write(result);
			buffer.newLine();
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

}
