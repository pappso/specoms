/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package tools;

import biodata.CompleteSpectrumModel;
import biodata.Spectrum;
import biodata.SpectrumShifter;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import proteo.Param;

/***
 * Data handler responsible of storing the answers
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class RecordHandler {

	// table stores all the PSMs above T key:id_spectra1, value = list of PSMs for
	// id_spectra1

	static Object2ObjectOpenHashMap<String, Match> getTable() {
		return table;
	}

	static void cleanTable() {
		table = new Object2ObjectOpenHashMap<>();
	}

	int[] error = new int[4];
	private static Object2ObjectOpenHashMap<String, Match> table = new Object2ObjectOpenHashMap<>(20_000);
	Spectrum[] spectra;

	/*
	 * private boolean open; private static FileWriter writer; private static
	 * BufferedWriter buffer; private static PrintWriter output = null;
	 */

	private Record best_solution;
	private Record solution;

	public RecordHandler(Spectrum[] spec) {
		spectra = spec;
	}

	public void filter(SpectrumShifter s) {

		Match pair;
		CompleteSpectrumModel cs = null;

		for (Object2ObjectOpenHashMap.Entry<String, Match> iter : table.object2ObjectEntrySet()) {
			pair = iter.getValue();

			if (Param.search_mode().equals("peptides_exact") || Param.search_mode().equals("peptides_duplicated")) {
				best_solution = Record.create_empty_record(spectra[pair.first].name(),
						spectra[pair.first].peptide().origin());
				solution = Record.create_empty_record(spectra[pair.first].name(),
						spectra[pair.first].peptide().origin());
			} else {
				best_solution = Record.create_empty_record(spectra[pair.first].name(), 4);
				solution = Record.create_empty_record(spectra[pair.first].name(), 4);
			}
			// System.out.println("Taille_peptide:"+spectra[pair.second(0)].peptide().asList().length);
			// System.out.println("delta:"+pair.accurateDelta(0));
			int cpt = 0;
			boolean best = false;

			// On ne retient que des solutions qui ont deltaM > deltaM min à récupérer en
			// paramètre
			// initialisation avec la première solution > valMin

			for (cpt = 0; cpt < pair.size(); cpt++) {
				if (pair.accurateDelta(cpt) > Param.minMassDelta()) {
					best_solution.fill_record(spectra[pair.second(cpt)].peptide().identifier(),
							spectra[pair.second(cpt)].peptide().asString(),
							spectra[pair.second(cpt)].peptide().origin(), pair.similarity(cpt), pair.similarity(cpt),
							-1, pair.accurateDelta(cpt), 0, 0, "");
					best = true;
					break;
				}
			}

			if (!best)
				return;
			// Il faut autoriser le shift de la solution initiale

			for (; cpt < pair.size(); ++cpt) {
				if ((pair.accurateDelta(cpt) > Param.minMassDelta())
						&& (pair.accurateDelta(cpt) < Param.maxMassDelta())) {
					cs = spectra[pair.second(cpt)].peptide().getCompleteSpectrumModel();
					s.initialise(cs, spectra[pair.first], pair.similarity(cpt), pair.massDelta(cpt),
							pair.accurateDelta(cpt));
					solution.fill_record(spectra[pair.second(cpt)].peptide().identifier(), cs.name(), cs.origin(),
							pair.similarity(cpt), pair.similarity(cpt), -1, pair.accurateDelta(cpt), 0, 0, "");
					s.shift(best_solution, solution);

					// Modif DT origine du peptide 09_06_2018 ajout ligne suivante pour récupérer
					// toutes les solutions
					if (!Param.singleMatch()) {
						DataWriter.write_record(solution);
					}
				} else {
					if (pair.accurateDelta(cpt) > Param.maxMassDelta())
						System.out.println("PSMs with deltaM greater than " + Param.maxMassDelta()
								+ " Da not considered. The maxMassDelta parameter can be increased (more memory used)");
				}
			}

			// Modif DT ajout cond 09_10_2018

			if (Param.singleMatch())
				DataWriter.write_record(best_solution);

		}
	}

	/*
	 * private static void write_record(SpectrumShifter s, Record best_solution){
	 * s.write_record(best_solution); }
	 * 
	 * public void close(){ if(open){ output.close(); } }
	 * 
	 * public void open(String file, Spectrum[] s){ try{ writer = new FileWriter(
	 * file, true ); buffer = new BufferedWriter(writer); output = new
	 * PrintWriter(buffer); open = true; spectra = s; }catch (IOException ex){ open
	 * = false; System.out.println(ex.toString()); } }
	 * 
	 * public void init(){ table = new Object2ObjectOpenHashMap<>(); }
	 * 
	 * public void erasePrevious(String file){ try{ writer = new FileWriter( file );
	 * buffer = new BufferedWriter(writer); output = new PrintWriter(buffer); }catch
	 * (IOException ex){ System.out.println(ex.toString()); }finally{
	 * output.close(); } }
	 */

	public void addSimilarityValue(int[][] data, int t) {
		int sim;
		String idFirst;
		String idSecond;
		double deltaMass;
		int convertedDeltaMass;
		double massSpec;
		double massPep;
		for (int cpt = 0; cpt < data.length; ++cpt) {
			if (data[cpt][0] != data[cpt][1]) {
				if (data[cpt][2] >= t) {
					sim = data[cpt][2];
					idFirst = spectra[data[cpt][0]].name();
					idSecond = spectra[data[cpt][1]].name();
					massSpec = spectra[data[cpt][0]].totalMass();
					massPep = spectra[data[cpt][1]].totalMass();
					deltaMass = massSpec - massPep;
					if (!idFirst.equals(idSecond)) {
						convertedDeltaMass = (int) Math.round(deltaMass * Param.accuracyFactor());
						if (table.containsKey(idFirst)) {
							table.get(idFirst).addCandidate(data[cpt][1], sim, convertedDeltaMass, deltaMass);
						} else {
							table.put(idFirst,
									new Match(data[cpt][0], sim, data[cpt][1], convertedDeltaMass, deltaMass));
						}
					}
				}
			}
		}
	}

	public void addSimilarityValue(int[][] data, int t, int max) {
		int sim;
		String idFirst;
		String idSecond;
		double deltaMass;
		int convertedDeltaMass;
		double massSpec;
		double massPep;
		for (int cpt = 0; cpt < max; ++cpt) {
			if (data[cpt][0] != data[cpt][1]) {
				if (data[cpt][2] >= t) {
					sim = data[cpt][2];
					idFirst = spectra[data[cpt][0]].name();
					idSecond = spectra[data[cpt][1]].name();
					massSpec = spectra[data[cpt][0]].totalMass();
					massPep = spectra[data[cpt][1]].totalMass();
					deltaMass = massSpec - massPep;
					if (!idFirst.equals(idSecond)) {
						convertedDeltaMass = (int) Math.round(deltaMass * Param.accuracyFactor());
						if (table.containsKey(idFirst)) {
							table.get(idFirst).addCandidate(data[cpt][1], sim, convertedDeltaMass, deltaMass);
						} else {
							table.put(idFirst,
									new Match(data[cpt][0], sim, data[cpt][1], convertedDeltaMass, deltaMass));
						}
					}
				}
			}

		}
	}

	public void clean_data_buffer() {
		table.clear();
	}

}