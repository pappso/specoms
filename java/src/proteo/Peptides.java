/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proteo;

import biodata.Peptide;
import biodata.Protein;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author E092585L
 */
public class Peptides {

	// some of those class functions are pretty experimental and hve been
	// implemented to satisfy a specific external request

	// Need some collection there to store a kind of peptide/protein relationship
	// int (pep) -> int[] (prot) is probably the right kind of interaction
	private static Int2ObjectOpenHashMap<String> protein_reference_set = new Int2ObjectOpenHashMap<>();

	private static ArrayList<Peptide> peptides;

	public static void insert_protein_reference(int peptide_index, int protein_index) {

		if (Peptides.protein_reference_set.containsKey(peptide_index)) {
			String temp = Peptides.protein_reference_set.get(peptide_index);
			Peptides.protein_reference_set.put(peptide_index, temp + " " + protein_index);
		} else {
			Peptides.protein_reference_set.put(peptide_index, "" + protein_index);
		}

	}

	// fetching the set of protein referenced in the tool for a given peptide
	public static String get_references_as_string(int peptide_index) {

		String to_return = "{ ";
		if (Peptides.protein_reference_set.containsKey(peptide_index)) {
			to_return = to_return + Peptides.protein_reference_set.get(peptide_index) + " }";
		} else {
			to_return = to_return + " }";
		}
		return to_return;
	}

	public static String getUniprotRef(int peptide_index) {
		String to_return = "{ ";
		if (Peptides.protein_reference_set.containsKey(peptide_index)) {
			// to_return = to_return + Peptides.protein_reference_set.get(peptide_index) + "
			// }";
			String listProteins = Peptides.protein_reference_set.get(peptide_index);
			Pattern p = Pattern.compile("^>.{2}\\|.*\\|");

			for (String prot_index : listProteins.split(" ")) {
				int index = Integer.parseInt(prot_index);
				String protName = Proteins.get_protein_reference(index);
				Matcher m = p.matcher(protName);

				if ((protName.length() > 0) && (m.lookingAt()))
					protName = protName.substring(4, m.end() - 1);
				else {
					if (protName.length() > 0)
						protName = protName.substring(1);
					if (protName.length() > 12)
						protName = protName.substring(0, 12);
				}

				to_return = to_return + "<" + protName + ">";
			}
			to_return = to_return + " }";
		} else {
			to_return = to_return + " }";
		}
		return to_return;
	}

	public void digest(Proteins protL) {

		peptides = digestProteins(protL.getProteinsList());
		AffixHandler.ComputeAffixMasses();

	}

	public void digest(Proteins protL, ObjectOpenHashSet<String> tempList) {

		peptides = digestProteins(protL.getProteinsList(), tempList);
		AffixHandler.ComputeAffixMasses();

	}

	// specific version of the function used when we need to compare peptides
	// against peptides ; allow to filter out the set
	// of non decoy peptides in case the decoy base is built. This set of nondecoy
	// peptides will then be used to build the set
	// of spectra to match against the base.
	// Is this method still required ? Because from 2020/02/18, we accept
	// decoy-decoy comparisons

	private static ArrayList<Peptide> digestProteins(ArrayList<Protein> proteinsList,
			ObjectOpenHashSet<String> tempList) {

		Object2ObjectOpenHashMap<Peptide, Peptide> pep = new Object2ObjectOpenHashMap<>();
		ArrayList<Peptide> peptides = new ArrayList<Peptide>();

		ArrayList<int[]> temp = null;
		IntArrayList bindings = new IntArrayList();

		int pep_index;
		int prot_index;
		int prot_type;
		for (int cpt = 0; cpt < proteinsList.size(); ++cpt) {

			prot_index = proteinsList.get(cpt).identifier();
			prot_type = proteinsList.get(cpt).type();
			temp = proteinsList.get(cpt).digest(Param.minPeptideLength(), Param.maxPeptideLength(), bindings);

			for (int cpt2 = 0; cpt2 < temp.size(); ++cpt2) {

				Peptide tempPep = (Peptide) Peptide.createFromIntArray(temp.get(cpt2));
				tempPep.setIdentifier(bindings.get(cpt2));

				if (!tempPep.containsUnknownAA()) {

					if (!pep.containsKey(tempPep)) {
						tempPep.setOrigin(prot_type);
						pep.put(tempPep, tempPep);
						Peptides.insert_protein_reference(tempPep.identifier(), prot_index);
						// prot_type == 0: => decoy, =1: target, =-1 : contaminant
						// if( prot_type != 0 ){
						tempList.add(tempPep.asString());
						// }

					} else {
						pep.get(tempPep).setOrigin(prot_type);
						Peptides.insert_protein_reference(pep.get(tempPep).identifier(), prot_index);
					}

				}
			}
		}

		for (Map.Entry<Peptide, Peptide> entry : pep.entrySet()) {
			peptides.add(entry.getKey());
		}

		/*
		 * // DEBUG System.out.println("START PEPTIDE LIST CHECK"); for(int cpt = 0; cpt
		 * < peptides.size();++cpt){ System.out.println(peptides.get(cpt).identifier() +
		 * " - " + peptides.get(cpt).asString()); }
		 * System.out.println("END PEPTIDE LIST CHECK"); //
		 */

		return peptides;
	}

	private static ArrayList<Peptide> digestProteins(ArrayList<Protein> proteinsList) {
		Object2ObjectOpenHashMap<Peptide, Peptide> pep = new Object2ObjectOpenHashMap<>();
		ArrayList<Peptide> peptides = new ArrayList<Peptide>();

		ArrayList<int[]> temp;
		IntArrayList bindings = new IntArrayList();

		int pep_index;
		int prot_index;
		int prot_type;

		for (int cpt = 0; cpt < proteinsList.size(); ++cpt) {

			prot_index = proteinsList.get(cpt).identifier();
			prot_type = proteinsList.get(cpt).type();
			temp = proteinsList.get(cpt).digest(Param.minPeptideLength(), Param.maxPeptideLength(), bindings);

			for (int cpt2 = 0; cpt2 < temp.size(); ++cpt2) {

				Peptide tempPep = (Peptide) Peptide.createFromIntArray(temp.get(cpt2));
				tempPep.setIdentifier(bindings.get(cpt2));

				if (!tempPep.containsUnknownAA()) {

					if (!pep.containsKey(tempPep)) {
						tempPep.setOrigin(prot_type);
						pep.put(tempPep, tempPep);
						pep_index = tempPep.identifier();
						Peptides.insert_protein_reference(pep_index, prot_index);
					} else {
						pep.get(tempPep).setOrigin(prot_type);
						Peptides.insert_protein_reference(pep.get(tempPep).identifier(), prot_index);
					}

				}
			}
		}

		for (Map.Entry<Peptide, Peptide> entry : pep.entrySet()) {
			peptides.add(entry.getKey());
		}

		return peptides;
	}

	public ArrayList<Peptide> getPeptidesList() {
		return this.peptides;
	}

	public static String get_peptide_name(int peptide_index) {
		return Peptides.peptides.get(peptide_index).asString();
	}

	public static int getOrigin(int peptide_index) {
		return Peptides.peptides.get(peptide_index).origin();
	}

	public Peptide getPeptide(int peptide_index) {
		return Peptides.peptides.get(peptide_index);
	}

	/*
	 * public void display() {
	 * 
	 * System.out.println("Size after digest : "); for(int cpt = 0; cpt <
	 * peptides.size(); cpt++){ System.out.println(peptides.get(cpt).asString()); }
	 * 
	 * 
	 * }
	 */

}
