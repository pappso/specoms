/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package proteo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import biodata.Peak;
import biodata.Peptide;
import biodata.Spectrum;
import filters.FilterWrapper;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import mvc.Model;
import mvc.ViewAlerts;
import uk.ac.ebi.pride.tools.mzxml_parser.MzXMLFile;
import uk.ac.ebi.pride.tools.mzxml_parser.MzXMLFile.MzXMLScanIterator;
import uk.ac.ebi.pride.tools.mzxml_parser.MzXMLParsingException;
import uk.ac.ebi.pride.tools.mzxml_parser.MzXMLSpectrum;
import uk.ac.ebi.pride.tools.mzxml_parser.mzxml.model.ParentFile;
import uk.ac.ebi.pride.tools.mzxml_parser.mzxml.model.Scan;

/***
 * Management class for spectra importation and handling. Responsible for
 * loading spectra data from different files format and handling them in a
 * collection, providing data on demand.
 *
 * @author Matthieu David
 * @version 0.1
 */
public class Spectra {
	// protected static final Logger logger =
	// Logger.getLogger(Spectra.class.getName());

	/**
	 * The limit between experimental and theoretical spectra ; this integer
	 * actually corresponds to the last theoretical spectrum in the collection TODO
	 * :: needs to be asserted !! In the specific case where we want to ompare
	 * peptides together, the limit_p_p is computed first and the result is affected
	 * to limit
	 * 
	 * @since 0.1
	 */
	private int limit;
	private int limit_p_p;

	/**
	 * Structure to temporarily handle the spectra object until all of them have
	 * been constructed.
	 * 
	 * @since 0.1
	 */
	private ArrayList<Spectrum> spectraList;

	/**
	 * Storage array for the spectra collection once all spectra have been built. A
	 * spectrum with an id value of i is expected to be stored at the position i in
	 * the array.
	 * 
	 * @since 0.1
	 */
	private static Spectrum[] spectra;

	/**
	 * Given the id of a spectrum in the spectra array, returns the name of this
	 * spectrum
	 * 
	 * @param spectra_index The index in the storage array of the spectrum whose
	 *                      name we request
	 * @return the name of the spectrum whose position in the array corresponds to
	 *         the provided id
	 * @since 0.1
	 */
	public static String get_spectra_name(int spectra_index) {
		return Spectra.spectra[spectra_index].name();
	}

	public static Peptide get_peptide(int spectra_index) {
		return Spectra.spectra[spectra_index].peptide();
	}

	/**
	 * Provides the total number of spectra stored in the datastructure
	 * 
	 * @return the number of elements in the spectra array // TODO : has to be
	 *         checked ; I think there is one too many here ! // should be : return
	 *         (this.spectra.length);
	 * @since 0.1
	 */
	public int numberOfSpectra() {
		return (this.spectra.length);
	}

	/**
	 * Access the collection of spectra stored in a spectrum array
	 * 
	 * @return the Spectrum array
	 * @since 0.1
	 */
	public Spectrum[] get() {
		return this.spectra;
	}

	/**
	 * Return the limit between the theoretical and experimental spectra in the
	 * collection
	 * 
	 * @return the number of theoretical spectra
	 * @since 0.1
	 */
	public int limit() {
		return this.limit;
	}

	/**
	 * Initialize a Spectrum array whose size is based on a spectra list and fills
	 * the array copying the spectra from the list without altering the order, then
	 * computes the hash of each spectrum.
	 * 
	 * @since 0.1
	 */
	public void prepare() {
		// array initialization and copy
		spectra = new Spectrum[spectraList.size()];
		spectraList.toArray(spectra);
		// allow garbage collection of the spectra list
		spectraList = null;
		// computation of the hash for each spectrum
		for (int cpt = 0; cpt < spectra.length; ++cpt) {
			spectra[cpt].hashSpectrum();
		}
	}

	/**
	 * Provided a list of peptides, this function builds the corresponding
	 * theoretical spectra and fills the spectraList with those.
	 * 
	 * @param pepList a list of peptides from which the spectra must be built
	 * @since 0.1
	 */
	public void build(Peptides pepList) {
		// user log
		System.out.println("Constructing theoretical spectra...");
		// spectra construction and counting
		spectraList = buildSpectra(pepList.getPeptidesList());
		limit = spectraList.size();
		// user log
		System.out.println(limit + " theoretical spectra added.");
	}

	/**
	 * Add experimental spectra read from the experimental dataset at the end of the
	 * spectra list without taking in account the device accuracy (no peak
	 * duplication) then updates the number of spectra in the colelction. TODO ::
	 * management of the limite should be clarified...anx improved
	 * 
	 * @since 0.1
	 */
	public void addNonDuplicatedSpectra() {
		// user log
		System.out.println("Loading experimental spectra database...");
		// initialization, insertion of spectra without peak duplication and count
		// update
		spectraList = new ArrayList<>();
		addNonDuplicatedExp();
		limit = spectraList.size();
		// user log
		System.out.println(limit + " spectra added in experimental database.");
	}

	/**
	 * Provided a list of peptides, this function builds the corresponding
	 * theoretical spectra and fills the spectraList with those. The produced
	 * spectra are stored in the same order as the peptides they originated from.
	 * 
	 * @param peptideList an ArrayList<Peptide> containing the peptides we want to
	 *                    build the spectra from
	 * @return ArrayList<Spectrum> the spectra built by the function
	 * @since 0.1
	 */
	private static ArrayList<Spectrum> buildSpectra(ArrayList<Peptide> peptideList) {
		// initialization
		ArrayList<Spectrum> spectra = new ArrayList<Spectrum>();
		// iterate over the peptide in the list and build the spectrum for each of them
		for (int cpt = 0; cpt < peptideList.size(); ++cpt) {
			Peptide p = peptideList.get(cpt);
			spectra.add(Spectrum.createFromPeptide(p, p.asString()));
		}
		// return the result
		return spectra;
	}

	/**
	 * Add experimental spectra read from the experimental dataset at the end of the
	 * spectra list, taking in account the device accuracy (peak duplication). As
	 * those spectra are considered as the tested dataset and not the database to
	 * match against, the limit is not updated. This loading is done with peak
	 * duplication to take in account device accuracy.
	 * 
	 * @since 0.1
	 */
	public void addExp() {
		// initialization and spectra load in a temporary list depending on the file
		// extension
		ArrayList<Spectrum> tempList;
		if (Param.experimentalDataset().endsWith(".mgf")) {
			tempList = loadExperimentalMGFdataDuplicated_robust();
		} else {
			tempList = loadExperimentalMzXMLdataDuplicated();
		}
		// copy of the temporary list in the specra list, conserving ordering
		spectraList.addAll(tempList);
		// user log
		System.out.println(spectraList.size() - limit + " experimental spectra added.");
	}

	/**
	 * Add experimental spectra read from the experimental dataset at the end of the
	 * spectra list, taking in account the device accuracy (peak duplication). As
	 * those spectra are considered as the tested dataset and not the database to
	 * match against, the limit is not updated. This loading is done without taking
	 * in account device accuracy (no peak duplication).
	 * 
	 * @since 0.1
	 */
	private void addNonDuplicatedExp() {
		// initialization and spectra load in a temporary list depending on the file
		// extension
		ArrayList<Spectrum> tempList;
		if (Param.experimentalDataset().endsWith(".mgf")) {
			tempList = loadExperimentalMGFdataNonDuplicated();
		} else {
			tempList = loadExperimentalMzXMLdataNonDuplicated();
		}
		// copy of the temporary list in the specra list, conserving ordering
		spectraList.addAll(tempList);
		// TODO :: add a user log ??
	}

	/**
	 * @brief process raw spectrum during MS data file read
	 * 
	 * @param spectra         the spectra list on which the processed spectrum will
	 *                        be added
	 * @param peakMap         the raw spectrum to process (from the MS data file)
	 * @param precursorCharge charge of the precursor
	 * @param precursorMz     m/z of the precursor
	 * @param title           the spectrum title
	 * 
	 * 
	 */
	private static void processRawSpectrumWhileReading(ArrayList<Spectrum> spectra, Map<Double, Double> peakMap,
			int precursorCharge, double precursorMz, String title) {

		FilterWrapper FW = Param.filters(precursorMz);
		peakMap = FW.filter(peakMap);

		// List<Map.Entry<Double, Double>> list2 = new LinkedList<Map.Entry<Double,
		// Double>>(peakMap.entrySet());

		// sort peaks by intensity and store them in a temporary list for easier
		// handling, and be sure they are ordered by intensity
		List<Map.Entry<Double, Double>> peaks = new LinkedList<Map.Entry<Double, Double>>(peakMap.entrySet());
		Collections.sort(peaks, new Comparator<Map.Entry<Double, Double>>() {
			public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		if (peaks.size() != 0) {
			IntArrayList peakListTemp = new IntArrayList();

			for (Map.Entry<Double, Double> P : peaks) {
				peakListTemp.add((int) Math.round(P.getKey() * Param.accuracyFactor()));
			}

			int[] peakList = new int[peakListTemp.size()];
			peakListTemp.toIntArray(peakList);
			if ((precursorCharge <= Param.maximalCharge()) && (precursorCharge >= Param.minimalCharge())) {
				Spectrum s = Spectrum.createFromIntArray(peakList, title, Param.accuracyValue(), precursorMz,
						precursorCharge);
				spectra.add(s);
			}
		}

	}
	// TODO :: The following pieces of codes are constituted of 4 times the same big
	// piece of code with subtle changes ; need to be cut down into functions to
	// improve readability / maintainability / correctedness...

	/**
	 * Load spectra from a MzXML file using the MzReader library. Device accuracy is
	 * taken in account (peak duplication).
	 * 
	 * @return ArrayList<Spectrum> the list of the spectra loaded from teh
	 *         exprimental dataset file
	 * @since 0.1
	 */
	private static ArrayList<Spectrum> loadExperimentalMzXMLdataDuplicated() {
		// initialization of a list to store spectra to return as a result and a
		// temporary list to store peaks
		ArrayList<Spectrum> spectra = new ArrayList<Spectrum>();
		ArrayList<Peak> peaks = new ArrayList<Peak>();
		// initialization of a set to control the absence of spectra with duplicated
		// names
		ObjectOpenHashSet<String> titleMap = new ObjectOpenHashSet<>();
		// the input file read from SpecOMS parameters
		File inputFile;
		// reading the file
		// logger.log(Level.INFO, "loadExperimentalMzXMLdataDuplicated");
		try {
			// MzXML paerser setup
			inputFile = new File(Param.experimentalDataset());
			MzXMLFile xmlFile = new MzXMLFile(inputFile);
			// set the MzXML iterator to only consider MS/MS spectra and ignore MS spectra
			MzXMLScanIterator ms2 = xmlFile.getMS2ScanIterator();
			Iterator<Scan> scans = ms2.iterator();
			// data scans and experimental spectrum variables
			Scan scan;
			MzXMLSpectrum spectrum;

			// Needed parameters
			int precursorCharge;
			double precursorMz;
			String title, scanID;
			List<ParentFile> parent = xmlFile.getParentFile();

			// Looping through all the spectra
			while (scans.hasNext()) {
				scan = scans.next();
				scanID = String.valueOf(scan.getNum());
				title = parent.iterator().next().getFileName() + "." + scanID;

				/*
				 * Skips the current scan as long as its title already exists in the titleMap.
				 * Errors are not displayed the same way whether it is called from the UI or
				 * command line.
				 */
				while (titleMap.contains(title)) {
					if (Model.isUI()) {
						ViewAlerts.parsingAlert();
					} else {
						System.out.println(
								"Your .mzXML file is not properly formated. You should abort the execution and correct it.");
						System.out.println("Press enter to continue. Results and behavior are not guaranteed anymore.");
					}
					scan = scans.next();
					scanID = String.valueOf(scan.getNum());
					title = parent.iterator().next().getFileName() + "." + scanID;
				}
				spectrum = new MzXMLSpectrum(scan);
				precursorCharge = spectrum.getPrecursorCharge();
				precursorMz = spectrum.getPrecursorMZ();

				processRawSpectrumWhileReading(spectra, spectrum.getPeakList(), precursorCharge, precursorMz, title);
			}
		} catch (

		MzXMLParsingException e) {
			e.printStackTrace();

			// logger.log(Level.SEVERE, "error in loadExperimentalMzXMLdataDuplicated", e);
		}
		return spectra;
	}

	/*
	 * Duplicate to handle robustness, should replace the old procedure once
	 * validated
	 *
	 */

	private static ArrayList<Spectrum> loadExperimentalMGFdataDuplicated_robust() {

		ArrayList<Spectrum> spectra = new ArrayList<>(); // handling spectra objects to return
		Map<Double, Double> peaks = new LinkedHashMap<Double, Double>();

		ObjectOpenHashSet<String> titleMap = new ObjectOpenHashSet<>(); // to handle titles and detect a possible
																		// duplicate in namings

		// to read the .mgf file
		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		try {
			inputFile = new FileInputStream(Param.experimentalDataset());
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);

			String line;
			String title = "";

			double pepMass = 0.0;
			int charge = 0;

			String[] tempRead;
			String scanID;

			Scanner sc = new Scanner(System.in);

			boolean charged = false;
			boolean titled = false;
			boolean pepmassed = false;

			while ((line = inputBuffer.readLine()) != null) {
				if (!line.isEmpty()) {
					if (line.contains("BEGIN IONS") || line.charAt(0) == '#') {
					} else {
						if (line.contains("END IONS")) {
							if (titleMap.contains(title)) {
								System.out.println(
										"Your .MGF file is not properly formated. You should abort the execution and correct it.");
								System.out.println(
										"Press enter to continue. Results and behavior are not guaranteed anymore.");
								sc.nextLine();
							} else {

								// TODO
								// use this static function:
								// processRawSpectrumWhileReading(spectra, spectrum.getPeakList(),
								// precursorCharge, precursorMz, title);
								// and replace the following code :
								titleMap.add(title);
								if (peaks.size() != 0) {

									processRawSpectrumWhileReading(spectra, peaks, charge, pepMass, title);

								}
								peaks.clear();
							}
							if (titled == false) {
								System.out.println(
										"There is an issue while reading your .mgf file(missing spectra titles). We cannot ensure proper functionning of SpecOMS. Please check your .mgf file formatting. If you cannot achieve loading your data using .mgf file, please use .mzxml format or contact us directly, we will be happy to help.");
							}
							if (charged == false) {
								System.out.println("Some spectra in the .mgf file do not have a charge associated.");
							}
							if (pepmassed == false) {
								System.out.println("Some spectra in the .mgf file do not have a mass associated.");
							}
							charged = false;
							titled = false;
							pepmassed = false;
						} else {
							if (line.startsWith("TITLE=")) {
								tempRead = line.split("=");
								tempRead = tempRead[1].split("\\\\");
								title = tempRead[tempRead.length - 1];
								titled = true;
							}
							if (line.startsWith("CHARGE=")) {
								tempRead = line.split("=");
								charge = Integer.parseInt(tempRead[1].substring(0, 1));
								charged = true;
							}
							if (line.startsWith("SCANS=") || line.startsWith("SCAN=")) {
								tempRead = line.split("=");
								scanID = tempRead[1];
								title = title + "." + scanID;
							}
							if (line.startsWith("PEPMASS=")) {
								tempRead = line.split("=");
								tempRead = tempRead[1].split(" ");
								pepMass = Double.parseDouble(tempRead[0]);
								pepmassed = true;
							}
							tempRead = line.split(" ");
							if (tempRead.length == 2) {
								Pattern p = Pattern.compile("([0-9]+\\.[0-9]*)");
								Matcher m = p.matcher(tempRead[0]);
								boolean match = m.matches();
								if (match) {
									// System.out.println(tempRead[0]);
									double one;
									one = Double.parseDouble(tempRead[0]);

									double two;
									if (!tempRead[1].contains("e") && !tempRead[1].contains("E")) {
										two = Double.parseDouble(tempRead[1]);
									} else {
										String number = tempRead[1].toLowerCase();
										String tempTwo[] = number.split("e");
										two = Double.parseDouble(tempTwo[0])
												* Math.pow(10, Integer.parseInt(tempTwo[1]));
									}
									if (two > 0.0) {
										peaks.put(one, two);
									}
								}
							}
						}
					}
				}
			}
			inputBuffer.close();
			// System.out.println(endCount + " - " + emptyCount);
			// file handling exception

			sc.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		// returning the spectra collection
		return spectra;
	}

	/*
	 * Robustness issues due to non standardization of .mgf files
	 */

	private static ArrayList<Spectrum> loadExperimentalMGFdataDuplicated() {

		ArrayList<Spectrum> spectra = new ArrayList<>();

		ArrayList<Peak> peaks = new ArrayList<>();

		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		ObjectOpenHashSet<String> titleMap = new ObjectOpenHashSet<>();

		try {

			inputFile = new FileInputStream(Param.experimentalDataset());
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);

			String line;
			String title = "";

			double pepMass = 0.0;
			int charge = 0;

			String[] tempRead;
			String scanID;

			Scanner sc = new Scanner(System.in);

			while ((line = inputBuffer.readLine()) != null) {

				if (!line.equals("")) {
					if (line.contains("BEGIN") || line.charAt(0) == '#') {
						// do nothing...
					} else {
						if (line.contains("END")) {
							if (titleMap.contains(title)) {
								System.out.println(
										"Your .MGF file is not properly formated. You should abort the execution and correct it.");
								System.out.println(
										"Press enter to continue. Results and behavior are not guaranteed anymore.");
								sc.nextLine();
							} else {
								titleMap.add(title);
								if (peaks.size() != 0) {
									Collections.sort(peaks);

									int limit = peaks.size();
									int amount = 0;

									IntArrayList peakListTemp = new IntArrayList();

									int pos = 0;
									int oldPeakMass = 0;
									int oldPeakIntensity = 0;
									int newPeakMass;
									int newPeakIntensity;

									while ((pos < limit) && (amount < Param.massFilterSize())) {
										newPeakMass = peaks.get(pos).Mass();
										newPeakIntensity = peaks.get(pos).Intensity();
										if (Math.abs(newPeakMass - oldPeakMass) > 2 * Param.accuracyValue()) {
											peakListTemp.add(newPeakMass);
											oldPeakMass = newPeakMass;
											oldPeakIntensity = newPeakIntensity;
											++amount;
										} else {
											if (oldPeakIntensity > newPeakIntensity) {
												// do nothing
											} else {
												peakListTemp.set(peakListTemp.size() - 1, newPeakMass);
												oldPeakMass = newPeakMass;
												oldPeakIntensity = newPeakIntensity;
											}
										}
										++pos;
									}
									int[] peakList = new int[peakListTemp.size()];
									peakListTemp.toIntArray(peakList);

									if ((charge <= Param.maximalCharge()) && (charge >= Param.minimalCharge())) {
										Spectrum s = Spectrum.createFromIntArray(peakList, title, Param.accuracyValue(),
												pepMass, charge);
										spectra.add(s);
									}
								}
								peaks.clear();
							}
						} else {
							if (line.contains("=")) {
								tempRead = line.split("=");
								if (line.contains("TITLE")) {
									tempRead = tempRead[1].split("\\\\");
									title = tempRead[tempRead.length - 1];
								}
								if (line.contains("CHARGE")) {
									charge = Integer.parseInt(tempRead[1].substring(0, 1));
								}
								if (line.contains("SCANS")) {
									scanID = tempRead[1];
									title = title + "." + scanID;
								}
								if (line.contains("PEPMASS")) {
									tempRead = tempRead[1].split(" ");
									pepMass = Double.parseDouble(tempRead[0]);
								}
							} else {

								tempRead = line.split(" ");
								double one;
								if (tempRead[0].contains(".")) {
									one = Double.parseDouble(tempRead[0]);
								} else {
									one = Integer.parseInt(tempRead[0]);
								}
								double two;
								if (!tempRead[1].contains("e") && !tempRead[1].contains("E")) {
									if (tempRead[1].contains(".")) {
										two = Double.parseDouble(tempRead[1]);
									} else {
										two = Long.parseLong(tempRead[1]);
									}
								} else {
									String number = tempRead[1].toLowerCase();
									String tempTwo[] = number.split("e");

									two = Double.parseDouble(tempTwo[0]) * Math.pow(10, Integer.parseInt(tempTwo[1]));
								}
								if (two > 0.0) {
									peaks.add(Peak.buildPeak(one, two, Param.accuracyFactor()));
								}
							}
						}
					}
				}
			}

			inputBuffer.close();
			sc.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		return spectra;
	}

	public void addDuplicatedPeptides(ObjectOpenHashSet<String> tempList) {
		// tempList contains the list of peptides that must be used as baits
		// may not contain decoy peptides for instance

		int max = spectraList.size();
		Spectrum s;
		for (int cpt = 0; cpt < max; ++cpt) {
			s = spectraList.get(cpt);
			if (tempList.contains(s.name())) {
				spectraList.add(s.copy_with_duplication());
			}
		}
	}

	public void addNonDuplicatedPeptides(ObjectOpenHashSet<String> tempList) {

		int max = spectraList.size();
		// System.out.println("limit and max :: " + max + " - " + limit_p_p);
		Spectrum s;
		for (int cpt = 0; cpt < max; ++cpt) {
			s = spectraList.get(cpt);
			if (tempList.contains(s.name())) {
				spectraList.add(s.copy());
			}
		}
	}

	private ArrayList<Spectrum> loadExperimentalMGFdataNonDuplicated() {
		ArrayList<Spectrum> spectra = new ArrayList<>();

		ArrayList<Peak> peaks = new ArrayList<Peak>();

		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		ObjectOpenHashSet<String> titleMap = new ObjectOpenHashSet<>();

		try {
			inputFile = new FileInputStream(Param.experimentalDataset());
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);
			String line;
			String title = "";
			double pepMass = 0.0;
			int charge = 0;

			String[] tempRead;
			String scanID;

			// String oldTitle="";

			Scanner sc = new Scanner(System.in);

			while ((line = inputBuffer.readLine()) != null) {

				if (!line.equals("")) {
					if (line.contains("BEGIN")) {
						// do nothing...
					} else {
						if (line.contains("END")) {
							if (titleMap.contains(title)) {
								System.out.println(
										"Your .MGF file is not properly formated. You should abort the execution and correct it.");
								System.out.println(
										"Press enter to continue. Results and behavior are not guaranteed anymore.");
								sc.nextLine();
							} else {
								titleMap.add(title);
								if (peaks.size() != 0) {
									Collections.sort(peaks);

									int limit = peaks.size();
									int amount = 0;

									IntArrayList peakListTemp = new IntArrayList();

									int pos = 0;
									int oldPeakMass = 0;
									int oldPeakIntensity = 0;
									int newPeakMass;
									int newPeakIntensity;

									while ((pos < limit) && (amount < Param.massFilterSize())) {
										newPeakMass = peaks.get(pos).Mass();
										newPeakIntensity = peaks.get(pos).Intensity();
										if (Math.abs(newPeakMass - oldPeakMass) > 2 * Param.accuracyValue()) {
											peakListTemp.add(newPeakMass);
											oldPeakMass = newPeakMass;
											oldPeakIntensity = newPeakIntensity;
											++amount;
										} else {
											if (oldPeakIntensity > newPeakIntensity) {
												// do nothing
											} else {
												peakListTemp.set(peakListTemp.size() - 1, newPeakMass);
												oldPeakMass = newPeakMass;
												oldPeakIntensity = newPeakIntensity;
											}
										}
										// a vérifier
										// oldPeakMass = newPeakMass;
										// oldPeakIntensity = newPeakIntensity;
										++pos;
									}

									// int[] peakList = peakListTemp.toArray();
									int[] peakList = new int[peakListTemp.size()];
									peakListTemp.toIntArray(peakList);

									if ((charge <= Param.maximalCharge()) && (charge >= Param.minimalCharge())) {
										Spectrum s = Spectrum.createFromIntArray(peakList, title, pepMass, charge);
										spectra.add(s);
									}
								}
								peaks.clear();
							}
						} else {
							// pas très clean ça ; factoriser
							if (line.contains("=")) {
								tempRead = line.split("=");
								if (line.contains("TITLE")) {
									tempRead = tempRead[1].split("\\\\");
									title = tempRead[tempRead.length - 1];
								}
								if (line.contains("CHARGE")) {
									charge = Integer.parseInt(tempRead[1].substring(0, 1));
								}
								if (line.contains("SCANS")) {
									scanID = tempRead[1];
									title = title + "." + scanID;
									// System.out.println(title);
								}
								// on ajoute pas la pepmass dans les pics... si ?
								if (line.contains("PEPMASS")) {
									tempRead = tempRead[1].split(" ");
									pepMass = Double.parseDouble(tempRead[0]);
								}
								if (line.contains("RTINSECONDS")) {
									// should be another variable
									// pepMass = Double.parseDouble(tempRead[1]);
								}

							} else {

								tempRead = line.split(" ");
								double one;
								if (tempRead[0].contains(".")) {
									one = Double.parseDouble(tempRead[0]);
								} else {
									one = Integer.parseInt(tempRead[0]);
								}
								double two;
								if (!tempRead[1].contains("e") && !tempRead[1].contains("E")) {
									if (tempRead[1].contains(".")) {

										two = Double.parseDouble(tempRead[1]);
									} else {

										two = Integer.parseInt(tempRead[1]);

									}
								} else {
									String number = tempRead[1].toLowerCase();
									String tempTwo[] = number.split("e");

									two = Double.parseDouble(tempTwo[0]) * Math.pow(10, Integer.parseInt(tempTwo[1]));
								}
								if (two > 0.0) {
									peaks.add(Peak.buildPeak(one, two, Param.accuracyFactor()));
								}
							}
						}
					}
				}
			}

			sc.close();
			inputBuffer.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		return spectra;
	}

	private ArrayList<Spectrum> loadExperimentalMzXMLdataNonDuplicated() {
		ArrayList<Spectrum> spectra = new ArrayList<>();
		ArrayList<Peak> peaks = new ArrayList<>();
		// Collect all the titles in order to avoid spectra with the same name
		ObjectOpenHashSet<String> titleMap = new ObjectOpenHashSet<>();

		File inputFile;

		try {
			inputFile = new File(Param.experimentalDataset());
			MzXMLFile xmlFile = new MzXMLFile(inputFile);

			// Only loop through the spectra obtained with the fragmentation of a peptide,
			// in the second spectrometer.
			MzXMLScanIterator ms2 = xmlFile.getMS2ScanIterator();
			Iterator<Scan> scans = ms2.iterator();
			Scan scan;
			MzXMLSpectrum spectrum;

			// Needed parameters
			int precursorCharge;
			double precursorMz, mz, intensity;
			Map<Double, Double> peakMap;
			String title, scanID;
			List<ParentFile> parent = xmlFile.getParentFile();

			// Looping through all the spectra
			while (scans.hasNext()) {
				scan = scans.next();
				scanID = String.valueOf(scan.getNum());
				title = parent.iterator().next().getFileName() + "." + scanID;

				/*
				 * Skips the current scan as long as its title already exists in the titleMap.
				 * Errors are not displayed the same way whether it is called from the UI or
				 * command line.
				 */
				while (titleMap.contains(title)) {
					if (Model.isUI()) {
						ViewAlerts.parsingAlert();
					} else {
						System.out.println(
								"Your .mzXML file is not properly formated. You should abort the execution and correct it.");
						System.out.println("Press enter to continue. Results and behavior are not guaranteed anymore.");
					}
					scan = scans.next();
					scanID = String.valueOf(scan.getNum());
					title = parent.iterator().next().getFileName() + "." + scanID;
				}

				spectrum = new MzXMLSpectrum(scan);
				precursorCharge = spectrum.getPrecursorCharge();
				precursorMz = spectrum.getPrecursorMZ();
				peakMap = spectrum.getPeakList();

				// Reading the peaks for each spectrum
				for (Map.Entry<Double, Double> entry : peakMap.entrySet()) {
					mz = entry.getKey();
					intensity = entry.getValue();
					if (intensity > 0.0) {
						peaks.add(Peak.buildPeak(mz, intensity, Param.accuracyFactor()));
					}
				}

				if (peaks.size() != 0) {
					Collections.sort(peaks);

					int limit = peaks.size();
					int amount = 0;

					IntArrayList peakListTemp = new IntArrayList();

					int pos = 0;
					int oldPeak = 0;

					while ((pos < limit) && (amount < Param.massFilterSize())) {
						int newPeak = peaks.get(pos).Mass();
						if (Math.abs(newPeak - oldPeak) > Param.accuracyValue()) {
							peakListTemp.add(newPeak);
							++amount;
						}
						oldPeak = newPeak;
						++pos;
					}

					int[] peakList = new int[peakListTemp.size()];
					peakListTemp.toIntArray(peakList);

					if ((precursorCharge <= Param.maximalCharge()) && (precursorCharge >= Param.minimalCharge())) {
						Spectrum s = Spectrum.createFromIntArray(peakList, title, precursorMz, precursorCharge);
						spectra.add(s);
					}
				}
				peaks.clear();
			}
		} catch (MzXMLParsingException e) {
			e.printStackTrace();
		}
		return spectra;
	}

	/*
	 * public void display() {
	 * 
	 * for(int cpt = 0; cpt < spectraList.size() ; cpt++){
	 * System.out.println(spectraList.get(cpt).id() + " - " +
	 * spectraList.get(cpt).name()); }
	 * 
	 * }
	 */

}
