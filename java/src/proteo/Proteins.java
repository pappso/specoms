/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package proteo;

import biodata.Protein;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import mvc.Model;

public class Proteins {
	//protected static final Logger logger = Logger.getLogger(Proteins.class.getName());

	public Proteins() {
		//logger.info("Proteins begin");
	}

	public static String get_protein_reference(int identifier) {
		return protein_reference_identifier.get(identifier);
	}

	private ArrayList<Protein> proteins;
	private int prot_number;

	// private static IntArrayList protein_reference_index = new IntArrayList();

	private static ObjectArrayList<String> protein_reference_identifier = new ObjectArrayList<>();

	public static void add_protein_reference(int index, String reference) {
		// Proteins.protein_reference_index.add(index);
		Proteins.protein_reference_identifier.add(reference);
	}

	public static void write_protein_reference_table() {

		try {
			FileWriter writer = new FileWriter(Param.protein_reference_file);
			BufferedWriter buffer = new BufferedWriter(writer);
			PrintWriter output = new PrintWriter(buffer);

			output.println("protein_index ; protein_identifier");
			for (int counter = 0; counter < protein_reference_identifier.size(); ++counter) {
				output.println((counter) + " ; " + protein_reference_identifier.get(counter));
			}
			output.close();

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		// protein_reference_index = null;
		// protein_reference_identifier = null;
	}

	public void addProteins() {

		//logger.log(Level.INFO, "addProteins begin");
		System.out.println("Parsing proteins...");

		proteins = loadProteinsFromFile();

		prot_number = proteins.size();
		System.out.println(proteins.size() + " database proteins added...");
		//logger.log(Level.INFO, "addProteins end");

	}

	public ArrayList<Protein> getProteinsList() {
		return this.proteins;
	}

	public void addContaminants() {
		System.out.println("Adding contaminants...");
		addContaminants(proteins);
		System.out.println((proteins.size() - prot_number) + " contaminants proteins added...");
	}

	private static void addContaminants(ArrayList<Protein> proteins) {

		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		try {
			inputFile = new FileInputStream(Param.contaminants());
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);
			String line;

			String protein_reference = "";
			String proteinRead = "";

			while ((line = inputBuffer.readLine()) != null) {
				if (!line.isEmpty()) {
					if (line.charAt(0) == '>') {
						if (!proteinRead.equals("")) {
							proteins.add(Protein.create(proteinRead, -1));
							Proteins.add_protein_reference(Protein.protein_current_identifier(), protein_reference);
							proteinRead = "";
						}
						protein_reference = line;// .substring(1, 10);

					} else {
						proteinRead = proteinRead + line;
					}
				}
			}

			proteins.add(Protein.create(proteinRead, -1));
			Proteins.add_protein_reference(Protein.protein_current_identifier(), protein_reference);

			inputBuffer.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

	}

	public static ArrayList<Protein> loadProteinsFromFile() {

		ArrayList<Protein> proteins = new ArrayList<Protein>();

		String proteinRead = "";

		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		try {
			inputFile = new FileInputStream(Param.database());
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);
			String line;
			String protein_reference = "";

			while ((line = inputBuffer.readLine()) != null) {
				if (!line.isEmpty()) {
					if (line.charAt(0) == '>') {

						if (!proteinRead.equals("")) {
							proteins.add(Protein.create(proteinRead, 1));
							Proteins.add_protein_reference(Protein.protein_current_identifier(), protein_reference);
							proteinRead = "";
						}
						protein_reference = line;// .substring(1, 10);

					} else {
						proteinRead = proteinRead + line;
					}
				}
			}

			proteins.add(Protein.create(proteinRead, 1));
			Proteins.add_protein_reference(Protein.protein_current_identifier(), protein_reference);

			inputBuffer.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		return proteins;
	}

	public void addDecoy() {

		int max = proteins.size();
		for (int cpt = 0; cpt < max; cpt++) {
			proteins.add(Protein.createDecoy(proteins.get(cpt)));
		}

		/*
		 * // System.out.println("Size after decoy : " + proteins.size() );
		 * 
		 * for(int cpt=0; cpt < proteins.size(); ++cpt){
		 * System.out.println(proteins.get(cpt).toString()); }
		 * System.out.println("End of proteins list after decoy");
		 */

	}

}
