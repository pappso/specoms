/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package proteo;

import biodata.AminoAcidHandler;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 *
 * @author E092585L
 */
public class AffixHandler {

	public static Int2ObjectOpenHashMap<int[]> prefix = new Int2ObjectOpenHashMap<>();
	public static Int2ObjectOpenHashMap<int[]> suffix = new Int2ObjectOpenHashMap<>();
	public static Int2DoubleOpenHashMap prefixMass = null;
	public static Int2DoubleOpenHashMap suffixMass = null;

	public static Int2ObjectOpenHashMap<int[]> proteins;

	public static void ComputeAffixMasses() {

		prefixMass = new Int2DoubleOpenHashMap(prefix.size());
		suffixMass = new Int2DoubleOpenHashMap(suffix.size());

		int id;

		for (Int2ObjectMap.Entry<int[]> entry : prefix.int2ObjectEntrySet()) {
			id = entry.getIntKey();
			prefixMass.put(id, AminoAcidHandler.computeMass(entry.getValue()));
		}

		for (Int2ObjectMap.Entry<int[]> entry : suffix.int2ObjectEntrySet()) {
			id = entry.getIntKey();
			suffixMass.put(id, AminoAcidHandler.computeMass(entry.getValue()));
		}

	}

}
