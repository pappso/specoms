/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package proteo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import biodata.AminoAcidHandler;
import filters.AccuracyValuePeakDiscriminationFilter;
import filters.Deisotoping;
import filters.DynamicRangeFilter;
import filters.FilterWrapper;
import filters.HighPass;
import filters.LowFilter;
import filters.ParentFilter;
import filters.UpFilter;
import filters.WindowFilter;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import mvc.Model;

/*
* <b> Retrieve and store parameters from the config file and path file. </b>
*
* <p> If no parameter is found, a default value is provided </p>
*
* @author Matthieu David
* @version 1.0
* 
*/
public class Param {

	private static int highest_mass_value = -1;

	/*
	 * Hydrogen isotopic mass. This value is intended to be implemented differently
	 * in future releases. *
	 */
	public static final double HYDROGEN = 1.00728;

	private static String CONFIGURATION_FILE = "execution_parameters.ini";
	private static final String INOUT_FILES = "files_config.ini";

	// public static int semitryptics = -57;
	// public static int tryptics_accuracy = 2;
	// public static double mc_accuracy = 0.02;
	// epsilon ?
	// public static double trypticLimit = -0.02;
	// public static final boolean EXTENSIVE_RESULT=false;

	/* ---- ---- FILES ---- ---- */

	private static String database_file = "";
	private static String contaminants_file = "";
	private static String experimental_data = "";
	private static boolean uniprotFormat = false;
	public static String protein_reference_file = "";

	// private static String time_measurements = ""; TODO :: add this later
	private static String results_file = "";

	/* ---- ---- PARAMETERS ---- ---- */

	private static String search_mode = "projection";
	private static boolean protein_info = true;

	private static int minimal_peptide_length = 7;
	private static int maximal_peptide_length = 25;

	private static int minimal_peptide_charge = 1;
	private static int maximal_peptide_charge = 3;

	private static int minimal_mass_delta = -500;
	private static int maximal_mass_delta = 1000;

	private static String chargeFragment = "1+";

	public static String getChargeFragment() {
		return chargeFragment;
	}

	private static int threshold = 8;
	// TODO Dans l'interface
	// private static int min_filter_size = 20;
	private static int mass_filter_size = -1;

	private static double accuracy = 0.02;
	private static int accuracy_decimals = 2;
	private static int accuracy_value = 2;
	private static int accuracy_factor = 100;

	private static double mc_tolerance = 0.02;

	public static double getMc_tolerance() {
		return mc_tolerance;
	}

	private static boolean decoy = false;
	private static boolean shift = true;

	private static boolean single_match = true;

	private static int minimum_score = 8;

	/* Gestion Enzyme */
	private static String ter = "C";
	private static String aaCleaved = "KR";
	private static String aaNotCleaved = "";
	private static String enzPattern = ".*?[KR]";
	private static int missCleavage = 0;
	private static boolean missDB = false;
	private static String massModification = "57.0214633721083@C";

	/* SpecFit priorities */

	public static String getMassModification() {
		return massModification;
	}

	private static int MC_PRIORITY = 6;

	public static int getMC_PRIORITY() {
		return MC_PRIORITY;
	}

	private static int ST_PRIORITY = 4;

	public static int getST_PRIORITY() {
		return ST_PRIORITY;
	}

	private static int SHIFT_PRIORITY = 2;

	public static int getSHIFT_PRIORITY() {
		return SHIFT_PRIORITY;
	}

	private static int EXACT_DELTA_PRIORITY = 8;

	public static int getEXACT_DELTA_PRIORITY() {
		return EXACT_DELTA_PRIORITY;
	}

	private static Object2ObjectOpenHashMap<String, String> properties;

	/* Filters parameters */
	private static Double ParentFilter_range = -1.0;
	private static Double ParentFilter_neutralLossMass = 18.01056; // by default, H20 mass
	private static Double LowFilter_threshold = -1.0;
	private static Double DynamicRangeFilter_min = -1.0;
	private static Double DynamicRangeFilter_max = -1.0;
	private static Double HighPass = -1.0;
	private static Double Deisotoping = -1.0;
	private static int WindowFilter_max = -1;
	private static Double WindowFilterSize = -1.0;

	public static void setConfiguration() {

		readConfigurationFile();
		initialiseProperties();

	}

	private static void initialiseProperties() {
		int i;
		boolean wrongAA = false;

		// minimum length for a peptide
		if (properties.containsKey("minimumPeptideLength")) {
			minimal_peptide_length = Integer.parseInt(properties.get("minimumPeptideLength"));
			if (minimal_peptide_length <= 0 || minimal_peptide_length > 200) {
				minimal_peptide_length = 7;
				System.out.println("minimumPeptideLength value is impossible. Set to default : 7.");
			}
		} else {
			System.out.println("minimumPeptideLength parameter is missing. Set to default : 7.");
			minimal_peptide_length = 7;
		}

		// maximum length for a peptide
		if (properties.containsKey("maximumPeptideLength")) {
			maximal_peptide_length = Integer.parseInt(properties.get("maximumPeptideLength"));
			if (maximal_peptide_length <= 0 || maximal_peptide_length > 200) {
				maximal_peptide_length = 25;
				System.out.println("maximumPeptideLength value is impossible. Set to default : 25.");
			}
		} else {
			System.out.println("maximumPeptideLength parameter is missing. Set to default : 25.");
			maximal_peptide_length = 25;
		}

		// threshold
		if (properties.containsKey("threshold")) {
			threshold = Integer.parseInt(properties.get("threshold"));
			if (threshold <= 1) {
				threshold = 8;
				System.out.println("threshold value is impossible. Set to default : 8.");
			}
		} else {
			System.out.println("threshold parameter is missing. Set to default : 8.");
			threshold = 8;
		}

		// maxMassesCount
		if (properties.containsKey("maxMassesCount")) {
			mass_filter_size = Integer.parseInt(properties.get("maxMassesCount"));
			if (mass_filter_size <= 0) {
				mass_filter_size = 50;
				System.out.println("maxMassesCount value is impossible. Set to default : 50.");
			}
		} else {
			mass_filter_size = -1;
		}

		if (properties.containsKey("modification"))
			massModification = properties.get("modification");
		else
			massModification = "57.021463721083@C";

		if (properties.containsKey("chargeFragment"))
			chargeFragment = properties.get("chargeFragment");
		else
			chargeFragment = "1+";

		// Minimum peptide charge
		if (properties.containsKey("minimumPeptideCharge")) {
			minimal_peptide_charge = Integer.parseInt(properties.get("minimumPeptideCharge"));
			if (minimal_peptide_charge <= 0) {
				minimal_peptide_charge = 1;
				System.out.println("minimumPeptideCharge value is impossible. Set to default : 1.");
			}
		} else {
			System.out.println("minimumPeptideCharge parameter is missing. Set to default : 1.");
			minimal_peptide_charge = 1;
		}

		// Maximum peptide charge
		if (properties.containsKey("maximumPeptideCharge")) {
			maximal_peptide_charge = Integer.parseInt(properties.get("maximumPeptideCharge"));
			if (maximal_peptide_charge <= 0) {
				maximal_peptide_charge = 3;
				System.out.println("maximumPeptideCharge value is impossible. Set to default : 3.");
			}
		} else {
			System.out.println("maximumPeptideCharge parameter is missing. Set to default : 3.");
			maximal_peptide_charge = 3;
		}

		// Number of decimals
		if (properties.containsKey("numberOfDecimals")) {
			accuracy_decimals = Integer.parseInt(properties.get("numberOfDecimals"));
			if (accuracy_decimals <= 0) {
				accuracy_decimals = 2;
				System.out.println("numberOfDecimals value is impossible. Set to default : 2.");

			}
		} else {
			System.out.println("numberOfDecimals parameter is missing. Set to default : 2.");
			accuracy_decimals = 2;
		}
		accuracy_factor = (int) Math.pow(10, accuracy_decimals);

		// Integer value of the accuracy
		if (properties.containsKey("decimalValue")) {
			accuracy_value = Integer.parseInt(properties.get("decimalValue"));
			if (accuracy_value <= 0) {
				accuracy_value = 2;
				System.out.println("decimalValue value is impossible. Set to default : 2.");

			}
		} else {
			System.out.println("decimalValue parameter is missing. Set to default : 2.");
			accuracy_value = 2;
		}

		accuracy = (double) accuracy_value / accuracy_factor;

		if (properties.containsKey("decoyBase")) {
			if (properties.get("decoyBase").equals("true") || properties.get("decoyBase").equals("false")) {
				decoy = Boolean.parseBoolean(properties.get("decoyBase"));
			} else {
				decoy = false;
				System.out.println("decoyBase value is impossible. Set to default : not selected.");
			}
		} else {
			System.out.println("decoyBase parameter is missing. Set to default : not selected.");
			decoy = false;
		}

		// shift
		if (properties.containsKey("shift")) {
			if (properties.get("shift").equals("true") || properties.get("shift").equals("false")) {
				shift = Boolean.parseBoolean(properties.get("shift"));
			} else {
				shift = true;
				System.out.println("shift value is impossible. Set to default : selected.");
			}
		} else {
			System.out.println("shift parameter is missing. Set to default : selected.");
			decoy = true;
		}

		// Miss-cleavage
		if (properties.containsKey("missDB")) {
			if (properties.get("missDB").equals("true") || properties.get("missDB").equals("false")) {
				missDB = Boolean.parseBoolean(properties.get("missDB"));
			} else {
				missDB = false;
				System.out.println("missDB value is impossible. Set to default : 'Dynamic'.");
			}
		} else {
			missDB = false;
			System.out.println("missDB parameter is missing. Set to default : 'Dynamic'.");
		}

		if (properties.containsKey("parentIonAccuracy")) {
			mc_tolerance = Double.parseDouble(properties.get("parentIonAccuracy"));
		} else {
			mc_tolerance = 0.02;
		}

		/*
		 * ter, aaCleaved and aaNotCleaved parameters are only useful when working with
		 * the UI, therefore when loading parameters from the console they are ignored
		 * and the regular expression only is used.
		 */
		if (Model.isUI()) {
			if (properties.containsKey("ter")) {
				ter = properties.get("ter");
				if (!ter.equals("C") && !ter.equals("N")) {
					ter = "C";
					System.out.println("ter value is impossible. Set to default : 'C'.");
				}
			} else {
				System.out.println("ter parameter is missing. Set to default : 'C'.");
				ter = "C";
			}

			if (properties.containsKey("aaCleaved")) {
				aaCleaved = properties.get("aaCleaved");
				// wrongAA is another condition to stop reading the amino acid list after the
				// first wrong AA.
				for (i = 0; (i < aaCleaved.length() && wrongAA == false); i++) {
					if (AminoAcidHandler.unknownAA((int) aaCleaved.charAt(i))) {
						aaCleaved = "KR";
						System.out.println("Unknown amino acid. Set to default : 'KR'.");
						wrongAA = true;
					}
				}
				wrongAA = false;
			} else {
				System.out.println("aaCleaved parameter is missing. Set to default : 'KR'.");
				aaCleaved = "KR";
			}

			if (properties.containsKey("aaNotCleaved")) {
				aaNotCleaved = properties.get("aaNotCleaved");
				if (aaNotCleaved.equals("<none>")) {
					aaNotCleaved = "";
				} else {
					for (i = 0; (i < aaNotCleaved.length() && wrongAA == false); i++) {
						if (AminoAcidHandler.unknownAA((int) aaNotCleaved.charAt(i))) {
							aaNotCleaved = "";
							System.out.println("Unknown amino acid. Set to default : ''.");
							wrongAA = true;
						}
					}
				}
			} else {
				System.out.println("aaNotCleaved parameter is missing. Set to default : ''.");
				aaNotCleaved = "";
			}
		}

		if (properties.containsKey("enzPattern")) {
			enzPattern = properties.get("enzPattern");
		} else {
			System.out.println("enzPattern parameter is missing. Set to default : '.*?[KR]'.");
			enzPattern = ".*?[KR]";
		}

		if (properties.containsKey("nbMissCleavage")) {
			missCleavage = Integer.parseInt(properties.get("nbMissCleavage"));
			if (missCleavage < 0 || missCleavage > 4) {
				missCleavage = 0;
				System.out.println("missCleavage value is impossible. Set to default : 0.");
			}
		} else {
			System.out.println("missCleavage parameter is missing. Set to default : 0.");
			missCleavage = 0;
		}

		// TODO : A vérifier pour l'intégration dans l'interface //

		// ** NEO4J **
		// storage of the data inside a neo4j database
		if (properties.containsKey("single_match")) {
			single_match = Boolean.parseBoolean(properties.get("single_match"));
		} else {
			single_match = true;
		}
		// ** NEO4J **

		// minimum score sed to filter results
		if (properties.containsKey("minimumScore")) {
			minimum_score = Integer.parseInt(properties.get("minimumScore"));
			if (minimum_score <= 1) {
				minimum_score = threshold;
				System.out.println("minimumScore value is impossible. Set to default : threshold value");
			}

		} else {
			System.out.println("minimumScore parameter is missing. Set to default : threshold value.");
			minimum_score = threshold;
		}

		if (properties.containsKey("searchMode")) {
			search_mode = properties.get("searchMode");
		} else {
			search_mode = "projection";
		}
		if (!(search_mode.equals("projection") || search_mode.equals("spectra") || search_mode.equals("peptides_exact")
				|| search_mode.equals("peptides_duplicated"))) {
			System.out.println(
					"The proposed search mode is incorrect. Correctedness is not ensured anymore, we suggest you fix the parameter settings and restart SpecOMS.");
		}

		if (properties.containsKey("proteins")) {
			protein_info = Boolean.parseBoolean(properties.get("proteins"));
		}

		if (properties.containsKey("minMassDelta")) {
			minimal_mass_delta = Integer.parseInt(properties.get("minMassDelta"));
		} else {
			minimal_mass_delta = -500;
		}

		if (properties.containsKey("maxMassDelta")) {
			maximal_mass_delta = Integer.parseInt(properties.get("maxMassDelta"));
		} else {
			maximal_mass_delta = 1000;
		}

		if (properties.containsKey("MC_PRIORITY")) {
			MC_PRIORITY = Integer.parseInt(properties.get("MC_PRIORITY"));
		} else {
			MC_PRIORITY = 6;
		}

		if (properties.containsKey("ST_PRIORITY")) {
			ST_PRIORITY = Integer.parseInt(properties.get("ST_PRIORITY"));
		} else {
			ST_PRIORITY = 4;
		}

		if (properties.containsKey("SHIFT_PRIORITY")) {
			SHIFT_PRIORITY = Integer.parseInt(properties.get("SHIFT_PRIORITY"));
		} else {
			SHIFT_PRIORITY = 2;
		}

		if (properties.containsKey("EXACT_DELTA_PRIORITY")) {
			EXACT_DELTA_PRIORITY = Integer.parseInt(properties.get("EXACT_DELTA_PRIORITY"));
		} else {
			EXACT_DELTA_PRIORITY = 8;
		}

		if (properties.containsKey("parentRangeFilter")) {
			ParentFilter_range = Double.parseDouble(properties.get("parentRangeFilter"));
		} else {
			ParentFilter_range = -1.0;
		}
		if (properties.containsKey("parentNeutralLossFilter")) {
			ParentFilter_neutralLossMass = Double.parseDouble(properties.get("parentNeutralLossFilter"));
		} else {
			ParentFilter_neutralLossMass = 18.01056;
		}

		if (properties.containsKey("lowFilter")) {
			LowFilter_threshold = Double.parseDouble(properties.get("lowFilter"));
		} else {
			LowFilter_threshold = -1.0;
		}

		if (properties.containsKey("dynamicRangeMinFilter")) {
			DynamicRangeFilter_min = Double.parseDouble(properties.get("dynamicRangeMinFilter"));
		} else {
			DynamicRangeFilter_min = -1.0;
		}

		if (properties.containsKey("dynamicRangeMaxFilter")) {
			DynamicRangeFilter_max = Double.parseDouble(properties.get("dynamicRangeMaxFilter"));
		} else {
			DynamicRangeFilter_max = -1.0;
		}

		if (properties.containsKey("highPassFilter")) {
			HighPass = Double.parseDouble(properties.get("highPassFilter"));
		} else {
			HighPass = -1.0;
		}

		if (properties.containsKey("deisotoping")) {
			Deisotoping = Double.parseDouble(properties.get("deisotoping"));
		} else {
			Deisotoping = -1.0;
		}

		if (properties.containsKey("windowMaxFilter")) {
			WindowFilter_max = Integer.parseInt(properties.get("windowMaxFilter"));
		} else {
			WindowFilter_max = -1;
		}

		if (properties.containsKey("windowSizeFilter")) {
			WindowFilterSize = Double.parseDouble(properties.get("windowSizeFilter"));
		} else {
			WindowFilterSize = -1.0;
		}
	}

	public static void massModificationOnAA() {

		String[] listMod = massModification.split(";");
		if (listMod.length == 0)
			return;
		for (int m = 0; m < listMod.length; m++) {
			int prefix = listMod[m].indexOf('@');
			if (prefix == -1)
				System.out.println(
						"one of the modification has not the correct pattern. Default modification on C will be applied");

			double mass = 0.0;
			try {
				mass = Double.parseDouble(listMod[m].substring(0, prefix - 1));
			} catch (NumberFormatException e) {
				System.out.println("one of the modification has not the correct pattern in the parameter file");
			}
			String[] aa = listMod[m].substring(prefix + 1).split(",");
			for (int j = 0; j < aa.length; j++) {
				AminoAcidHandler.modifMass(aa[j], mass);
				AminoAcidHandler.modifMass((int) (aa[j].charAt(0)), mass);
			}
		}
	}

	private static void readConfigurationFile() {

		properties = new Object2ObjectOpenHashMap<>();

		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		String[] data;

		try {
			inputFile = new FileInputStream(CONFIGURATION_FILE);
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);
			String line;

			while ((line = inputBuffer.readLine()) != null) {
				if (line.isEmpty()) {
				} else {
					data = line.split("=");
					// System.out.println(line);
					if (data.length == 2) {
						properties.put(data[0], data[1]);
					} else {
						System.out.println(
								"One of the parameters is not properly formatted. Parameters are written as such, one per line :\nkey=value");
						System.out.println("Correct functioning of the program is not ensured.");
					}
				}
			}
			inputBuffer.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

	}

	// TODO : à refaire plus proprement en utilisant une hashMap
	public static void setInputOutput() {

		InputStream inputFile;
		InputStreamReader inputStream;
		BufferedReader inputBuffer = null;

		try {
			inputFile = new FileInputStream(INOUT_FILES);
			inputStream = new InputStreamReader(inputFile);
			inputBuffer = new BufferedReader(inputStream);
			String line;

			// while((line = inputBuffer.readLine()) != null){
			line = inputBuffer.readLine();
			database_file = line;

			line = inputBuffer.readLine();
			contaminants_file = line;

			line = inputBuffer.readLine();
			experimental_data = line;

			line = inputBuffer.readLine();
			results_file = line;
			Param.protein_reference_file = line.substring(0, line.length() - 4) + "_protein_reference_table.txt";

			// }
			inputBuffer.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
	}

	// TODO : need to securise the getters for the properties
	public static int accuracyFactor() {
		return accuracy_factor;
	}

	public static String resultsFile() {
		return results_file;
	}

	public static int maxPeptideLength() {
		return maximal_peptide_length;
	}

	public static String contaminants() {
		return contaminants_file;
	}

	public static String database() {
		return database_file;
	}

	public static int minPeptideLength() {
		return minimal_peptide_length;
	}

	public static String experimentalDataset() {
		return experimental_data;
	}

	public static int minimalCharge() {
		return minimal_peptide_charge;
	}

	public static int maximalCharge() {
		return maximal_peptide_charge;
	}

	public static int threshold() {
		return threshold;
	}

	public static int massFilterSize() {
		return mass_filter_size;
	}

	public static double accuracy() {
		return accuracy;
	}

	public static int accuracyDec() {
		return accuracy_decimals;
	}

	public static int accuracyValue() {
		return accuracy_value;
	}

	public static boolean decoy() {
		return decoy;
	}

	public static boolean uniprotFormat() {
		return uniprotFormat;
	}

	public static double tolerance() {
		return mc_tolerance;
	}

	public static boolean shift() {
		return shift;
	}

	public static String enzPattern() {
		return enzPattern;
	}

	public static Boolean ter() {
		if (ter.equals("N"))
			return true;
		else
			return false;
	}

	public static int MC_PRIORITY() {
		return MC_PRIORITY;
	}

	public static int ST_PRIORITY() {
		return ST_PRIORITY;
	}

	public static int SHIFT_PRIORITY() {
		return SHIFT_PRIORITY;
	}

	public static int EXACT_MASS_DELTA_PRIORITY() {
		return MC_PRIORITY;
	}

	public static String aaCleaved() {
		return aaCleaved;
	}

	public static String aaNotCleaved() {
		return aaNotCleaved;
	}

	public static String missCleavage() {
		return String.valueOf(missCleavage);
	}

	public static boolean missDB() {
		return missDB;
	};

	public static boolean singleMatch() {
		return single_match;
	}

	public static int minimumScore() {
		return minimum_score;
	}

	public static int minMassDelta() {
		return minimal_mass_delta;
	}

	public static int maxMassDelta() {
		return maximal_mass_delta;
	}

	public static Double ParentFilter_range() {
		return ParentFilter_range;
	}

	public static Double LowFilter_threshold() {
		return LowFilter_threshold;
	}

	public static Double DynamicRangeFilter_min() {
		return DynamicRangeFilter_min;
	}

	public static Double DynamicRangeFilter_max() {
		return DynamicRangeFilter_max;
	}

	public static Double HighPass() {
		return HighPass;
	}

	public static Double Deisotoping() {
		return Deisotoping;
	}

	public static int WindowFilter_max() {
		return WindowFilter_max;
	}

	public static Double WindowFilterSize() {
		return WindowFilterSize;
	}

	/**
	 * @brief this create all the filters demanded by the user
	 *
	 * @param ParentFilter_mass
	 */
	public static FilterWrapper filters(Double ParentFilter_mass) {
		// create a new FilterWraper to tore each filters in an ordered order :
		FilterWrapper FW = new FilterWrapper();

		// first always delete the parent ion, except if you know it is not present (no
		// ParentFilter_range)
		if (ParentFilter_range != -1) {
			FW.addFilter(new ParentFilter(ParentFilter_range, ParentFilter_mass, ParentFilter_neutralLossMass));
		}

		if (LowFilter_threshold != -1) {
			FW.addFilter(new LowFilter(LowFilter_threshold));
		}

		// then delete the redundant peaks
		FW.addFilter(new AccuracyValuePeakDiscriminationFilter(accuracy));

		// then all of the possible filters (order to discriminate ?)
		if (DynamicRangeFilter_min != -1) {
			FW.addFilter(new DynamicRangeFilter(DynamicRangeFilter_max, DynamicRangeFilter_min));
		}

		if (HighPass != -1) {
			FW.addFilter(new HighPass(HighPass));
		}

		if (Deisotoping != -1) {
			FW.addFilter(new Deisotoping(Deisotoping));
		}

		if (WindowFilter_max != -1) {
			FW.addFilter(new WindowFilter(WindowFilter_max, WindowFilterSize));
		}

		if (mass_filter_size != -1) {
			FW.addFilter(new UpFilter(mass_filter_size));
		}

		return FW;

	}

	/*
	 * Setters Used after checking the validity of every parameters to retrieve
	 * values from every text field and every UI control. Note that integers are
	 * written in formal parameters because the casting is done while calling the
	 * method. Not the best idea maybe.
	 */
	public static void setExpData(String path) {
		experimental_data = path;
	};

	public static void setCont(String path) {
		contaminants_file = path;
	};

	public static void setDatab(String path) {
		database_file = path;
	};

	public static void setUniprotFormat(boolean format) {
		uniprotFormat = format;
	};

	public static void setOutput(String path) {
		results_file = path;
	};

	public static void setAcc(double newDble) {
		accuracy = newDble;
	}

	public static void setAccuracyValue(int newVal) {
		accuracy_value = newVal;
	};

	public static void setAccFac(int newVal) {
		accuracy_decimals = newVal;
		accuracy_factor = (int) Math.pow(10, accuracy_decimals);
	};

	public static void setMaxMassCount(int newMMC) {
		mass_filter_size = newMMC;
	};

	public static void setMinPep(int newMinPep) {
		minimal_peptide_length = newMinPep;
	};

	public static void setMaxPep(int newMaxPep) {
		maximal_peptide_length = newMaxPep;
	};

	public static void setThresh(int newThresh) {
		threshold = newThresh;
	};

	public static void setScore(int newScore) {
		minimum_score = newScore;
	};

	public static void setMinCharg(int newMinCharg) {
		minimal_peptide_charge = newMinCharg;
	};

	public static void setMaxCharg(int newMaxCharg) {
		maximal_peptide_charge = newMaxCharg;
	};

	public static void setDecoy(boolean newDecoy) {
		decoy = newDecoy;
	};

	public static void setShift(boolean newShift) {
		shift = newShift;
	};

	public static void setEnzPattern(String newPattern) {
		enzPattern = newPattern;
	};

	public static void setTer(String newTer) {
		ter = newTer;
	};

	public static void setCleaved(String newCleave) {
		aaCleaved = newCleave;
	};

	public static void setNotCleaved(String newNoCleave) {
		aaNotCleaved = newNoCleave;
	};

	public static void setMissCleavage(String newNb) {
		missCleavage = Integer.parseInt(newNb);
	};

	public static void setMissDB(boolean newBool) {
		missDB = newBool;
	};

	public static void SetConfigFile(String newPath) {
		CONFIGURATION_FILE = newPath;
	};

	public static String search_mode() {
		return search_mode;
	}

	public static boolean protein_information() {
		return protein_info;
	}

	public static String database_file() {
		return Param.database_file;
	}

	public static String contam_file() {
		return Param.contaminants_file;
	}

	public static String exp_file() {
		return Param.experimental_data;
	}

	public static String results_file() {
		return Param.results_file;
	}

	public static int highestMassValue() {
		return Param.highest_mass_value;
	}

	public static void defineHighestMassValue(int mass) {
		Param.highest_mass_value = mass;
	}

	public static void setProteinsFileOutput() {
		Param.protein_reference_file = results_file.substring(0, results_file.length() - 4)
				+ "_protein_reference_table.txt";
	}

}
