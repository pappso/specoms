/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import proteo.Param;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.sun.org.apache.xalan.internal.xsltc.runtime.Parameter;

/***
 * Implementation of a memory efficient spectrum representation. intensity is
 * ignored and mass is scaled using an scale factor. Accuracy is taken in
 * account using discretisation and duplication. Note: This class uses a static
 * defined counter, it must be reset to use with multiple peptide collections.
 * Note: This implementation is unreliable for concurrent use. Note: This
 * representation both fits theoretical and experimental spectra. Might be
 * simplified through an interface.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class Spectrum {
	// private static final Logger LOGGER =
	// Logger.getLogger(Spectrum.class.getName());
	/**
	 * Static instanciation counter used to attribute an identifier to a spectrum.
	 * Note: This identifier is used to refer to the spectrum through SpecTrees.
	 * Note: Concurrent use averse.
	 * 
	 * @since 0.1
	 */
	private static int idCount = 0;

	/**
	 * unique spectrum identifier. Note: Identifies a spectrum in SpecTrees.
	 * 
	 * @since 0.1
	 */
	private int identifier;

	/**
	 * Litteral identifier for a nexperimental spectrum, amino-acid sequence for
	 * theoretical spectrum.
	 * 
	 * @since 0.1
	 */
	private String name;

	/**
	 * Number of charges of the spectrum. Note: usually onsidered as 1 for the
	 * theoretical spectra.
	 * 
	 * @since 0.1
	 */
	private int charge;

	/**
	 * Primitive integer array based representation of the masses in the spectrum.
	 * 
	 * @since 0.1
	 */
	private int[] asList;

	/**
	 * HashSet representation of the masses in the spectrum. Note: built on the fly
	 * if needed.
	 */
	private IntOpenHashSet asHashSet;

	/**
	 * HashSet instantiation witness. Note: False if the spectrum hashSet is not
	 * instanciated, true otherwise. Efficiency purposes.
	 * 
	 * @since 0.1
	 */
	boolean isHashed;

	/**
	 * Reference on the peptide at the origin of this spectrum. Note: use to provide
	 * information to the user on results writing. Note: Clumsy implementation.
	 * 
	 * @since 0.1
	 */
	Peptide pep;

	/**
	 * Total mass associated to the spectrum under an accurate format.
	 * 
	 * @since 0.1
	 */
	private double totalMass;

	/**
	 * Protected constructor to avoid wrong instanciation of the class but allow
	 * inheritance.
	 * 
	 * @since 0.1
	 */
	protected Spectrum() {
	}

	/**
	 * Provides access to the experimental spectrum name or the theoretical spectrum
	 * amino-acid sequence.
	 * 
	 * @return spectrum experimental name or theoretical spectrum sequence
	 * @since 0.1
	 */
	public String name() {
		return name;
	}

	/**
	 * Assign the name to a spectrum.
	 * 
	 * @param n the name to be assigned to the spectrum
	 * @since 0.1
	 */
	public void setName(String n) {
		name = n;
	}

	/**
	 * Provides access to the spectrum accurate total mass.
	 * 
	 * @return total mass
	 * @since 0.1
	 */
	public double totalMass() {
		return this.totalMass;
	}

	/**
	 * Provides access to the spectrum charge.
	 * 
	 * @return charge since 0.1
	 */
	public int charge() {
		return this.charge;
	}

	/**
	 * Build a theoretical spectrum from a peptide. This spectrum model is built
	 * considering the peptide was charged once by default. But if the parameter
	 * chargeFragment=2+ (April 2021) double charged fragments are also modelized.
	 * This spectrum model uses a scaled integer masses representation. The final
	 * spectrum exhibits an array sorted by increasing mass values with no
	 * dinstinction between B and Y ions. Note: method signature can be simplified
	 * 
	 * @param p     the peptide from which the spectrum has to be built
	 * @param chain the amino-acid sequence to use as a name
	 * @return a spectrum corresponding to the fed in peptide
	 * @since 0.1
	 */
	public static Spectrum createFromPeptide(Peptide p, String chain) {
		// LOGGER.log(Level.INFO, p.asString());
		// Instanciation and variables copy
		Spectrum newSpectrum = new Spectrum();
		newSpectrum.pep = p;
		int[] aa = p.asList();
		newSpectrum.name = chain;

		// Generation of the new unique identifier and static identifier tracker update
		newSpectrum.identifier = idCount;
		++idCount;

		// allocation of the space required to store the masses
		int length = aa.length;
		if (Param.getChargeFragment().equals("2+"))
			newSpectrum.asList = new int[4 * length];
		else
			newSpectrum.asList = new int[2 * length];

		// mass values are accurately computed using a aggregation on doubles and being
		// rounded and casted whenever needed from those sums
		double bSum = AminoAcidHandler.getMass("H+"); // N-term
		double ySum = AminoAcidHandler.getMass("CT") + AminoAcidHandler.getMass("NT") + AminoAcidHandler.getMass("H+"); // C-term

		// temporary data handlers for conversion
		int bValue;
		int yValue;
		double tempValue;
		double bSum2, ySum2;

		// iteration on all amino-acids to perform iterative sum and conversions for
		// each amino-acid added on both B and Y side
		for (int cpt = 0; cpt < length; ++cpt) {

			tempValue = AminoAcidHandler.getMass(aa[cpt]);
			bSum += tempValue;
			bValue = (int) (Math.round(bSum * Param.accuracyFactor()));
			newSpectrum.asList[cpt] = bValue;
			if (Param.getChargeFragment().equals("2+")) {
				bSum2 = (bSum - AminoAcidHandler.getMass("H+")) / 2 + AminoAcidHandler.getMass("H+");
				bValue = (int) (Math.round(bSum2 * Param.accuracyFactor()));
				newSpectrum.asList[cpt + 2 * length] = bValue;
			}

			tempValue = AminoAcidHandler.getMass(aa[length - cpt - 1]);
			ySum += tempValue;
			yValue = (int) (Math.round(ySum * Param.accuracyFactor()));
			newSpectrum.asList[cpt + length] = yValue; // *2
			if (Param.getChargeFragment().equals("2+")) {
				ySum2 = (ySum - AminoAcidHandler.getMass("H+")) / 2 + AminoAcidHandler.getMass("H+");
				yValue = (int) (Math.round(ySum2 * Param.accuracyFactor()));
				newSpectrum.asList[cpt + 3 * length] = yValue;
			}

		}

		// adjustement of the sum for the total mass
		newSpectrum.totalMass = bSum + AminoAcidHandler.getMass("CT") + AminoAcidHandler.getMass("NT");
		// mass array sorting
		Arrays.sort(newSpectrum.asList);

		// filtering out the potentially duplicated masses
		IntArrayList filter = new IntArrayList(newSpectrum.asList.length);
		int previousValue = newSpectrum.asList[0];
		filter.add(previousValue);
		for (int cpt = 1; cpt < newSpectrum.asList.length; ++cpt) {
			if (previousValue == newSpectrum.asList[cpt]) {
			} else {
				previousValue = newSpectrum.asList[cpt];
				filter.add(previousValue);
			}
		}
		// copy of the arrayList into a primitive integer array
		newSpectrum.asList = new int[filter.size()];
		filter.toIntArray(newSpectrum.asList);
		newSpectrum.isHashed = false;

		// LOGGER.log(Level.INFO, Arrays.toString(newSpectrum.asList));
		return newSpectrum;
	}

	/**
	 * Creates an experimental spectrum from a data read. This spectrum model uses a
	 * scaled integer masses representation. The final spectrum exhibits an array
	 * sorted by increasing mass values with no dinstinction between B and Y ions.
	 * The input data was filtered to keep only a given number of the most intense
	 * peaks with no two peaks into a given exclusion window.
	 * 
	 * @param pics     the list of masses fed in input
	 * @param expID    the experimental spectrum identifier
	 * @param accuracy the accuracy used for duplication in the discretisation
	 *                 process
	 * @param mass     the total mass of the precursor
	 * @param charge   the charge associated with the spectrum
	 * @return an experimental spectrum
	 * @since 0.1
	 */
	public static Spectrum createFromIntArray(int[] pics, String expID, int accuracy, double mass, int charge) {

		// LOGGER.log(Level.INFO, expID);
		// LOGGER.log(Level.INFO, Arrays.toString(pics));
		// Instanciation and variables copy
		Spectrum s = new Spectrum();
		s.name = expID;
		s.charge = charge;
		// unique idenfitier allocation and static identifier tracker update
		s.identifier = idCount;
		++idCount;
		// total mass calculation from the precursor mass and the charge
		s.totalMass = (mass * charge) - ((charge - 1) * AminoAcidHandler.getMass("H+"));
		// Computation of the required space for masses storage and allocation of the
		// structure
		int picCount = (2 * accuracy + 1) * pics.length;
		IntOpenHashSet pSet = new IntOpenHashSet(picCount);
		// duplication of the masses according to the accuracy and structure filling
		for (int cpt0 = 0; cpt0 < pics.length; ++cpt0) {
			pSet.add(pics[cpt0]);
			for (int cpt1 = 0; cpt1 < accuracy; ++cpt1) {
				pSet.add(pics[cpt0] + cpt1 + 1);
				pSet.add(pics[cpt0] - cpt1 - 1);
			}
		}
		// conversion into a primitive integer array and sorting
		s.asList = new int[pSet.size()];
		pSet.toIntArray(s.asList);
		Arrays.sort(s.asList);
		s.isHashed = false;
		// LOGGER.log(Level.INFO, Arrays.toString(s.asList));
		return s;
	}

	public static Spectrum createFromIntArray(int[] pics, String expID, double mass, int charge) {
		// Instanciation and variables copy
		Spectrum s = new Spectrum();
		s.name = expID;
		s.charge = charge;
		// unique idenfitier allocation and static identifier tracker update
		s.identifier = idCount;
		++idCount;
		// total mass calculation from the precursor mass and the charge
		s.totalMass = mass * charge - (charge - 1) * AminoAcidHandler.getMass("H+");
		// Computation of the required space for masses storage and allocation of the
		// structure
		int picCount = pics.length;
		IntOpenHashSet pSet = new IntOpenHashSet(picCount);
		// duplication of the masses according to the accuracy and structure filling

		for (int cpt0 = 0; cpt0 < pics.length; ++cpt0) {
			pSet.add(pics[cpt0]);
			/*
			 * for(int cpt1 = 0; cpt1 < accuracy; ++cpt1){ pSet.add(pics[cpt0] + cpt1 + 1);
			 * pSet.add(pics[cpt0] - cpt1 - 1); }
			 */
		}
		// conversion into a primitive integer array and sorting
		s.asList = new int[pSet.size()];
		pSet.toIntArray(s.asList);
		Arrays.sort(s.asList);
		s.isHashed = false;
		return s;
	}

	/**
	 * Provides access to the list of masses under the primitive integer array
	 * representation.
	 * 
	 * @return the integer array containing the masses
	 * @since 0.1
	 */
	public int[] asList() {
		return asList;
	}

	/**
	 * Provides access to the unique spectrum identifier
	 * 
	 * @return spectrum identifier
	 * @since 0.1
	 */
	public int id() {
		return identifier;
	}

	/**
	 * Implementation of the toString method to return the spectrum name. The
	 * returned name for an exeperimental spectrum is its exeprimental identfier.
	 * The returned name for a theoretical spectrum is the amino-acid sequence from
	 * which the spectrum was created.
	 * 
	 * @return spectrum name
	 * @since 0.1
	 */
	@Override
	public String toString() {
		return this.name;
	}

	/**
	 * Resets the static spectra count. Note: to be used between two executions of
	 * SpecTrees on different datasets. Note: this has to be changed for another
	 * more convenient to use implementation.
	 * 
	 * @since 0.1
	 */
	public static void reset() {
		idCount = 0;
	}

	/**
	 * Provides access to the HashSet representation of the spectrum. Note: This
	 * hashSet must be built first. Note: this should maybe be ensured there...
	 * 
	 * @return the hashSet containing the masses
	 * @since 0.1
	 */
	public IntOpenHashSet asHashSet() {
		return asHashSet;
	}

	/**
	 * Computes and store the hashSet representation of the spectrum from its array
	 * representation if needed and not already done. This hashSet representation is
	 * used into the SpectrumShifter while searching for a better alignment.
	 * 
	 * @since 0.1
	 */
	public void hashSpectrum() {
		isHashed = true;
		asHashSet = new IntOpenHashSet(asList.length, 1);
		for (int cpt = 0; cpt < asList.length; cpt++) {
			asHashSet.add(asList[cpt]);
		}
	}

	/**
	 * Provides access to the peptide referenced in the spectrum
	 * 
	 * @return peptide
	 * @since 0.1
	 */
	public Peptide peptide() {
		return this.pep;
	}

	public Spectrum copy() {

		Spectrum newSpectrum = new Spectrum();
		newSpectrum.pep = this.pep;
		int[] aa = this.pep.asList();
		newSpectrum.name = this.name;

		newSpectrum.totalMass = this.totalMass;

		newSpectrum.identifier = idCount;
		++idCount;

		// allocation of the space required to store the masses
		int length = aa.length;
		newSpectrum.asList = new int[2 * length];

		for (int cpt = 0; cpt < this.asList.length; ++cpt) {
			newSpectrum.asList[cpt] = this.asList[cpt];
		}
		newSpectrum.isHashed = false;

		return newSpectrum;
	}

	public Spectrum copy_with_duplication() {

		Spectrum newSpectrum = new Spectrum();
		newSpectrum.pep = this.pep;
		int[] aa = this.pep.asList();
		newSpectrum.name = this.name;

		newSpectrum.totalMass = this.totalMass;

		newSpectrum.identifier = idCount;
		++idCount;

		// allocation of the space required to store the masses
		int length = aa.length;

		// newSpectrum.asList = new int[2*length];
		// size properly afterwards

		int picCount = (2 * Param.accuracyDec() + 1) * length;
		IntOpenHashSet pSet = new IntOpenHashSet(picCount);

		for (int cpt = 0; cpt < this.asList.length; ++cpt) {
			pSet.add(this.asList[cpt]);
			for (int cpt1 = 0; cpt1 < Param.accuracyDec(); ++cpt1) {
				pSet.add(this.asList[cpt] + cpt1);
				pSet.add(this.asList[cpt] - cpt1);
			}

		}
		newSpectrum.asList = new int[pSet.size()];
		pSet.toIntArray(newSpectrum.asList);
		Arrays.sort(newSpectrum.asList);
		newSpectrum.isHashed = false;

		return newSpectrum;

	}

}
