/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, UniversitÃ© de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

import java.util.ArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import proteo.AffixHandler;
import proteo.Param;
import proteo.Proteins;

/***
 * Minimalist protein implementation. Reads proteins from fasta files and digest
 * them into peptides.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class Protein {

	/**
	 * Static instanciation counter used to attribute an identifier to a peptide
	 * produced from the protein. Note: Concurrent use averse. Note: There is some
	 * redundancy there, should be usable joint to the peptide identifier from the
	 * peptide class.
	 * 
	 * @since 0.1
	 */
	private static int peptideIdentifier = 0;

	private int type = -2; // 0 decoy, 1 target, -1 contaminant, -2 non intialisé

	public static Protein createDecoy(Protein source_prot) {
		Protein newProt = new Protein();
		newProt.identifier = idCount;
		++idCount;

		newProt.type = 0;

		String ref = "decoy_" + Proteins.get_protein_reference(source_prot.identifier());
		Proteins.add_protein_reference(Protein.protein_current_identifier(), ref);

		newProt.aminoAcidChain = decoySequence(source_prot.aminoAcidChain);

		return newProt;
	}

	/*
	 * private static StringBuilder decoySequence(StringBuilder aminoAcidChain) {
	 * StringBuilder new_sequence = new StringBuilder(); StringBuilder temp_sequence
	 * = new StringBuilder();
	 * 
	 * System.out.println("PROTEIN :: " + aminoAcidChain);
	 * 
	 * for(int cpt = 0; cpt < aminoAcidChain.length(); ++cpt){
	 * 
	 * if( (aminoAcidChain.charAt(cpt))=='R' || (aminoAcidChain.charAt(cpt))=='K') {
	 * temp_sequence.reverse(); new_sequence.append(temp_sequence);
	 * new_sequence.append(aminoAcidChain.charAt(cpt)); temp_sequence.setLength(0);
	 * } else { if( aminoAcidChain.charAt(cpt) == 'L' ){ temp_sequence.append('I');
	 * } else { temp_sequence.append(aminoAcidChain.charAt(cpt)); } } }
	 * temp_sequence.reverse(); new_sequence.append(temp_sequence);
	 * 
	 * System.out.println("DECOY :: " + new_sequence);
	 * 
	 * return new_sequence; }
	 */

	private static StringBuilder decoySequence(StringBuilder aminoAcidChain) {
		StringBuilder new_sequence = new StringBuilder();
		for (int cpt = 0; cpt < aminoAcidChain.length(); ++cpt) {
			new_sequence.append(aminoAcidChain.charAt(cpt));
		}
		return new_sequence.reverse();
	}

	/**
	 * Primitive integer array representtion of an amino-acid sequence.
	 * 
	 * @since 0.1
	 */
//    private int[] aminoAcidChain;
	private StringBuilder aminoAcidChain;

	/**
	 * Unique protein identifier. Note: Unused in the current version.
	 * 
	 * @since 0.1
	 */
	private int identifier;

	public int identifier() {
		return this.identifier;
	}

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	private Protein() {
	}

	/**
	 * Static instanciation counter used to attribute an identfier to a protein.
	 * Note: Concurrent use averse.
	 * 
	 * @since 0.1
	 */
	private static int idCount = 0;

	/**
	 * Protein creation from an existing string of amino-acids. Note: During the
	 * process, Isoleucin replaces Leucin. Destructive.
	 * 
	 * @param p the string of amino-acids
	 * @return the created protein
	 */
	public static Protein create(String p, int type) {

		// Instantiation and initialisation
		Protein newProt = new Protein();
		newProt.identifier = idCount;
		newProt.type = type;
		++idCount;

		newProt.aminoAcidChain = new StringBuilder(p);

		// leucin to isoleucin conversion
		// Note: That might not be clever at this step

		for (int cpt = 0; cpt < newProt.aminoAcidChain.length(); ++cpt) {
			if (newProt.aminoAcidChain.charAt(cpt) == 'L') {
				newProt.aminoAcidChain.setCharAt(cpt, 'I');
			}
		}
		return newProt;
	}

	/**
	 * Digestion of a protein into peptidic sequences with respect to an enzyme
	 * behavior. Peptides not matching a minimal/maximal size fork are ignored.
	 * During this process, a table of peptide relations is filled. The bind table
	 * is concurrent use averse.
	 * 
	 * @param min   minimum allowed peptide length
	 * @param max   maximum allowed peptide length
	 * @param binds peptide relation table
	 * @return peptide sequence arraylist
	 */
	public ArrayList<int[]> digest(int min, int max, IntArrayList binds) {

		// datastructure instanciation and digestion with trypsin
		ArrayList<int[]> filteredPeptidesList = new ArrayList<int[]>();
		ArrayList<int[]> peptidesList = Cleaver.digest(aminoAcidChain);

		// to ensure a clean table is rebuilt if the program uses multiple datasets in a
		// row.
		binds.clear();

		// peptides filtering and copy along with bind table filling
		for (int cpt = 0; cpt < peptidesList.size(); ++cpt) {
			if ((peptidesList.get(cpt).length <= max) && (peptidesList.get(cpt).length >= min)) {
				++peptideIdentifier;
				filteredPeptidesList.add(peptidesList.get(cpt));
				binds.add(peptideIdentifier);

				if ((!Param.missCleavage().equals("0")) && (!Param.missDB())) {
					// connection to a peptide prefix/suffix if relevant
					if (cpt != 0) {
						AffixHandler.prefix.put(peptideIdentifier, peptidesList.get(cpt - 1));
					}
					if (cpt != peptidesList.size() - 1) {
						AffixHandler.suffix.put(peptideIdentifier, peptidesList.get(cpt + 1));
					}

					/*
					 * If two missed cleavage are allowed, the direct neighbours (as done above), as
					 * well as the two direct neighbours must be added to the table.
					 */
					if (Param.missCleavage().equals("2")) {
						int[] twoPep;

						if (cpt > 1) {
							twoPep = new int[peptidesList.get(cpt - 2).length + peptidesList.get(cpt - 1).length];
							System.arraycopy(peptidesList.get(cpt - 2), 0, twoPep, 0, peptidesList.get(cpt - 2).length);
							System.arraycopy(peptidesList.get(cpt - 1), 0, twoPep, peptidesList.get(cpt - 2).length,
									peptidesList.get(cpt - 1).length);
							AffixHandler.prefix.put(peptideIdentifier, twoPep);
						}

						if (cpt < peptidesList.size() - 2) {
							twoPep = new int[peptidesList.get(cpt + 1).length + peptidesList.get(cpt + 2).length];
							System.arraycopy(peptidesList.get(cpt + 1), 0, twoPep, 0, peptidesList.get(cpt + 1).length);
							System.arraycopy(peptidesList.get(cpt + 2), 0, twoPep, peptidesList.get(cpt + 1).length,
									peptidesList.get(cpt + 2).length);
							AffixHandler.suffix.put(peptideIdentifier, twoPep);
						}
					}

				}
			}
		}
		return filteredPeptidesList;
	}

	/**
	 * Resets the static protein count. Note: to be used between two executions of
	 * SpecTrees on different datasets. Note: this has to be changed for another
	 * more convenient to use implementation.
	 * 
	 * @since 0.1
	 */
	public static void reset() {
		idCount = 0;
		peptideIdentifier = 0;
	}

	public static int protein_current_identifier() {
		return Protein.idCount;
	}

	public int type() {
		return this.type;
	}

	@Override
	public String toString() {
		return this.aminoAcidChain.toString();
	}

}
