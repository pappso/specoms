/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import proteo.Param;
import it.unimi.dsi.fastutil.ints.IntArrayList;

/***
 * Representation of a cleavage enzyme and reproduction of its behavior. Until
 * now, this implementation only supports Trypsin, but is designed to be easily
 * modified to add other enzymes. Note: ideally, enzymes behabviors should be
 * read from files and scriptables.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class Cleaver {

	/**
	 * Reproduction of the digestion procedure carried by Trypsin.
	 * 
	 * @param aminoAcidChain The amino acids chain to digest
	 * @return the list of peptides resulting from the digestion
	 * @since 0.1
	 */

	// Pattern depends of the user choice.
	private static Pattern pattern;
	// Allowed number of missed cleavage
	private static int miss;

	public static ArrayList<int[]> digest(StringBuilder aminoAcidChain) {

//    	System.out.println(pattern);
//    	System.out.println(miss);

		miss = Integer.parseInt(Param.missCleavage());
		pattern = Pattern.compile(Param.enzPattern());

		// structure allocation for peptides storage
		ArrayList<int[]> peptides = new ArrayList<int[]>();

		// Variables needed to convert the string into an array of integers.
		int[] intChain;
		IntArrayList tempAA;

		// List of all the peptides created for the protein, needed to to create the
		// missed cleavage.
		ArrayList<String> allPeptides = new ArrayList<String>();
		// Recreated peptides due to a missed cleavage
		String missPep;
		// Position of the current peptide in the list
		int count = 0;
		// Loop
		int i;

		// Length of every digested peptide in order to retrieve the last peptide
		int totalLength = 0;
		// Length of one peptide
		int length = 0;

		/*
		 * Iteration over the amino acids sequence and splitting at each detected
		 * amino-acid for which the enzyme should cut and not followed by one of the
		 * exception. Subsequences included between two cuts are copied to int arrays
		 * using their code and stored into the peptide collection to return.
		 */
		Matcher m = pattern.matcher(aminoAcidChain.toString());
		while (m.find()) {
			String chain = m.group(0);
			length = chain.length();
			totalLength += length;

//        		System.out.println("cut: " + chain);

			allPeptides.add(count, m.group(0));
			count++;

			tempAA = new IntArrayList(length);
			for (int cpt = 0; cpt < length; ++cpt) {
				tempAA.add(chain.charAt(cpt));
			}
			intChain = new int[tempAA.size()];
			tempAA.toIntArray(intChain);

			peptides.add(intChain);
		}

		/*
		 * Checks whether the digestion leaves a N or C-ter extremity. If it does, the
		 * remaining peptide is added to the collection.
		 */
		if (totalLength != aminoAcidChain.length()) {
			String finalPep = "";
			if (Param.ter()) {
				finalPep = aminoAcidChain.substring(0, aminoAcidChain.length() - totalLength);
				allPeptides.add(0, finalPep);
			} else {
				finalPep = aminoAcidChain.substring(totalLength, aminoAcidChain.length());
				allPeptides.add(count, finalPep);
			}
//        		System.out.println("final: " + finalPep);

			length = finalPep.length();
			tempAA = new IntArrayList(length);
			for (int cpt = 0; cpt < length; ++cpt) {
				tempAA.add(finalPep.charAt(cpt));
			}
			intChain = new int[tempAA.size()];
			tempAA.toIntArray(intChain);

			peptides.add(intChain);
		}

		/*
		 * Add peptides which are created through one or two missed cleavage. Two miss
		 * cleavage can be consecutive or not, in which case it does not add new
		 * peptides compared to the single miss cleavage.
		 */
		if ((miss > 0) && (Param.missDB())) {

			for (i = 0; i < allPeptides.size() - 1; i++) {
				missPep = allPeptides.get(i) + allPeptides.get(i + 1);

//        			System.out.println("1 missed: " + missPep);

				length = missPep.length();
				tempAA = new IntArrayList(length);
				for (int cpt = 0; cpt < length; ++cpt) {
					tempAA.add(missPep.charAt(cpt));
				}
				intChain = new int[tempAA.size()];
				tempAA.toIntArray(intChain);

				peptides.add(intChain);
			}

			if (miss > 1) {

				for (i = 0; i < allPeptides.size() - 2; i++) {
					missPep = allPeptides.get(i) + allPeptides.get(i + 1) + allPeptides.get(i + 2);

//        				System.out.println("2 missed: " + missPep);

					length = missPep.length();
					tempAA = new IntArrayList(length);
					for (int cpt = 0; cpt < length; ++cpt) {
						tempAA.add(missPep.charAt(cpt));
					}
					intChain = new int[tempAA.size()];
					tempAA.toIntArray(intChain);

					peptides.add(intChain);
				}

			}

			if (miss > 2) {

				for (i = 0; i < allPeptides.size() - 3; i++) {
					missPep = allPeptides.get(i) + allPeptides.get(i + 1) + allPeptides.get(i + 2)
							+ allPeptides.get(i + 3);

//        				System.out.println("2 missed: " + missPep);

					length = missPep.length();
					tempAA = new IntArrayList(length);
					for (int cpt = 0; cpt < length; ++cpt) {
						tempAA.add(missPep.charAt(cpt));
					}
					intChain = new int[tempAA.size()];
					tempAA.toIntArray(intChain);

					peptides.add(intChain);
				}

			}

			if (miss > 3) {

				for (i = 0; i < allPeptides.size() - 4; i++) {
					missPep = allPeptides.get(i) + allPeptides.get(i + 1) + allPeptides.get(i + 2)
							+ allPeptides.get(i + 3) + allPeptides.get(i + 4);

//        				System.out.println("2 missed: " + missPep);

					length = missPep.length();
					tempAA = new IntArrayList(length);
					for (int cpt = 0; cpt < length; ++cpt) {
						tempAA.add(missPep.charAt(cpt));
					}
					intChain = new int[tempAA.size()];
					tempAA.toIntArray(intChain);

					peptides.add(intChain);
				}

			}
		}

		return peptides;
	}

}
