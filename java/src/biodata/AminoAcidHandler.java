/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/***
 * Handles amin-acid related biological data. Some of those data should probably
 * later be stored and read fro ma configuration file to ensure better
 * adaptability.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class AminoAcidHandler {

	/**
	 * Map representation of String to double amino-acids masses. Note: should be
	 * externalised. Note: Cystein carries a +57Da systematic modification. Note:
	 * some exotic amino-acids String entries are represented with a 0 mass, this is
	 * not a long term satisfying solution.
	 * 
	 * @since 0.1
	 */
	private static Map<String, Double> massTable = new HashMap<String, Double>();
	static {
		massTable.put("G", 57.021463721083); // Glycine
		massTable.put("A", 71.037113785565); // Alanine
		massTable.put("S", 87.032028405125); // Serine
		massTable.put("P", 97.052763850047); // Proline
		massTable.put("V", 99.068413914529); // Valine
		massTable.put("T", 101.047678469607); // Threonine
		massTable.put("C", 103.009184785565); // Cysteine
		massTable.put("I", 113.084063979011); // Isoleucine
		massTable.put("L", 113.084063979011); // Leucine
		massTable.put("N", 114.042927442166); // Asparagine
		massTable.put("D", 115.026943024685); // Aspartic Acid
		massTable.put("Q", 128.058577506648); // Glutamine
		massTable.put("K", 128.094963016052); // Lysine
		massTable.put("E", 129.042593089167); // Glutamic Acid
		massTable.put("M", 131.040484914529); // Methionine
		massTable.put("H", 137.058911859647); // Histidine
		massTable.put("F", 147.068413914529); // Phenylalanine
		massTable.put("R", 156.101111025652); // Arginine
		massTable.put("Y", 163.063328534089); // Tyrosine
		massTable.put("W", 186.07931295157); // Tryptophan
		massTable.put("U", 168.964198469607); // Selenocysteine
		massTable.put("O", 255.158291550141); // Pyrrolysine

		massTable.put("NT", 1.007825032241); // H
		massTable.put("H+", 1.007276466879); // H+
		massTable.put("CT", 15.99491461956 + 1.007825032241); // OH

		massTable.put("X", 0.00000); // to be changed
		massTable.put("*", 0.00000); // to be changed

	}

	/**
	 * Returns a mass value for a given amino-acid String litteral representation.
	 * Note: To ensure software robustness, a mass of 0.0Da is returned when the
	 * amino-acid is not found, this his however not a biologically relevant
	 * solution.
	 * 
	 * @param aminoAcid The amino-acid String litteral representation
	 * @return The mass value
	 * @since 0.1
	 */
	public static double getMass(String aminoAcid) {
		if (massTable.containsKey(aminoAcid)) {
			return massTable.get(aminoAcid);
		} else {
			return 0.0;
		}
	}

	public static void modifMass(String aminoAcid, double modification) {
		if (massTable.containsKey(aminoAcid)) {
			massTable.put(aminoAcid, getMass(aminoAcid) + modification);
		}
	}

	public static void modifMass(int aminoAcid, double modification) {
		if (massTableInt.containsKey(aminoAcid)) {
			massTableInt.put(aminoAcid, getMass(aminoAcid) + modification);
		}
	}

	/**
	 * Returns a mass value for a given amino-acid integer corresponding to its
	 * String litteral representation. Note: Is limited to the representation of
	 * amino-acids with a single litteral. Note: To ensure software robustness, a
	 * mass of 0.0Da is returned when the amino-acid is not found, this his however
	 * not a biologically relevant solution.
	 * 
	 * @param aminoAcid The amino-acid integer
	 * @return The mass value
	 * @since 0.1
	 */
	public static double getMass(int aminoAcid) {
		if (massTableInt.containsKey(aminoAcid)) {
			return massTableInt.get(aminoAcid);
		} else {
			return 0.0;
		}
	}

	/**
	 * Map representation of integer corresponding to an amino-Acid String litteral
	 * to double amino-acids masses. Note: should be externalised. Note: Cystein
	 * carries a +57Da systematic modification. Note: Is limited to the
	 * represeentation of amino-acids with a single litteral. Note: some exotic
	 * amino-acids String entries are represented with a 0 mass, this is not a long
	 * term satisfying solution.
	 * 
	 * @since 0.1
	 */
	private static Map<Integer, Double> massTableInt = new HashMap<Integer, Double>();
	static {
		massTableInt.put((int) 'G', 57.021463721083); // Glycine
		massTableInt.put((int) 'A', 71.037113785565); // Alanine
		massTableInt.put((int) 'S', 87.032028405125); // Serine
		massTableInt.put((int) 'P', 97.052763850047); // Proline
		massTableInt.put((int) 'V', 99.068413914529); // Valine
		massTableInt.put((int) 'T', 101.047678469607); // Threonine
		massTableInt.put((int) 'C', 103.009184785565); // Cysteine
		massTableInt.put((int) 'I', 113.084063979011); // Isoleucine
		massTableInt.put((int) 'L', 113.084063979011); // Leucine
		massTableInt.put((int) 'N', 114.042927442166); // Asparagine
		massTableInt.put((int) 'D', 115.026943024685); // Aspartic Acid
		massTableInt.put((int) 'Q', 128.058577506648); // Glutamine
		massTableInt.put((int) 'K', 128.094963016052); // Lysine
		massTableInt.put((int) 'E', 129.042593089167); // Glutamic Acid
		massTableInt.put((int) 'M', 131.040484914529); // Methionine
		massTableInt.put((int) 'H', 137.058911859647); // Histidine
		massTableInt.put((int) 'F', 147.068413914529); // Phenylalanine
		massTableInt.put((int) 'R', 156.101111025652); // Arginine
		massTableInt.put((int) 'Y', 163.063328534089); // Tyrosine
		massTableInt.put((int) 'W', 186.07931295157); // Tryptophan
		massTableInt.put((int) 'U', 168.964198469607); // Selenocysteine
		massTableInt.put((int) 'O', 255.158291550141); // Pyrrolysine

		massTableInt.put((int) 'X', 000.00000); // to be changed
		massTableInt.put((int) '*', 000.00000); // to be changed
		massTableInt.put((int) 'B', 000.00000); // to be changed
		massTableInt.put((int) 'Z', 000.00000); // to be changed

	}

	/**
	 * Detect if an amino-acid int to String representation is unknown.
	 * 
	 * @param aminoAcid the amino-acid to check
	 * @return true if unknown, false otherwise
	 * @since 0.1
	 */
	public static boolean unknownAA(int aminoAcid) {
		boolean toReturn = false;
		if (!massTableInt.containsKey(aminoAcid)) {
			toReturn = true;
		}
		return toReturn;
	}

	/**
	 * Compute the mass of a sequence of integer representations of aminoAcids
	 * 
	 * @param sequence The sequence of amino-acids to compute the mass from
	 * @return the computed mass
	 * @since 0.1
	 */
	public static double computeMass(int[] sequence) {
		double mass = 0.0;
		for (int cpt = 0; cpt < sequence.length; ++cpt) {
			mass += massTableInt.get(sequence[cpt]);
		}
		return mass;
	}

	/**
	 * Convert an integer representation sequence of amino-acids into is String
	 * litteral representation.
	 * 
	 * @param sequence The sequence of integers representation to convert
	 * @return a String representation of the sequence
	 * @since 0.1
	 */
	static String asString(int[] sequence) {
		StringBuffer res = new StringBuffer();
		char temp;
		if (sequence == null) {
			return "";
		} else {
			for (int cpt = 0; cpt < sequence.length; ++cpt) {
				temp = (char) sequence[cpt];
				res.append(temp);
			}
		}
		return res.toString();
	}

}
