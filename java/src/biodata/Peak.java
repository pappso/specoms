/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

/***
 * Implementation of a mass-intensity peak representation from a mass spectrum.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class Peak implements Comparable<Peak> {

	/**
	 * Mass of a peak represented in its integer scaled value.
	 * 
	 * @since 0.1
	 */
	private int mass;

	/**
	 * Intensity of a peak, rounded into an integer. Note: This representation is
	 * not ideal, but aims for speed at the cost of a neglectable approximation.
	 * 
	 * @since 0.1
	 */
	private int intensity;

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	private Peak() {
	}

	/**
	 * Builds a peak representation from the mass and intensity values according a
	 * given scale factor.
	 * 
	 * @param mass      The mass value of the peak
	 * @param intensity The intensity of the peak
	 * @param scale     The factor used to scale the mass value
	 * @return The peak represetation
	 * @since 0.1
	 */
	public static Peak buildPeak(double mass, double intensity, int scale) {
		Peak p = new Peak();

		// scaling of the mass into an integer and rounding of the intensity
		p.mass = (int) (mass * scale);
		p.intensity = (int) (intensity);

		return p;
	}

	/**
	 * Provides access to the scaled mass value of the peak.
	 * 
	 * @return The scaled mass value
	 * @since 0.1
	 */
	public int Mass() {
		return this.mass;
	}

	/**
	 * Provides access to the intensity of the peak.
	 * 
	 * @return The intensity
	 * @since 0.1
	 */
	public int Intensity() {
		return this.intensity;
	}

	/**
	 * Implementation of the comparable interface in order to be able to compare two
	 * peaks and sort them accordingly to their mass value.
	 * 
	 * @param p The peak to compare with the current one
	 * @return -1 if the peak p has asmaller mass, +1 if its mass is bigger, 0 if
	 *         they are equal
	 * @since 0.1
	 */
	@Override
	public int compareTo(Peak p) {

		if (this.intensity < p.intensity)
			return 1;
		if (this.intensity > p.intensity)
			return -1;
		return 0;

	}

}
