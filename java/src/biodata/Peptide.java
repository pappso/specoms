/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

/***
 * Memory-cheap representation of a peptide. Note: This class uses a static
 * defined counter, it must be reset to use with multiple peptide collections.
 * Note: This implementation is unreliable for concurrent use.
 * 
 * @author Matthieu David
 * @version 0.2
 */
public class Peptide {

	/**
	 * Static instanciation counter used to attribute an identifier to a peptide.
	 * Note: Concurrent use averse.
	 * 
	 * @since 0.1
	 */
	private static int idCount = 0;

	/**
	 * Unique peptide identifier.
	 * 
	 * @since 0.1
	 */
	private int identifier;

	/**
	 * Witness the peptide origin : target or decoy 0 is for decoy, 1 is for target,
	 * 2 if the peptide comes from both
	 * 
	 * @since 0.2
	 */
	private int origin = -2;

	/**
	 * Provides access to the peptide origin
	 * 
	 * @return the peptide origin
	 * @since 0.2
	 */
	public int origin() {
		return this.origin;
	}

	/**
	 * Primitive integer array representation of an amino-acids sequence.
	 * 
	 * @since 0.1
	 */
	private int[] aminoAcids;

	/**
	 * Complete model of a spectrum associated to this peptide. Note: instanciated
	 * only on the fly to reduce memory and time consumption.
	 * 
	 * @since 0.1
	 */
	CompleteSpectrumModel cs = null;

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	protected Peptide() {
	}

	/**
	 * Instanciation of a peptide from an integer array of amino-acids.
	 * 
	 * @param aa The amino-acids integer array
	 * @return The instanciated peptide
	 * @since 0.1
	 */
	public static Peptide createFromIntArray(int[] aa) {
		Peptide newPeptide = new Peptide();

		// attribution of a unique identifier and update of the identifiers static
		// variable
		newPeptide.identifier = idCount;
		++idCount;

		newPeptide.aminoAcids = aa;
		return newPeptide;
	}

	/**
	 * Copy constructor. Note: the new peptide will have the exact same identifier.
	 * Note: The relevance of this implementation is to be checked, maybe the
	 * default copy constructor is enough. Unsure though.
	 * 
	 * @param p a peptide co copy
	 * @return a copy of the peptide p
	 * @since 0.1
	 */
	public static Peptide copy(Peptide p) {
		Peptide newPeptide = new Peptide();
		newPeptide.identifier = p.identifier;
		newPeptide.origin = p.origin;
		newPeptide.aminoAcids = new int[p.aminoAcids.length];
		for (int cpt = 0; cpt < p.aminoAcids.length; ++cpt) {
			newPeptide.aminoAcids[cpt] = p.aminoAcids[cpt];
		}
		return newPeptide;
	}

	/**
	 * Checks wether or not a peptide contains an unidentified amino-acid.
	 * 
	 * @return true if one of the peptides amino-acid is unknown
	 * @since 0.1
	 */
	public boolean containsUnknownAA() {
		boolean toReturn = false;
		for (int cpt = 0; cpt < aminoAcids.length; ++cpt) {
			if (AminoAcidHandler.unknownAA(aminoAcids[cpt])) {
				toReturn = true;
			}
		}
		return toReturn;
	}

	/**
	 * Used to manually set the identifier of a peptide. Note: This is an
	 * implementation flaw and should not be needed ; identifier should be
	 * read-only. To correct.
	 * 
	 * @param a the identifier to give to the peptide
	 * @since 0.1
	 */
	public void setIdentifier(int a) {
		identifier = a;
	}

	/**
	 * Provides access to the peptide unique idetifier.
	 * 
	 * @return the identifier
	 * @snice 0.1
	 */
	public int identifier() {
		return identifier;
	}

	/**
	 * Provides access to the amino-acids composing the peptide.
	 * 
	 * @return the integer sequence of amino-acids
	 * @since 0.1
	 */
	public int[] asList() {
		return aminoAcids;
	}

	/**
	 * Returns the amino-acid integer representation at a given position in the
	 * peptide.
	 * 
	 * @param pos the position of the amino-acid to return
	 * @return the integer representation of the amino-acid
	 * @since 0.1
	 */
	public int aminoAcid(int pos) {
		return aminoAcids[pos];
	}

	/**
	 * Implementation of the equality of two peptides based on their amino-acids
	 * sequence. Note: identifiers equality is not required.
	 * 
	 * @param obj the peptide to compare to the current one
	 * @return true if the peptides have an equal sequence, false otherwise
	 * @since 0.1
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Peptide))
			return false;
		Peptide lp = (Peptide) obj;

		if (lp.aminoAcids.length == this.aminoAcids.length) {
			for (int cpt = 0; cpt < lp.aminoAcids.length; ++cpt) {
				if (this.aminoAcids[cpt] != lp.aminoAcids[cpt])
					return false;
			}
			return true;
		} else
			return false;
	}

	/**
	 * Implementation of the hashcode of a peptide based on its amino-acid sequence.
	 * 
	 * @return the hashcode
	 * @since 0.1
	 */
	@Override
	public int hashCode() {
		int hashRes = 17;
		for (int cpt = 0; cpt < aminoAcids.length; ++cpt) {
			hashRes = 37 * hashRes + aminoAcids[cpt];
		}
		return hashRes;
	}

	/**
	 * Provides the String representation of the amino-acid sequence of a peptide.
	 * 
	 * @return the amino-acids string
	 * @since 0.1
	 */
	public String asString() {
		StringBuilder res = new StringBuilder();
		char temp;
		for (int cpt = 0; cpt < aminoAcids.length; ++cpt) {
			temp = (char) aminoAcids[cpt];
			res.append(temp);
		}
		return res.toString();
	}

	/**
	 * Resets the static peptide count. Note: to be used between two executions of
	 * SpecTrees on different datasets. Note: this has to be changed for another
	 * more convenient to use implementation.
	 * 
	 * @since 0.1
	 */
	public static void reset() {
		idCount = 0;
	}

	/**
	 * Computes the decoy peptide by reverting all sequence but the last amino-acid.
	 * Note: Destructive method. Inversible.
	 * 
	 * @since 0.1
	 */
	/*
	 * public void decoy(){ int l = aminoAcids.length; int[] decoy = new int[l];
	 * for(int cpt = 0 ; cpt < l-1 ; cpt++){ decoy[cpt] = aminoAcids[l-2-cpt]; }
	 * decoy[l-1] = aminoAcids[l-1]; aminoAcids = decoy; }
	 */

	/**
	 * Builds instantiates and returns, if needed, the complete spectrum
	 * representation associated to the peptide.
	 * 
	 * @return the complete spectrum model
	 * @since 0.1
	 */
	public CompleteSpectrumModel getCompleteSpectrumModel() {
		if (cs == null) {
			cs = CompleteSpectrumModel.createFromPeptide(this);
		}
		return cs;
	}

	// 0.3 (DT)
	// prot_type=1 (target), -1 (contaminant), 0 (decoy)
	// origin = 2 (target ou contaminant + decoy)
	// origin == 3 (target + contaminant)

	public void setOrigin(int prot_type) {
		if (origin < -1) { // uninitialized
			origin = prot_type;
		} else {
			switch (prot_type) {
			case 0:
				if (origin != 0) {
					origin = 2;
				}
				break;
			case 1:
				if (origin == -1) {
					origin = 3;
				} else if (origin == 0) {
					origin = 2;
				}
				break;
			case -1:
				if (origin == 0) {
					origin = 2;
				} else if (origin == 1) {
					origin = 3;
				}
				break;
			}
		}
	}

	public static String getOrigin(int origin) {
		switch (origin) {
		case -2:
			return ("Error");
		case -1:
			return ("Contaminant");
		case 0:
			return ("Decoy");
		case 1:
			return ("Target");
		case 2:
			return ("Multiple");
		case 3:
			return ("Contaminant and Target");
		default:
			return ("***");
		}
	}

}
