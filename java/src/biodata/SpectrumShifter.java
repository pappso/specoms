/* * * * *
 * This software is licensed under the Apache 2 license, quoted below.
 *
 * Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * * * * */

package biodata;

import java.io.BufferedWriter;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import proteo.AffixHandler;
import proteo.Param;
import tools.Record;

/*
 * @author E092585L
 */
public class SpectrumShifter {
	// private static final Logger LOGGER =
	// Logger.getLogger(SpectrumShifter.class.getName());

	/**** Temporary values to clean later ***/

	private double total_delta;
	private CompleteSpectrumModel csRef;
	private int best_score;

	private boolean isMC = false;
	private boolean isST = false;
	private int cutPosLeft = -1;
	private int cutPosRight = -1;

	String prefix = "";
	String suffix = "";
	String suffixSequence = "";
	String prefixSequence = "";

	private int massDelta;

	private int decimal_factor;

	String file = Param.resultsFile();
	private FileWriter writer;
	private BufferedWriter buffer;
	private PrintWriter output = null;
	boolean open;
	/****************************************/

	// TODO :: those values should be computed preliminary using the maximum mass
	// ever read in an experimental spectrum
	// private static final int SAFE_SPACE = 10_000;
	// private static final int SAFE_FACTOR = 200;

	private boolean[] experimental_spectrum_buffer;
	private int[] experimental_spectrum_use_state;

	private int[] bions_unmodified_buffer;
	private int[] bions_modified_buffer;
	private int[] yions_unmodified_buffer;
	private int[] yions_modified_buffer;

	private int[] experimental_spectrum;

	private int score;
	private int similarity;

	private int best_position;

	public static SpectrumShifter create() {

		SpectrumShifter shift = new SpectrumShifter();

		// Pour donner de la marge a SpecFit, la taille du spectre exp = 2* taille utile
		// sans decalage

		shift.decimal_factor = Param.accuracyValue();
		int deltaMax = Param.maxMassDelta() * Param.accuracyFactor();
		shift.experimental_spectrum_buffer = new boolean[Param.highestMassValue() + deltaMax];
		shift.experimental_spectrum_use_state = new int[Param.highestMassValue() + deltaMax];

		// bellow: use to take incertainty +-1 due to addition of deltaM into account
		if (Param.search_mode().equals("peptides_exact"))
			shift.decimal_factor = 1;

		return shift;
	}

	public void initialise(CompleteSpectrumModel cs, Spectrum spectra, int t, int delta, double accDelta) {

		// LOGGER.log(Level.INFO, cs.name());
		// LOGGER.log(Level.INFO, String.valueOf(delta));
		// LOGGER.log(Level.INFO, String.valueOf(accDelta));

		this.similarity = t;
		this.best_score = t;
		this.best_position = -1;

		this.total_delta = accDelta; // temporary
		this.csRef = cs; // temporary
		this.massDelta = delta;

		bufferize_experimental_spectrum(spectra);
		bufferize_theoretical_spectrum(cs, delta);

		//

		this.score = 0;
		int position_b_side = 0;
		int position_y_side = this.yions_modified_buffer.length - 1;
		do {
			if (experimental_spectrum_buffer[bions_unmodified_buffer[position_b_side]]) {
				// take accuracy into account
				add_ion_presence(bions_unmodified_buffer[position_b_side]);
				// A peak must be counted only once
				if (experimental_spectrum_use_state[bions_unmodified_buffer[position_b_side]] == 1) {
					++score;
				}
			}
			if (experimental_spectrum_buffer[yions_modified_buffer[position_y_side]]) {
				add_ion_presence(yions_modified_buffer[position_y_side]);
				if (experimental_spectrum_use_state[yions_modified_buffer[position_y_side]] == 1) {
					++score;
				}
			}

			++position_b_side;
			--position_y_side;
		} while (position_b_side < this.yions_modified_buffer.length - 1);

		// Finally, all y-ions are modified, all b-ions are unmodified except the last
		// one

		if (experimental_spectrum_buffer[bions_modified_buffer[position_b_side]]) {
			// take accuracy into account
			add_ion_presence(bions_modified_buffer[position_b_side]);
			// A peak must be counted only once
			if (experimental_spectrum_use_state[bions_modified_buffer[position_b_side]] == 1) {
				++score;
			}
		}

		if (experimental_spectrum_buffer[yions_modified_buffer[position_y_side]]) {
			add_ion_presence(yions_modified_buffer[position_y_side]);
			if (experimental_spectrum_use_state[yions_modified_buffer[position_y_side]] == 1) {
				++score;
			}
		}

		if (score > best_score) {
			best_score = score;
			best_position = position_b_side;
		}

	}

	private void bufferize_experimental_spectrum(Spectrum s) {

		this.experimental_spectrum = s.asList();

		for (int cpt = 0; cpt < experimental_spectrum.length; ++cpt) {
			this.experimental_spectrum_buffer[experimental_spectrum[cpt]] = true;

			// incertitude is added so that mass(peak) + deltaM can match even though deltam
			// has been rounded
			if (Param.search_mode().equals("peptides_exact")) {
				this.experimental_spectrum_buffer[experimental_spectrum[cpt] + 1] = true;
				if (experimental_spectrum[cpt] > 0) {
					this.experimental_spectrum_buffer[experimental_spectrum[cpt] - 1] = true;
				}
			}
		}
		// we ensure a peak of mass 0 is never gonna be accounted as a match, so we can
		// stack negative deltas into the first slot of the experimental array
		this.experimental_spectrum_buffer[0] = false;
	}

	private void reset_experimental_spectrum_buffer() {
		/*
		 * for (int cpt = 0; cpt < experimental_spectrum.length; ++cpt) { for (int
		 * counter = 0; counter <= this.decimal_factor; ++counter) {
		 * this.experimental_spectrum_buffer[experimental_spectrum[cpt] + counter] =
		 * false; if (experimental_spectrum[cpt] - counter > 0) {
		 * this.experimental_spectrum_buffer[experimental_spectrum[cpt] - counter] =
		 * false; } }
		 */
		for (int cpt = 0; cpt < experimental_spectrum.length; ++cpt) {
			experimental_spectrum_buffer[experimental_spectrum[cpt]] = false;
			if (Param.search_mode().equals("peptides_exact")) {
				this.experimental_spectrum_buffer[experimental_spectrum[cpt] + 1] = false;
				if (experimental_spectrum[cpt] > 0) {
					this.experimental_spectrum_buffer[experimental_spectrum[cpt] - 1] = false;
				}
			}
			reset_ion_presence();
		}
	}

	private void bufferize_theoretical_spectrum(CompleteSpectrumModel cs, int delta) {

		this.bions_unmodified_buffer = new int[cs.bIonsList().length];
		this.bions_modified_buffer = new int[cs.bIonsList().length];

		for (int i = 0; i < cs.bIonsList().length; i++) {
			bions_unmodified_buffer[i] = cs.bIonsList()[i];
		}
		this.yions_unmodified_buffer = new int[cs.yIonsList().length];
		this.yions_modified_buffer = new int[cs.yIonsList().length];

		// yions_unmodified_buffer[0]=0;

		for (int i = 0; i < cs.yIonsList().length; i++) {
			yions_unmodified_buffer[i] = cs.yIonsList()[i];
		}
		int mass_value;

		for (int cpt = 0; cpt < bions_unmodified_buffer.length; cpt++) {
			mass_value = bions_unmodified_buffer[cpt] + delta;
			if (mass_value < 0) {
				mass_value = 0;
			}
			bions_modified_buffer[cpt] = mass_value;
			mass_value = yions_unmodified_buffer[cpt] + delta;
			if (mass_value < 0) {
				mass_value = 0;
			}
			yions_modified_buffer[cpt] = mass_value;
		}
	}

	// there is a risk of out of bound access there, if a peak is at
	// [position-compteur]<0 or [position+compteur]>limit
	private void add_ion_presence(int ion_position) {
		this.experimental_spectrum_use_state[ion_position] += 1;
		for (int counter = 1; counter <= this.decimal_factor; ++counter) {
			this.experimental_spectrum_use_state[ion_position + counter] += 1;
			if (ion_position - counter > 0)
				this.experimental_spectrum_use_state[ion_position - counter] += 1;
		}
	}

	// there is a risk of out of bound access there, if a peak is at
	// [position-compteur]<0 or [position+compteur]>limit
	private void remove_ion_presence(int ion_position) {
		this.experimental_spectrum_use_state[ion_position] -= 1;
		for (int counter = 1; counter <= this.decimal_factor; ++counter) {
			this.experimental_spectrum_use_state[ion_position + counter] -= 1;
			if (ion_position - counter > 0)
				this.experimental_spectrum_use_state[ion_position - counter] -= 1;
		}
	}

	// idem autres ion_presence
	private void reset_ion_presence() {

		for (int counter = 0; counter <= this.decimal_factor + 1; ++counter) {

			for (int i = 0; i < bions_unmodified_buffer.length; i++) {
				this.experimental_spectrum_use_state[bions_unmodified_buffer[i] + counter] = 0;
				if (bions_unmodified_buffer[i] - counter > 0)
					this.experimental_spectrum_use_state[bions_unmodified_buffer[i] - counter] = 0;
				this.experimental_spectrum_use_state[bions_modified_buffer[i] + counter] = 0;
				if (bions_modified_buffer[i] - counter > 0)
					this.experimental_spectrum_use_state[bions_modified_buffer[i] - counter] = 0;
				this.experimental_spectrum_use_state[yions_unmodified_buffer[i] + counter] = 0;
				if (yions_unmodified_buffer[i] - counter > 0)
					this.experimental_spectrum_use_state[yions_unmodified_buffer[i] - counter] = 0;
				this.experimental_spectrum_use_state[yions_modified_buffer[i] + counter] = 0;
				if (yions_modified_buffer[i] - counter > 0)
					this.experimental_spectrum_use_state[yions_modified_buffer[i] - counter] = 0;
			}
		}

	}

	/*
	 * VERSION AVANT 18/03/2019
	 *
	 * 
	 * private void shift_peaks(double total_delta){
	 * 
	 * System.out.println("Entree dans shift_peaks : "+total_delta);
	 * 
	 * int position_b_side = this.bions_unmodified_buffer.length-1; int
	 * position_y_side = 0; best_position = -1;
	 * 
	 * do{ // fetch the B-ion boolean old_b_ion =
	 * experimental_spectrum_buffer[bions_unmodified_buffer[position_b_side]];
	 * boolean new_b_ion =
	 * experimental_spectrum_buffer[bions_modified_buffer[position_b_side]];
	 * if(old_b_ion){ remove_ion_presence(bions_unmodified_buffer[position_b_side]);
	 * if(experimental_spectrum_use_state[bions_unmodified_buffer[position_b_side]]=
	 * =0){--score;} } if(new_b_ion){
	 * add_ion_presence(bions_modified_buffer[position_b_side]);
	 * if(experimental_spectrum_use_state[bions_modified_buffer[position_b_side]]==1
	 * ){++score;} }
	 * 
	 * // fetch the Y-ion boolean old_y_ion =
	 * experimental_spectrum_buffer[yions_modified_buffer[position_y_side]]; boolean
	 * new_y_ion =
	 * experimental_spectrum_buffer[yions_unmodified_buffer[position_y_side]];
	 * if(old_y_ion){ remove_ion_presence(yions_modified_buffer[position_y_side]);
	 * if(experimental_spectrum_use_state[yions_modified_buffer[position_y_side]]==0
	 * ){--score;} } if(new_y_ion){
	 * add_ion_presence(yions_unmodified_buffer[position_y_side]);
	 * if(experimental_spectrum_use_state[yions_unmodified_buffer[position_y_side]]=
	 * =1){++score;} }
	 * 
	 * 
	 * if(score>best_score){ best_score = score; best_position = position_b_side;
	 * System.out.println("best_position :"+best_position+","+best_score); }
	 * 
	 * ++position_y_side; --position_b_side; }while(position_b_side >= 0); }
	 */

	private void shift_peaks(double deltaM) {

		int position_b_side = this.bions_unmodified_buffer.length - 2;
		int position_y_side = 0;
		int correction = 0;

		// System.out.println("best_score:"+ best_score);
		// System.out.println("******************************************************************");

		do {

			correction = 0;
			// fetch the B-ion
			boolean old_b_ion = experimental_spectrum_buffer[bions_unmodified_buffer[position_b_side]];
			boolean new_b_ion = experimental_spectrum_buffer[bions_modified_buffer[position_b_side]];
			if (old_b_ion) {
				remove_ion_presence(bions_unmodified_buffer[position_b_side]);
				if (experimental_spectrum_use_state[bions_unmodified_buffer[position_b_side]] == 0) {
					--score;
				}
			}
			if (deltaM < 0 && !old_b_ion) {
				for (int i = position_b_side; i >= 0; i--) { // chevauchement des zones : on ne compte pas
					if (bions_unmodified_buffer[i] >= bions_modified_buffer[position_b_side]) {
						// remove_ion_presence(bions_unmodified_buffer[i]);
						if (experimental_spectrum_use_state[bions_unmodified_buffer[i]] == 1) {
							correction++;
						}
					} else
						break;
				}
			}
			if (new_b_ion) {
				add_ion_presence(bions_modified_buffer[position_b_side]);
				if (experimental_spectrum_use_state[bions_modified_buffer[position_b_side]] == 1) {
					++score;
				}
			}

			// fetch the Y-ion
			boolean old_y_ion = experimental_spectrum_buffer[yions_modified_buffer[position_y_side]];
			boolean new_y_ion = experimental_spectrum_buffer[yions_unmodified_buffer[position_y_side]];
			if (old_y_ion) {
				remove_ion_presence(yions_modified_buffer[position_y_side]);
				if (experimental_spectrum_use_state[yions_modified_buffer[position_y_side]] == 0) {
					--score;
				}
			}

			int dim = this.bions_unmodified_buffer.length - 1;
			if (deltaM < 0 && !old_y_ion) {
				for (int i = position_y_side; i <= dim; i++) { // chevauchement des zones : on ne compte pas
					if (yions_modified_buffer[i] <= yions_unmodified_buffer[position_y_side]) {
						// remove_ion_presence(yions_unmodified_buffer[i]);
						if (experimental_spectrum_use_state[yions_modified_buffer[i]] == 1) {
							correction++;
						}
					} else
						break;
				}
			}

			if (new_y_ion) {
				add_ion_presence(yions_unmodified_buffer[position_y_side]);
				if (experimental_spectrum_use_state[yions_unmodified_buffer[position_y_side]] == 1) {
					++score;
				}
			}

			if (score - correction > best_score) {
				best_score = score - correction;
				best_position = position_b_side;
				// System.out.println("best_score :"+best_score);
				// System.out.println("best_position :" +position_b_side);
			}

			++position_y_side;
			--position_b_side;

		} while (position_b_side >= 0);

		this.reset_experimental_spectrum_buffer();
	}

	public void shift(Record best_sol, Record solution) {

		// Record solution = Record.create_empty_record(best_sol.spectrum_name());
		// solution.fill_record(csRef.name(), similarity, best_score, best_position,
		// total_delta, 0, 0, "");

		// evaluates if solution must replace best_sol

		best_sol.replace(solution);

		// MODIFICATION DT
		// if(total_delta <= 0.02 && total_delta >= -0.02){

		if (total_delta <= Param.accuracy() && total_delta >= -Param.accuracy() && similarity >= Param.minimumScore()) {
			// replace the solution if the new score is better than the old one
			solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(), similarity, best_score, -1,
					total_delta, Param.EXACT_MASS_DELTA_PRIORITY(), 0, "");
			best_sol.replace(solution);

		} else {
			String newSeq;
			int newPeaks;

			if ((total_delta < -Param.accuracy()) && (similarity >= Param.minimumScore())) {
				this.searchCutSide();
				if (this.isST) {
					if (this.cutPosLeft != -1) {
						newSeq = csRef.name();
						newSeq = newSeq.substring(cutPosLeft + 1);
						CompleteSpectrumModel newCs = CompleteSpectrumModel.createFromSequence(newSeq);
						newPeaks = newCs.compareTo(this.experimental_spectrum_buffer);

						if (newPeaks > similarity) {
							solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(), similarity, newPeaks,
									cutPosLeft, total_delta, Param.ST_PRIORITY(), -1, "");
							best_sol.replace(solution);
						}
					}
					if (this.cutPosRight != -1) {
						newSeq = csRef.name();
						newSeq = newSeq.substring(0, cutPosRight);
						CompleteSpectrumModel newCs = CompleteSpectrumModel.createFromSequence(newSeq);
						newPeaks = newCs.compareTo(this.experimental_spectrum_buffer);
						// MODIFICATION DT
						if (newPeaks > similarity) {
							solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(), similarity, newPeaks,
									cutPosRight, total_delta, Param.ST_PRIORITY(), 1, "");
							best_sol.replace(solution);
						}
					}
				}
				this.clean();

			}

			if ((total_delta > Param.accuracy()) && (similarity >= Param.minimumScore())) {
				this.checkMissC();
				if (this.isMC) {
					if (!prefix.equals("")) {
						newSeq = prefixSequence + csRef.name();
						CompleteSpectrumModel newCs = CompleteSpectrumModel.createFromSequence(newSeq);
						newPeaks = newCs.compareTo(this.experimental_spectrum_buffer);
						// MODIFICATION DT
						if (newPeaks > similarity) {
							// solution.fill_record(csRef.identifier(), csRef.name(),csRef.origin(),
							// similarity, newPeaks, -1, total_delta, MC_PRIORITY, 0, prefixSequence);
							solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(), similarity, newPeaks,
									-1, total_delta, Param.MC_PRIORITY(), 0, prefix);
							best_sol.replace(solution);
						}
					}
					if (!suffix.equals("")) {
						newSeq = csRef.name() + suffixSequence;
						CompleteSpectrumModel newCs = CompleteSpectrumModel.createFromSequence(newSeq);
						newPeaks = newCs.compareTo(this.experimental_spectrum_buffer);
						// MODIFICATION DT
						if (newPeaks > similarity) {
							// solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(),
							// similarity, newPeaks, -1, total_delta, MC_PRIORITY, 0, suffixSequence);
							solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(), similarity, newPeaks,
									-1, total_delta, Param.MC_PRIORITY(), 0, suffix);
							best_sol.replace(solution);
						}
					}
				}
				this.clean();
			}
			if (Param.shift()) {
				this.shift_peaks(total_delta);
				solution.fill_record(csRef.identifier(), csRef.name(), csRef.origin(), similarity, best_score,
						best_position, total_delta, Param.SHIFT_PRIORITY(), 0, "");
				best_sol.replace(solution);
				this.clean();
			}
		}
		this.reset_experimental_spectrum_buffer();
	}

	private void clean() {
		prefix = "";
		suffix = "";
		suffixSequence = "";
		prefixSequence = "";
		this.cutPosLeft = -1;
		this.cutPosRight = -1;
		this.isST = false;
		this.isMC = false;
	}

	// do not touch for now
	private double checkMissC() {

		double to_return = 0.0;

		int id = csRef.identifier();

		if ((AffixHandler.prefixMass.containsKey(id))) {

			if ((total_delta - AffixHandler.prefixMass.get(id) >= (-Param.tolerance()))
					&& (total_delta - AffixHandler.prefixMass.get(id) <= Param.tolerance())) {
				prefix = "Prefix(" + AminoAcidHandler.asString(AffixHandler.prefix.get(id)) + ")";
				prefixSequence = AminoAcidHandler.asString(AffixHandler.prefix.get(id));
				isMC = true;
				to_return = total_delta - AffixHandler.prefixMass.get(id);
			}
		}

		if (AffixHandler.suffixMass.containsKey(id)) {

			if ((total_delta - AffixHandler.suffixMass.get(id) >= (-Param.tolerance()))
					&& (total_delta - AffixHandler.suffixMass.get(id) <= Param.tolerance())) {
				suffix = "Suffix(" + AminoAcidHandler.asString(AffixHandler.suffix.get(id)) + ")";
				suffixSequence = AminoAcidHandler.asString(AffixHandler.suffix.get(id));
				isMC = true;
				to_return = total_delta - AffixHandler.suffixMass.get(id);
			}

		}

		return to_return;

	}

	// do not touch for now
	private void searchCutSide() {

		int remainingDeltaLeft = massDelta;
		int remainingDeltaRight = massDelta;

		boolean overLeft = false;
		boolean overRight = false;

		// int posRight = bions_unmodified_buffer.length-1;
		int posRight = bions_unmodified_buffer.length - 2;
		int posLeft = 0;

		String sequence = csRef.name();
		String charac;

		while (!overLeft && !overRight) {

			charac = "" + sequence.charAt(posLeft);
			remainingDeltaLeft = remainingDeltaLeft
					+ (int) (Math.round(AminoAcidHandler.getMass(charac) * Param.accuracyFactor()));
			charac = "" + sequence.charAt(posRight);
			remainingDeltaRight = remainingDeltaRight
					+ (int) (Math.round(AminoAcidHandler.getMass(charac) * Param.accuracyFactor()));

			if ((remainingDeltaLeft >= -Param.accuracyDec()) && (remainingDeltaLeft <= Param.accuracyDec())) {
				cutPosLeft = posLeft;
				isST = true;
			}
			if ((remainingDeltaRight >= -Param.accuracyDec()) && (remainingDeltaRight <= Param.accuracyDec())) {
				cutPosRight = posRight;
				isST = true;
			}

			++posLeft;
			--posRight;
			if (posRight == 0) {
				overLeft = true;
				overRight = true;
			}

		}

	}

}
