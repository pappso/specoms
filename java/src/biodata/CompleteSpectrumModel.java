/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package biodata;

import proteo.Param;

/***
 * Representation of a spectrum used for peak matching using constant time
 * accesses. Might be further merged with the spectrum class to avoid
 * duplication.
 *
 * @author Matthieu David
 * @version 0.1
 */

public class CompleteSpectrumModel {

	private int origin;

	/**
	 * The spectrum unique identifier.
	 * 
	 * @since 0.1
	 */
	private int identifier;

	/**
	 * The spectrum name in the dataset.
	 * 
	 * @since 0.1
	 */
	private String name;

	/**
	 * The computed total mass of the spectrum.
	 * 
	 * @since 0.1
	 */
	private double totalMass;

	/**
	 * Array representation of the spectrum B ions.
	 * 
	 * @since 0.1
	 */
	private int[] bIons;

	/**
	 * Array representation of the spectrum Y ions.
	 * 
	 * @since 0.1
	 */
	private int[] yIons;

	/**
	 * Protected constructor to avoid inheritance out of the package.
	 * 
	 * @since 0.1
	 */
	protected CompleteSpectrumModel() {
	}

	/**
	 * Build a model from a peptide amino-acid sequence, computing the masses for
	 * each B and Y ion.
	 * 
	 * @param p the source peptide from which the model is built
	 * @return complete spectrum model
	 * @since 0.1
	 */
	public static CompleteSpectrumModel createFromPeptide(Peptide p) {

		// empty model initialisation
		CompleteSpectrumModel s = new CompleteSpectrumModel();

		// attributes copy from the peptide to the model
		s.identifier = p.identifier();
		s.name = p.asString();
		s.origin = p.origin();
		int[] aa = p.asList();

		fillModel(s, aa);

		return s;
	}

	private static void fillModel(CompleteSpectrumModel csm, int[] aa) {

		// initialisation of empty arrays of appropriate size to contain B and Y ions
		int length = csm.name.length();
		csm.bIons = new int[length];
		csm.yIons = new int[length];

		// initialisation of terminis
		double bSum = AminoAcidHandler.getMass("H+");
		double ySum = AminoAcidHandler.getMass("CT") + AminoAcidHandler.getMass("NT") + AminoAcidHandler.getMass("H+");

		// temporary accumulators and working variables
		int bValue;
		int yValue;
		double tempValue;

		// computes the next B and Y ion for each amino-acid until the whole sequence is
		// iterated and fills the corresponding arrays with the masses
		for (int cpt = 0; cpt < length; ++cpt) {

			// B ion processing
			tempValue = AminoAcidHandler.getMass(aa[cpt]); // fetch the mass of the corresponding character in the
															// sequence
			bSum += tempValue;
			bValue = (int) (Math.round(bSum * Param.accuracyFactor())); // scale and round the accumulated mass to
																		// obtain an integer to work with
																		// (discretization)
			csm.bIons[cpt] = bValue;

			// Y ion processing
			tempValue = AminoAcidHandler.getMass(aa[length - cpt - 1]); // fetch the mass of the corresponding character
																		// in the sequence
			ySum += tempValue;
			yValue = (int) (Math.round(ySum * Param.accuracyFactor())); // scale and round the accumulated mass to
																		// obtain an integer to work with
																		// (discretization)
			csm.yIons[cpt] = yValue;

		}

		// computation of the total mass by addition of the N-terminus to the B ion
		// accumulator
		csm.totalMass = bSum + AminoAcidHandler.getMass("CT") + AminoAcidHandler.getMass("H+");
	}

	/**
	 * Convert a string into an array of itnegers in which each cell contains the
	 * integer value corresponding to a string character.
	 * 
	 * @param seq the string to convert
	 * @return an array of integer, each corresponding to a character in the
	 *         sequence
	 * @since 0.1
	 */
	private static int[] convert_to_integer_array(String seq) {
		int[] aa = new int[seq.length()];
		for (int cpt = 0; cpt < seq.length(); ++cpt) {
			aa[cpt] = seq.charAt(cpt);
		}
		return aa;
	}

	/**
	 * Build a model from an amino-acid string, computing the masses for each B and
	 * Y ion. TODO :: This method is very similar to the one building the model from
	 * a peptide ; there should be a factorisation
	 * 
	 * @param seq the sttring from which the model is built
	 * @return complete spectrum model
	 * @since 0.1
	 */
	public static CompleteSpectrumModel createFromSequence(String seq) {

		CompleteSpectrumModel s = new CompleteSpectrumModel();

		s.identifier = -1;
		s.name = seq;
		int[] aa = convert_to_integer_array(seq);

		fillModel(s, aa);

		return s;
	}

	public int length() {
		return bIons.length;
	}

	public int[] bIonsList() {
		return bIons;
	}

	public int[] yIonsList() {
		return yIons;
	}

	public String name() {
		return this.name;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int cpt;
		int length = bIons.length;
		sb.append("B ");
		for (cpt = 0; cpt < length; ++cpt) {
			sb.append(" ; ");
			sb.append(bIons[cpt]);
		}
		sb.append(System.lineSeparator());
		sb.append("Y ");
		for (cpt = 0; cpt < length; ++cpt) {
			sb.append(" ; ");
			sb.append(yIons[cpt]);
		}
		sb.append(System.lineSeparator());
		return sb.toString();
	}

	public double mass() {
		return totalMass;
	}

	public int identifier() {
		return identifier;
	}

	public int compareTo(boolean[] spectrum) {

		int newPeaks = 0;

		for (int cpt = 0; cpt < this.bIons.length; ++cpt) {
			if (spectrum[bIons[cpt]] == true) {
				++newPeaks;
			}
			if (spectrum[yIons[cpt]] == true) {
				++newPeaks;
			}
		}

		return newPeaks;
	}

	public int origin() {
		return this.origin;
	}

}
