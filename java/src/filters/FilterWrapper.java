package filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief FilterWrapper allow you to use multiple Filters at once
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 * 
 */

public class FilterWrapper implements FilterInterface {

	/**
	 * @brief list of all Filters one at a time
	 * 
	 */
	List<FilterInterface> filters = new ArrayList<FilterInterface>();

	/**
	 * @brief FilterWrapper constructor, empty if you want to add Filters on the fly
	 */
	public FilterWrapper() {
	}

	/**
	 * @brief FilterWrapper constructor allow you to store multiple filter at once
	 *        in order to retrieve them later
	 *
	 * @param _filters a list of filters
	 */
	public FilterWrapper(List<FilterInterface> _filters) {
		this.filters = _filters;
	}

	/**
	 * @brief the filter method allow to execute all the filters stored inside the
	 *        FilterWrapper
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList
	 * 
	 * @throws IndexOutOfBoundsException if the peak list is empty
	 */
	public Map<Double, Double> filter(Map<Double, Double> peakList) {

		for (FilterInterface fi : this.filters) {
			peakList = fi.filter(peakList);
		}

		return peakList;
	}

	/**
	 * @brief the addFilters allow to append a filter to the list
	 * 
	 * @param _filter filter to append
	 * 
	 */
	public void addFilter(FilterInterface _filter) {
		this.filters.add(_filter);
	}
}
