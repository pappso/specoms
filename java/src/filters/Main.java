package filters;

import java.util.Map;

public class Main {
	public static void main(String[] args) {
		
		Mgfreader mg = new Mgfreader("../test/testFilters/one/one.mgf");

		PeakListMapStorageInFile storage = new PeakListMapStorageInFile();
		
		// ParentFilter pf = new ParentFilter(20.0, mg.get_parent_ion_mass(), 0.0);

		// DynamicRangeFilter dr = new DynamicRangeFilter(100.0, 2.0);

		// HighPass hp = new HighPass(10000.0);

		// Deisotoping dyna = new Deisotoping(10);

		// UpFilter up = new UpFilter(10);

		// LowFilter low = new LowFilter(150.0);
		
		WindowFilter win = new WindowFilter(1, 100.0);

		////////////////////////////////////////////////////////////
		// just the base file

		Map<Double, Double> map_mz_intensity = mg.get_mzAndIntensity();
		
		storage.store_in_file("../test/testFilters/one/ba", map_mz_intensity);

		////////////////////////////////////////////////////////////
		// remove the parent ion
		// map_mz_intensity = mg.get_mzAndIntensity();
		// map_mz_intensity = pf.filter(map_mz_intensity);

		// storage.store_in_file("../test/testFilters/one/pf", map_mz_intensity);

		////////////////////////////////////////////////////////////
		// then filtered with the high pass filter
		
		// map_mz_intensity = mg.get_mzAndIntensity();
		// map_mz_intensity = hp.filter(map_mz_intensity);

		// storage.store_in_file("../test/testFilters/one/hp", map_mz_intensity);

		////////////////////////////////////////////////////////////
		// then filtered with DynamicRangeEliminationOfLowIntensityPeaksFilter

		// map_mz_intensity = mg.get_mzAndIntensity();
		// map_mz_intensity = dyna.filter(map_mz_intensity);

		// storage.store_in_file("../test/testFilters/one/dy", map_mz_intensity);
		
		// then filtered with UpFilter
		
		// map_mz_intensity = mg.get_mzAndIntensity();
		// map_mz_intensity = up.filter(map_mz_intensity);
		
		// storage.store_in_file("../test/testFilters/one/up", map_mz_intensity);
		
		////////////////////////////////////////////////////////////
		// then LowFilter	

		// map_mz_intensity = mg.get_mzAndIntensity();
		// map_mz_intensity = low.filter(map_mz_intensity);

		// storage.store_in_file("../test/testFilters/one/lo", map_mz_intensity);		

		////////////////////////////////////////////////////////////
		// and finally with WindowFilter
		
		map_mz_intensity = mg.get_mzAndIntensity();
		map_mz_intensity = win.filter(map_mz_intensity);
		
		storage.store_in_file("../test/testFilters/one/wi", map_mz_intensity);
		
		////////////////////////////////////////////////////////////
		// then remove dynamicaly the ions with too low intensity

		// map_mz_intensity = mg.get_mzAndIntensity();
		// map_mz_intensity = dr.filter(map_mz_intensity);

		// storage.store_in_file("../test/testFilters/one/dr", map_mz_intensity);
		
		/*
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win1.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/win5", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			//
		}
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win2.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/win10", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			//
		}
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win3.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/win20", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			//
		}		
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win4.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/win50", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			//
		}	

		
		
		
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win5.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/winm1", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			System.out.println("oh");//
		}
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win6.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/winm5", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			System.out.println("oh");//
		}
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win7.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/winm10", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			System.out.println("oh");//
		}		
		try {
			map_mz_intensity = mg.get_mzAndIntensity();
			map_mz_intensity = win8.filter(map_mz_intensity);
			storage.store_in_file("../test/testFilters/one/winm20", map_mz_intensity);
		}catch(Exception IndexOutOfBoundsException) {
			System.out.println("oh");//
		}	
		*/
	}
}
