package filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief DynamiqueRangeEliminationOfLowIntensityPeaksFilter filter peak lists
 *        by intensity value; for each peak,if an other close one (in a certain
 *        window) has a higher intensity then do not select the first one ! has
 *        one information : the size of the window (in mass)
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */
public class Deisotoping implements FilterInterface {

	/**
	 * @brief size of the window
	 */
	Double win_size;

	/**
	 * @brief DynamiqueRangeEliminationOfLowIntensityPeaksFilter constructor
	 * @brief will create a DynamiqueRangeEliminationOfLowIntensityPeaksFilter. The
	 *        window size is fixed by the user
	 *
	 * @param _win_size size of the window (mass)
	 * 
	 * @throws IndexOutOfBoundsException : raised if _win_size < 0
	 */
	public Deisotoping(Double _win_size) throws IndexOutOfBoundsException {
		if (_win_size < 0) {
			throw new IndexOutOfBoundsException(
					"Your window should at least be 0, otehrwise your window is backwards...");
		}

		this.win_size = _win_size;
	}

	/**
	 * @brief filter peak lists by intensity value; for each peak if an other close
	 *        one (in a certain window) has a higher intensity then do not select
	 *        the first one !
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) {

		// sort peaks by mass and store them in a temporary list for easier
		// handling, and be sure they are ordered by mass
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Double, Double>>() {
			public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// selection
		List<Map.Entry<Double, Double>> acceptedlist = new LinkedList<Map.Entry<Double, Double>>();
		for(Map.Entry<Double, Double> m : list) {
			boolean accepted = true;
			for(Map.Entry<Double, Double> a : acceptedlist) {
				if(m.getKey() >= a.getKey() && m.getKey() <= a.getKey() + this.win_size) {
					accepted = false;
					break;
				}
			}
			if(accepted) {
				acceptedlist.add(m);
			}
		}

		// and then reconvet to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : acceptedlist) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;

	}
}
