package filters;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @brief HighPass filter peak lists by intensity value; will conserve only the
 *        peaks with an intensity > to a certain amount chosen by the user
 * @brief has one information : the minimum intensity to pass
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */
public class HighPass implements FilterInterface {
	//private static final Logger LOGGER = Logger.getLogger(HighPass.class.getName());

	/**
	 * @brief minimum intensity for a peak to be conserved
	 */
	Double int_min;

	/**
	 * @brief HighPass constructor
	 * @brief will create a HighPass. The minimum intensity is fixed by the user
	 *
	 * @param _int_min minimum intensity for a peak to be conserved
	 * 
	 * @throws IndexOutOfBoundsException : raised if _int_min < 0
	 */
	public HighPass(Double _int_min) throws IndexOutOfBoundsException {
		if (_int_min < 0) {
			throw new IndexOutOfBoundsException("You annot have a negative intensity !");
		}

		this.int_min = _int_min;
	}

	/**
	 * @brief filter peak lists by intensity value; only the ones that pass the
	 *        int_min bar are conserved
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) {
		// store the peaks in a list or easy handling
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());

		// selection of peaks high enougth
		List<Map.Entry<Double, Double>> filteredlist = new LinkedList<Map.Entry<Double, Double>>();
		for (Map.Entry<Double, Double> peak : list) {
			//LOGGER.log(Level.INFO, "int_min=" + int_min + " value=" + peak.getValue());
			if (peak.getValue() >= this.int_min) {

				//LOGGER.log(Level.INFO, "int_min=" + int_min + " value_added=" + peak.getValue());
				filteredlist.add(peak);
			}
		}

		// and then reconvet to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : filteredlist) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;

	}

}
