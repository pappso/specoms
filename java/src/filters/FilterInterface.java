
/*******************************************************************************
 * Copyright (c) 2020 ANR DeepProt
 *
 * This file is part of the DeepProt software.
 *
 *     DeepProt is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DeepProt is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with DeepProt.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

package filters;

import java.util.Map;

/**
 * 
 * @brief generic interface to create filters on any peaklist
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */
abstract public interface FilterInterface {

	/**
	 * @brief apply any filter to the peaklist, modify and return results
	 * @param peakList : map of mz keys and intensities values param : list of
	 *                 parameter specific to the filter
	 * @return the filtered peaklist
	 */
	abstract Map<Double, Double> filter(Map<Double, Double> peakList);
}
