package filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief AccuracyValuePeakDiscriminationFilter filter peak lists by looking if
 *        two peaks are sufficiently close to be merged into one (due to
 *        accuracy of the Spectro)
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */

public class AccuracyValuePeakDiscriminationFilter implements FilterInterface {

	/**
	 * @brief accuracy of the spectro
	 * 
	 *        this is the accuracy of mass measurement in spectrometer, around 0.02
	 *        dalton usually
	 * 
	 */
	double accuracy;

	/**
	 * @brief AccuracyValuePeakDiscriminationFilter constructor
	 * 
	 *        will create an AccuracyValuePeakDiscriminationFilter. The accuracy is
	 *        not fixed by the user but rather use the already given Param value
	 * 
	 * @param accuracy the accuracy of MS/MS measurement in dalton (usually 0.02
	 *                 dalton)
	 */
	public AccuracyValuePeakDiscriminationFilter(double accuracy) {
		this.accuracy = accuracy;
	}

	/**
	 * @brief filter peak lists according to the accuracy value (dscriminate if two
	 *        peaks should be merged)
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) {

		// sort peaks by mass and store them in a temporary list for easier
		// handling, and be sure they are ordered by mass
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Double, Double>>() {
			public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
				return (o2.getKey()).compareTo(o1.getKey());
			}
		});

		List<Map.Entry<Double, Double>> filteredlist = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());
		int pos = 0;
		int max = list.size();
		Double previousMass = 0.0;
		Double presentMass;
		for (int i = 0; i < max; ++i) {
			presentMass = list.get(pos).getKey();
			if (presentMass - previousMass >= accuracy) { // since the peaks are sorted on mass no need for
															// a abs() to take place
				filteredlist.add(list.get(pos));
			}
		}

		// and then reconvet to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : filteredlist) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;
	}
}
