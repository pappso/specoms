package filters;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * 
 * @brief This is just for writing down the information of a map of peak into a
 *        file for testing purposes only
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */

public class PeakListMapStorageInFile {

	/**
	 * @brief peak_list_map_storage_in_file constructor
	 * @brief will create a peak_list_map_storage_in_file in order to store data
	 *        from a map of peaks into a file
	 * 
	 * @param _max_peaks maximum number of peaks allowed in a peak list
	 */
	public PeakListMapStorageInFile() {
	}

	public void store_in_file(String file_name, Map<Double, Double> peak_list) {

		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		
		try {
			File myObj = new File(file_name + ".csv");
			if (myObj.createNewFile()) {
				// if the file dose not exist create it
			} else {
				// if not do nothing just open it later
			}
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		try {
			FileWriter myWriter = new FileWriter(file_name + ".csv");
			// write in the file
			for (Map.Entry<Double, Double> entry : peak_list.entrySet()) {
				myWriter.write(entry.getKey() + ";" + entry.getValue() + "\n");
			}
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}
}
