package filters;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief UpFilter filter peak lists by intensity value, will only retrieve a
 *        specified number of them has one information : a maximum of peaks to
 *        put inside a spectrum
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */

public class ParentFilter implements FilterInterface {

	/**
	 * @brief minimum mass to remove peaks that have a mass close to the parent one
	 */
	Double min_mass;

	/**
	 * @brief maximum mass to remove peaks that have a mass close to the parent one
	 */
	Double max_mass;

	/**
	 * @brief ParentFilter constructor
	 * @brief will create a ParentFilter. remove peaks that have amass similar to
	 *        the parent ion
	 *
	 * @param _mass_range     mass range around which remove all peaks closes to the
	 *                        parent ion (plus and minus)
	 * 
	 * @param _parent_mass    the mass of the parent ion
	 * @param neutralLossMass the neutral loss mass, usually H2O (18.01056)
	 * 
	 * @throws IndexOutOfBoundsException : raised if _mass_range < 0 or _parent_mass
	 *                                   <= 0
	 */
	public ParentFilter(Double _mass_range, Double _parent_mass, double neutralLossMass)
			throws IndexOutOfBoundsException {
		if (_mass_range < 0) {
			throw new IndexOutOfBoundsException("You can't have a negative range of mass !");
		}

		if (_parent_mass <= 0) {
			throw new IndexOutOfBoundsException("You can't have a negative  mass !");
		}

		// _parent_mass = ((_parent_mass - (MHPLUS * parent_charge)) * parent_charge) +
		// MHPLUS;
		_parent_mass = _parent_mass - neutralLossMass;

		this.min_mass = _parent_mass - _mass_range;
		this.max_mass = _parent_mass + _mass_range;
	}

	/**
	 * @brief filter the peaks closes (in mass) to the parent ion
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) throws IndexOutOfBoundsException {

		// sort peaks by intensity and store them in a temporary list for easier
		// handling
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());

		// find all peaks that are nor like the parent mass (conserve the ones that have
		// a mass above or beyond the range)
		List<Map.Entry<Double, Double>> filteredlist = new LinkedList<Map.Entry<Double, Double>>();
		for (Map.Entry<Double, Double> peak : list) {
			if (peak.getKey() < this.min_mass || peak.getKey() > this.max_mass) {
				filteredlist.add(peak);
			} else {
				// do nothing
			}
		}

		// and then re-convert to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : filteredlist) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;
	}
}
