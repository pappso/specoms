package filters;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import biodata.Spectrum;

/**
 * @brief rescales spectrum intensities to defined maximum
 * 
 *        look for the most intense peak in spectrum and rescales it to the
 *        chosen maximum value. other peaks are then rescaled according to this
 *        maximum value.
 * 
 * @see FilterInterface
 * 
 * @author Alexandre Bonomo <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */
public class DynamicRangeFilter implements FilterInterface {
	//private static final Logger LOGGER = Logger.getLogger(DynamicRangeFilter.class.getName());
	/**
	 * @brief minimum intensity threshold required in the rescaled spectrum
	 */
	Double min_intensity;

	/**
	 * @brief set the maximal intensity reached by the resulting spectrum
	 */
	Double set_max_intensity;

	/**
	 * @brief DynamicRangeFilter constructor
	 * @brief will create a DynamicRangeFilter. The minimum intensity percentage is
	 *        fixed by the user
	 *
	 * @param set_max_intensity; the maximum intensity for the peak list; all is
	 *                           resized according to it
	 * 
	 * @param _min_intensity     minimum intensity percentage for a peak to be
	 *                           conserved
	 * 
	 * @throws IndexOutOfBoundsException : raised if _min_intensity <= 0
	 */
	public DynamicRangeFilter(Double maximum_intensity, Double intensity_threshold) throws IndexOutOfBoundsException {
		if (intensity_threshold <= 0) {
			throw new IndexOutOfBoundsException("A percentage cannot be negative !");
		}

		this.min_intensity = intensity_threshold;

		this.set_max_intensity = maximum_intensity;
	}

	/**
	 * @brief filter peak lists by intensity percentage value
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) throws IndexOutOfBoundsException {

		// sort peaks by intensity and store them in a temporary list for easier
		// handling
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());

		// find the highest intensity peak
		Double max_int = 0.0;
		for (Map.Entry<Double, Double> Me : list) {
			if (max_int < Me.getValue()) {
				max_int = Me.getValue();
			}
		}

		// select the peaks with an intensity higher or equal than the minimum
		List<Map.Entry<Double, Double>> filteredlist = new LinkedList<Map.Entry<Double, Double>>();
		for (Map.Entry<Double, Double> peak : list) {
			peak.setValue(peak.getValue() * this.set_max_intensity / max_int);
			if (peak.getValue() >= this.min_intensity) {
				filteredlist.add(peak);
			}
		}

		// and then reconvet to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : filteredlist) {
			filteredpeaks.put(entry.getKey(), entry.getValue());

			//LOGGER.log(Level.INFO, "key=" + entry.getKey() + " value=" + entry.getValue());
		}

		return filteredpeaks;
	}
}
