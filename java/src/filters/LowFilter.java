package filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief LowFilter filter peak lists by mass value, will delete all peaks with
 *        a mass under a certain threshold
 * @brief has one information : the mass threshold
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */

public class LowFilter implements FilterInterface {

	/**
	 * @brief mass threshold
	 */
	Double mass_threshold;

	/**
	 * @brief LowFilter constructor
	 * @brief will create a LowFilter. The mass threshold is fixed by the user
	 *
	 * @param _mass_threshold mass threshold under which a peak is discarded
	 * 
	 * @throws IndexOutOfBoundsException : raised if _mass_threshold <= 0
	 */
	public LowFilter(Double _mass_threshold) throws IndexOutOfBoundsException {
		if (_mass_threshold <= 0.0) {
			throw new IndexOutOfBoundsException("a mass cannot be negative");
		}

		this.mass_threshold = _mass_threshold;
	}

	/**
	 * @brief filter peak lists by mass
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by mass
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) throws IndexOutOfBoundsException {

		// store peaks in a temporary list for easier handling
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());

		// Discard peaks that are under the mass limit
		List<Map.Entry<Double, Double>> listFiltered = new LinkedList<Map.Entry<Double, Double>>();
		for (Map.Entry<Double, Double> m : list) {
			if (m.getKey() < this.mass_threshold) {
				// do nothing
			} else {
				listFiltered.add(m);
			}
		}

		// and then reconvet to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : listFiltered) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;
	}
}