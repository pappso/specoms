package filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief UpFilter filter peak lists by intensity value, will only retrieve a
 *        specified number of them
 * @brief has one information : a maximum of peaks to put inside a spectrum
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */

public class UpFilter implements FilterInterface {

	/**
	 * @brief max number of peaks allowed in the spectrum
	 */
	int max_peaks;

	/**
	 * @brief UpFilter constructor
	 * @brief will create an UpFilter. The maximum number of peaks is fixed by the
	 *        user
	 *
	 * @param _max_peaks maximum number of peaks allowed in a peak list
	 * 
	 * @throws IndexOutOfBoundsException : raised if _max_peaks <= 0
	 */
	public UpFilter(int _max_peaks) throws IndexOutOfBoundsException {
		if (_max_peaks <= 0) {
			throw new IndexOutOfBoundsException("You should at least one peak in your spectrums");
		}

		this.max_peaks = _max_peaks;
	}

	/**
	 * @brief filter peak lists by intensity value
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 */
	@Override
	public Map<Double, Double> filter(Map<Double, Double> peakList) throws IndexOutOfBoundsException {

		// sort peaks by intensity and store them in a temporary list for easier
		// handling, and be sure they are ordered by intensity
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Double, Double>>() {
			public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		
		// apply the cutoff of maximum peaks to include inside the spectrum
		list = list.subList(0, Math.min(this.max_peaks, peakList.size()));
		
		// and then reconvet to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : list) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;
	}
}
