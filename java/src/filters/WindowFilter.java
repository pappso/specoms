package filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @brief WindowFilter filter peak lists by intensity value, but not on the
 *        whole peak list but window by window
 * @brief has two information :
 * @brief a maximum of peaks to put inside a spectrum
 * @brief a window size (in mass)
 * 
 * @see FilterInterface
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 * 
 */

public class WindowFilter implements FilterInterface {

	/**
	 * @brief max number of peaks allowed in each window
	 * 
	 */
	int max_peaks;

	/**
	 * @brief size of window (in mass !)
	 */
	Double win_size;

	/**
	 * @brief WindowFilter constructor
	 * @brief will create a WindowFilter. The maximum number of peaks and the window
	 *        size is fixed by the user
	 *
	 * @param _max_peaks maximum number of peaks allowed in a peak list
	 * 
	 * @param _win_size  size of the windows (in mass)
	 * 
	 * @throws IndexOutOfBoundsException if the _max_peaks or the _win_size are
	 *                                   inferior to 0
	 */
	public WindowFilter(int _max_peaks, Double _win_size) throws IndexOutOfBoundsException {
		if (_max_peaks <= 0 || _win_size <= 0) {
			throw new IndexOutOfBoundsException("You should have at least one peak in your spectrums/windows");
		}

		this.max_peaks = _max_peaks;
		this.win_size = _win_size;
	}
	
	/**
	 * @brief filter peak lists by intensity value BUT apply it's filter in multiple
	 *        window of masses
	 * 
	 * @param peakList : list of peaks of a spectrum
	 * 
	 * @return a peakList filtered by intensity
	 * 
	 * @throws IndexOutOfBoundsException if the peak list is empty
	 */
	public Map<Double, Double> filter(Map<Double, Double> peakList) throws IndexOutOfBoundsException {

		// turns the map into a list for easier handling and make sure are sorted by
		// mass
		List<Map.Entry<Double, Double>> list = new LinkedList<Map.Entry<Double, Double>>(peakList.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Double, Double>>() {
			public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
				return (o1.getKey()).compareTo(o2.getKey());
			}
		});

		// cut the peakList in smaller chunks "windows" and, by window, cutoff
		List<Map.Entry<Double, Double>> window = new LinkedList<Map.Entry<Double, Double>>();
		List<Map.Entry<Double, Double>> finalList = new LinkedList<Map.Entry<Double, Double>>();

		Double startMass = 0.0;
		
		for (Map.Entry<Double, Double> i : list) {

			if (startMass + this.win_size >= i.getKey()) {
				window.add(i);
			} else {
				// need to sort the entries by intensity
				Collections.sort(window, new Comparator<Map.Entry<Double, Double>>() {
					public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
						return (o2.getValue()).compareTo(o1.getValue());
					}
				});
				try {
					finalList.addAll(window.subList(0, this.max_peaks));
				} catch (IndexOutOfBoundsException e) {
					finalList.addAll(window);
				}
				startMass = startMass + this.win_size;
				window.clear();
				window.add(i);
			}
		}
		
		Collections.sort(window, new Comparator<Map.Entry<Double, Double>>() {
			public int compare(Map.Entry<Double, Double> o1, Map.Entry<Double, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		try {
			finalList.addAll(window.subList(0, this.max_peaks));
		} catch (IndexOutOfBoundsException e) {
			finalList.addAll(window);
		}

		
		// and then reconvert to a map
		Map<Double, Double> filteredpeaks = new LinkedHashMap<Double, Double>();
		for (Map.Entry<Double, Double> entry : finalList) {
			filteredpeaks.put(entry.getKey(), entry.getValue());
		}

		return filteredpeaks;
	}
}
