package filters;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @brief store data from a mgf file into a map of Intensity keyed by mass
 *        (mzAndIntensity)
 * 
 * @author alexandre <alexandre.bonomo@etu.univ-nantes.fr>
 *
 */

public class Mgfreader {

	private Map<Double, Double> mzAndIntensity;

	private Double parent_ion_mass;

	/**
	 * @brief call to open and store data from a mgf file into a map of Intensity
	 *        keyed by mass (mzAndIntensity)
	 * @param filename : name of the mgf file to open
	 */
	public Mgfreader(String filename) {

		try {

			BufferedReader reader = new BufferedReader(new FileReader(filename));

			// map of intensity, keyed by mass
			Map<Double, Double> mzAndIntensity = new LinkedHashMap<Double, Double>();

			String line;
			
			while ((line = reader.readLine()) != null) {

				String[] parts = line.split(" ", 2);
				if (parts.length == 2) // avoid line without 2 parts
				{
					try {
						Double key = Double.parseDouble(parts[0]);
						Double value = Double.parseDouble(parts[1]);
						mzAndIntensity.put(key, value);
					} catch (java.lang.NumberFormatException e) {
						if (parts[0].contains("PEPMASS=")) {
							// just to know the mass of the parent ion
							this.parent_ion_mass = Double.parseDouble(parts[0].substring(8));
						}
					}
				}
			}

			this.mzAndIntensity = mzAndIntensity;

			reader.close();

		} catch (FileNotFoundException e) {
			System.out.println(filename + "not found !");
		} catch (IOException e) {
			System.out.println(filename + "cannot be closed !");
		}

	}

	/**
	 * @brief retrieve data from the reader
	 * @return return the map of intensity keyed by mass
	 */
	public Map<Double, Double> get_mzAndIntensity() {
		return this.mzAndIntensity;
	}

	/**
	 * @brief retrieve data from the reader
	 * @return return the parent ion mass
	 */
	public Double get_parent_ion_mass() {
		return this.parent_ion_mass;
	}

}