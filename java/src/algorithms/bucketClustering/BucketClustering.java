/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package algorithms.bucketClustering;

import java.util.Arrays;
import biodata.Spectrum;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Map.Entry;
import proteo.Param;

/***
 * Implements a collection of lexicographically sortable buckets (according to
 * the positive integer identifiers they contain).
 * 
 * @author Matthieu David
 * @version 0.1
 */

public class BucketClustering {

	/**
	 * Map of the mass value to the bucket representing this mass value. This
	 * representation is mainly used to fill the data and is then stored into a raw
	 * array and disposed.
	 * 
	 * @since 0.1
	 */
	private Int2ObjectOpenHashMap<Bucket> bucketMap;

	/**
	 * Store the amount of spectra identifiers for further use.
	 * 
	 * @since 0.1
	 */
	private int spectraCount;

	/**
	 * Upper bound on the maximum amount of nodes that have ot be inserted in
	 * SpecTrees. This bound can verylikely be tighten up if necessary to reduce
	 * memory consumption.
	 * 
	 * @since 0.1
	 */
	private int maxElements;

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	private BucketClustering() {
	}

	/**
	 * Initializes and fill the map collection containing the buckets and their
	 * content (spectra identifiers). The spectra identifiers and masses sets are
	 * read from Spectrum objects.
	 * 
	 * @param spectra an array of Spectrum
	 * @return a map of integers to buckets
	 */
	public static BucketClustering clusterSpectra(Spectrum[] spectra) {

		int max_mass = -1;

		BucketClustering clusters = new BucketClustering();
		clusters.bucketMap = new Int2ObjectOpenHashMap<Bucket>();
		clusters.spectraCount = spectra.length;
		clusters.maxElements = 0;

		int[] picList;
		// for each spectrum, the list of masses is fetched and the spectrum identifier
		// is added in a bucket for each mass in the list (such a bucket is created if
		// it does not already exists)
		for (int cpt = 0; cpt < spectra.length; cpt++) {

			picList = spectra[cpt].asList();

			// System.out.println("**************"+picList.length);
			//

			for (int cpt2 = 0; cpt2 < picList.length; ++cpt2) {

				// the upper bound is necessarily at most the number of spectrum identifiers
				// inserted in the collection
				clusters.maxElements = clusters.maxElements + 1;

				if (clusters.bucketMap.containsKey(picList[cpt2])) {
					clusters.bucketMap.get(picList[cpt2]).addItem(spectra[cpt].id());
				} else {
					clusters.bucketMap.put(picList[cpt2], Bucket.createEmptyBucket(picList[cpt2]));
					clusters.bucketMap.get(picList[cpt2]).addItem(spectra[cpt].id());
					if (picList[cpt2] > max_mass) {
						max_mass = picList[cpt2];
					}
				}
			}
		}
		Param.defineHighestMassValue(max_mass);
		return clusters;
	}

	/**
	 * Iterate the buckets present in the map collection and store them into a raw
	 * array of buckets lexicographically sorted based on the buckets content. Erase
	 * the map representation afterwards to spare memory.
	 * 
	 * @return an array of buckets
	 * @since 0.1
	 */
	public Bucket[] asSortedList() {

		Bucket[] bucketList = new Bucket[bucketMap.size()];
		int index = 0;

		ObjectIterator<Int2ObjectMap.Entry<Bucket>> it = bucketMap.int2ObjectEntrySet().iterator();

		Entry pair;
		while (it.hasNext()) {
			pair = it.next();
			bucketList[index] = (Bucket) pair.getValue();

			++index;
		}

		bucketMap = null;

		Arrays.sort(bucketList);
		return bucketList;
	}

	/**
	 * Provides access to the number of buckets present in the collection.
	 * 
	 * @return The size of the collection
	 * @since 0.1
	 */
	public int size() {
		return bucketMap.size();
	}

	/**
	 * Provides access to the estimated upper bound on the number of elements to be
	 * later inserted in SpecTrees.
	 * 
	 * @return the upper bound
	 * @since 0.1
	 */
	public int getElementCount() {
		return maxElements;
	}

	/**
	 * Provides access to the number of spectra.
	 * 
	 * @return the number of spectra
	 * @since 0.1
	 */
	public int spectraCount() {
		return spectraCount;
	}

}
