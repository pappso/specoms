/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package algorithms.bucketClustering;

import java.util.Collections;

import it.unimi.dsi.fastutil.ints.IntArrayList;

/***
 * Contains the identifiers from spectra which possess a mass correspoding to the one associated to the bucket. Once the filling is complete, a bucket must be sorted.
 * Spectra identifiers need to be positive integers to ensure sort correctness.
 * 
 * @author Matthieu David
 * @version 0.1
 */

public class Bucket implements Comparable<Bucket> {

    /**
     * The mass value associated to the bucket.
     * This value cannot be modified.
     * @since 0.1
     */
    private int value;
        
        /**
         * Spectrum identifiers stored in an optimized array to reduce the memory occupation (fastutils).
         * This list can only be filled, sorted or accessed in read-only.
         * @since 0.1
         */
        private IntArrayList itemList;
	
        /**
         * Private constructor to avoid wrong instantiation of the class.
         * @since 0.1
	 */
        private Bucket(){};
	
        /**
         * Bucket creation with appropriated initialisation for the identifiers and the mass value.
         * @param val the mass value associated to the bucket
         * @return b a bucket object properly instanciated
         * @since 0.1
         */
	public static Bucket createEmptyBucket(int val){
            Bucket b = new Bucket();
            b.value = val;
            b.itemList = new IntArrayList();
            return b;
	}
	
        /**
         * Accessor to the mass value associated to the bucket.
         * @return the mass value
         * @since 0.1
         */
	public int getMass(){ return value; }
	
        /**
         * Insertion of a new identifier in the bucket. Insertion happen to the end.
         * @param item The identifier to add in the bucket
         * @since 0.1
         */
	public void addItem(int item){ itemList.add(item); }
	
        /**
         * Sort the identifiers by increasing order
         * @since 0.1
         */
	public void sort(){ Collections.sort(itemList); }
	
        /**
         * Return the content of the bucket under the form of a primitive array of integers.
         * @return the primitive integer array
         * @since 0.1
         */
	public int[] content(){ return this.itemList.toIntArray(); }
	
        /**
         * Return the number of identifiers contained in the bucket.
         * @return the number of identifiers
         * @since 0.1
         */
	public int size(){ return this.itemList.size(); }
	
        /**
         * Provide the identifier present at a given position in the bucket.
         * @param i The position at which is located the identifier
         * @return The identifier
         * @since 0.1
         */
	public int getValue(int i){ return itemList.get(i); }

        /**
         * Implementation of the comparable interface in order to be able to compare two buckets and sort a collection of buckets lexicographically.
         * @param b The bucket to compare with the current one
         * @return -x if the bucket is lexicographically smaller, +x if the bucket is lexicographically bigger and 0 if they have the same content, where x is a non-zero integer.
         * @since 0.1
         */
        @Override
	public int compareTo(Bucket b) {
                
            // The iteration limit is defined by the smallest bucket size
            int lone = this.size();
            int ltwo = b.size();
            int lim = Math.min(lone, ltwo);
                
            int pos = 0;
		
            // Identifiers are compared and iterated as long as they are identical, their difference is returned otherwise    
            while(pos < lim){
		int valOne = this.itemList.get(pos);
		int valTwo = b.getValue(pos);
		
                // This can only work for positive integers stored in the buckets
		if(valOne != valTwo) return valOne - valTwo;
		++pos;
            }
            
            // When the buckets have an equal content until the limit, the shortest one is returned first
            return lone - ltwo;
	}
	
}
