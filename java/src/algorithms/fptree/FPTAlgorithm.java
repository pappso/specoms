/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package algorithms.fptree;

import algorithms.fptree.builder.FPTBuilder;
import algorithms.fptree.extractor.FPTExtractor;

/***
 * Building and exraction algorithms going side by side with the SpecTrees
 * datastructure. They are instanciated at the creation of SpecTrees. Additional
 * algorithms can be developped and paired duplicating/adapting the
 * useFastAlgorithm prodedure. Note : this approach might be completely
 * suboptimal in term of application design depending on further developments.
 * Must be adjusted if necessary.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class FPTAlgorithm {

	/**
	 * Construction algorithm used for SpecTrees.
	 * 
	 * @since 0.1
	 */
	private FPTBuilder builder;

	/**
	 * Extraction algorithm used for SpecTrees.
	 * 
	 * @since 0.1
	 */
	private FPTExtractor extractor;

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	private FPTAlgorithm() {
	} // constructeur évitant l'héritage

	/**
	 * Assignation of a pair of fast and memory efficient default builder and
	 * extractor.
	 * 
	 * @param elCount the maximum number of elements to insert in SpecTrees.
	 * @return the algorithm for SPecTrees
	 * @since 0.1
	 */
	public static FPTAlgorithm useFastAlgorithm(int elCount) {
		FPTAlgorithm algorithm = new FPTAlgorithm();
		algorithm.builder = FPTBuilder.initialize();
		algorithm.extractor = FPTExtractor.initialize(elCount);
		return algorithm;
	}

	/**
	 * Provides access to the builder defined for SpecTrees.
	 * 
	 * @return the builder
	 * @since 0.1
	 */
	public FPTBuilder builder() {
		return this.builder;
	}

	/**
	 * Provides access to the extractor defined for SpecTrees.
	 * 
	 * @return the extractor
	 * @since 0.1
	 */
	public FPTExtractor extractor() {
		return this.extractor;
	}

}
