/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package algorithms.fptree.builder;

import algorithms.bucketClustering.Bucket;
import algorithms.bucketClustering.BucketClustering;
import algorithms.fptree.FPTree;

/***
 * SpecTrees filling algorithm used to minimize the construction time and avoid
 * the memory costly recursion construction generally used while still ensuring
 * all constraints on the properties needed for SpecTrees.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class FPTBuilder {

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	private FPTBuilder() {
	}

	/**
	 * Keep track of the index of the last inserted node for each spectrum
	 * identifier. Used to update the transversal accessor.
	 * 
	 * @since 0.1
	 */
	private int[] last;

	/**
	 * Builder instanciation.
	 * 
	 * @return Ready-to-use builder
	 * @since 0.1
	 */
	public static FPTBuilder initialize() {
		FPTBuilder builder = new FPTBuilder();
		return builder;
	}

	/**
	 * Fills SpecTrees using the data from a bucket clustering. During insertion,
	 * each bucket is compared with the previously inserted, and a slightly
	 * different procedure insertion is applied depending on wether we are in a
	 * prefix common part, or once we went through a first differing element.
	 * 
	 * @param tree       SpecTrees to be filled
	 * @param clustering The bucket clustering to read the data from
	 * @since 0.1
	 */
	public void build(FPTree tree, BucketClustering clustering) {

		// initialisation to the number of distinct spectrum identifiers
		last = new int[clustering.spectraCount()];

		for (int cpt = 0; cpt < clustering.spectraCount(); ++cpt) {
			last[cpt] = tree.NULL();
		}

		// data is fetched from the bucket clustering and the index of the node and its
		// spectrum identifier value are initialised to the first spectrum in the first
		// bucket
		Bucket[] bins = clustering.asSortedList();
		int nodeIndex = 0;
		int nodeValue = bins[0].getValue(0);

		// Specific processing for the first bucket : the first node and all its
		// children are added in SpecTrees and the transversal accessor is updated
		// accordingly
		tree.addNewNode(nodeIndex, tree.NULL(), tree.NULL(), nodeValue, 1);
		manageSideAccess(tree, nodeIndex, nodeValue);
		++nodeIndex;
		for (int cpt = 1; cpt < bins[0].size(); ++cpt) {
			nodeValue = bins[0].getValue(cpt);
			tree.addNewNode(nodeIndex, nodeIndex - 1, tree.NULL(), nodeValue, 1);
			manageSideAccess(tree, nodeIndex, nodeValue);
			++nodeIndex;
		}

		// For the rest of the buckets, we increment the node counter in prefix common
		// parts without creating new nodes.
		// Once out of the prefix common part, new nodes are created similarily as they
		// were for the first bucket.
		for (int cpt = 1; cpt < bins.length; ++cpt) {
			int pos = 0;
			boolean common;
			int parentNode = tree.NULL();

			// detection of a common first element
			if (bins[cpt].getValue(pos) == bins[cpt - 1].getValue(pos)) {
				common = true;
			} else {
				common = false;
			}
			// insertion procedure for a common prefix part insertion : we go through
			// existing nodes and increment their counter value by one
			while (common) {
				nodeValue = bins[cpt].getValue(pos);
				int tempIndex = last[nodeValue];
				tree.incrementCounter(tempIndex);
				// checking on the prefix common part persistence conditions ; if not valid, we
				// are not in a common prefix part anymore for the currently inserted bucket
				if ((bins[cpt - 1].size() - 1 == pos) || (bins[cpt].size() - 1 == pos)) {
					common = false;
				} else {
					if (bins[cpt - 1].getValue(pos + 1) != bins[cpt].getValue(pos + 1)) {
						common = false;
					}
				}
				parentNode = tempIndex;
				++pos;
			}
			// insertion procedure for all the remaining nodes not in the prefix common part
			// : we create new nodes with a counter value of 1 and update the transversal
			// accessor and last inserted node list
			while (pos < bins[cpt].size()) {
				nodeValue = bins[cpt].getValue(pos);
				tree.addNewNode(nodeIndex, parentNode, tree.NULL(), nodeValue, 1);
				manageSideAccess(tree, nodeIndex, nodeValue);
				parentNode = nodeIndex;
				++pos;
				++nodeIndex;
			}
		}
		// once SpecTrees is filled, we free memory from the table keeping the last
		// inserted element list as it is not needed anymore
		last = null;
	}

	/**
	 * Update the transversal accessor of SpecTrees, adding a node index at the end
	 * of the accessor of a given spectrum identifier.
	 * 
	 * @param tree     SpecTrees whose transversal acecssor must be updated
	 * @param position The index to add in the transversal accessor
	 * @param aValue   The spectrum identifier for which the value has to be added
	 * @since 0.1
	 */
	private void manageSideAccess(FPTree tree, int position, int aValue) {
		// The spectrum identifier is added as a first element of the transversal
		// accesor and as the last inserted element if the transversal accessor does not
		// already contain a node index for this given spectrum identifier
		if (tree.first(aValue) == tree.NULL()) {
			tree.addSideAccess(aValue, position);
			last[aValue] = position;
			// Otherwise the node is added after the last inserted node and becomes the last
			// inserted node, on the condition they are not equal (if so, nothing is done)
		} else {
			if (last[aValue] == position) {
			} else {
				tree.updateSideAccess(last[aValue], position);
				last[aValue] = position;
			}
		}
	}

}
