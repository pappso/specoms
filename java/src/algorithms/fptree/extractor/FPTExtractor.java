/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package algorithms.fptree.extractor;

import algorithms.fptree.FPTree;
import biodata.Spectrum;
import biodata.SpectrumShifter;
import tools.RecordHandler;

import javafx.beans.property.SimpleDoubleProperty;
import mvc.Model;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import proteo.Param;
import tools.DataWriter;

/***
 * Fast extraction algorithm to retrieve similarities above a given threshold
 * between multiple sets of spectra while prioritizing small memory footprint
 * and execution speed. The threshold is used to avoid the size of the provided
 * result to be inflated by low significance and very abundant pairs of spectra.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class FPTExtractor {

	/* Gestion progression et annulation */
	/*
	 * Variables added to monitor and act on the service since the loop which is
	 * doing 90% of the run time is written in this class.
	 */

	/*
	 * Incremented each time 1% of the spectra are computed, the progressProperty of
	 * the service listens to it and is incremented as well.
	 */
	private static double totalProgress = 0;
	private static DoubleProperty totalProgressProg = new SimpleDoubleProperty(0);

	/*
	 * Progress written in the standard output.
	 */
	private static String starProgress = "[          ]";

	/*
	 * Two variables for cancellation. isProgramCancelled corresponds to the
	 * cancellation signal given by the button "Cancel". checkCancel is set to true
	 * only when the loop has reached the control passage and the statement break;
	 * has been called.
	 */
	private static SimpleBooleanProperty isProgramCancelled = new SimpleBooleanProperty(false);
	private static SimpleBooleanProperty checkCancel = new SimpleBooleanProperty(false);

	/*
	 * Checks if the condition which breaks the loop has been entered.
	 */
	public static SimpleBooleanProperty getCheckCancel() {
		return checkCancel;
	}

	public static DoubleProperty getProgress() {
		return totalProgressProg;
	}

	/*
	 * Resets all the varaibles involved in the cancellation.
	 */
	public static void progressZero() {
		totalProgress = 0;
		totalProgressProg.set(0);
		isProgramCancelled.set(false);
		checkCancel.set(false); // TODO It worked without, should I remove it ?
	}

	public static SimpleBooleanProperty getCancel() {
		return isProgramCancelled;
	}

	public static void cancelProgram() {
		isProgramCancelled.set(true);
	}
	/* end */

	/**
	 * Aggregate similarities for a given spectrum with all others during one
	 * extraction iteration.
	 * 
	 * @since 0.1
	 */
	private int[] totalExtractedCount;

	/**
	 * Witnesses the use (or not) of a given aggregation counter during the current
	 * extraction iteration by storing the last iteration index used for each
	 * spectrum.
	 * 
	 * @since 0.1
	 */
	private int[] useWitness;

	/**
	 * Stores a list of spectra possessing a similarity with the current considered
	 * spectrum. This array is used as a stack.
	 * 
	 * @since 0.1
	 */
	private int[] commonElements;

	/**
	 * Stack pointer for the commonElements stack.
	 * 
	 * @since 0.1
	 */
	private int elementIndex;

	/**
	 * Stores a list of spectra possessing a similarity above the threshold with the
	 * current considered spectrum. This array contains a subset of the elements
	 * present in commonElements. This array is used as a stack.
	 * 
	 * @since 0.1
	 */
	private int[] elementsAbove;

	/**
	 * Stores the aggregated counter values of the spectra index present in the
	 * elementsAbove array. This array is used as a stack.
	 * 
	 * @since 0.1
	 */
	private int[] aboveThreshold;

	/**
	 * Stack pointer for the elementsAbove and aboveThreshold stacks.
	 * 
	 * @since 0.1
	 */
	private int aboveThresholdIndex;

	/**
	 * Private constructor to avoid wrong instantiation of the class.
	 * 
	 * @since 0.1
	 */
	private FPTExtractor() {
		// System.out.println("c'est extractor");
	}

	// static constructor ; takes in parameter the amount of distinct elements in
	// the tree

	/**
	 * Build and return an extractor with the required structures initialised in
	 * memory with their maximum possible size.
	 * 
	 * @param elCount number of distinct spectra in SpecTrees
	 * @return the ready to use extractor
	 * @since 0.1
	 */
	public static FPTExtractor initialize(int elCount) {

		FPTExtractor ex = new FPTExtractor();

		// memory allocation of the required structures. This time expansive step must
		// be done once and for all and not once for each spectrum for which we want to
		// extract the similarities.
		// the complexity of the extractor implementation is mostly due to this
		// restriction.
		ex.totalExtractedCount = new int[elCount];
		ex.useWitness = new int[elCount];
		ex.commonElements = new int[elCount];
		ex.aboveThreshold = new int[elCount];
		ex.elementsAbove = new int[elCount];

		// useWitness initialised to a normally non-happening value to ensure flawless
		// execution
		for (int cpt = 0; cpt < ex.useWitness.length; ++cpt) {
			ex.useWitness[cpt] = -1;
		}

		return ex;
	}

	/**
	 * Performs the extraction of the similarities above a given threshold for all
	 * spectra between the deepest one and a given index in the transversal
	 * accessor. The extracted pairs are feed to a shifter to try to improve the
	 * spectrum identification. Afterwards, the retained results are written to the
	 * given file. This extraction step represents most of SpecOMS execution time.
	 * Note: This function heavily evolved through the development process to match
	 * new needs, it became clumsy and heavy. A clean refactor might be appropriate.
	 * 
	 * @param tree      SpecTrees from which extract the data
	 * @param spectra   Collection of spectra to provide information to the user
	 * @param threshold The threshold used for the extraction
	 * @param limit     The index in the transversal accessor upt owhich the
	 *                  extraction is to be performed
	 * @param file      File to which the results have to be written
	 * @since 0.1
	 */
	public void extractCount(FPTree tree, Spectrum[] spectra, int threshold, int limit, String file) {
		int countProgress = 0;
		int onePercent = (spectra.length - (limit + 1)) / 100;

		if (!Model.isUI()) {
			System.out.print(starProgress);
			System.out.flush();
			System.out.print("\b\b\b\b\b\b\b\b\b\b\b");
			System.out.flush();
		}

		// File writer and shifter initialisation
		RecordHandler r = new RecordHandler(spectra);

		DataWriter.erasePrevious(file);
		DataWriter.open(file);

		SpectrumShifter shifter = SpectrumShifter.create();

		// initialisation of the structure to store the compared pairs
		int[][] results = new int[tree.count()][3];
		int count = 0;

		// iteration on the transversal accessor starting from the deepest spetrum and
		// climbing up to limit
		for (int start = totalExtractedCount.length - 1; start >= limit; --start) {

			// System.out.println(start);

			// similarity computation for the current spectrum with all others
			extractSpectrumCount(tree, start, threshold);

			// storage of the obtained similarities above the threshold into records that
			// will be written in the file by chunks
			// after recent developemnts, it appears that provigding similarities one by one
			// and writing them later in chunks would be better
			for (int cpt = 0; cpt < elementIndex; ++cpt) {

				// create and register a similarity triple
				int[] temp = new int[3];
				temp[0] = start;
				temp[1] = commonElements[cpt];
				temp[2] = totalExtractedCount[temp[1]];

				// avoid to write the similarity of a spectrum with itself
				if (temp[0] != temp[1]) {
					// count the number of registered triples
					if ((temp[0] >= limit) && (temp[1] < limit)) {
						results[count] = temp;
						++count;
					}
				}

				// write them once they exceed the chunk size, here defined as the number of
				// distinct elements in SpecTrees (arbitrary choice, might be any constant)
				if (count >= tree.count()) {
					count = 0;
					r.addSimilarityValue(results, threshold);

					if (Param.search_mode().equals("projection") || (Param.search_mode().equals("peptides_exact"))
							|| (Param.search_mode().equals("peptides_duplicated"))) {
						r.filter(shifter);
						r.clean_data_buffer();
					}
				}

			}
			// write the last chunk of results
			r.addSimilarityValue(results, threshold, count);

			if (Param.search_mode().equals("projection") || (Param.search_mode().equals("peptides_exact"))
					|| (Param.search_mode().equals("peptides_duplicated"))) {
				r.filter(shifter);
				r.clean_data_buffer();
			}
			if (Param.search_mode().equals("peptides_duplicated")) {
				DataWriter.write_peptides_results(limit);
				DataWriter.close();
				DataWriter.reopen(file);
				DataWriter.clean_data_buffer();

			}
			if (Param.search_mode().equals("spectra")) {
				DataWriter.write_spectra_results(limit);
				DataWriter.close();
				DataWriter.reopen(file);
				DataWriter.clean_data_buffer();
			}

			count = 0;

			if (isProgramCancelled.get()) {
				checkCancel.set(true);
				System.out.println("Cancelled");
				progressZero();
				break;
			}

			++countProgress;
			if (countProgress == onePercent) {
				countProgress = 0;
				++totalProgress;

				// Does not print progress
				if (!Model.isUI()) {
					if (totalProgress % 10 == 0) {
						System.out.print("*");
						// System.out.println(start);
					}

					if (totalProgress == 100) {
						System.out.print("]\n");
						// System.out.println(start);
					}
				}
				totalProgressProg.set(totalProgress / 100);
			}

		}

		DataWriter.close();

	}

	/**
	 * Extraction of the similarities above a given threshold between a given
	 * spectrum and all other spectra higher located in SpecTrees.
	 * 
	 * @param tree      SpecTrees the data has to be extracted from
	 * @param sOne      The spectrum with which we want to extract similarities
	 * @param threshold The threshold to use for the extraction
	 * @since 0.1
	 */
	private void extractSpectrumCount(FPTree tree, int sOne, int threshold) {

		// we position the current node at the first occurence of the spectrum in the
		// transversal accessor and initialise variables accordingly
		int lastLeaf = tree.first(sOne);
		elementIndex = 0;
		aboveThresholdIndex = 0;
		int position;
		int countValue;
		boolean finishedLevel = false;
		boolean finishedBranch;

		// we iterate over all nodes containing this spectrum identifier present in the
		// transversal accessor
		while (!finishedLevel) {
			position = lastLeaf;
			finishedBranch = false;
			// the counter value associated to the current node is memorized before branch
			// climbing
			countValue = tree.count(position);
			// for each node iterated, we climb up to the root of the branch they are in
			while (!finishedBranch) {
				// we add the counter value to the aggregator for all spectra identifiers
				// present in the encountered nodes if the witness tells that they were already
				// used during this iteration
				if (useWitness[tree.value(position)] == sOne) {

					totalExtractedCount[tree.value(position)] += countValue;
					// if at one time, a similarity exceeds the threshold value for the first time,
					// it is written into the proper stack
					if ((countValue >= threshold) && (aboveThreshold[tree.value(position)] == 0)) {
						aboveThreshold[tree.value(position)] = 1;
						elementsAbove[aboveThresholdIndex] = tree.value(position);
						++aboveThresholdIndex;
					}
					// otherwise we first initialise the aggreagated value to 0, update the witness
					// state and then add the counter value
				} else {
					useWitness[tree.value(position)] = sOne;
					totalExtractedCount[tree.value(position)] = countValue;
					// if at one time, a similarity exceeds the threshold value for the first time,
					// it is written into the proper stack
					if (countValue >= threshold) {
						aboveThreshold[tree.value(position)] = 1;
						elementsAbove[aboveThresholdIndex] = tree.value(position);
						++aboveThresholdIndex;
					}
					commonElements[elementIndex] = tree.value(position);
					++elementIndex;
				}
				// moving to the next branch
				position = tree.parent(position);
				if (position == tree.NULL())
					finishedBranch = true;
			}
			// level finished, moving out to call again on the next level if needed
			lastLeaf = tree.next(lastLeaf);
			if (lastLeaf == tree.NULL())
				finishedLevel = true;
		}
	}

}
