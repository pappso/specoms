/* * * * *
* This software is licensed under the Apache 2 license, quoted below.
*
* Copyright 2017 Institut National de la Recherche Agronomique, Université de Nantes.
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations under
* the License.
* * * * */

package algorithms.fptree;

import algorithms.bucketClustering.BucketClustering;
import biodata.Spectrum;

/***
 * SpecTrees datastructure aiming for cheap memory consumption and efficient
 * processing. The following implementation simulates the forest of inverted
 * trees used in SpecTrees along with the transversal accessor using mostly only
 * primitive arrays of integers.
 * 
 * @author Matthieu David
 * @version 0.1
 */
public class FPTree {

	/**
	 * Definition of a null value to represent the absence of further node.
	 * 
	 * @since 0.1
	 */
	private static final int NULL = -1;

	/**
	 * Storage of the index at which are located the data of the first node in
	 * SpecTrees for each spectrum. Let it be j for an example.
	 * 
	 * @since 0.1
	 */
	private int[] first;

	/**
	 * The spectrum index stored in each node.
	 * 
	 * @since 0.1
	 */
	private int[] value;

	/**
	 * The value of the counter stored in each node.
	 * 
	 * @since 0.1
	 */
	private int[] count;

	/**
	 * The index of the parent node for each node in SpecTrees.
	 * 
	 * @since 0.1
	 */
	private int[] parent;

	/**
	 * The index of the next node in the transversal chain node after each node in
	 * SpecTrees.
	 * 
	 * @since 0.1
	 */
	private int[] next;

	/**
	 * Total amount of nodes currently inserted in SpecTrees. Must be smaller to
	 * upper bound at all time.
	 * 
	 * @since 0.1
	 */
	private int totalNodeCount = 0;

	/**
	 * Total amount of different spectra present in SpecTrees.
	 * 
	 * @since 0.1
	 */
	private int elCount;

	/**
	 * Upper bound on the total number of nodes that can possibly be inserted in
	 * SpecTrees.
	 * 
	 * @since 0.1
	 */
	private int maxNode;

	/**
	 * The pair of algorithms used to build SpecTrees and extract the data. They are
	 * defined at the initialisation of SpecTrees.
	 * 
	 * @since 0.1
	 */
	FPTAlgorithm algorithm; // the pair of algorithms (builder/extractor) used to process the FPTree

	/**
	 * Initialisation of SpecTrees. The proper of algorithms is set and SpecTrees
	 * structures are then initialised to their maximum possible size. SpecTrees is
	 * finally built (the structures are filled) using the previously chosen
	 * algorithm.
	 * 
	 * @param clustering The bucket clustering in which spectra identifiers have
	 *                   been sorted according to their masses
	 * @param algorithm  The pair of algorithms used to build and extract data from
	 *                   SpecTrees
	 * @return Built and filled SpecTrees datastructure
	 */
	public static FPTree buildTree(BucketClustering clustering, FPTAlgorithm algorithm) {

		// properties initialisation from the parameters
		FPTree newTree = new FPTree();
		newTree.algorithm = algorithm;
		newTree.elCount = clustering.spectraCount();
		newTree.maxNode = clustering.getElementCount();

		// datastructures memory allocation and initialisation to the NULL-defined value
		newTree.first = new int[newTree.elCount];
		for (int cpt = 0; cpt < newTree.elCount; ++cpt) {
			newTree.first[cpt] = NULL;
		}

		newTree.count = new int[newTree.maxNode];
		newTree.parent = new int[newTree.maxNode];
		newTree.next = new int[newTree.maxNode];
		newTree.value = new int[newTree.maxNode];

		for (int cpt = 0; cpt < newTree.maxNode; ++cpt) {
			newTree.count[cpt] = NULL;
			newTree.parent[cpt] = NULL;
			newTree.next[cpt] = NULL;
			newTree.value[cpt] = NULL;
		}

		// Empty SpecTrees datastructure given to the builder to be filled
		newTree.algorithm.builder().build(newTree, clustering);
		return newTree;
	}

	/**
	 * Change the value of the parent node index for a given node position.
	 * 
	 * @param nodeIndex   The index of the node whose parent has to be changed
	 * @param parentIndex The new parent index to assign
	 * @since 0.1
	 */
	public void updateParent(int nodeIndex, int parentIndex) {
		parent[nodeIndex] = parentIndex;
	}

	/**
	 * Change the value of the next node index for a given node position.
	 * 
	 * @param nodeIndex The index of the node whose next node has to be changed
	 * @param nextIndex The new next node index to assign
	 * @since 0.1
	 */
	public void updateSideAccess(int nodeIndex, int nextIndex) {
		next[nodeIndex] = nextIndex;
	}

	/**
	 * Insertion of a starting element in the transversal accessor.
	 * 
	 * @param position The position in the transversal accessor at which to insert
	 *                 the first element
	 * @param value    The element index to be inserted in the accessor
	 * @since 0.1
	 */
	public void addSideAccess(int position, int value) {
		first[position] = value;
	}

	/**
	 * Insertion of a new node in the datastructure. The array structures are
	 * properly filled with indexing information concerning the newly inserted node;
	 * 
	 * @param position The position of the new node index values in each array
	 * @param aParent  The index of the parent node
	 * @param aNext    The index of the next node in the transversal accessor
	 * @param aValue   The spectrum index associated to the inserted node
	 * @param aCount   The counter value associated to the insterted node
	 * @since 0.1
	 */
	public void addNewNode(int position, int aParent, int aNext, int aValue, int aCount) {
		// updating the number of inserted nodes
		++totalNodeCount;
		// indexing information update
		parent[position] = aParent;
		next[position] = aNext;
		value[position] = aValue;
		count[position] = aCount;
	}

	/**
	 * Increment of the counter value associated to a given node index.
	 * 
	 * @param position The index of the node whose counter value must be incremented
	 * @since 0.1
	 */
	public void incrementCounter(int position) {
		this.count[position] = this.count[position] + 1;
	}

	/**
	 * Extract all similarities above a threshold value between all spectra from the
	 * deepest one up to the given limit using the predefined extraction algorithm.
	 * The results are stored in the mentionned file.
	 * 
	 * @param spectra   The spectra data used to complement information returned to
	 *                  the user
	 * @param threshold The minimum threshold for a similarity to be reported
	 * @param limit     The position in the transversal accessor up to which the
	 *                  similarities must be reported
	 * @param file      The file in which the results must be stored
	 */
	public void extract(Spectrum[] spectra, int threshold, int limit, String file) {
		this.algorithm.extractor().extractCount(this, spectra, threshold, limit, file);
	}

	/**
	 * Return the spectrum identifier contained in a node given its index.
	 * 
	 * @param nodeIndex The index of the node to querry
	 * @return spectrum identifier
	 * @since 0.1
	 */
	public int value(int nodeIndex) {
		return this.value[nodeIndex];
	}
	// renvoie le compteur associé au node à la position id

	/**
	 * Return the counter value contained in a node given its index.
	 * 
	 * @param nodeIndex The index of the node to querry
	 * @return the counter value
	 * @since 0.1
	 */
	public int count(int nodeIndex) {
		return this.count[nodeIndex];
	}

	/**
	 * Return the parent index contained in a node given its index.
	 * 
	 * @param nodeIndex The index of the node to querry
	 * @return the parent index
	 * @since 0.1
	 */
	public int parent(int nodeIndex) {
		return this.parent[nodeIndex];
	}

	/**
	 * Return the next node index contained in a node given its index.
	 * 
	 * @param nodeIndex The index of the node to querry
	 * @return the next node index
	 * @since 0.1
	 */
	public int next(int nodeIndex) {
		return this.next[nodeIndex];
	}

	// renvoie la capacité totale de l'arbre

	/**
	 * Return the maximum capacity of SpecTrees (upper bound).
	 * 
	 * @return the maximum capacity
	 * @since 0.1
	 */
	public int capacity() {
		return this.maxNode;
	}

	/**
	 * Returns the current filling rate of SpecTrees (number of nodes inserted).
	 * 
	 * @return the current number of nodes in SpecTrees
	 * @since 0.1
	 */
	public int size() {
		return this.totalNodeCount;
	}

	/**
	 * Return the number of distinct spectra stored in SpecTrees.
	 * 
	 * @return then umber of discint spectra in SpecTrees
	 * @since 0.1
	 */
	public int count() {
		return this.elCount;
	}

	/**
	 * Return the NULL-defined value for the current instance of SpecTrees.
	 * 
	 * @return current NULL-defined value
	 * @since 0.1
	 */
	public int NULL() {
		return FPTree.NULL;
	}

	/**
	 * Returns the index of the first node in the transversal accessor for a given
	 * spectrum identifier.
	 * 
	 * @param identifier The spectrum identifier
	 * @return the first node index
	 * @since 0.1
	 */
	public int first(int identifier) {
		return this.first[identifier];
	}

}
