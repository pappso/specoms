/**
 * \file utils/monitorcli.cpp
 * \date 7/11/2018
 * \author Olivier Langella
 * \brief monitor for command line interface
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "monitorcli.h"

#include <QObject>

MonitorCli::MonitorCli(QTextStream &cout) : m_out(cout)
{
  m_maxValue        = 100;
  m_currentProgress = 0;
}

void
MonitorCli::message(const QString &message)
{
  if(m_currentProgress > 0)
    {
      m_out << Qt::endl;
      m_currentProgress = 0;
    }
  m_out << message << Qt::endl;
  m_out.flush();
}


void
MonitorCli::finished(const QString &message)
{
  if(m_currentProgress > 0)
    {
      m_out << Qt::endl;
      m_currentProgress = 0;
    }
  m_out << message << Qt::endl;
  m_out.flush();
}

void
MonitorCli::messagePercent(const QString &message [[maybe_unused]],
                           std::size_t value)
{
  QMutexLocker locker(&m_mutex);
  // m_out << m_currentProgress << endl;
  int percent = ((float)value / (float)m_maxValue) * (float)100;
  if(m_currentProgress < percent)
    {
      int n = percent - m_currentProgress;
      // m_out << m_maxValue << " " << value << " " << percent << " " << n <<
      // endl;

      for(int i = 0; i < n; i++)
        {
          m_out << "*";
        }
      m_out.flush();
      m_currentProgress += n;
    }
}

void
MonitorCli::setProgressMaximumValue(std::size_t max_value)
{
  m_maxValue        = max_value;
  m_currentProgress = 0;
}
void
MonitorCli::appendText(const QString &text)
{
  if(m_currentProgress > 0)
    {
      m_out << Qt::endl;
      m_currentProgress = 0;
    }
  m_out << text << Qt::endl;
  m_out.flush();
}

void
MonitorCli::setStatus(const QString &text)
{
  if(m_currentProgress > 0)
    {
      m_out << Qt::endl;
      m_currentProgress = 0;
    }
  m_out << text << Qt::endl;
  m_out.flush();
}

void
MonitorCli::finishedMonitoredOperation(MonitoredOperation operation)
{
  if(m_currentProgress > 0)
    {
      m_out << Qt::endl;
      m_currentProgress = 0;
    }
  m_out << QObject::tr("%1 took %2 seconds")
             .arg(getOperationName(operation))
             .arg(m_mapQtimeOperations.at(operation).elapsed() / 1000)
        << Qt::endl;
  m_out.flush();
}
