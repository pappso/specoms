/**
 * \file utils/decisionbox.h
 * \date 16/11/2019
 * \author Olivier Langella
 * \brief handle priorities between peptide candidates to choose the best
 * solution
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <pappsomspp/mzrange.h>
#include "../core/peptide_database/peptidedatabase.h"

class SpectrumMatcher;
class PeptideCandidate;
class DeepProtParams;

/**
 * @todo write docs
 */
class DecisionBox
{
  friend DeepProtParams;

  public:
  /**
   * Default constructor
   */
  DecisionBox();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  DecisionBox(const DecisionBox &other);

  /**
   * Destructor
   */
  ~DecisionBox();


  void decideForPeptideCandidate(SpectrumMatcher *p_spectrumMatcher,
                                 PeptideCandidate &peptide_candidate) const;

  void setPeptideDatabaseSPtr(PeptideDatabaseSPtr sp_peptideDatabase);

  protected:
  void setDeepProtParams(const DeepProtParams &params);


  private:
  unsigned int m_minimum_similarity;
  double m_minimum_mass_delta = -500;
  pappso::PrecisionPtr m_ms1_precision;
  unsigned int m_maximum_missed_cleavage;
  std::vector<int> m_matchTypePriority;

  PeptideDatabaseSPtr msp_peptideDatabase;
};
