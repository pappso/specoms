/**
 * \file utils/utils.h
 * \date 11/04/2019
 * \author Olivier Langella
 * \brief deepprot utilities
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "types.h"
#include <pappsomspp/trace/trace.h>
#include "../output/odsoutput.h"
#include "../output/mzidentml.h"
#include "../output/mzidentmlallcandidates.h"

class Utils
{
  public:
  static std::size_t doubleMassToIntegerMass(double mass,
                                             unsigned int accuracy_factor);
  static std::size_t doubleMassRoundToIntegerMass(double mass,
                                                  unsigned int accuracy_factor);

  static std::vector<std::size_t>
  TraceToIntegerSpectrum(const pappso::Trace &mass_spectrum,
                         unsigned int accuracy_factor);

  static QString
  IntegerSpectrumToString(std::vector<std::size_t> integer_spectrum);

  static QString EnumToString(pappso::DeepProtMatchType);
  static QString EnumToString(pappso::DeepProtPeptideCandidateStatus);

  static OutputInterface *newOutputInterface(QString outputFileStr,
                                             bool all_candidates);

  static std::vector<std::size_t>
  getMapKeys(const std::map<std::size_t, std::size_t> &map_table);
  static std::vector<std::size_t>
  getMapValues(const std::map<std::size_t, std::size_t> &map_table);


  /** @brief expand integer spectrum
   * it adds flanking values for each peak, depending on the mass accuracy
   * measurement needed to allow match on neighboring masses
   * @param integer_spectrum mass list to expand
   * @param accuracy accuracy value to use
   */
  static void expandIntegerSpectrum(std::vector<std::size_t> &integer_spectrum,
                                    unsigned int accuracy);
};
