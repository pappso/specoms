/**
 * \file utils/deepprotparams.cpp
 * \date 15/10/2018
 * \author Olivier Langella
 * \brief store DeepProt parameters
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QFile>
#include <QDebug>
#include <QFileInfo>
#include <cmath>
#include <odsstream/odsexception.h>
#include <odsstream/odsdocreader.h>
#include <odsstream/tsvreader.h>
#include <odsstream/odsexception.h>
#include <pappsomspp/types.h>
#include <pappsomspp/pappsoexception.h>
#include "utils.h"

#include "deepprotparams.h"

DeepProtParams *DeepProtParams::mp_instance = nullptr;

class OdsParamHandler : public OdsDocHandlerInterface
{
  public:
  OdsParamHandler(DeepProtParams *p_params)
  {
    mp_params = p_params;
  };

  bool
  readOk() const
  {
    return m_isParams;
  };
  /**
   * callback that indicates the begining of a data sheet. Override it in
   * order to retrieve information about the current data sheet.
   *
   */
  virtual void
  startSheet(const QString &sheet_name [[maybe_unused]])
  {
    m_paramName = "";
  };

  /**
   * callback that indicates the end of the current data sheet. Override it if
   * needed
   */
  virtual void
  endSheet()
  {
    qDebug() << "endSheet";
  };

  /**
   * callback that indicates a new line start. Override it if needed.
   */

  virtual void
  startLine()
  {
    m_paramName = "";
  };

  /**
   * callback that indicates a line ending. Override it if needed.
   */

  virtual void
  endLine()
  {
    m_paramName = "";
  };

  /**
   * callback that report the content of the current cell in a dedicated Cell
   * object. Override it if you need to retrieve cell content.
   */
  virtual void
  setCell(const OdsCell &cell)
  {
    qDebug() << cell.toString();
    if(m_paramName.isEmpty())
      {
        if(cell.isString())
          {
            m_paramName = cell.getStringValue();
            if(m_paramName == "threshold")
              {
                m_isParams = true;
              }
          }
      }

    else
      {
        if(cell.isEmpty())
          {
            mp_params->setEmptyValue(m_paramName);
          }
        else
          {
            mp_params->setValueByName(m_paramName, cell);
          }
        m_paramName = "";
      }
    // qDebug() << " " << cell.toString();
  };

  /**
   * callback that report the end of the ODS document. Override it if you need
   * to know that reading is finished.
   */
  virtual void endDocument(){};

  private:
  DeepProtParams *mp_params;
  QString m_paramName;

  bool m_isParams = false;
};


DeepProtParams &
DeepProtParams::getInstance()
{
  if(DeepProtParams::mp_instance == nullptr)
    {
      DeepProtParams::mp_instance = new DeepProtParams();
    }
  return *DeepProtParams::mp_instance;
}

DeepProtParams::DeepProtParams()
{

  // AccuracyDalton=1,///< double accuracy = 0.02;

  // AccuracyDecimals=2,///< int accuracy_decimals = 2;
  m_params[(unsigned int)DeepProtParam::AccuracyDecimals] = QVariant(2);
  m_names[(unsigned int)DeepProtParam::AccuracyDecimals]  = "numberOfDecimals";

  // AccuracyValue=3,///< int accuracy_value = 2;
  m_params[(unsigned int)DeepProtParam::AccuracyValue] = QVariant(2);
  m_names[(unsigned int)DeepProtParam::AccuracyValue]  = "decimalValue";

  // accuracy_factor = ( int ) Math.pow ( 10, accuracy_decimals );

  m_params[(unsigned int)DeepProtParam::MinimalPeptideCharge] = QVariant(1);
  m_names[(unsigned int)DeepProtParam::MinimalPeptideCharge] =
    "minimumPeptideCharge";

  m_params[(unsigned int)DeepProtParam::MaximalPeptideCharge] = QVariant(3);
  m_names[(unsigned int)DeepProtParam::MaximalPeptideCharge] =
    "maximumPeptideCharge";

  m_params[(unsigned int)DeepProtParam::SpectrumRemoveIsotope] =
    QVariant(false);
  m_types[(unsigned int)DeepProtParam::SpectrumRemoveIsotope] =
    DeepProtParamType::boolean;
  m_names[(unsigned int)DeepProtParam::SpectrumRemoveIsotope] = "removeIsotope";

  m_params[(unsigned int)DeepProtParam::SpectrumExcludeParentIon] =
    QVariant(false);
  m_types[(unsigned int)DeepProtParam::SpectrumExcludeParentIon] =
    DeepProtParamType::boolean;

  m_params[(unsigned int)DeepProtParam::SpectrumExcludeParentLowerDalton] =
    QVariant(2.0);
  m_params[(unsigned int)DeepProtParam::SpectrumExcludeParentUpperDalton] =
    QVariant(2.0);

  m_params[(unsigned int)DeepProtParam::SpectrumNmostIntense] = QVariant(50);
  m_names[(unsigned int)DeepProtParam::SpectrumNmostIntense] = "maxMassesCount";


  m_params[(unsigned int)DeepProtParam::SpectrumWindowFilterWidth] =
    QVariant(0);
  m_names[(unsigned int)DeepProtParam::SpectrumWindowFilterWidth] =
    "windowFilterWidth";
  m_types[(unsigned int)DeepProtParam::SpectrumWindowFilterWidth] =
    DeepProtParamType::number;

  m_params[(unsigned int)DeepProtParam::SpectrumWindowFilterMax] = QVariant(0);
  m_names[(unsigned int)DeepProtParam::SpectrumWindowFilterMax] =
    "windowFilterMax";
  m_types[(unsigned int)DeepProtParam::SpectrumWindowFilterMax] =
    DeepProtParamType::integer;

  m_params[(unsigned int)DeepProtParam::SpectrumComplementIonEnhancer] =
    QVariant(false);
  m_names[(unsigned int)DeepProtParam::SpectrumComplementIonEnhancer] =
    "complementIonEnhancer";
  m_types[(unsigned int)DeepProtParam::SpectrumComplementIonEnhancer] =
    DeepProtParamType::boolean;


  m_params[(unsigned int)DeepProtParam::SpectrumDynamicRange] = QVariant(100);
  m_names[(unsigned int)DeepProtParam::SpectrumDynamicRange] =
    "spectrumDynamicRange";

  m_params[(unsigned int)DeepProtParam::SpectrumExcludeParentIonNeutralLoss] =
    QVariant(false);
  m_types[(unsigned int)DeepProtParam::SpectrumExcludeParentIonNeutralLoss] =
    DeepProtParamType::boolean;

  m_params[(unsigned int)DeepProtParam::SpectrumNeutralLossMass] =
    QVariant(pappso::MASSH2O);
  m_types[(unsigned int)DeepProtParam::SpectrumNeutralLossMass] =
    DeepProtParamType::number;

  m_params[(unsigned int)DeepProtParam::SpectrumNeutralLossMassWindowDalton] =
    QVariant(0.5);
  m_types[(unsigned int)DeepProtParam::SpectrumNeutralLossMassWindowDalton] =
    DeepProtParamType::number;


  m_params[(unsigned int)DeepProtParam::ProteinReverse] = QVariant(false);
  m_types[(unsigned int)DeepProtParam::ProteinReverse] =
    DeepProtParamType::boolean;
  m_names[(unsigned int)DeepProtParam::ProteinReverse] = "decoyBase";

  m_params[(
    unsigned int)DeepProtParam::PeptideDigestionNumberOfMissedCleavages] =
    QVariant(0);
  m_names[(
    unsigned int)DeepProtParam::PeptideDigestionNumberOfMissedCleavages] =
    "nbMissCleavage";

  m_params[(unsigned int)DeepProtParam::PeptideDigestionPeptideMinimumSize] =
    QVariant(7);
  m_names[(unsigned int)DeepProtParam::PeptideDigestionPeptideMinimumSize] =
    "minimumPeptideLength";

  m_params[(unsigned int)DeepProtParam::PeptideDigestionPeptideMaximumSize] =
    QVariant(25);
  m_names[(unsigned int)DeepProtParam::PeptideDigestionPeptideMaximumSize] =
    "maximumPeptideLength";

  m_params[(unsigned int)DeepProtParam::PeptideDigestionFixedModifications] =
    QVariant("MOD:00397@C");
  m_types[(unsigned int)DeepProtParam::PeptideDigestionFixedModifications] =
    DeepProtParamType::string;
  m_names[(unsigned int)DeepProtParam::PeptideDigestionFixedModifications] =
    "fixedModifications";

  m_params[(unsigned int)DeepProtParam::PeptideDigestionVariableModifications] =
    QVariant("MOD:00719@M");
  m_types[(unsigned int)DeepProtParam::PeptideDigestionVariableModifications] =
    DeepProtParamType::string;
  m_names[(unsigned int)DeepProtParam::PeptideDigestionVariableModifications] =
    "variableModifications";

  m_params[(
    unsigned int)DeepProtParam::PeptideDigestionPotentialNterCyclisation] =
    QVariant(false);
  m_types[(
    unsigned int)DeepProtParam::PeptideDigestionPotentialNterCyclisation] =
    DeepProtParamType::boolean;

  m_params[(
    unsigned int)DeepProtParam::ProteinDigestionPotentialNterAcetylation] =
    QVariant(false);
  m_types[(
    unsigned int)DeepProtParam::ProteinDigestionPotentialNterAcetylation] =
    DeepProtParamType::boolean;

  m_params[(
    unsigned int)DeepProtParam::ProteinDigestionPotentialMethioninRemoval] =
    QVariant(false);
  m_types[(
    unsigned int)DeepProtParam::ProteinDigestionPotentialMethioninRemoval] =
    DeepProtParamType::boolean;

  m_params[(unsigned int)DeepProtParam::SpectrumLowerMzLimit] = QVariant(150.0);
  m_types[(unsigned int)DeepProtParam::SpectrumLowerMzLimit] =
    DeepProtParamType::number;
  m_names[(unsigned int)DeepProtParam::SpectrumLowerMzLimit] =
    "spectrumLowerMzLimit";

  m_params[(unsigned int)DeepProtParam::SpectrumUpperMzLimit] =
    QVariant(1500.0);
  m_types[(unsigned int)DeepProtParam::SpectrumUpperMzLimit] =
    DeepProtParamType::number;
  m_names[(unsigned int)DeepProtParam::SpectrumUpperMzLimit] =
    "spectrumUpperMzLimit";

  m_params[(unsigned int)DeepProtParam::SpectrumModelIonTypeList] =
    QVariant("y b");
  m_types[(unsigned int)DeepProtParam::SpectrumModelIonTypeList] =
    DeepProtParamType::string;
  m_names[(unsigned int)DeepProtParam::SpectrumModelIonTypeList] =
    "modeledIons";


  m_params[(
    unsigned int)DeepProtParam::ExperimentalSpectrumModelMinimumNumberOfPeaks] =
    10;
  m_types[(unsigned int)DeepProtParam::SpectrumUpperMzLimit] =
    DeepProtParamType::integer;
  m_names[(
    unsigned int)DeepProtParam::ExperimentalSpectrumModelMinimumNumberOfPeaks] =
    "spectrumMinimumNumberOfPeaks";


  m_params[(unsigned int)DeepProtParam::SpecXtractMinimumSimilarity] = 6;
  m_names[(unsigned int)DeepProtParam::SpecXtractMinimumSimilarity] =
    "threshold";

  m_params[(unsigned int)DeepProtParam::SpecFitMinimumSimilarity] = 8;
  m_names[(unsigned int)DeepProtParam::SpecFitMinimumSimilarity] =
    "minimumScore";
  m_types[(unsigned int)DeepProtParam::SpecFitMinimumSimilarity] =
    DeepProtParamType::number;

  m_params[(unsigned int)DeepProtParam::SpecFitMinimalMassDelta] =
    QVariant(-500.0);
  m_names[(unsigned int)DeepProtParam::SpecFitMinimalMassDelta] =
    "minimalMassDelta";
  m_types[(unsigned int)DeepProtParam::SpecFitMinimalMassDelta] =
    DeepProtParamType::number;


  m_params[(unsigned int)DeepProtParam::ParentIonMassTolerancePrecision] =
    QVariant(10.0);
  m_types[(unsigned int)DeepProtParam::ParentIonMassTolerancePrecision] =
    DeepProtParamType::number;
  m_names[(unsigned int)DeepProtParam::ParentIonMassTolerancePrecision] =
    "parentIonMassTolerance";

  m_params[(unsigned int)DeepProtParam::ParentIonMassToleranceUnit] =
    QVariant("ppm");
  m_types[(unsigned int)DeepProtParam::ParentIonMassToleranceUnit] =
    DeepProtParamType::string;
  m_names[(unsigned int)DeepProtParam::ParentIonMassToleranceUnit] =
    "parentIonMassToleranceUnit";


  m_params[(unsigned int)DeepProtParam::MorpheusScoreIonTypeList] =
    QVariant("y b");
  m_types[(unsigned int)DeepProtParam::MorpheusScoreIonTypeList] =
    DeepProtParamType::string;

  m_params[(unsigned int)DeepProtParam::MsmsPrecision] = QVariant(0.02);
  m_types[(unsigned int)DeepProtParam::MsmsPrecision] =
    DeepProtParamType::number;
  m_names[(unsigned int)DeepProtParam::MsmsPrecision] = "msmsTolerance";

  m_params[(unsigned int)DeepProtParam::MsmsPrecisionUnit] = QVariant("dalton");
  m_types[(unsigned int)DeepProtParam::MsmsPrecisionUnit] =
    DeepProtParamType::string;
  m_names[(unsigned int)DeepProtParam::MsmsPrecisionUnit] = "msmsToleranceUnit";


  m_params[(unsigned int)DeepProtParam::NoSpectrumProcessing] = QVariant(false);
  m_types[(unsigned int)DeepProtParam::NoSpectrumProcessing] =
    DeepProtParamType::boolean;
  m_names[(unsigned int)DeepProtParam::NoSpectrumProcessing] =
    "NoSpectrumProcessing";


  m_types[(unsigned int)DeepProtParam::PeptideDigestionEnzymePattern] =
    DeepProtParamType::string;
  m_params[(unsigned int)DeepProtParam::PeptideDigestionEnzymePattern] =
    QVariant("([KR])([^P])");
  m_names[(unsigned int)DeepProtParam::PeptideDigestionEnzymePattern] =
    "enzymePattern";


  m_params[(unsigned int)DeepProtParam::PeptideMissedCleavageInSpecTree] =
    QVariant(false);
  m_types[(unsigned int)DeepProtParam::PeptideMissedCleavageInSpecTree] =
    DeepProtParamType::boolean;
  m_names[(unsigned int)DeepProtParam::PeptideMissedCleavageInSpecTree] =
    "missDB";


  m_params[(unsigned int)DeepProtParam::SpectrumFilterChargeDeconvolution] =
    QVariant(false);
  m_names[(unsigned int)DeepProtParam::SpectrumFilterChargeDeconvolution] =
    "spectrumFilterChargeDeconvolution";
  m_types[(unsigned int)DeepProtParam::SpectrumFilterChargeDeconvolution] =
    DeepProtParamType::boolean;
}


// accuracy = ( double ) accuracy_value/accuracy_factor;
pappso::pappso_double
DeepProtParams::getAccuracyDalton() const
{
  pappso::pappso_double dalton;
  dalton = get(DeepProtParam::AccuracyValue).toDouble() / getAccuracyFactor();

  return dalton;
}

unsigned int
DeepProtParams::getAccuracyFactor() const
{
  pappso::pappso_double factor =
    std::pow(10, get(DeepProtParam::AccuracyDecimals).toInt());

  return factor;
}


const QVariant &
DeepProtParams::get(DeepProtParam param) const
{
  return m_params[(unsigned int)param];
}


std::vector<pappso::PeptideIon>
DeepProtParams::getIonTypeList(DeepProtParam param) const
{

  std::vector<pappso::PeptideIon> ion_list;
  if((param == DeepProtParam::MorpheusScoreIonTypeList) ||
     (param == DeepProtParam::SpectrumModelIonTypeList))
    {
      QStringList ion_str_list =
        get(DeepProtParam::SpectrumModelIonTypeList).toString().split(" ");
      for(const QString &str : ion_str_list)
        {
          if(str == "b")
            {
              // b=0, ///< Nter acylium ions
              ion_list.push_back(pappso::PeptideIon::b);
            }
          if(str == "bstar")
            {
              //    bstar=1, ///< Nter acylium ions + NH3 loss
              ion_list.push_back(pappso::PeptideIon::bstar);
            }

          if(str == "bo")
            {
              // bo=2, ///< Nter acylium ions + H2O loss
              ion_list.push_back(pappso::PeptideIon::bo);
            }
          if(str == "a")
            {
              // a=3, ///< Nter aldimine ions
              ion_list.push_back(pappso::PeptideIon::a);
            }

          if(str == "astar")
            {
              // astar=4, ///< Nter aldimine ions + NH3 loss
              ion_list.push_back(pappso::PeptideIon::astar);
            }
          if(str == "ao")
            {
              // ao=5, ///< Nter aldimine ions + H2O loss
              ion_list.push_back(pappso::PeptideIon::ao);
            }
          if(str == "bp")
            {
              // bp=6,
              ion_list.push_back(pappso::PeptideIon::bp);
            }

          if(str == "c")
            {
              // c=7, ///< Nter amino ions
              ion_list.push_back(pappso::PeptideIon::c);
            }
          if(str == "y")
            {
              // y=8, ///< Cter amino ions
              ion_list.push_back(pappso::PeptideIon::y);
            }
          if(str == "ystar")
            {
              // ystar=9, ///< Cter amino ions + NH3 loss
              ion_list.push_back(pappso::PeptideIon::ystar);
            }
          if(str == "yo")
            {
              // yo=10, ///< Cter amino ions + H2O loss
              ion_list.push_back(pappso::PeptideIon::yo);
            }
          if(str == "z")
            {
              // z=11, ///< Cter carbocations
              ion_list.push_back(pappso::PeptideIon::z);
            }
          if(str == "yp")
            {
              // yp=12,
              ion_list.push_back(pappso::PeptideIon::yp);
            }
          if(str == "x")
            {
              // x=13 ///< Cter acylium ions
              ion_list.push_back(pappso::PeptideIon::x);
            }
        }
    }

  return ion_list;
}

pappso::PrecisionPtr
DeepProtParams::getMsPrecision() const
{
  if(get(DeepProtParam::ParentIonMassToleranceUnit) == "ppm")
    {
      return pappso::PrecisionFactory::getPpmInstance(
        get(DeepProtParam::ParentIonMassTolerancePrecision).toDouble());
    }
  return pappso::PrecisionFactory::getDaltonInstance(
    get(DeepProtParam::ParentIonMassTolerancePrecision).toDouble());
}

pappso::PrecisionPtr
DeepProtParams::getMsmsPrecision() const
{
  if(get(DeepProtParam::MsmsPrecisionUnit) == "ppm")
    {
      return pappso::PrecisionFactory::getPpmInstance(
        get(DeepProtParam::MsmsPrecision).toDouble());
    }
  return pappso::PrecisionFactory::getDaltonInstance(
    get(DeepProtParam::MsmsPrecision).toDouble());
}


void
DeepProtParams::load(const QString &ods_file_str)
{
  qDebug();
  QFile file(ods_file_str);
  if(!file.exists())
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR reading ODS parameter file\n%1 does not exists")
          .arg(QFileInfo(ods_file_str).absoluteFilePath()));
    }
  OdsParamHandler handler(this);

  try
    {
      if(QFileInfo(ods_file_str).suffix() == "ods")
        {
          OdsDocReader reader(handler);
          reader.parse(file);
          file.close();
        }
      else
        { // try txt file
          qDebug() << "reading tsv file";
          TsvReader reader_tsv(handler);
          reader_tsv.parse(file);
          file.close();
        }
    }
  catch(OdsException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR reading parameter file %1 : %2")
          .arg(QFileInfo(ods_file_str).absoluteFilePath())
          .arg(error.qwhat()));
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR reading parameter file %1 : %2")
          .arg(QFileInfo(ods_file_str).absoluteFilePath())
          .arg(error.qwhat()));
    }


  if(!handler.readOk())
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR reading parameter file %1 : no parameters in file")
          .arg(QFileInfo(ods_file_str).absoluteFilePath()));
    }

  qDebug();
}

void
DeepProtParams::save(CalcWriterInterface &writer) const
{
  qDebug();
  try
    {
      writer.writeSheet("parameters");

      writer.writeCell("Date");
      writer.writeCell(QDateTime::currentDateTime());
      writer.writeCell(QString(SOFTWARE_NAME));
      writer.writeCell(QString(VERSION));
      writer.writeLine();
      writer.writeLine();

      writer.writeCell("Protein digestion parameters");

      writeParam(writer, DeepProtParam::PeptideDigestionEnzymePattern);

      // minimumPeptideLength=7
      writeParam(writer, DeepProtParam::PeptideDigestionPeptideMinimumSize);
      // maximumPeptideLength=30
      writeParam(writer, DeepProtParam::PeptideDigestionPeptideMaximumSize);
      // decoyBase=true
      writeParam(writer, DeepProtParam::ProteinReverse);
      writeParam(writer, DeepProtParam::PeptideDigestionFixedModifications);
      writeParam(writer, DeepProtParam::PeptideDigestionVariableModifications);
      // shift=true
      // missDB=true
      // ter=C
      // enzPattern=.*?[KR]
      //  writeParam(writer, DeepProtParam::);

      // nbMissCleavage=1
      writeParam(writer,
                 DeepProtParam::PeptideDigestionNumberOfMissedCleavages);
      writeParam(writer, DeepProtParam::PeptideMissedCleavageInSpecTree);

      writer.writeLine();
      writer.writeLine();
      writer.writeCell("SpecOms");

      // threshold=6
      writeParam(writer, DeepProtParam::SpecXtractMinimumSimilarity);
      // minimumScore=9
      writeParam(writer, DeepProtParam::SpecFitMinimumSimilarity);
      writeParam(writer, DeepProtParam::SpecFitMinimalMassDelta);
      // numberOfDecimals=2
      writeParam(writer, DeepProtParam::AccuracyDecimals);
      // decimalValue=2
      writeParam(writer, DeepProtParam::AccuracyValue);

      writer.writeLine();
      writer.writeLine();
      writer.writeCell("Parent ion selection");
      // minimumPeptideCharge=1
      writeParam(writer, DeepProtParam::MinimalPeptideCharge);
      // maximumPeptideCharge=3
      writeParam(writer, DeepProtParam::MaximalPeptideCharge);
      writeParam(writer, DeepProtParam::ParentIonMassToleranceUnit);
      writeParam(writer, DeepProtParam::ParentIonMassTolerancePrecision);
      /*
        writer.setCellAnnotation(
          "potential removal of the first Methionin of the protein sequence "
          "(default "
          "is TRUE)");
        writeParam(writer,
                   PeptiderParamBool::ProteinDigestionPotentialMethioninRemoval);
        writer.setCellAnnotation(
          "potential acetylation on every amino acid of the protein Nter
        sequence "
          "(default is TRUE)");
        writeParam(writer,
                   PeptiderParamBool::ProteinDigestionPotentialNterAcetylation);
        writer.setCellAnnotation(
          "potential cyclisation of peptides on Q, E or C with carbamido "
          "modification (default is TRUE)");
        writeParam(writer,
                   PeptiderParamBool::PeptideDigestionPotentialNterCyclisation);
      */
      /*
        writer.writeLine();
        writer.writeLine();
        writer.writeCell("Second pass identification parameters");
        writer.setCellAnnotation(
          "try deeper inspection of proteins identified in the first pass");
        writeParam(writer, PeptiderParamBool::SecondPassUse);
        writer.setCellAnnotation(
          "select first pass protein identified with one peptide evalue beneath
        " "this " "threshold"); writeParam(writer,
                   PeptiderParamPappsoDouble::SecondPassPeptideEvalueSelection);
        writer.setCellAnnotation("use semi enzymatic peptide digestion");
        writeParam(writer, PeptiderParamBool::SecondPassSemiEnzymaticDigestion);
      */


      writer.writeLine();
      writer.writeLine();
      writer.writeCell("Spectrum processing");
      /*
      writeParam(writer, PeptiderParamBool::TandemSpectrumModelExcludeParent);
      writeParam(writer, PeptiderParamBool::TandemSpectrumModelRemoveIsotope);
      writeParam(writer,
                 PeptiderParamBool::TandemSpectrumModelExcludeParentNeutralLoss);
      writeParam(writer,
                 PeptiderParamPappsoDouble::TandemSpectrumModelNeutralLossMass);
      writeParam(
        writer,
        PeptiderParamPappsoDouble::TandemSpectrumModelNeutralLossWindowDalton);
        */
      writeParam(writer, DeepProtParam::SpectrumRemoveIsotope);
      writeParam(writer, DeepProtParam::SpectrumLowerMzLimit);
      writeParam(writer, DeepProtParam::SpectrumUpperMzLimit);
      writeParam(writer,
                 DeepProtParam::ExperimentalSpectrumModelMinimumNumberOfPeaks);
      writeParam(writer, DeepProtParam::SpectrumDynamicRange);
      // maxMassesCount=60
      writeParam(writer, DeepProtParam::SpectrumNmostIntense);
      writeParam(writer, DeepProtParam::SpectrumModelIonTypeList);
      writeParam(writer, DeepProtParam::SpectrumComplementIonEnhancer);
      writeParam(writer, DeepProtParam::SpectrumWindowFilterWidth);
      writeParam(writer, DeepProtParam::SpectrumWindowFilterMax);
      writeParam(writer, DeepProtParam::SpectrumFilterChargeDeconvolution);


      writer.writeLine();
      writer.writeLine();
      writer.writeCell("PSM scoring");
      /*
      writeParam(writer,
      PeptiderParamBool::TandemSpectrumModelRefinePeptideModel);
      writeParam(writer, PeptiderParamString::TandemSpectrumModelIonScoreList);
      writeParam(
        writer,
      PeptiderParamPappsoDouble::TandemSpectrumModelMinimumNumberOfPeaks);
        */
      writeParam(writer, DeepProtParam::MsmsPrecisionUnit);
      writeParam(writer, DeepProtParam::MsmsPrecision);
      // writeParam(writer,
      // PeptiderParamPappsoDouble::MaximumPeptideEvalueThreshold);
    }
  catch(OdsException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR reading parameter file %1").arg(error.qwhat()));
    }

  qDebug();
  // writer.close();
}

void
DeepProtParams::writeParam(CalcWriterInterface &writer,
                           DeepProtParam param) const
{

  // qDebug();
  writer.writeLine();

  auto param_number            = (unsigned int)param;
  DeepProtParamType param_type = m_types[param_number];

  writer.writeCell(m_names[param_number]);

  QVariant value = m_params[param_number];
  // qDebug() << " value=" << value;

  switch(param_type)
    {
      case DeepProtParamType::number:
        writer.writeCell(value.toDouble());
        break;
      case DeepProtParamType::integer:
        writer.writeCell(value.toInt());
        break;
      case DeepProtParamType::boolean:
        writer.writeCell(value.toBool());
        break;
      default:
        writer.writeCell(value.toString());
    }
  // qDebug();
}

void
DeepProtParams::setValueByName(const QString &name, const OdsCell &cell)
{
  qDebug() << " " << name << " " << cell.toString();
  auto it = std::find(std::begin(m_names), std::end(m_names), name);
  if(it == std::end(m_names))
    {
      // name not in vector
    }
  else
    {

      auto param_number            = std::distance(std::begin(m_names), it);
      DeepProtParamType param_type = m_types[param_number];

      QVariant value = m_params[param_number];
      qDebug() << " " << value;

      switch(param_type)
        {
          case DeepProtParamType::number:
            value = QVariant(cell.getDoubleValue());
            break;
          case DeepProtParamType::integer:
            value = QVariant(cell.getDoubleValue());
            break;
          case DeepProtParamType::boolean:
            value = QVariant(cell.getBooleanValue());
            break;

          case DeepProtParamType::string:
            value = QVariant(cell.getStringValue());
            break;
          default:
            value = QVariant(cell.toString());
        }
      m_params[param_number] = value;
    }
  qDebug() << " " << name << " " << cell.toString();
}

void
DeepProtParams::set(DeepProtParam param, const QString &value)
{
  qDebug() << " " << value;
  DeepProtParamType param_type = m_types[(unsigned int)param];

  if(param_type == DeepProtParamType::string)
    {
      m_params[(unsigned int)param] = value;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR setting DeepProt param %1 : the value MUST be an integer")
          .arg(m_names[(unsigned int)param]));
    }
  qDebug();
}

void
DeepProtParams::set(DeepProtParam param, int int_value)
{
  qDebug() << " " << int_value;
  DeepProtParamType param_type = m_types[(unsigned int)param];

  QVariant value = m_params[(unsigned int)param];
  qDebug() << " " << value;


  switch(param_type)
    {
      case DeepProtParamType::integer:
        value = QVariant(int_value);
        break;

      case DeepProtParamType::number:
        value = QVariant(int_value);
        break;
      default:
        throw pappso::PappsoException(
          QObject::tr(
            "ERROR setting DeepProt param %1 : the value MUST be an integer")
            .arg(m_names[(unsigned int)param]));
        break;
    }
  m_params[(unsigned int)param] = value;
  qDebug();
}


void
DeepProtParams::set(DeepProtParam param, bool bool_value)
{
  qDebug() << " " << bool_value;
  DeepProtParamType param_type = m_types[(unsigned int)param];

  QVariant value = m_params[(unsigned int)param];
  qDebug() << " " << value;


  switch(param_type)
    {
      case DeepProtParamType::boolean:
        value = QVariant(bool_value);
        break;
      default:
        throw pappso::PappsoException(
          QObject::tr("ERROR setting SpecOMS param %1 : the value MUST be a "
                      "boolean TRUE or FALSE")
            .arg(m_names[(unsigned int)param]));
        break;
    }
  m_params[(unsigned int)param] = value;
  qDebug();
}

void
DeepProtParams::setEmptyValue(const QString &name)
{
  auto it = std::find(std::begin(m_names), std::end(m_names), name);
  if(it == std::end(m_names))
    {
      // name not in vector
    }
  else
    {

      auto param_number            = std::distance(std::begin(m_names), it);
      DeepProtParamType param_type = m_types[param_number];


      switch(param_type)
        {
          case DeepProtParamType::string:
            m_params[param_number] = QVariant("");
            break;
          default:
            break;
        }
    }
}


DecisionBox &
DeepProtParams::getDecisionBox()
{
  m_decisionBox.setDeepProtParams(*this);
  return m_decisionBox;
}

void
DeepProtParams::writeMzid(pappso::MzIdentMlWriter &writer) const
{

  writer.writeComment("Protein digestion parameters");

  writeParam(writer, DeepProtParam::PeptideDigestionEnzymePattern);

  // minimumPeptideLength=7
  writeParam(writer, DeepProtParam::PeptideDigestionPeptideMinimumSize);
  // maximumPeptideLength=30
  writeParam(writer, DeepProtParam::PeptideDigestionPeptideMaximumSize);
  // decoyBase=true
  writeParam(writer, DeepProtParam::ProteinReverse);
  writeParam(writer, DeepProtParam::PeptideDigestionFixedModifications);
  writeParam(writer, DeepProtParam::PeptideDigestionVariableModifications);

  // nbMissCleavage=1
  writeParam(writer, DeepProtParam::PeptideDigestionNumberOfMissedCleavages);
  writeParam(writer, DeepProtParam::PeptideMissedCleavageInSpecTree);

  writer.writeComment("SpecOms");

  // threshold=6
  writeParam(writer, DeepProtParam::SpecXtractMinimumSimilarity);
  // minimumScore=9
  writeParam(writer, DeepProtParam::SpecFitMinimumSimilarity);
  writeParam(writer, DeepProtParam::SpecFitMinimalMassDelta);
  // numberOfDecimals=2
  writeParam(writer, DeepProtParam::AccuracyDecimals);
  // decimalValue=2
  writeParam(writer, DeepProtParam::AccuracyValue);
  writer.writeComment("Parent ion selection");
  // minimumPeptideCharge=1
  writeParam(writer, DeepProtParam::MinimalPeptideCharge);
  // maximumPeptideCharge=3
  writeParam(writer, DeepProtParam::MaximalPeptideCharge);
  writeParam(writer, DeepProtParam::ParentIonMassToleranceUnit);
  writeParam(writer, DeepProtParam::ParentIonMassTolerancePrecision);

  writer.writeComment("Spectrum processing");

  writeParam(writer, DeepProtParam::SpectrumRemoveIsotope);
  writeParam(writer, DeepProtParam::SpectrumLowerMzLimit);
  writeParam(writer, DeepProtParam::SpectrumUpperMzLimit);
  writeParam(writer,
             DeepProtParam::ExperimentalSpectrumModelMinimumNumberOfPeaks);
  writeParam(writer, DeepProtParam::SpectrumDynamicRange);
  // maxMassesCount=60
  writeParam(writer, DeepProtParam::SpectrumNmostIntense);
  writeParam(writer, DeepProtParam::SpectrumModelIonTypeList);
  writeParam(writer, DeepProtParam::SpectrumComplementIonEnhancer);
  writeParam(writer, DeepProtParam::SpectrumWindowFilterWidth);
  writeParam(writer, DeepProtParam::SpectrumWindowFilterMax);
  writeParam(writer, DeepProtParam::SpectrumFilterChargeDeconvolution);

  writer.writeComment("PSM scoring");
  /*
  writeParam(writer,
  PeptiderParamBool::TandemSpectrumModelRefinePeptideModel);
  writeParam(writer, PeptiderParamString::TandemSpectrumModelIonScoreList);
  writeParam(
    writer,
  PeptiderParamPappsoDouble::TandemSpectrumModelMinimumNumberOfPeaks);
    */
  writeParam(writer, DeepProtParam::MsmsPrecisionUnit);
  writeParam(writer, DeepProtParam::MsmsPrecision);
}

void
DeepProtParams::writeParam(pappso::MzIdentMlWriter &writer,
                           DeepProtParam param) const
{

  auto param_number            = (unsigned int)param;
  DeepProtParamType param_type = m_types[param_number];

  writer.writeUserParam(QString("specoms:%1").arg(m_names[param_number]),
                        m_params[param_number].toString());
}
