/**
 * \file utils/monitorinterface.h
 * \date 7/11/2018
 * \author Olivier Langella
 * \brief monitor interface
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QString>
#include <map>
#include <QElapsedTimer>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include "../exportimportconfig.h"

enum class MonitoredOperation
{
  overall,
  readingMzData,
  SpecXtract,
  SpecFit,
  BuildingTheoreticalSpectrum,
  Scoring,
  First = SpecXtract,
  Last  = SpecXtract
};


class MonitorInterface : public pappso::UiMonitorInterface
{

  public:
  /** @brief indicates that a job or a long computation is terminated
   * @param message the message to display
   */
  virtual void finished(const QString &message) = 0;

  /** @brief message to display while a computation is running
   * @param message the message to display
   */
  virtual void message(const QString &message) = 0;

  /** @brief message to display while a computation is running, with a progress
   * bar
   * @param message the message to display
   * @param value the current value of the progress bar (converted in percent
   * using the maximum value)
   */
  virtual void messagePercent(const QString &message, std::size_t value) = 0;

  /** @brief sets the upper limit of the progress bar when using messagePercent
   * @param max_value used to compute progress percentage
   */
  virtual void setProgressMaximumValue(std::size_t max_value) = 0;

  /** @brief appends some text to existing one
   * @param text text to append
   */
  virtual void appendText(const QString &text) override = 0;


  /** @brief status to display
   * @param text status text
   */
  virtual void setStatus(const QString &text) override = 0;


  /** @brief start specXtract
   */
  virtual void startMonitoredOperation(MonitoredOperation operation);

  /** @brief start specXtract
   */
  virtual void finishedMonitoredOperation(MonitoredOperation operation) = 0;

  /** @brief tell if an external process wants to stop operations
   * @return boolean true if current job has to stop
   */
  virtual bool shouldIstop() override;

  QString getOperationName(MonitoredOperation operation) const;


  /** @brief count steps
   * report when a step is computed in an algorithm
   */
  virtual void count() override ;

  /** @brief current kind of process running
   * @param title process title
   */
  virtual void setTitle(const QString &title) override;


  protected:
  std::map<MonitoredOperation, QElapsedTimer> m_mapQtimeOperations;
  std::size_t m_count=0;
};


class MonitorFake : public MonitorInterface
{

  public:
  void message(const QString &message) override;

  void finished(const QString &message) override;
  void messagePercent(const QString &message, std::size_t value) override;
  void setProgressMaximumValue(std::size_t max_value) override;
  void appendText(const QString &text) override;

  void setStatus(const QString &text) override;

  void finishedMonitoredOperation(MonitoredOperation operation) override;
};
