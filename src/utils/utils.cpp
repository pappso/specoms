/**
 * \file utils/utils.cpp
 * \date 11/04/2019
 * \author Olivier Langella
 * \brief deepprot utilities
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "utils.h"
#include <QFileInfo>
#include <cmath>
#include <odsstream/odsdocwriter.h>
#include <pappsomspp/psm/deepprot/deepprotenum.h>

std::size_t
Utils::doubleMassToIntegerMass(double mass, unsigned int accuracy_factor)
{
  return (std::size_t)(mass * (double)accuracy_factor);
}

std::size_t
Utils::doubleMassRoundToIntegerMass(double mass, unsigned int accuracy_factor)
{
  return (std::size_t)(std::round(mass * (double)accuracy_factor));
}

std::vector<std::size_t>
Utils::TraceToIntegerSpectrum(const pappso::Trace &mass_spectrum,
                              unsigned int accuracy_factor)
{

  std::vector<std::size_t> integer_spectrum;
  integer_spectrum.reserve(mass_spectrum.size());

  for(auto &&peak : mass_spectrum)
    {
      integer_spectrum.push_back(
        (std::size_t)(std::round(peak.x * (double)accuracy_factor)));
    }
  // keep unique masses
  integer_spectrum.erase(
    std::unique(integer_spectrum.begin(), integer_spectrum.end()),
    integer_spectrum.end());

  return integer_spectrum;
}

QString
Utils::IntegerSpectrumToString(std::vector<std::size_t> integer_spectrum)
{
  QStringList peak_list;
  for(auto &&peak : integer_spectrum)
    {
      peak_list << QString("%1").arg(peak);
    }
  return peak_list.join(" ");
}

QString
Utils::EnumToString(pappso::DeepProtMatchType match_type)
{
  return pappso::DeepProtEnumStr::toString(match_type);
}

QString
Utils::EnumToString(pappso::DeepProtPeptideCandidateStatus status)
{
  return pappso::DeepProtEnumStr::toString(status);
}


OutputInterface *
Utils::newOutputInterface(QString outputFileStr, bool all_candidates)
{

  //************************************************************
  // OUTPUT
  //************************************************************
  OutputInterface *p_output = nullptr;
  qDebug() << outputFileStr;
  if(outputFileStr.isEmpty())
    {
      outputFileStr = "deepprot_report.ods";
      p_output      = new OdsOutput("deepprot_report.ods");
    }
  else
    {
      if((QFileInfo(outputFileStr).suffix() == "tsv") ||
         (QFileInfo(outputFileStr).suffix() == "ods"))
        {
          p_output = new OdsOutput(outputFileStr);
        }
      else
        {
          // mzid
          if(all_candidates)
            {
              p_output = new MzIdentMlAllCandidates(outputFileStr);
            }
          else
            {
              p_output = new MzIdentMl(outputFileStr);
            }
        }
    }
  p_output->setOutputAllPsmCandidates(all_candidates);
  // QFile *p_output_file = nullptr;
  // monitor.message(QObject::tr("generating output file
  // %1").arg(outputFileStr));
  /*
  if(p_ods_writer != nullptr)
    {
      spec_fit.generateOdsReport(p_ods_writer);
    }
  else
    {
      // mzid

      p_output_file = new QFile(outputFileStr);

      if(p_output_file->open(QIODevice::WriteOnly))
        {
          QXmlStreamWriter *p_output_stream = new QXmlStreamWriter();
          p_output_stream->setDevice(p_output_file);

          MzIdentMl mzidentml_oputput(&spec_fit, msrun_reader);

          mzidentml_oputput.write(p_output_stream);
          p_output_file->close();
          p_output_file = nullptr;
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("error : cannot open the mzIdentML output file : %1\n")
              .arg(QFileInfo(outputFileStr).absoluteFilePath()));
        }
    }
*/
  return p_output;
}


std::vector<std::size_t>
Utils::getMapKeys(const std::map<std::size_t, std::size_t> &map_table)
{
  std::vector<std::size_t> vector;

  for(auto &&pair : map_table)
    vector.push_back(pair.first);

  return vector;
}


std::vector<std::size_t>
Utils::getMapValues(const std::map<std::size_t, std::size_t> &map_table)
{
  std::vector<std::size_t> vector;

  for(auto &&pair : map_table)
    vector.push_back(pair.second);

  return vector;
}


void
Utils::expandIntegerSpectrum(std::vector<std::size_t> &integer_spectrum,
                             unsigned int accuracy)
{

  std::vector<std::size_t> new_spectrum(integer_spectrum);
  // duplication of the masses according to the accuracy and structure filling
  for(std::size_t mass : integer_spectrum)
    {
      // integer_spectrum.push_back(mass);
      for(unsigned int cpt1 = 0; cpt1 < accuracy; ++cpt1)
        {
          std::size_t delta = 1 + cpt1;
          new_spectrum.push_back(mass + delta);
          if(mass > delta)
            {
              new_spectrum.push_back(mass - delta);
            }
        }
    }
  // conversion into a primitive integer array and sorting
  std::sort(new_spectrum.begin(), new_spectrum.end());

  // keep unique masses
  // TODO ensure unique without this trick
  new_spectrum.erase(std::unique(new_spectrum.begin(), new_spectrum.end()),
                     new_spectrum.end());

  integer_spectrum = new_spectrum;
}
