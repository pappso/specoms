/**
 * \file utils/deepprotparams.h
 * \date 15/10/2018
 * \author Olivier Langella
 * \brief store DeepProt parameters
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QVariant>
#include <odsstream/calcwriterinterface.h>
#include <odsstream/reader/odscell.h>
#include <pappsomspp/precision.h>
#include <pappsomspp/processing/xml/mzidentmlwriter.h>

#include "../config.h"
#include "decisionbox.h"
#include "../exportimportconfig.h"

enum class DeepProtParamType : std::uint8_t
{
  integer,
  number,
  boolean,
  date,
  string,
};

enum class DeepProtParam : unsigned int
{
  // AccuracyDalton   = 1, ///< double accuracy = 0.02;
  // accuracy = ( double ) accuracy_value/accuracy_factor;

  AccuracyDecimals = 2, ///< int accuracy_decimals = 2;
  AccuracyValue =
    3, ///< int accuracy_value = 2;
       // AccuracyFactor   = 4, ///< int accuracy_factor = 100;
       // accuracy_factor = ( int ) Math.pow ( 10, accuracy_decimals );

  MinimalPeptideCharge = 5, ///< int minimal_peptide_charge = 1;
  MaximalPeptideCharge = 6, ///< int maximal_peptide_charge = 3;

  SpectrumRemoveIsotope = 7, ///< if true : remove isotope in mass spectrum
  SpectrumExcludeParentIon =
    8, ///< if true : exclude parent ion in mass spectrum
  SpectrumExcludeParentLowerDalton = 9,  ///< lower dalton range to exclude
  SpectrumExcludeParentUpperDalton = 10, ///< upper dalton range to exclude
  SpectrumNmostIntense =
    12, ///< maximum number of peaks (most intense) to consider in spectrum
  SpectrumDynamicRange = 13, ///< dynamic to consider intensity in spectrum
  SpectrumExcludeParentIonNeutralLoss =
    14, ///< if true : exclude parent ion neutral loss in mass spectrum
  SpectrumNeutralLossMass = 15, ///< parent ion neutral loss mass
  SpectrumNeutralLossMassWindowDalton =
    16, ///< parent ion neutral loss mass window to remove in dalton


  ProteinReverse = 17, ///< Compute reverse protein sequences for scoring;

  PeptideDigestionNumberOfMissedCleavages =
    18, ///< Maximum number of missed cleavages in peptide digestion;
  PeptideDigestionPeptideMinimumSize =
    19, ///< Peptide minimum size in peptide digestion;
  PeptideDigestionPeptideMaximumSize =
    20, ///< Peptide maximum size in peptide digestion;
  PeptideDigestionFixedModifications    = 21, ///< Fixed modifications;
  PeptideDigestionVariableModifications = 22, ///< Variable modifications;
  PeptideDigestionPotentialNterCyclisation =
    23, ///< peptide digestion : potential Nter cyclisation on Q, E or C with
        ///< carbamido;
  ProteinDigestionPotentialNterAcetylation =
    24, ///< protein digestion : potential Nter acetylation;
  ProteinDigestionPotentialMethioninRemoval =
    25, ///< protein digestion : potential methionin removal;

  SpectrumModelIonTypeList =
    26, ///< list of type of ion to modelize in theoretical spectrum

  SpectrumLowerMzLimit = 27, ///< spectrum lower mz limit
  SpectrumUpperMzLimit = 28, ///< spectrum upper mz limit
  ExperimentalSpectrumModelMinimumNumberOfPeaks =
    29, ///< experimental spectrum min peak number
  SpecXtractMinimumSimilarity =
    30, ///< minimum similarity to extract spectrum pair
  SpecFitMinimumSimilarity =
    31, ///< minimum similarity to extract spectrum pair


  ParentIonMassTolerancePrecision =
    32, ///< Parent ion mass upper tolerance, dalton or ppm depending on unit;
  ParentIonMassToleranceUnit =
    33, ///< Parent ion mass tolerance unit : dalton or ppm;


  MorpheusScoreIonTypeList =
    34, ///< list of ion types to use for Morpheus score

  MsmsPrecision = 35, ///< double to set the precision to use on MS2 fragments

  MsmsPrecisionUnit =
    36, ///< precision unit to use on MS2 fragments (dalton or ppm)


  NoSpectrumProcessing =
    37, ///< boolean to tell if we use filters on raw spectrum (true) or if we
        ///< only take SpectrumNmostIntense peaks

  SpecFitMinimalMassDelta =
    38, ///< the minimal possible mass delta (-500 by default)

  PeptideDigestionEnzymePattern =
    39, ///< regular expression used by Enzyme to recognize cleavage sites

  PeptideMissedCleavageInSpecTree =
    40, ///< add missed cleavage peptides in SpecTree structure to be able to
        ///< find modifications. If TRUE, the spectree structure is bigger in
        ///< memory, modifications can be seen. if FALSE, spectree is ligther
        ///< but missed cleavage peptides are only seen with 0 delta mass. In
        ///< SpecOMS, this parameter is called "missDB"

  SpectrumComplementIonEnhancer =
    41, ///< add a filter on MS2 spectrum to enhance ion complements (targeted
        ///< to increase b ion intentities)
  SpectrumWindowFilterWidth =
    42, ///< add a filter on MS2 spectrum that limits to a maximum number of
        ///< peaks within a window, this is the window width in dalton
  SpectrumWindowFilterMax =
    43, ///< add a filter on MS2 spectrum that limits to a maximum number of
        ///< peaks within a window, this is the maximum number of peaks in the
        ///< window
  SpectrumFilterChargeDeconvolution =
    44, ///< add a filter on MS2 spectrum to deconvolute signal
  last = 45
};

class OdsParamHandler;

class DeepProtParams
{
  friend OdsParamHandler;

  public:
  static DeepProtParams &getInstance();

  const QVariant &get(DeepProtParam param) const;
  void set(DeepProtParam param, bool bool_value);
  void set(DeepProtParam param, int int_value);
  void set(DeepProtParam param, const QString &value);
  std::vector<pappso::PeptideIon> getIonTypeList(DeepProtParam param) const;


  pappso::PrecisionPtr getMsPrecision() const;
  pappso::PrecisionPtr getMsmsPrecision() const;
  void save(CalcWriterInterface &writer) const;
  void load(const QString &ods_file_str);

  // accuracy = ( double ) accuracy_value/accuracy_factor;
  pappso::pappso_double getAccuracyDalton() const;
  // accuracy_factor = ( int ) Math.pow ( 10, accuracy_decimals );
  unsigned int getAccuracyFactor() const;

  DecisionBox &getDecisionBox();

  void writeMzid(pappso::MzIdentMlWriter &writer) const;

  private:
  void setValueByName(const QString &name, const OdsCell &cell);
  void setEmptyValue(const QString &name);
  void writeParam(CalcWriterInterface &writer, DeepProtParam param) const;
  void writeParam(pappso::MzIdentMlWriter &writer, DeepProtParam param) const;

  private:
  DeepProtParams();

  private:
  static DeepProtParams *mp_instance;

  private:
  DecisionBox m_decisionBox;
  QVariant m_params[100];
  QString m_names[100]           = {""};
  DeepProtParamType m_types[100] = {DeepProtParamType::integer};
};
