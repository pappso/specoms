/**
 * \file utils/monitorcli.h
 * \date 7/11/2018
 * \author Olivier Langella
 * \brief monitor for command line interface
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QTextStream>
#include <QMutex>
#include "monitorinterface.h"

class MonitorCli : public MonitorInterface
{
  private:
  QTextStream &m_out;
  std::size_t m_maxValue;
  int m_currentProgress;

  public:
  MonitorCli(QTextStream &cout);

  void message(const QString &message) override;

  void finished(const QString &message) override;
  void messagePercent(const QString &message, std::size_t value) override;
  void setProgressMaximumValue(std::size_t max_value) override;
  void appendText(const QString &text) override;

  void setStatus(const QString &text) override;

  /** @brief start specXtract
   */
  void finishedMonitoredOperation(MonitoredOperation operation) override;

  private:
  QMutex m_mutex;
};
