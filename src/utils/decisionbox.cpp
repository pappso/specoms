/**
 * \file utils/decisionbox.h
 * \date 16/11/2019
 * \author Olivier Langella
 * \brief handle priorities between peptide candidates to choose the best
 * solution
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "decisionbox.h"
#include "../core/spectrum_matcher_legacy/spectrummatcher.h"
#include "../core/spectrum_matcher_legacy/peptidecandidate.h"
#include "../core/spectrum_matcher_legacy/peptidespectrumshifter.h"
#include "../core/spectrum_matcher_legacy/missedcleavagefinder.h"
#include "../core/spectrumint.h"
#include "../utils/deepprotparams.h"

DecisionBox::DecisionBox()
{
  m_matchTypePriority.resize(10);

  m_matchTypePriority[(std::uint8_t)pappso::DeepProtMatchType::ZeroMassDelta] =
    10;
  m_matchTypePriority[(std::uint8_t)pappso::DeepProtMatchType::uncategorized] =
    0;
  m_matchTypePriority[(
    std::uint8_t)pappso::DeepProtMatchType::ZeroMassDeltaMissedCleavage] = 9;
  m_matchTypePriority[(
    std::uint8_t)pappso::DeepProtMatchType::ZeroMassDeltaSemiTryptic]    = 9;
  m_matchTypePriority[(std::uint8_t)pappso::DeepProtMatchType::DeltaPosition] =
    5;
  m_matchTypePriority[(
    std::uint8_t)pappso::DeepProtMatchType::NoDeltaPosition] = 5;
}

DecisionBox::DecisionBox(const DecisionBox &other)
{

  m_minimum_similarity      = other.m_minimum_similarity;
  m_minimum_mass_delta      = other.m_minimum_mass_delta;
  m_ms1_precision           = other.m_ms1_precision;
  m_maximum_missed_cleavage = other.m_maximum_missed_cleavage;
  m_matchTypePriority       = other.m_matchTypePriority;

  msp_peptideDatabase = other.msp_peptideDatabase;
}

DecisionBox::~DecisionBox()
{
}


void
DecisionBox::setDeepProtParams(const DeepProtParams &params)
{

  m_minimum_similarity =
    params.get(DeepProtParam::SpecFitMinimumSimilarity).toUInt();
  m_minimum_mass_delta =
    params.get(DeepProtParam::SpecFitMinimalMassDelta).toDouble();
  m_ms1_precision = params.getMsPrecision();
  m_maximum_missed_cleavage =
    params.get(DeepProtParam::PeptideDigestionNumberOfMissedCleavages).toUInt();
}

void
DecisionBox::setPeptideDatabaseSPtr(PeptideDatabaseSPtr peptide_database)
{
  msp_peptideDatabase = peptide_database;
}

void
DecisionBox::decideForPeptideCandidate(
  SpectrumMatcher *p_spectrumMatcher, PeptideCandidate &peptide_candidate) const
{

  const SpectrumInt *p_experimentalSpectrumInt =
    p_spectrumMatcher->getExperimentalSpectrumInt();
  std::size_t count = peptide_candidate.getOriginalCount();

  std::vector<PeptideCandidate> &peptideCandidate_list =
    p_spectrumMatcher->getPeptideCandidates();

  if(peptide_candidate.m_delta < m_minimum_mass_delta)
    {
      // no good : unlikely to happen if the mass is so low
      qDebug() << peptide_candidate.getPeptideSp().get()->getSequence()
               << " delta=" << peptide_candidate.getMassDelta();
      return;
    }

  qDebug() << peptide_candidate.getPeptideSp().get()->getSequence()
           << " delta=" << peptide_candidate.getMassDelta();

  unsigned int charge =
    p_experimentalSpectrumInt->getQualifiedMassSpectrumSPtr()
      .get()
      ->getPrecursorCharge();
  pappso::pappso_double target_mz =
    p_experimentalSpectrumInt->getQualifiedMassSpectrumSPtr()
      .get()
      ->getPrecursorMz();
  pappso::MzRange target_mz_range(target_mz, m_ms1_precision);
  if(peptide_candidate.isZeroMassDelta(target_mz_range, charge))
    {
      // zero mass delta
      peptide_candidate.m_status =
        pappso::DeepProtPeptideCandidateStatus::ZeroMassDelta;
      // cleaning existing candidate


      if(count >= m_minimum_similarity)
        {
          // zero mass delta priority

          // erase previous peptide candidates if they are not zero mass delta
          // or their score is below the current peptide candidate
          peptideCandidate_list.erase(
            std::remove_if(peptideCandidate_list.begin(),
                           peptideCandidate_list.end(),
                           [count](PeptideCandidate &pc) {
                             return (pc.m_status !=
                                     pappso::DeepProtPeptideCandidateStatus::
                                       ZeroMassDelta) ||
                                    (pc.m_originalCount < count);
                           }),
            peptideCandidate_list.end());
        }
      else
        {
          // the score is below similarity threshold, there is a room for
          // competition
        }

      p_spectrumMatcher->setMatchType(pappso::DeepProtMatchType::ZeroMassDelta,
                                      count);
      peptideCandidate_list.push_back(peptide_candidate);
      return;
    }
  else
    {
      if((p_spectrumMatcher->getMatchType() ==
          pappso::DeepProtMatchType::ZeroMassDelta) &&
         (p_spectrumMatcher->getMatchTypeCount(
            pappso::DeepProtMatchType::ZeroMassDelta) >= m_minimum_similarity))
        {
          // there is a better explanation
          return;
        }

      PeptideSpectrumShifter shifter(&peptide_candidate,
                                     p_experimentalSpectrumInt);
      // not in range:
      if(peptide_candidate.m_delta < 0)
        {
          // check for semi tryptic
          // peptide_candidate.searchCutSide();

          shifter.searchCutSide();

          qDebug();
          if(shifter.isSemiTrypticCter())
            {
              if(shifter.getPeptideCandidate().m_fittedCount >=
                 m_minimum_similarity)
                {
                  // erase bad candidates (less good score)
                  std::size_t tmp_count =
                    shifter.getPeptideCandidate().m_fittedCount;
                  peptideCandidate_list.erase(
                    std::remove_if(peptideCandidate_list.begin(),
                                   peptideCandidate_list.end(),
                                   [tmp_count](PeptideCandidate &pc) {
                                     if(pc.m_fittedCount < tmp_count)
                                       return true;
                                     return false;
                                   }),
                    peptideCandidate_list.end());

                  p_spectrumMatcher->setMatchType(
                    pappso::DeepProtMatchType::ZeroMassDeltaSemiTryptic,
                    tmp_count);
                  peptideCandidate_list.push_back(
                    shifter.getPeptideCandidate());
                  return;
                }
            }
          else if(shifter.isSemiTrypticNter())
            {
              if(shifter.getPeptideCandidate().m_fittedCount >=
                 m_minimum_similarity)
                {

                  // erase bad candidates (less good score)
                  std::size_t tmp_count =
                    shifter.getPeptideCandidate().m_fittedCount;
                  peptideCandidate_list.erase(
                    std::remove_if(peptideCandidate_list.begin(),
                                   peptideCandidate_list.end(),
                                   [tmp_count](PeptideCandidate &pc) {
                                     if(pc.m_fittedCount < tmp_count)
                                       return true;
                                     return false;
                                   }),
                    peptideCandidate_list.end());

                  p_spectrumMatcher->setMatchType(
                    pappso::DeepProtMatchType::ZeroMassDeltaSemiTryptic,
                    tmp_count);
                  peptideCandidate_list.push_back(
                    shifter.getPeptideCandidate());
                  return;
                }
            }
        }
      else
        {
          qDebug() << m_maximum_missed_cleavage;
          // check for missed cleavages
          if(m_maximum_missed_cleavage > 0)
            {
              MissedCleavageFinder missed_cleavage_finder(
                msp_peptideDatabase.get(),
                peptide_candidate,
                p_experimentalSpectrumInt,
                m_minimum_similarity);
              if(missed_cleavage_finder.getBestPeptideCandidateSptr() !=
                 nullptr)
                {
                  if(missed_cleavage_finder.getBestPeptideCandidateSptr()
                       .get()
                       ->isZeroMassDelta(target_mz_range, charge))
                    {
                      p_spectrumMatcher->setMatchType(
                        pappso::DeepProtMatchType::ZeroMassDeltaMissedCleavage,
                        missed_cleavage_finder.getBestPeptideCandidateSptr()
                          .get()
                          ->getFittedCount());
                      peptideCandidate_list.push_back(
                        *(missed_cleavage_finder.getBestPeptideCandidateSptr()
                            .get()));
                      return;
                    }
                }
            }
        }
      // in all case, find best delta position
      // return;


      if(p_spectrumMatcher->getMatchType() ==
         pappso::DeepProtMatchType::ZeroMassDeltaSemiTryptic)
        {
          // there is a better explanation
          return;
        }

      // shifter.findBestDeltaPosition(getCharge());
      std::size_t max_score_delta_position =
        p_spectrumMatcher->getMatchTypeCount(
          pappso::DeepProtMatchType::DeltaPosition);
      shifter.findBestDeltaPosition(1);
      if((shifter.getPeptideCandidate().m_fittedCount >=
          peptide_candidate.m_originalCount) &&
         (shifter.getPeptideCandidate().m_fittedCount >=
          max_score_delta_position))
        {
          max_score_delta_position =
            shifter.getPeptideCandidate().m_fittedCount;

          // erase bad candidates (less good score)
          peptideCandidate_list.erase(
            std::remove_if(peptideCandidate_list.begin(),
                           peptideCandidate_list.end(),
                           [max_score_delta_position](PeptideCandidate &pc) {
                             if(pc.m_fittedCount < max_score_delta_position)
                               {
                                 return true;
                               }
                             return false;
                           }),
            peptideCandidate_list.end());


          p_spectrumMatcher->setMatchType(
            pappso::DeepProtMatchType::DeltaPosition, max_score_delta_position);
          peptideCandidate_list.push_back(shifter.getPeptideCandidate());

          // return;
        }
      else
        {
          // shifter failed, better keep original solution
          if(peptide_candidate.m_originalCount >= max_score_delta_position)
            {
              max_score_delta_position = peptide_candidate.m_originalCount;
              peptide_candidate.m_status =
                pappso::DeepProtPeptideCandidateStatus::NoDeltaPosition;


              // erase bad candidates (less good score)
              peptideCandidate_list.erase(
                std::remove_if(
                  peptideCandidate_list.begin(),
                  peptideCandidate_list.end(),
                  [max_score_delta_position](PeptideCandidate &pc) {
                    if(pc.m_fittedCount < max_score_delta_position)
                      {
                        return true;
                      }

                    return false;
                  }),
                peptideCandidate_list.end());


              p_spectrumMatcher->setMatchType(
                pappso::DeepProtMatchType::NoDeltaPosition,
                max_score_delta_position);
              peptideCandidate_list.push_back(peptide_candidate);
            }
        }
    }
}
