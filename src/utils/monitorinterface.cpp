/**
 * \file utils/monitorinterface.cpp
 * \date 7/11/2018
 * \author Olivier Langella
 * \brief monitor interface
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "monitorinterface.h"


QString
MonitorInterface::getOperationName(MonitoredOperation operation) const
{
  switch(operation)
    {
      case MonitoredOperation::overall:
        return "overall process";
      case MonitoredOperation::readingMzData:
        return "Building experimental spectrum";
      case MonitoredOperation::SpecXtract:
        return "SpecXtract";
      case MonitoredOperation::SpecFit:
        return "SpecFit";
      case MonitoredOperation::BuildingTheoreticalSpectrum:
        return "Building theoretical spectrum";
      case MonitoredOperation::Scoring:
        return "Scoring";
    }
  return "not known";
}

void
MonitorInterface::startMonitoredOperation(MonitoredOperation operation)
{

  std::pair<std::map<MonitoredOperation, QElapsedTimer>::iterator, bool>
    ret_map_qtime = m_mapQtimeOperations.insert(
      std::pair<MonitoredOperation, QElapsedTimer>(operation, QElapsedTimer()));

  ret_map_qtime.first->second.start();
}

bool
MonitorInterface::shouldIstop()
{
  // do not stop by default
  return false;
}

void
MonitorInterface::setTitle(const QString &title)
{
  message(title);
}


void
MonitorFake::message(const QString &message [[maybe_unused]])
{
}

void
MonitorFake::finished(const QString &message [[maybe_unused]])
{
}
void
MonitorFake::messagePercent(const QString &message [[maybe_unused]],
                            std::size_t value [[maybe_unused]])
{
}
void
MonitorFake::setProgressMaximumValue(std::size_t max_value [[maybe_unused]])
{
}
void
MonitorFake::appendText(const QString &text [[maybe_unused]])
{
}

void
MonitorFake::setStatus(const QString &text [[maybe_unused]])
{
}

void
MonitorFake::finishedMonitoredOperation(MonitoredOperation operation
                                        [[maybe_unused]])
{
}

void
MonitorInterface::count()
{
  m_count++;
  messagePercent("", m_count);
}
