#pragma once

#include <QtCore/QtGlobal>

namespace pappso
{
#if defined(DEEPPROT_LIBRARY)
//#pragma message("PMSPP_LIBRARY is defined: defining PMSPP_LIB_DECL
// Q_DECL_EXPORT")
#define DEEPPROT_LIB_DECL Q_DECL_EXPORT
#else
//#pragma message("PMSPP_LIBRARY is not defined: defining PMSPP_LIB_DECL
// Q_DECL_IMPORT")
#define DEEPPROT_LIB_DECL Q_DECL_IMPORT
#endif

} // namespace pappso
