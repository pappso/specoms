/**
 * \file /gui/workerthread.cpp
 * \date 14/4/2020
 * \author Olivier Langella
 * \brief worker thread
 */

/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "workerthread.h"
#include <pappsomspp/pappsoexception.h>
#include <odsstream/odsexception.h>
#include <QDebug>
#include <QThreadPool>
#include "monitorgui.h"
#include "../utils/utils.h"
#include "../output/odsoutput.h"
#include "../output/mzidentml.h"
#include "../core/runningdeepprot.h"
#include "deepprot-gui.h"

WorkerThread::WorkerThread(DeepProtGui *p_main_window)
{
  mp_mainWindow = p_main_window;
  qDebug();

  mpa_monitor = new MonitorGui(mp_mainWindow->getWaitingMessageDialog());


  // worker message
  connect(mpa_monitor,
          &MonitorGui::workerMessage,
          mp_mainWindow,
          &DeepProtGui::doDisplayLoadingMessage);
  connect(mpa_monitor,
          &MonitorGui::workerMessagePercent,
          mp_mainWindow,
          &DeepProtGui::doDisplayLoadingMessagePercent);


  connect(this,
          &WorkerThread::loadingMessage,
          p_main_window,
          &DeepProtGui::doDisplayLoadingMessage);


  connect(this,
          &WorkerThread::operationFailed,
          p_main_window,
          &DeepProtGui::doOperationFailed);
  connect(this,
          &WorkerThread::operationFinished,
          p_main_window,
          &DeepProtGui::doOperationFinished);
  qDebug();
}


WorkerThread::~WorkerThread()
{
  delete mpa_monitor;
  qDebug();
}

void
WorkerThread::doRunningDeepProt(
  DeepProtParams *deepprot_params,
  pappso::MsRunReaderSPtr msrun_reader,
  std::vector<QFileInfo> fasta_fileinfo_list,
  std::vector<QFileInfo> decoy_fasta_fileinfo_list,
  QFileInfo output_file,
  int threads,
  bool all_candidates)
{

  try
    {
      RunningDeepProt deepprot;

      emit loadingMessage(tr("running peptide identification, please wait"));
      OutputInterface *p_output = Utils::newOutputInterface(
        output_file.absoluteFilePath(), all_candidates);

      deepprot.run(*deepprot_params,
                   *mpa_monitor,
                   msrun_reader,
                   fasta_fileinfo_list,
                   decoy_fasta_fileinfo_list,
                   p_output,
                   threads);
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Oops! an error occurred in deepprot. Dont Panic :\n%1")
          .arg(error.qwhat()));
    }

  // mpa_monitor->getOperationDuration();
  mpa_monitor->message(QObject::tr("peptide identification is finished"));
  emit operationFinished(
    WorkerThreadOperationType::deepprotRun,
    QString("theoretical spectrum build took : "
            "%1\nspectxtract took : %2")
      .arg(mpa_monitor->getOperationDuration(
        MonitoredOperation::BuildingTheoreticalSpectrum))
      .arg(mpa_monitor->getOperationDuration(MonitoredOperation::SpecXtract)));
}
