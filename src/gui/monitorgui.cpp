/**
 * \file gui/monitorgui.cpp
 * \date 14/04/2020
 * \author Olivier Langella
 * \brief monitor for GUI
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "monitorgui.h"
#include "waiting_message_dialog/waitingmessagedialog.h"

MonitorGui::MonitorGui(WaitingMessageDialog *main_win)
{
  m_maxValue      = 100;
  m_timerDuration = 200;
  m_time.restart();
  mp_waitingDialog = main_win;
}

void
MonitorGui::message(const QString &message)
{
  if(m_time.elapsed() > m_timerDuration)
    {
      m_time.restart();
      emit workerMessage(message);
    }
}


void
MonitorGui::finished(const QString &message [[maybe_unused]])
{
  if(m_currentProgress > 0)
    {
      // m_out << endl;
      m_currentProgress = 0;
    }
}

void
MonitorGui::messagePercent(const QString &message, std::size_t value)
{
  QMutexLocker locker(&m_mutex);
  // m_out << m_currentProgress << endl;


  if(m_time.elapsed() > m_timerDuration)
    {
      int percent = ((float)value / (float)m_maxValue) * (float)100;

      m_time.restart();
      emit workerMessagePercent(message, percent);
    }
}

void
MonitorGui::setProgressMaximumValue(std::size_t max_value)
{
  m_maxValue        = max_value;
  m_currentProgress = 0;
}
void
MonitorGui::appendText(const QString &text [[maybe_unused]])
{
  if(m_currentProgress > 0)
    {
      // m_out << endl;
      m_currentProgress = 0;
    }
  // m_out << text << endl;
  // m_out.flush();
}

void
MonitorGui::setStatus(const QString &text [[maybe_unused]])
{
  if(m_currentProgress > 0)
    {
      // m_out << endl;
      m_currentProgress = 0;
    }
  // m_out << text << endl;
  // m_out.flush();
}

void
MonitorGui::finishedMonitoredOperation(MonitoredOperation operation
                                       [[maybe_unused]])
{
  // MonitorInterface::finishedMonitoredOperation(operation);
  std::pair<std::map<MonitoredOperation, qint64>::iterator, bool>
    ret_map_duration = m_mapDurationOperations.insert(
      std::pair<MonitoredOperation, qint64>(operation, 0));


  std::pair<std::map<MonitoredOperation, QElapsedTimer>::iterator, bool>
    ret_map_qtime = m_mapQtimeOperations.insert(
      std::pair<MonitoredOperation, QElapsedTimer>(operation, QElapsedTimer()));

  ret_map_duration.first->second = ret_map_qtime.first->second.elapsed();

  if(m_currentProgress > 0)
    {
      // m_out << endl;
      m_currentProgress = 0;
    } /*
   m_out << QObject::tr("%1 took %2 seconds")
              .arg(getOperationName(operation))
              .arg(m_mapQtimeOperations.at(operation).elapsed() / 1000)
         << endl;
   m_out.flush();*/
}

bool
MonitorGui::shouldIstop()
{
  return mp_waitingDialog->stopWorkerThread();
}

qint64
MonitorGui::getOperationDuration(MonitoredOperation operation)
{

  std::pair<std::map<MonitoredOperation, qint64>::iterator, bool>
    ret_map_duration = m_mapDurationOperations.insert(
      std::pair<MonitoredOperation, qint64>(operation, 0));

  return ret_map_duration.first->second;
}
