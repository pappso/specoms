/**
 * \file gui/monitorgui.h
 * \date 14/04/2020
 * \author Olivier Langella
 * \brief monitor for GUI
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QTextStream>
#include <QElapsedTimer>
#include <QMutex>
#include "../utils/monitorinterface.h"
#include <QObject>

class WaitingMessageDialog;

class MonitorGui : public QObject, public MonitorInterface
{
  Q_OBJECT

  public:
  MonitorGui(WaitingMessageDialog *main_win);

  void message(const QString &message) override;

  void finished(const QString &message) override;
  void messagePercent(const QString &message, std::size_t value) override;
  void setProgressMaximumValue(std::size_t max_value) override;
  void appendText(const QString &text) override;

  void setStatus(const QString &text) override;

  /** @brief start specXtract
   */
  void finishedMonitoredOperation(MonitoredOperation operation) override;

  bool shouldIstop() override;

  /** @brief  Returns the number of milliseconds since this operation was
   * started.
   */
  qint64 getOperationDuration(MonitoredOperation operation);

  signals:
  void workerJobFinished(QString message);
  void workerMessage(QString message);
  void workerMessagePercent(QString message, int value);
  void workerAppendText(const char *p_char);
  void workerSetText(QString text);

  private:
  std::size_t m_maxValue;
  int m_currentProgress;


  QMutex m_mutex;
  QElapsedTimer m_time;
  int m_timerDuration;

  WaitingMessageDialog *mp_waitingDialog;

  std::map<MonitoredOperation, qint64> m_mapDurationOperations;
};
