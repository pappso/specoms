/**
 * \file /gui/workerthread.h
 * \date 14/4/2020
 * \author Olivier Langella
 * \brief worker thread
 */

/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QThread>
#include <QCloseEvent>
#include <QFileInfo>
#include "monitorgui.h"
#include "../utils/deepprotparams.h"
#include "../core/msspectrumreader.h"

class DeepProtGui;


enum class WorkerThreadOperationType : qint8
{
  deepprotRun = 0,
  last        = 1
};


class WorkerThread : public QObject
{
  Q_OBJECT

  public:
  WorkerThread(DeepProtGui *parent);
  virtual ~WorkerThread();


  protected:
  void closeEvent(QCloseEvent *event);

  public slots:
  void doRunningDeepProt(DeepProtParams *deepprot_params,
                         pappso::MsRunReaderSPtr msrun_reader,
                         std::vector<QFileInfo> fasta_fileinfo,
                         std::vector<QFileInfo> decoy_fasta_fileinfo_list,
                         QFileInfo output_file,
                         int threads,
                         bool all_candidates);

  signals:

  signals:
  void loadingMessage(QString message);
  void operationFinished(WorkerThreadOperationType type, QString message);
  void operationFailed(QString error);


  private:
  MonitorGui *mpa_monitor;
  DeepProtGui *mp_mainWindow;
};
