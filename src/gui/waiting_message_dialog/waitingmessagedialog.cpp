
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ui_waiting_message_dialog.h"
#include "waitingmessagedialog.h"
#include <QDebug>
#include <QLabel>
#include <QMutexLocker>

WaitingMessageDialog::WaitingMessageDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::WaitingMessageDialog)
{
  qDebug();
  ui->setupUi(this);
  this->setModal(true);
  this->setWindowFlags(Qt::Window | Qt::WindowTitleHint |
                       Qt::CustomizeWindowHint);
  // this->setWindowFlags(Qt::FramelessWindowHint);
  // this->setWindowFlags(Qt::WindowTitleHint);

  // ui->movie_label->setMovie(_p_movie);
  ui->movie_label->setText("");
  ui->progress_bar->setVisible(false);

  ui->progress_bar->setMaximum(100);
  ui->progress_bar->setMinimum(0);
  ui->console_plaintextedit->setHidden(true);
  ui->stop_push_button->setVisible(false);
  _asked_to_stop = false;
  // movie->start();
  qDebug() << "WaitingMessageDialog::WaitingMessageDialog end";
}

WaitingMessageDialog::~WaitingMessageDialog()
{
  qDebug() << "WaitingMessageDialog::~WaitingMessageDialog";
  delete ui;
  qDebug() << "WaitingMessageDialog::~WaitingMessageDialog end";
}

void
WaitingMessageDialog::toggledStopButton(bool pushed [[maybe_unused]])
{
  QMutexLocker mutex_locker(&_mutex);
  _asked_to_stop = true;
}

bool
WaitingMessageDialog::stopWorkerThread()
{
  QMutexLocker mutex_locker(&_mutex);
  if(!ui->stop_push_button->isVisible())
    {
      ui->stop_push_button->setChecked(false);
      ui->stop_push_button->setVisible(true);
      _asked_to_stop = false;
      return false;
    }
  return _asked_to_stop;
}

void
WaitingMessageDialog::message(const QString &message)
{

  ui->progress_bar->setVisible(false);
  ui->message_label->setText(message);
}

void
WaitingMessageDialog::message(const QString &message, int progress_value)
{
  qDebug() << "WaitingMessageDialog::message " << progress_value;
  ui->progress_bar->setVisible(true);
  ui->progress_bar->setValue(progress_value);
  ui->message_label->setText(message);
}

void
WaitingMessageDialog::appendText(const char *text)
{
  ui->console_plaintextedit->setHidden(false);
  // ui->console_plaintextedit->appendText(text);

  ui->console_plaintextedit->moveCursor(QTextCursor::End);
  ui->console_plaintextedit->insertPlainText(text);
}

void
WaitingMessageDialog::setText(const QString text)
{
  ui->console_plaintextedit->setHidden(false);
  // ui->console_plaintextedit->appendText(text);
  ui->console_plaintextedit->setPlainText(text);
}

void
WaitingMessageDialog::hideEvent(QHideEvent *event)
{
  QWidget::hideEvent(event);
  qDebug() << "WaitingMessageDialog::hideEvent";
  //_p_movie->stop();
  ui->console_plaintextedit->setHidden(true);
}
void
WaitingMessageDialog::showEvent(QShowEvent *event)
{
  QWidget::showEvent(event);
  // _p_movie->start();
  ui->stop_push_button->setChecked(false);
  ui->stop_push_button->setVisible(false);
  _asked_to_stop = false;
}
