/**
 * \file gui/deepprot-gui.cpp
 * \date 13/04/2020
 * \author Olivier Langella
 * \brief simple GUI to launch deepprot job
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QCoreApplication>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include "deepprot-gui.h"
#include "ui_deepprot_main.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include "output/odsoutput.h"
#include <odsstream/odsdocwriter.h>
#include <odsstream/odsexception.h>
#include <QDesktopServices>


DeepProtGui::DeepProtGui(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::DeepProtMain)
{
  ui->setupUi(this);

  ui->parameterLabel->hide();
  ui->mzDataLabel->hide();

  mpa_waitingMessageDialog = new WaitingMessageDialog(this);

  mpa_worker = new WorkerThread(this);
  mpa_worker->moveToThread(&m_workerThread);
  m_workerThread.start();

  connect(this,
          &DeepProtGui::runDeepProt,
          mpa_worker,
          &WorkerThread::doRunningDeepProt);
}

DeepProtGui::~DeepProtGui()
{
  delete mpa_worker;
}
void
DeepProtGui::closeEvent(QCloseEvent *event)
{
  event->accept();
}


void
DeepProtGui::hideWaitingMessage()
{

  mpa_waitingMessageDialog->hide();
  ui->statusbar->showMessage("");
}

void
DeepProtGui::viewError(QString error)
{
  hideWaitingMessage();
  QMessageBox::warning(
    this, tr("Oops! an error occurred in DeepProt. Dont Panic :"), error);
}


void
DeepProtGui::doDisplayLoadingMessage(QString message)
{
  qDebug() << message;
  mpa_waitingMessageDialog->message(message);
  ui->statusbar->showMessage(message);
}

void
DeepProtGui::doDisplayLoadingMessagePercent(QString message, int value)
{
  qDebug() << message << " " << value;
  mpa_waitingMessageDialog->message(message, value);
  ui->statusbar->showMessage(message);
}


void
DeepProtGui::doOperationFailed(QString error)
{
  hideWaitingMessage();
  viewError(error);
}
void
DeepProtGui::doOperationFinished(WorkerThreadOperationType type,
                                 QString message)
{
  mpa_waitingMessageDialog->hide();
  if(type == WorkerThreadOperationType::deepprotRun)
    {
      QString title = "DeepProt run finished";
      QMessageBox::information(
        this, title, message, QMessageBox::StandardButton::Ok);
    }
}
void
DeepProtGui::showWaitingMessage(const QString title)
{
  mpa_waitingMessageDialog->setWindowTitle(title);
  mpa_waitingMessageDialog->show();
  mpa_waitingMessageDialog->raise();
  mpa_waitingMessageDialog->activateWindow();
}

void
DeepProtGui::onMzDataFileClicked()
{

  try
    {
      QSettings settings;
      QString default_mz_location =
        settings.value("path/mzfiles_directory", "").toString();

      QString filename = QFileDialog::getOpenFileName(
        this,
        tr("select peak list file"),
        default_mz_location,
        tr("any mz files (*.mzXML *.mzxml *.mzML *.mzml *.mgf *.tdf);;mzXML "
           "(*.mzXML "
           "*.mzxml);;mzML (*.mzML *.mzml);;MGF (*.mgf);; tdf (*.tdf);;all "
           "files (*)"));

      if(filename.isEmpty())
        {
          return;
        }
      else
        {
          settings.setValue("path/mzfiles_directory",
                            QFileInfo(filename).absolutePath());

          pappso::MsFileAccessor accessor(filename, 0);
          accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                              pappso::FileReaderType::tims_ms2);
          msp_msrunReader = accessor.getMsRunReaderSPtrByRunId("", "msrun");

          if(accessor.getFileFormat() == pappso::MsDataFormat::brukerTims)
            {
              qDebug();
              pappso::TimsMsRunReaderMs2 *tims2_reader =
                dynamic_cast<pappso::TimsMsRunReaderMs2 *>(
                  msp_msrunReader.get());
              if(tims2_reader != nullptr)
                {
                  qDebug();
                  tims2_reader->setMs2FilterCstSPtr(
                    std::make_shared<pappso::FilterSuiteString>(
                      "chargeDeconvolution|0.02dalton mzExclusion|0.01dalton"));
                  qDebug();
                }
              qDebug();
            }
          ui->mzDataLabel->setText(QFileInfo(filename).fileName());
          ui->mzDataLabel->show();
        }
    }
  catch(pappso::PappsoException &error)
    {
      QMessageBox::warning(
        this,
        tr("Error choosing mz data file :%1").arg(error.qwhat()),
        "error");
    }
}

void
DeepProtGui::onGenerateParameterFileClicked()
{
  try
    {

      QString specoms_parameter_path =
        QFileDialog::getSaveFileName(this,
                                     tr("Choose parameter file destination"),
                                     "specoms_parameters.ods",
                                     tr("Spreadsheet (*.ods)"));
      if(!specoms_parameter_path.isEmpty())
        {
          OdsDocWriter *p_writer = nullptr;
          try
            {
              qDebug() << specoms_parameter_path;
              p_writer = new OdsDocWriter(specoms_parameter_path);
              DeepProtParams parameters = DeepProtParams::getInstance();
              parameters.set(DeepProtParam::PeptideDigestionPeptideMaximumSize,
                             30);
              parameters.set(DeepProtParam::ProteinReverse, true);
              parameters.set(
                DeepProtParam::PeptideDigestionVariableModifications,
                QString());
              parameters.set(
                DeepProtParam::PeptideDigestionNumberOfMissedCleavages, 1);
              parameters.set(DeepProtParam::PeptideMissedCleavageInSpecTree,
                             true);
              parameters.set(DeepProtParam::SpecXtractMinimumSimilarity, 6);
              parameters.set(DeepProtParam::SpecFitMinimalMassDelta, -500);
              parameters.set(DeepProtParam::SpectrumNmostIntense, 100);
              parameters.set(DeepProtParam::SpectrumRemoveIsotope, false);
              parameters.set(DeepProtParam::SpectrumFilterChargeDeconvolution,
                             true);
              parameters.set(DeepProtParam::SpectrumDynamicRange, 0);
              parameters.set(DeepProtParam::SpectrumUpperMzLimit, 2000);
              parameters.set(
                DeepProtParam::ExperimentalSpectrumModelMinimumNumberOfPeaks,
                10);
              parameters.set(DeepProtParam::SpectrumComplementIonEnhancer,
                             true);
              parameters.save(*p_writer);
              qDebug();
              p_writer->close();

              qDebug();

              launchDesktopSoftwareOnFile(specoms_parameter_path);
              qDebug();
            }
          catch(OdsException &error)
            {
              if(p_writer != nullptr)
                p_writer->close();
              throw pappso::PappsoException(
                tr("error while writing parameters :\n%1").arg(error.qwhat()));
            }
          catch(pappso::PappsoException &error)
            {
              if(p_writer != nullptr)
                p_writer->close();
              throw pappso::PappsoException(
                tr("error while writing parameters :\n%1").arg(error.qwhat()));
            }
        }
    }
  catch(pappso::PappsoException &error)
    {
      QMessageBox::warning(
        this,
        tr("Error generating parameter file :%1").arg(error.qwhat()),
        "error");
    }
}

void
DeepProtGui::onFastaFilesClicked()
{

  try
    {
      QSettings settings;
      QString default_fasta_location =
        settings.value("path/tandemrun_fastafiles_directory", "").toString();

      QStringList filenames = QFileDialog::getOpenFileNames(
        this,
        tr("FASTA files"),
        default_fasta_location,
        tr("FASTA files (*.fasta);;all files (*)"));

      if(filenames.size() > 0)
        {
          settings.setValue("path/tandemrun_fastafiles_directory",
                            QFileInfo(filenames[0]).absolutePath());
        }

      ui->fastaFileListWidget->clear();
      m_fastaFileinfoList.clear();
      for(QString filename_str : filenames)
        {
          m_fastaFileinfoList.push_back(QFileInfo(filename_str));
          ui->fastaFileListWidget->addItem(
            QFileInfo(filename_str).completeBaseName());
        }
    }
  catch(pappso::PappsoException &error)
    {
      // QMessageBox::warning(this,
      //                  tr("Error choosing identification result files :
      //                  %1").arg(error.qwhat()), error);
    }
}
void
DeepProtGui::onParameterFileClicked()
{

  try
    {
      QSettings settings;
      QString default_param_location =
        settings.value("path/param_file", "").toString();

      QString filename = QFileDialog::getOpenFileName(
        this,
        tr("select DeepProt parameter file"),
        default_param_location,
        tr("any tabulated file (*.ods *.csv *.tsv);;all files (*)"));

      if(filename.isEmpty())
        {
          return;
        }
      else
        {

          m_parameterFile = QFileInfo(filename);
          settings.setValue("path/param_file",
                            m_parameterFile.absoluteFilePath());
          ui->parameterLabel->show();
          ui->parameterLabel->setText(m_parameterFile.fileName());
        }
    }
  catch(pappso::PappsoException &error)
    {
      // QMessageBox::warning(this,
      //                  tr("Error choosing identification result files :
      //                  %1").arg(error.qwhat()), error);
    }
}

WaitingMessageDialog *
DeepProtGui::getWaitingMessageDialog() const
{
  return mpa_waitingMessageDialog;
}

void
DeepProtGui::onRunClicked()
{
  try
    {
      if(m_fastaFileinfoList.size() == 0)
        {
          throw pappso::PappsoException(
            QObject::tr("Unable to run DeepProt on empty Fasta file list"));
        }
      if(msp_msrunReader == nullptr)
        {
          throw pappso::PappsoException(
            QObject::tr("Unable to run DeepProt without mz data file"));
        }


      QSettings settings;
      QString filename;
      QString default_location;
      QAbstractButton *checkedFormatButton =
        ui->outputFormatGroup->checkedButton();
      if(checkedFormatButton == ui->odsFormatButton)
        {
          default_location = settings.value("path/output_ods", "").toString();

          filename = QFileDialog::getSaveFileName(
            this,
            tr("Save ODS output file"),
            QString("%1/untitled.ods").arg(default_location),
            tr("Open Document Spreadsheet (*.ods)"));

          settings.setValue("path/output_ods",
                            QFileInfo(filename).absolutePath());
          qDebug() << settings.value("path/output_ods", "").toString();
        }
      else if(checkedFormatButton == ui->mzIdentMlButton)
        {

          default_location = settings.value("path/output_mzid", "").toString();
          filename         = QFileDialog::getSaveFileName(
            this,
            tr("Save mzIdentML file"),
            QString("%1/untitled.mzid").arg(default_location),
            tr("mzIdentML (*.mzid)"));

          settings.setValue("path/output_mzid",
                            QFileInfo(filename).absolutePath());
          qDebug() << settings.value("path/output_mzid", "").toString();
        }
      else
        {
          default_location = settings.value("path/output_tsv", "").toString();
          filename         = QFileDialog::getExistingDirectory(
            this, tr("Save TSV files"), QString("%1").arg(default_location));

          QDir parent(filename);
          parent.cdUp();
          settings.setValue("path/output_tsv", parent.canonicalPath());
          qDebug() << settings.value("path/output_tsv", "").toString();
        }
      if(filename.isEmpty())
        {
          return;
        }


      showWaitingMessage(tr("running"));
      mpa_waitingMessageDialog->stopWorkerThread();

      DeepProtParams &deepprot_params = DeepProtParams::getInstance();
      if(m_parameterFile.exists())
        {
          deepprot_params.load(m_parameterFile.absoluteFilePath());
        }

      bool all_psm_candidates = false;
      if(ui->allPsmsGroup->checkedButton() == ui->allPsmsButton)
        {
          all_psm_candidates = true;
        }

      emit runDeepProt(&deepprot_params,
                       msp_msrunReader,
                       m_fastaFileinfoList,
                       m_decoyFastaFileinfoList,
                       QFileInfo(filename),
                       ui->threadNumberSpinBox->value(),
                       all_psm_candidates);
      // emit operateXpipFile(filename);
    }
  catch(pappso::PappsoException &error)
    {
      viewError(tr("Error while running DeepProt :\n%1").arg(error.qwhat()));
    }
}


void
DeepProtGui::launchDesktopSoftwareOnFile(const QString &filePath) const
{
  QDesktopServices::openUrl(
    QUrl(QString("file:///").append(filePath), QUrl::TolerantMode));
}
