/**
 * \file gui/deepprot-gui.h
 * \date 13/04/2020
 * \author Olivier Langella
 * \brief simple GUI to launch deepprot job
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once


#include <QMainWindow>
#include <QCloseEvent>
#include <QThread>
#include "workerthread.h"
#include "../utils/deepprotparams.h"
#include "waiting_message_dialog/waitingmessagedialog.h"


namespace Ui
{
class DeepProtMain;
}


class DeepProtGui : public QMainWindow
{
  Q_OBJECT

  public:
  explicit DeepProtGui(QWidget *parent = 0);
  ~DeepProtGui();

  public slots:
  void onMzDataFileClicked();
  void onFastaFilesClicked();
  void onParameterFileClicked();
  void onGenerateParameterFileClicked();
  void onRunClicked();
  void doDisplayLoadingMessage(QString message);
  void doDisplayLoadingMessagePercent(QString message, int value);
  void doOperationFailed(QString);
  void doOperationFinished(WorkerThreadOperationType type, QString message);

  signals:

  void runDeepProt(DeepProtParams *deepprot_params,
                   pappso::MsRunReaderSPtr msrun_reader,
                   std::vector<QFileInfo> fasta_fileinfo,
                   std::vector<QFileInfo> decoy_fasta_fileinfo_list,
                   QFileInfo output_file,
                   int threads,
                   bool all_candidates);

  protected:
  void closeEvent(QCloseEvent *event) override;
  void launchDesktopSoftwareOnFile(const QString &filePath) const;


  public:
  void showWaitingMessage(const QString title);
  void hideWaitingMessage();
  void viewError(QString error);

  WaitingMessageDialog *getWaitingMessageDialog() const;


  private:
  Ui::DeepProtMain *ui;
  QThread m_workerThread;
  WorkerThread *mpa_worker;
  WaitingMessageDialog *mpa_waitingMessageDialog;


  pappso::MsRunReaderSPtr msp_msrunReader;
  std::vector<QFileInfo> m_fastaFileinfoList;
  std::vector<QFileInfo> m_decoyFastaFileinfoList;
  QFileInfo m_parameterFile;
};
