
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <QApplication>
#include <iostream>
#include <pappsomspp/pappsoexception.h>
#include "deepprot-gui.h"

using namespace std;

int
main(int argc, char *argv[])
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QApplication app(argc, argv);

  qRegisterMetaType<std::vector<QFileInfo>>("std::vector<QFileInfo>");
  qRegisterMetaType<WorkerThreadOperationType>("WorkerThreadOperationType");
  try
    {
      QCoreApplication::setOrganizationName("PAPPSO");
      QCoreApplication::setOrganizationDomain("pappso.inrae.fr");
      QCoreApplication::setApplicationName("DeepProt");
      DeepProtGui window;
      window.show();

      return app.exec();
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in DeepProt. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      app.exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in DeepProt. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      app.exit(1);
    }
}
