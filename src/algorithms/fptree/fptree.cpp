/**
 * \file algorithms/fptree/fptree.cpp
 * \date 21/10/2018
 * \author Olivier Langella
 * \brief SpecTrees datastructure
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "fptree.h"
#include <QDebug>
#include <pappsomspp/exception/exceptionoutofrange.h>


FPTree::~FPTree()
{
}

FPTree::FPTree(pappso::spectree::BucketClustering &clustering)
  : m_specTree(clustering)
{
  qDebug();
}


void
FPTree::extractor(MonitorInterface &monitor,
                  SpecTreeExtractorReporterInterface *reporter_p,
                  std::size_t threshold,
                  std::size_t limit) const
{
  qDebug() << "threshold=" << threshold << " limit=" << limit;

  monitor.startMonitoredOperation(MonitoredOperation::SpecXtract);
  
  qDebug();
  monitor.message(
    QObject::tr("starting %1 operation")
      .arg(monitor.getOperationName(MonitoredOperation::SpecXtract)));
  
  qDebug();
  reporter_p->startingExtraction();
  
  qDebug();
  std::size_t spectrum_array_size =
    m_specTree.getSpectrumFirstNodeIndex().size();
  monitor.setProgressMaximumValue(spectrum_array_size - limit);

  qDebug();
  m_specTree.xtract(monitor,
                    *reporter_p,
                    threshold,
                    spectrum_array_size - 1,
                    limit,
                    limit - 1,
                    0);

  reporter_p->endingExtraction();
  monitor.finishedMonitoredOperation(MonitoredOperation::SpecXtract);
  qDebug();
  // DataWriter.close();
}

void
FPTree::writeTree(CalcWriterInterface *p_writer)
{
  qDebug();
  OdsTableCellStyle style;
  style.setBackgroundColor(QColor("yellow"));
  style.setTextColor(QColor("red"));

  OdsTableCellStyleRef ref_parent = p_writer->getTableCellStyleRef(style);


  p_writer->writeSheet("spec trees");
  p_writer->writeCell("spectrum index");
  p_writer->writeLine();
  for(std::size_t spectrum_index = 0;
      spectrum_index < m_specTree.getSpectrumFirstNodeIndex().size();
      spectrum_index++)
    {
      p_writer->writeCell((int)spectrum_index);
      std::size_t node_position =
        m_specTree.getSpectrumFirstNodeIndex()[spectrum_index];
      while(node_position > 0)
        {
          p_writer->setTableCellStyleRef(ref_parent);
          // p_writer->writeCell((int)m_value[m_parent[node_position]]);
          p_writer->clearTableCellStyleRef();
          // p_writer->writeCell((int)m_count[node_position]);
          // node_position = m_next[node_position];
        }
      p_writer->writeLine();
    }
  qDebug();
}
