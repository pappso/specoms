/**
 * \file algorithms/fptree/fptree.h
 * \date 21/10/2018
 * \author Olivier Langella
 * \brief SpecTrees datastructure
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <pappsomspp/processing/spectree/bucketclustering.h>
#include <pappsomspp/processing/spectree/spectree.h>
#include "spectreeextractorreporterinterface.h"
#include <odsstream/calcwriterinterface.h>
#include "../../utils/monitorinterface.h"

/***
 * SpecTrees datastructure aiming for cheap memory consumption and efficient
 * processing. The following implementation simulates the forest of inverted
 * trees used in SpecTrees along with the transversal accessor using mostly only
 * primitive arrays of integers.
 * @author Matthieu David
 * @version 0.1
 */
class FPTree
{

  public:
  /**
   * Initialisation of SpecTrees.
   * The proper of algorithms is set and SpecTrees structures are then
   * initialised to their maximum possible size. SpecTrees is finally built (the
   * structures are filled) using the previously chosen algorithm.
   * @param clustering The bucket clustering in which spectra identifiers have
   * been sorted according to their masses
   * @param algorithm The pair of algorithms used to build and extract data from
   * SpecTrees
   * @return Built and filled SpecTrees datastructure
   */
  FPTree(pappso::spectree::BucketClustering &clustering);

  virtual ~FPTree();

  /** @brief write spec trees into a spreadsheet
   */
  void writeTree(CalcWriterInterface *p_writer);


  /**
   * Extract all similarities above a threshold value between all spectra from
   * the deepest one up to the given limit using the predefined extraction
   * algorithm. The results are stored in the mentionned reporter.
   *
   * Performs the extraction of the similarities above a given threshold for all
   * spectra between the deepest one and a given index in the transversal
   * accessor. The extracted pairs are feed to a shifter to try to improve the
   * spectrum identification. Afterwards, the retained results are written to
   * the given reporter. This extraction step represents most of SpecOMS
   * execution time. Note: This function heavily evolved through the development
   * process to match new needs, it became clumsy and heavy. A clean refactor
   * might be appropriate.
   *
   * @param monitor progress monitor, indicates progression in spectree
   * @param reporter report results of similarities to the user (write in file
   * or consolidate)
   * @param threshold The minimum threshold for a similarity to be reported
   * @param limit The position in the transversal accessor up to which the
   * similarities must be reported
   */
  virtual void extractor(MonitorInterface &monitor,
                         SpecTreeExtractorReporterInterface *reporter_p,
                         std::size_t threshold,
                         std::size_t limit) const;


  protected:
  pappso::spectree::SpecTree m_specTree;
};
