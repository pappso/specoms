/**
 * \file algorithms/fptree/threadedfptree.cpp
 * \date 5/4/2020
 * \author Olivier Langella
 * \brief Thread resistant SpecTrees datastructure
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "threadedfptree.h"
#include <QDebug>
#include <QtConcurrent/QtConcurrent>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <pappsomspp/exception/exceptioninterrupted.h>

ThreadedFptree::ThreadedFptree(pappso::spectree::BucketClustering &clustering)
  : FPTree(clustering)
{
}

ThreadedFptree::~ThreadedFptree()
{
}

void
ThreadedFptree::extractor(MonitorInterface &monitor,
                          SpecTreeExtractorReporterInterface *reporter_p,
                          std::size_t threshold,
                          std::size_t limit) const
{
  qDebug() << "threshold=" << threshold << " limit=" << limit;

  monitor.startMonitoredOperation(MonitoredOperation::SpecXtract);
  monitor.message(
    QObject::tr("starting %1 operation")
      .arg(monitor.getOperationName(MonitoredOperation::SpecXtract)));
  reporter_p->startingExtraction();

  std::size_t spectrum_array_size =
    m_specTree.getSpectrumFirstNodeIndex().size();

  std::atomic_size_t total(spectrum_array_size - limit);

  std::size_t chunck_size = 100;
  monitor.setProgressMaximumValue(total / chunck_size);

  // iteration on the transversal accessor starting from the deepest spetrum
  // and climbing up to limit
  std::vector<std::size_t> spectra_list;

  //  exit(1);
  for(std::size_t spectra1 = spectrum_array_size - 1; spectra1 >= limit;
      spectra1 -= chunck_size)
    {
      qDebug() << "spectra1=" << spectra1;
      spectra_list.push_back(spectra1);
      if(spectra1 < chunck_size)
        break;
    }


  const ThreadedFptree *p_this = this;

  QtConcurrent::blockingMap(
    spectra_list.begin(),
    spectra_list.end(),
    [p_this, &monitor, reporter_p, threshold, limit, chunck_size](
      std::size_t &one_spectra) {
      monitor.count();

      if(monitor.shouldIstop())
        {
          throw pappso::ExceptionInterrupted(
            QObject::tr("SpecXtract job stopped"));
        }
      qDebug() << one_spectra;
      // System.out.println(start);
      std::size_t minimum_to_extract = 0;
      if(one_spectra >= chunck_size)
        minimum_to_extract = one_spectra - chunck_size;
      if(minimum_to_extract < limit)
        {
          minimum_to_extract = limit;
        }
      pappso::UiMonitorVoid monitor_loop;
      p_this->m_specTree.xtract(monitor_loop,
                                *reporter_p,
                                threshold,
                                one_spectra,
                                minimum_to_extract,
                                limit - 1,
                                0);
    });

  reporter_p->endingExtraction();
  monitor.finishedMonitoredOperation(MonitoredOperation::SpecXtract);
  qDebug();
}
