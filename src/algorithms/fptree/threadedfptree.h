/**
 * \file algorithms/fptree/threadedfptree.h
 * \date 5/4/2020
 * \author Olivier Langella
 * \brief Thread resistant SpecTrees datastructure
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#pragma once

#include "fptree.h"

/**
 * inherited class of FPTree that gives multi thread capabilities
 */
class ThreadedFptree : public FPTree
{
  public:
  /**
   * Default constructor
   */
  ThreadedFptree(pappso::spectree::BucketClustering &clustering);

  /**
   * Destructor
   */
  virtual ~ThreadedFptree();

  virtual void extractor(MonitorInterface &monitor,
                         SpecTreeExtractorReporterInterface *reporter_p,
                         std::size_t threshold,
                         std::size_t limit) const override;
};
