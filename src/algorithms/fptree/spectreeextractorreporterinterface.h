/**
 * \file algorithms/bucket_clustering/spectreeextractorreporterinterface.h
 * \date 22/10/2018
 * \author Olivier Langella
 * \brief visitor interface to report extraction results of SpecTrees
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include "../../utils/types.h"
#include <pappsomspp/processing/spectree/specxtractinterface.h>

class SpecTreeExtractorReporterInterface
  : public pappso::spectree::SpecXtractInterface
{
  public:
  virtual void startingExtraction();
  virtual void endingExtraction() = 0;


  virtual void reportSimilarity(std::size_t cart_id_a,
                                std::size_t cart_id_b,
                                std::size_t similarity) override      = 0;
  virtual void beginItemCartExtraction(std::size_t spectra1) override = 0;
  virtual void endItemCartExtraction(std::size_t spectra1) override   = 0;

};
