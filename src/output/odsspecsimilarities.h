/**
 * \file output/odsspecsimiliraties.h
 * \date 23/10/2018
 * \author Olivier Langella
 * \brief Simple ODS report of similarities between spectrum
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include "../algorithms/fptree/spectreeextractorreporterinterface.h"
#include <odsstream/calcwriterinterface.h>
#include "../core/spectrumintstore.h"

class OdsSpecSimilarities : public SpecTreeExtractorReporterInterface
{
  private:
  CalcWriterInterface *mp_writer;
  SpectrumIntStoreSPtr msp_spectrum_int_store;

  public:
  OdsSpecSimilarities(CalcWriterInterface *p_writer,
                      SpectrumIntStoreSPtr &spectrum_int_store);
  virtual void startingExtraction() override;
  virtual void reportSimilarity(std::size_t cart_id_a,
                                std::size_t cart_id_b,
                                std::size_t similarity) override;
};
