/**
 * \file output/odsoutput.h
 * \date 01/04/2020
 * \author Olivier Langella
 * \brief dedicated output in ODS file
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "outputinterface.h"
#include <odsstream/calcwriterinterface.h>
/**
 * @todo write docs
 */
class OdsOutput : public OutputInterface
{
  public:
  /**
   * Default constructor
   */
  OdsOutput(const QString &outputFileStr);

  /**
   * Destructor
   */
  virtual ~OdsOutput();


  virtual void
  setMsRunReaderSPtr(pappso::MsRunReaderSPtr msrun_reader) override;
  virtual void
  setDeepProtParams(const DeepProtParams &deepprot_params) override;
  virtual void close() override;

  virtual void
  setPeptideDatabaseSPtr(PeptideDatabaseSPtr &msp_peptide_database) override;


  virtual void writePeptideCandidateList(
    const pappso::QualifiedMassSpectrum &experimental_spectrum_int,
    const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
    pappso::DeepProtMatchType match_type) override;

  private:
  void writeSpecfitHeader() const;

  void writePeptideCandidate(
    const pappso::QualifiedMassSpectrum &experimental_spectrum_int,
    const OutputPeptideCandidateStruct &peptide_candidate) const;

  private:
  CalcWriterInterface *mpa_calcWriter;
  PeptideDatabaseSPtr msp_peptideDatabase = nullptr;
  pappso::MsRunReaderSPtr msp_msrunReader = nullptr;


  QMutex m_mutex;
};
