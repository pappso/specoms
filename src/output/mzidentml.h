/**
 * \file output/mzidentml.h
 * \date 24/10/2019
 * \author Olivier Langella
 * \brief Write identification results in mzIdentML format
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QFile>
#include <QXmlStreamWriter>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/amino_acid/aa.h>
#include <pappsomspp/processing/xml/mzidentmlwriter.h>
#include "outputinterface.h"
#include "../core/spectrum_matcher_legacy/peptidecandidate.h"
/**
 * @todo write docs
 */
class MzIdentMl : public OutputInterface
{
  public:
  /**
   * Default constructor
   */
  MzIdentMl(const QString &mzIdentMlFile);

  /**
   * Destructor
   */
  virtual ~MzIdentMl();


  virtual void
  setMsRunReaderSPtr(pappso::MsRunReaderSPtr msrun_reader) override;
  virtual void
  setDeepProtParams(const DeepProtParams &deepprot_params) override;

  virtual void
  setPeptideDatabaseSPtr(PeptideDatabaseSPtr &msp_peptide_database) override;


  virtual void writePeptideCandidateList(
    const pappso::QualifiedMassSpectrum &experimental_spectrum,
    const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
    pappso::DeepProtMatchType match_type) override;

  /** @brief definitively close this output (flush, write content if needed)
   */
  virtual void close() override;


  protected:
  struct PeptideCandidateStruct
  {
    pappso::PeptideSp refinedPeptide;
    std::size_t originalCount;
    std::size_t fittedCount;
    pappso::DeepProtPeptideCandidateStatus peptideCandidateStatus;
    std::vector<std::size_t> proteinIndexList;
    pappso::DeepProtMatchType matchType;
    double massDelta;
    std::vector<std::size_t> deltaPositions;
  };

  struct experimentalSpectrumResultsStruct
  {
    bool isEmpty() const;
    unsigned int charge;
    QString title;
    std::size_t spectrumIndex;
    double mass;
    std::vector<PeptideCandidateStruct> peptideCandidateList;
  };

  private:
  void writeHeader();

  void writeResults();
  void writeSequenceCollection();
  void writePeptideModifications(const pappso::Aa &aa, unsigned int position);

  void writeAnalysisCollection();
  void writeAnalysisProtocolCollection();
  void writeUserParam(const QString &name,
                      const std::vector<std::size_t> &values);
  void writeInputs();
  void writeFragmentationTable();
  void writeSpectrumIdentificationResultList();
  void writeSpectrumIdentificationResult(
    const experimentalSpectrumResultsStruct &spectrum_result);

  /** @brief get identified proteins index list
   * protein index are managed by FastaSourceList
   */
  std::vector<std::size_t> getIdentifiedProteinIndexList() const;
  std::vector<std::size_t> getIdentifiedPeptideIndexList() const;


  protected:
  const DeepProtParams *mp_deepProtParams = nullptr;
  PeptideDatabaseSPtr msp_peptideDatabase;
  // pappso::MsFileAccessor m_mzDataFile;
  pappso::MzIdentMlWriter *mpa_xmlWriter;
  pappso::MsRunReaderSPtr msp_msrunReader;
  std::size_t m_sir_num = 0;
  // map peptide absolute string to their xml id
  std::map<QString, std::size_t> m_mapPeptideXmlId;
  // map scan numbers to possible peptide hit references
  std::multimap<QString, QString>
    m_mapScanNumberPeptideXmlIdAndPeptideEvidenceRef;


  QString m_spectraDataRefId;

  QFile *mpa_outputFile = nullptr;


  std::vector<experimentalSpectrumResultsStruct> m_resultList;
  QMutex m_mutex;
};
