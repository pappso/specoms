/**
 * \file output/odsspecsimiliraties.cpp
 * \date 23/10/2018
 * \author Olivier Langella
 * \brief Simple ODS report of similarities between spectrum
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "odsspecsimilarities.h"
#include <QDebug>


OdsSpecSimilarities::OdsSpecSimilarities(
  CalcWriterInterface *p_writer, SpectrumIntStoreSPtr &spectrum_int_store)
{
  mp_writer              = p_writer;
  msp_spectrum_int_store = spectrum_int_store;
}


void
OdsSpecSimilarities::startingExtraction()
{
  SpecTreeExtractorReporterInterface::startingExtraction();
  qDebug();
  mp_writer->writeSheet("similarities");
  mp_writer->writeCell("spectra1");
  mp_writer->writeCell("title1");
  mp_writer->writeCell("spectra2");
  mp_writer->writeCell("title2");
  mp_writer->writeCell("count");
  mp_writer->writeLine();


  qDebug();
}


void
OdsSpecSimilarities::reportSimilarity(std::size_t spectra1,
                                      std::size_t spectra2,
                                      std::size_t similarity)
{
  qDebug();
  const SpectrumInt &spectra1_data =
    msp_spectrum_int_store.get()->getSpectrumInt(spectra1);
  const SpectrumInt &spectra2_data =
    msp_spectrum_int_store.get()->getSpectrumInt(spectra2);
  mp_writer->writeCell((int)spectra1);
  mp_writer->writeCell(
    msp_spectrum_int_store.get()->getSpectrumInt(spectra1).getDescriptor());
  mp_writer->writeCell((int)spectra2);
  mp_writer->writeCell(
    msp_spectrum_int_store.get()->getSpectrumInt(spectra2).getDescriptor());
  mp_writer->writeCell((int)similarity);
  mp_writer->writeCell(spectra1_data.getMass() - spectra2_data.getMass());
  mp_writer->writeLine();

  qDebug();
}
