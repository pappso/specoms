/**
 * \file output/mzidentml.h
 * \date 24/10/2019
 * \author Olivier Langella
 * \brief Write identification results in mzIdentML format
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mzidentml.h"
#include <QDateTime>
#include <pappsomspp/exception/exceptionnotfound.h>
#include "../core/fastafile.h"
#include "../core/fastasourcelist.h"


bool
MzIdentMl::experimentalSpectrumResultsStruct::isEmpty() const
{
  return false;
}
MzIdentMl::MzIdentMl(const QString &mzIdentMlFile)
{
  // mp_specXtractMatcher = p_spec_xtract_matcher;
  // msp_peptideDatabase  = mp_specXtractMatcher->getPeptideDatabase();
  // msp_msrunReader      = msrun_reader;

  qDebug();

  mpa_outputFile = new QFile(mzIdentMlFile);

  if(mpa_outputFile->open(QIODevice::WriteOnly))
    {
      mpa_xmlWriter = new pappso::MzIdentMlWriter();
      mpa_xmlWriter->setDevice(mpa_outputFile);

      // MzIdentMl mzidentml_oputput(&spec_fit, msrun_reader);

      // mzidentml_oputput.write(p_output_stream);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("error : cannot open the mzIdentML output file : %1\n")
          .arg(QFileInfo(mzIdentMlFile).absoluteFilePath()));
    }

  qDebug();
}

void
MzIdentMl::close()
{

  if(mpa_outputFile != nullptr)
    {


      writeHeader();
      qDebug();
      writeResults();
      qDebug();

      mpa_xmlWriter->writeEndDocument();

      delete mpa_xmlWriter;
      qDebug();
      mpa_outputFile->close();
      qDebug();
      delete mpa_outputFile;
      mpa_outputFile = nullptr;
    }
}

MzIdentMl::~MzIdentMl()
{
  close();
}


void
MzIdentMl::setMsRunReaderSPtr(pappso::MsRunReaderSPtr msrun_reader)
{
  qDebug();
  msp_msrunReader = msrun_reader;
  qDebug() << msp_msrunReader.get();
}
void
MzIdentMl::setDeepProtParams(const DeepProtParams &deepprot_params
                             [[maybe_unused]])
{
  mp_deepProtParams = &deepprot_params;
}

void
MzIdentMl::setPeptideDatabaseSPtr(PeptideDatabaseSPtr &peptide_database)
{
  msp_peptideDatabase = peptide_database;
}
void
MzIdentMl::writePeptideCandidateList(
  const pappso::QualifiedMassSpectrum &experimental_spectrum_int,
  const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
  pappso::DeepProtMatchType match_type)
{

  QMutexLocker locker(&m_mutex);
  if(peptide_candidate_list.size() == 0)
    {
      qDebug();
    }
  else
    {
      m_resultList.push_back(experimentalSpectrumResultsStruct());

      experimentalSpectrumResultsStruct &experimentalSpectrumResult =
        m_resultList.back();

      experimentalSpectrumResult.charge =
        experimental_spectrum_int.getPrecursorCharge();
      experimentalSpectrumResult.title =
        experimental_spectrum_int.getMassSpectrumId().getNativeId();
      experimentalSpectrumResult.spectrumIndex =
        experimental_spectrum_int.getMassSpectrumId().getSpectrumIndex();
      experimentalSpectrumResult.mass =
        experimental_spectrum_int.getPrecursorMass();


      qDebug();
      const OutputPeptideCandidateStruct &best_peptide_candidate =
        peptide_candidate_list[getBestPeptideCandidateIndex(
          peptide_candidate_list)];
      qDebug();
      m_resultList.back().peptideCandidateList.resize(1);
      m_resultList.back().peptideCandidateList[0].proteinIndexList.clear();


      std::size_t peptide_index = best_peptide_candidate.protoPeptideNum;


      qDebug();
      // for each protein
      std::vector<std::size_t> protein_index_list_b =
        msp_peptideDatabase.get()->getProteinIndexList(peptide_index);

      qDebug();

      for(std::size_t protein_index : protein_index_list_b)
        {
          m_resultList.back()
            .peptideCandidateList[0]
            .proteinIndexList.push_back(protein_index);
        }


      qDebug();
      std::sort(
        m_resultList.back().peptideCandidateList[0].proteinIndexList.begin(),
        m_resultList.back().peptideCandidateList[0].proteinIndexList.end());
      auto it = std::unique(
        m_resultList.back().peptideCandidateList[0].proteinIndexList.begin(),
        m_resultList.back().peptideCandidateList[0].proteinIndexList.end());
      m_resultList.back().peptideCandidateList[0].proteinIndexList.resize(
        std::distance(
          m_resultList.back().peptideCandidateList[0].proteinIndexList.begin(),
          it));

      qDebug();

      PeptideCandidateStruct &peptideCandidateResult =
        m_resultList.back().peptideCandidateList[0];
      peptideCandidateResult.fittedCount = best_peptide_candidate.fittedCount;
      peptideCandidateResult.originalCount =
        best_peptide_candidate.originalCount;

      peptideCandidateResult.peptideCandidateStatus =
        best_peptide_candidate.status;
      peptideCandidateResult.matchType = match_type;
      peptideCandidateResult.massDelta = best_peptide_candidate.massDelta;
      peptideCandidateResult.deltaPositions =
        best_peptide_candidate.deltaPositions;

      QStringList str_positions;
      for(auto position : best_peptide_candidate.deltaPositions)
        {
          str_positions << QString("%1").arg(position);
        }
      qDebug() << str_positions.join(" ");
      // best_peptide_candidate.print(qDebug());

      peptideCandidateResult.refinedPeptide =
        getRefinedPeptideSp(best_peptide_candidate.peptideModel,
                            best_peptide_candidate.massDelta,
                            best_peptide_candidate.deltaPositions);

      qDebug();
    }
}

void
MzIdentMl::writeHeader()
{
  qDebug();

  mpa_xmlWriter->writeStartMzIdentMlDocument("a1specoms", "1.2.0");

  mpa_xmlWriter->writeCvList();
  //<AnalysisSoftwareList xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">
  mpa_xmlWriter->writeStartElement("AnalysisSoftwareList");
  //<AnalysisSoftware version="X! Tandem Vengeance (2015.12.15.2)" name="X!
  // Tandem" id="ID_software">
  mpa_xmlWriter->writeStartElement("AnalysisSoftware");
  mpa_xmlWriter->writeAttribute("version", VERSION);
  mpa_xmlWriter->writeAttribute("name", SOFTWARE_NAME);
  mpa_xmlWriter->writeAttribute("id", "as1");
  //<SoftwareName>
  mpa_xmlWriter->writeStartElement("SoftwareName");
  //<cvParam accession="MS:1001476" cvRef="PSI-MS" name="X\! Tandem"/>
  // writeCvParam("MS:1001476", "PSI-MS", "X!Tandem");
  mpa_xmlWriter->writeUserParam("SpecOMS:name", SOFTWARE_NAME);
  mpa_xmlWriter->writeUserParam("SpecOMS:version", VERSION);
  //</SoftwareName>
  mpa_xmlWriter->writeEndElement();
  //</AnalysisSoftware>
  mpa_xmlWriter->writeEndElement();
  //</AnalysisSoftwareList>
  mpa_xmlWriter->writeEndElement();


  qDebug();
  if(msp_msrunReader == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("ERROR msp_msrunReader == nullptr in :\n%1 %2 %3")
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  m_spectraDataRefId =
    QString("sid%1").arg(msp_msrunReader.get()->getMsRunId().get()->getXmlId());

  qDebug();
}


void
MzIdentMl::writeResults()
{

  qDebug();
  try
    {


      //<SequenceCollection xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">
      mpa_xmlWriter->writeStartElement("SequenceCollection");
      writeSequenceCollection();

      //<SequenceCollection
      mpa_xmlWriter->writeEndElement();


      writeAnalysisCollection();

      writeAnalysisProtocolCollection();


      //<DataCollection xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">
      mpa_xmlWriter->writeStartElement("DataCollection");

      writeInputs();
      //<AnalysisData>
      mpa_xmlWriter->writeStartElement("AnalysisData");

      //<SpectrumIdentificationList id="SI_LIST_1">
      mpa_xmlWriter->writeStartElement("SpectrumIdentificationList");
      mpa_xmlWriter->writeAttribute("id", "sil1");

      writeFragmentationTable();

      writeSpectrumIdentificationResultList();
      //</SpectrumIdentificationList>
      mpa_xmlWriter->writeEndElement();

      //</AnalysisData>
      mpa_xmlWriter->writeEndElement();
      //</DataCollection
      mpa_xmlWriter->writeEndElement();
    }

  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("error writing mzIdentML file. PappsoException :\n%1")
          .arg(error.qwhat()));
    }

  catch(std::exception &error)
    {
      throw pappso::PappsoException(
        QObject::tr("error writing mzIdentML file. exception :\n%1")
          .arg(error.what()));
    }

  qDebug();
}

void
MzIdentMl::writeSpectrumIdentificationResultList()
{
  qDebug();
  for(const experimentalSpectrumResultsStruct &spectrum_matcher : m_resultList)
    {
      writeSpectrumIdentificationResult(spectrum_matcher);
    }
  qDebug();
}

void
MzIdentMl::writeSpectrumIdentificationResult(
  const experimentalSpectrumResultsStruct &spectrum_result)
{
  qDebug();
  if(!spectrum_result.isEmpty())
    {
      std::size_t scan_number = spectrum_result.spectrumIndex;
      //<SpectrumIdentificationResult spectraData_ref="SID_1"
      // spectrumID="000.mzXML scan 134 (charge 1)" id="SIR_134">
      mpa_xmlWriter->writeStartElement("SpectrumIdentificationResult");
      mpa_xmlWriter->writeAttribute("spectraData_ref", m_spectraDataRefId);
      mpa_xmlWriter->writeAttribute("spectrumID", spectrum_result.title);
      mpa_xmlWriter->writeAttribute("id", QString("sir%1").arg(scan_number));


      // important : one peptide result by scan
      std::size_t rank = 1;
      for(auto &peptide_candidate : spectrum_result.peptideCandidateList)
        {
          pappso::PeptideSp peptide_sp = peptide_candidate.refinedPeptide;


          //<SpectrumIdentificationItem passThreshold="true" rank="1"
          // peptide_ref="Pep_134_1_1" calculatedMassToCharge="804.4102"
          ////experimentalMassToCharge="804.4080" chargeState="1"
          /// id="SII_134_0">

          mpa_xmlWriter->writeStartElement("SpectrumIdentificationItem");
          mpa_xmlWriter->writeAttribute("passThreshold", "true");
          mpa_xmlWriter->writeAttribute("rank", QString("%1").arg(rank));

          auto peptide_xml_id_it =
            m_mapPeptideXmlId.find(peptide_sp.get()->toAbsoluteString());
          if(peptide_xml_id_it == m_mapPeptideXmlId.end())
            {
              throw pappso::PappsoException(
                QObject::tr(
                  "error in MzIdentMl::writeSpectrumIdentificationResult "
                  "peptide_xml_id_it == m_mapPeptideXmlId.end():\n%1")
                  .arg(peptide_sp.get()->toAbsoluteString()));
            }
          QString peptide_id = QString("pep%1").arg(peptide_xml_id_it->second);
          mpa_xmlWriter->writeAttribute("peptide_ref", peptide_id);
          //
          mpa_xmlWriter->writeAttribute(
            "calculatedMassToCharge",
            pappso::MzIdentMlWriter::toXmlMass(
              peptide_sp.get()->getMz(spectrum_result.charge)));

          mpa_xmlWriter->writeAttribute(
            "experimentalMassToCharge",
            pappso::MzIdentMlWriter::toXmlMass(
              (spectrum_result.mass +
               ((double)spectrum_result.charge * pappso::MHPLUS)) /
              ((double)spectrum_result.charge)));
          m_sir_num++;
          mpa_xmlWriter->writeAttribute("id", QString("sii%1").arg(m_sir_num));


          mpa_xmlWriter->writeAttribute(
            "chargeState", QString("%1").arg(spectrum_result.charge));

          QString pep_ev_ref_key =
            QString("%1_%2").arg(scan_number).arg(peptide_id);
          std::multimap<QString, QString>::iterator itscan =
            m_mapScanNumberPeptideXmlIdAndPeptideEvidenceRef.find(
              pep_ev_ref_key);
          while((itscan !=
                 m_mapScanNumberPeptideXmlIdAndPeptideEvidenceRef.end()) &&
                (itscan->first == pep_ev_ref_key))
            {
              mpa_xmlWriter->writeStartElement("PeptideEvidenceRef");
              mpa_xmlWriter->writeAttribute("peptideEvidence_ref",
                                            itscan->second);

              mpa_xmlWriter->writeEndElement();
              itscan++;
            }


          // writeCvParam("MS:1001330","PSI-MS"
          // ,getXmlFloat(spectrum.get()->getEvalue(score.hyperscore))
          // ,"X!Tandem:expect");
          //<cvParam accession="MS:1001331" cvRef="PSI-MS" value="18.3"
          // name="X\!Tandem:hyperscore"/> writeCvParam("MS:1001331","PSI-MS"
          // ,getXmlFloat(score.hyperscore) ,"X!Tandem:hyperscore");
          mpa_xmlWriter->writeUserParam(
            "DeepProt:original_count",
            QString("%1").arg(peptide_candidate.originalCount));
          mpa_xmlWriter->writeUserParam(
            "DeepProt:fitted_count",
            QString("%1").arg(peptide_candidate.fittedCount));
          mpa_xmlWriter->writeUserParam(peptide_candidate.matchType);
          mpa_xmlWriter->writeUserParam(
            peptide_candidate.peptideCandidateStatus);
          mpa_xmlWriter->writeUserParam("DeepProt:mass_delta",
                                        peptide_candidate.massDelta);
          writeUserParam("DeepProt:delta_positions",
                         peptide_candidate.deltaPositions);

          mpa_xmlWriter->writeUserParam(
            "DeepProt:fitted_proForma",
            peptide_candidate.refinedPeptide.get()->toProForma());
          //</SpectrumIdentificationItem>
          mpa_xmlWriter->writeEndElement();

          //<cvParam accession="MS:1000894" cvRef="PSI-MS" name="retention time"
          // value="5468.0193" unitAccession="UO:0000010" unitName="second"
          // unitCvRef="UO"/>
          pappso::QualifiedMassSpectrum spectrum =
            msp_msrunReader.get()->qualifiedMassSpectrum(
              spectrum_result.spectrumIndex, false);
          mpa_xmlWriter->writeCvParamUo(
            "PSI-MS",
            "MS:1000016",
            "scan start time",
            pappso::MzIdentMlWriter::toXmlDouble(spectrum.getRtInSeconds()),
            "UO",
            "UO:0000010",
            "second");

          //<cvParam accession="MS:1001115" cvRef="PSI-MS" value="134"
          // name="scan
          // number(s)"/>
          //[Term]
          // id: MS:1003062
          // name: spectrum index
          // def: "Integer index value associated with a spectrum within a
          // collection of spectra, often in a spectral library. By custom,
          // index counters should begin with 0." [PSI:PI] is_a: MS:1000499 !
          // spectrum attribute

          mpa_xmlWriter->writeCvParam(
            "PSI-MS",
            "MS:1003062",
            "spectrum index",
            QString("%1").arg(spectrum_result.spectrumIndex));

          rank++;
        }
      //</SpectrumIdentificationResult>
      mpa_xmlWriter->writeEndElement();
    }
  qDebug();
}

void
MzIdentMl::writeFragmentationTable()
{

  /*
   <FragmentationTable>
  <Measure id="Measure_MZ">
  <cvParam accession="MS:1001225" cvRef="PSI-MS" unitCvRef="PSI-MS"
  unitName="m/z" unitAccession="MS:1000040" name="product ion m/z"/>
  </Measure>
  </FragmentationTable>
  */
}
void
MzIdentMl::writeInputs()
{

  qDebug();
  //<Inputs>
  mpa_xmlWriter->writeStartElement("Inputs");

  const std::vector<FastaFile> fasta_file_list =
    msp_peptideDatabase.get()->getFastaSourceList().get()->getFastaFileList();

  for(const FastaFile &fasta_file : fasta_file_list)
    {
      //<SearchDatabase
      // location="/gorgone/pappso/moulon/users/Olivier/20160830_publi_xtp_yeast_gold_standard/database/orf_trans_all_short.20090508.fasta"
      // id="SearchDB_0">
      mpa_xmlWriter->writeStartElement("SearchDatabase");
      mpa_xmlWriter->writeAttribute(
        "location", QFileInfo(fasta_file.getFastaFile()).absoluteFilePath());
      mpa_xmlWriter->writeAttribute("id", fasta_file.getXmlId());
      //<FileFormat>
      mpa_xmlWriter->writeStartElement("FileFormat");
      //<cvParam accession="MS:1001348" cvRef="PSI-MS" name="FASTA format"/>
      mpa_xmlWriter->writeCvParam("PSI-MS", "MS:1001348", "FASTA format", "");
      //</FileFormat>
      mpa_xmlWriter->writeEndElement();
      //<DatabaseName>
      mpa_xmlWriter->writeStartElement("DatabaseName");
      //	<userParam name="orf_trans_all_short.20090508.fasta"/>
      mpa_xmlWriter->writeStartElement("userParam");
      mpa_xmlWriter->writeAttribute("name", fasta_file.fileName());
      mpa_xmlWriter->writeEndElement();
      //</DatabaseName>
      mpa_xmlWriter->writeEndElement();
      //</SearchDatabase>
      mpa_xmlWriter->writeEndElement();
    }

  const std::vector<FastaFile> fasta_file_decoy_list =
    msp_peptideDatabase.get()
      ->getFastaSourceList()
      .get()
      ->getDecoyFastaFileList();

  for(const FastaFile &fasta_file : fasta_file_decoy_list)
    {
      //<SearchDatabase
      // location="/gorgone/pappso/moulon/users/Olivier/20160830_publi_xtp_yeast_gold_standard/database/orf_trans_all_short.20090508.fasta"
      // id="SearchDB_0">
      mpa_xmlWriter->writeStartElement("SearchDatabase");
      mpa_xmlWriter->writeAttribute(
        "location", QFileInfo(fasta_file.getFastaFile()).absoluteFilePath());
      mpa_xmlWriter->writeAttribute("id", fasta_file.getXmlId());
      //<FileFormat>
      mpa_xmlWriter->writeStartElement("FileFormat");
      //<cvParam accession="MS:1001348" cvRef="PSI-MS" name="FASTA format"/>
      mpa_xmlWriter->writeCvParam("PSI-MS", "MS:1001348", "FASTA format", "");
      //</FileFormat>
      mpa_xmlWriter->writeEndElement();
      //<DatabaseName>
      mpa_xmlWriter->writeStartElement("DatabaseName");
      //	<userParam name="orf_trans_all_short.20090508.fasta"/>
      mpa_xmlWriter->writeStartElement("userParam");
      mpa_xmlWriter->writeAttribute("name", fasta_file.fileName());
      mpa_xmlWriter->writeEndElement();
      //</DatabaseName>
      mpa_xmlWriter->writeEndElement();
      mpa_xmlWriter->writeCvParam(
        "PSI-MS", "MS:1001195", "decoy DB type reverse", "");
      //<cvParam accession="MS:1001197" cvRef="PSI-MS" name="DB composition
      // target+decoy"/> <cvParam accession="MS:1001283" cvRef="PSI-MS"
      // value="^XXX" name="decoy DB accession regexp"/> <cvParam
      // accession="MS:1001195" cvRef="PSI-MS" name="decoy DB type
      // reverse"/>

      //</SearchDatabase>
      mpa_xmlWriter->writeEndElement();
    }

  //<SpectraData
  // location="/gorgone/pappso/moulon/users/Olivier/20160830_publi_xtp_yeast_gold_standard/raw/000.mzXML"
  // name="000.mzXML" id="SID_1">
  mpa_xmlWriter->writeStartElement("SpectraData");
  mpa_xmlWriter->writeAttribute(
    "location", msp_msrunReader.get()->getMsRunId().get()->getFileName());
  mpa_xmlWriter->writeAttribute(
    "name", msp_msrunReader.get()->getMsRunId().get()->getSampleName());
  mpa_xmlWriter->writeAttribute("id", m_spectraDataRefId);

  //<FileFormat>
  mpa_xmlWriter->writeStartElement("FileFormat");
  //<cvParam accession="MS:1000058" cvRef="PSI-MS" name="mzML file"/>
  mpa_xmlWriter->writeCvParam("PSI-MS", "MS:1000058", "mzML file", "");
  //</FileFormat>
  mpa_xmlWriter->writeEndElement();
  //<SpectrumIDFormat>
  mpa_xmlWriter->writeStartElement("SpectrumIDFormat");
  //<cvParam accession="MS:1000768" cvRef="PSI-MS" name="Thermo nativeID
  // format"/>
  mpa_xmlWriter->writeCvParam(
    "PSI-MS", "MS:1000768", "Thermo nativeID format", "");
  //</SpectrumIDFormat>

  mpa_xmlWriter->writeEndElement();
  //</SpectraData>
  mpa_xmlWriter->writeEndElement();

  //</Inputs>
  mpa_xmlWriter->writeEndElement();

  qDebug();
}

void
MzIdentMl::writeAnalysisProtocolCollection()
{


  qDebug();
  //<AnalysisProtocolCollection xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">

  mpa_xmlWriter->writeStartElement("AnalysisProtocolCollection");
  //<SpectrumIdentificationProtocol analysisSoftware_ref="ID_software"
  // id="SearchProtocol_1">
  mpa_xmlWriter->writeStartElement("SpectrumIdentificationProtocol");
  mpa_xmlWriter->writeAttribute("analysisSoftware_ref", "as1");
  mpa_xmlWriter->writeAttribute("id", "sip1");
  //<SearchType>
  mpa_xmlWriter->writeStartElement("SearchType");
  //<cvParam accession="MS:1001083" cvRef="PSI-MS" name="ms-ms search"/>
  mpa_xmlWriter->writeCvParam("PSI-MS", "MS:1001083", "ms-ms search", "");
  //</SearchType>
  mpa_xmlWriter->writeEndElement();


  mpa_xmlWriter->writeStartElement("AdditionalSearchParams");

  mp_deepProtParams->writeMzid(*mpa_xmlWriter);
  // AdditionalSearchParams
  mpa_xmlWriter->writeEndElement();


  //<ModificationParams>

  //_p_digestion_pipeline->writeMzIdentMlModificationParams(mp_xmlWriter);

  //_p_digestion_pipeline->writeMzIdentMlEnzymes(mp_xmlWriter);

  //<ParentTolerance>
  mpa_xmlWriter->writeStartElement("ParentTolerance");
  //<cvParam accession="MS:1001412" cvRef="PSI-MS" unitCvRef="UO"
  // unitName="parts per million" unitAccession="UO:0000169" value="20.0"
  // name="search tolerance plus value"/> <cvParam accession="MS:1001413"
  // cvRef="PSI-MS" unitCvRef="UO" unitName="parts per million"
  // unitAccession="UO:0000169" value="20.0" name="search tolerance minus
  // value"/>
  /*
  writeCvParamUo("PSI-MS",
                 "MS:1001412",
                 "search tolerance minus value",
                 getXmlFloat(mp_specXtractMatcher->),
                 "UO:0000169",
                 "parts per million");
                 */
  //</ParentTolerance>
  mpa_xmlWriter->writeEndElement();
  //<Threshold>
  mpa_xmlWriter->writeStartElement("Threshold");
  //<cvParam accession="MS:1001494" cvRef="PSI-MS" name="no threshold"/>
  mpa_xmlWriter->writeCvParam("PSI-MS", "MS:1001494", "no threshold", "");
  //</Threshold>
  mpa_xmlWriter->writeEndElement();
  //</SpectrumIdentificationProtocol>
  mpa_xmlWriter->writeEndElement();
  //</AnalysisProtocolCollection>
  mpa_xmlWriter->writeEndElement();

  qDebug();
}


void
MzIdentMl::writeAnalysisCollection()
{

  qDebug();
  //<AnalysisCollection xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">
  mpa_xmlWriter->writeStartElement("AnalysisCollection");
  //<SpectrumIdentification spectrumIdentificationList_ref="SI_LIST_1"
  // spectrumIdentificationProtocol_ref="SearchProtocol_1" id="SpecIdent_1">
  mpa_xmlWriter->writeStartElement("SpectrumIdentification");
  mpa_xmlWriter->writeAttribute("spectrumIdentificationList_ref", "sil1");
  mpa_xmlWriter->writeAttribute("spectrumIdentificationProtocol_ref", "sip1");
  mpa_xmlWriter->writeAttribute("id", "si1");
  //<InputSpectra spectraData_ref="SID_1"/>
  mpa_xmlWriter->writeStartElement("InputSpectra");
  mpa_xmlWriter->writeAttribute("spectraData_ref", m_spectraDataRefId);
  mpa_xmlWriter->writeEndElement();

  for(const FastaFile &fasta_file :
      msp_peptideDatabase.get()->getFastaSourceList().get()->getFastaFileList())
    {
      //<SearchDatabaseRef searchDatabase_ref="SearchDB_0"/>
      mpa_xmlWriter->writeStartElement("SearchDatabaseRef");
      mpa_xmlWriter->writeAttribute("searchDatabase_ref",
                                    fasta_file.getXmlId());

      mpa_xmlWriter->writeEndElement();
    }

  for(const FastaFile &fasta_file : msp_peptideDatabase.get()
                                      ->getFastaSourceList()
                                      .get()
                                      ->getDecoyFastaFileList())
    {
      //<SearchDatabaseRef searchDatabase_ref="SearchDB_0"/>
      mpa_xmlWriter->writeStartElement("SearchDatabaseRef");
      mpa_xmlWriter->writeAttribute("searchDatabase_ref",
                                    fasta_file.getXmlId());

      mpa_xmlWriter->writeEndElement();
    }
  //</SpectrumIdentification>
  mpa_xmlWriter->writeEndElement();
  //</AnalysisCollection>
  mpa_xmlWriter->writeEndElement();

  qDebug();
}

std::vector<std::size_t>
MzIdentMl::getIdentifiedProteinIndexList() const
{
  std::vector<std::size_t> protein_index_list;


  for(auto &&oneResult : m_resultList)
    {


      for(auto &peptide_candidate : oneResult.peptideCandidateList)
        {
          for(std::size_t protein_index : peptide_candidate.proteinIndexList)
            {
              protein_index_list.push_back(protein_index);
            }
        }
    }

  std::sort(protein_index_list.begin(), protein_index_list.end());
  auto it = std::unique(protein_index_list.begin(), protein_index_list.end());
  protein_index_list.resize(std::distance(protein_index_list.begin(), it));
  qDebug() << protein_index_list.size();
  return protein_index_list;
}


std::vector<std::size_t>
MzIdentMl::getIdentifiedPeptideIndexList() const
{
  std::vector<std::size_t> peptide_index_list;

  return peptide_index_list;
}
void
MzIdentMl::writeSequenceCollection()
{

  qDebug();

  try
    {
      // qDebug() << " MzIdentMlReporter::writeSequenceCollection() begin" ;
      std::vector<std::size_t> protein_index_list =
        getIdentifiedProteinIndexList();
      qDebug();
      const FastaSourceList &fasta_source_list =
        *(msp_peptideDatabase.get()->getFastaSourceList().get());
      for(std::size_t protein_index : protein_index_list)
        {
          qDebug() << protein_index;
          pappso::Protein protein =
            fasta_source_list.getProteinByIndex(protein_index);
          qDebug() << protein_index;
          FastaFile fasta_file =
            fasta_source_list.getFastaFileByProteinIndex(protein_index);

          qDebug();
          //<DBSequence accession="YGR192C" searchDatabase_ref="SearchDB_0"
          // length="333" id="DBSeq5157">
          QString id = QString("dbseq%1").arg(protein_index);
          qDebug();


          if(protein.getAccession().endsWith(":reversed"))
            {
              mpa_xmlWriter->writeDBSequence(
                id, fasta_file.getXmlId(), protein, true);
            }
          else
            {
              mpa_xmlWriter->writeDBSequence(
                id, fasta_file.getXmlId(), protein, false);
            }
        }
      qDebug();
      for(auto &&oneResult : m_resultList)
        {

          for(auto &peptide_candidate : oneResult.peptideCandidateList)
            {
              pappso::PeptideSp peptide_sp = peptide_candidate.refinedPeptide;

              auto ret =
                m_mapPeptideXmlId.insert(std::pair<QString, std::size_t>(
                  peptide_sp.get()->toAbsoluteString(),
                  m_mapPeptideXmlId.size()));
              if(ret.second)
                {

                  mpa_xmlWriter->writeStartElement("Peptide");
                  QString id = QString("pep%1").arg(ret.first->second);
                  mpa_xmlWriter->writeAttribute("id", id);
                  //<PeptideSequence>ADWGCAED</PeptideSequence>

                  mpa_xmlWriter->writeTextElement(
                    "PeptideSequence", peptide_sp.get()->getSequence());

                  std::vector<pappso::Aa>::const_iterator it =
                    peptide_sp.get()->begin();
                  std::vector<pappso::Aa>::const_iterator itend =
                    peptide_sp.get()->end();
                  unsigned int pos = 1;
                  while(it != itend)
                    {
                      writePeptideModifications(*it, pos);
                      it++;
                      pos++;
                    }
                  //</Peptide>
                  mpa_xmlWriter->writeEndElement();
                }
            }
        }

      qDebug();
      for(auto &&oneResult : m_resultList)
        {
          // const PeptideCandidate &peptide_candidate =
          //  spectrum_matcher.get()->getBestPeptideCandidate();
          // std::size_t peptide_index =
          // peptide_candidate.getProtoPeptideNum();
          for(auto &peptide_candidate : oneResult.peptideCandidateList)
            {
              pappso::PeptideSp peptide_sp = peptide_candidate.refinedPeptide;

              auto peptide_xml_id_it =
                m_mapPeptideXmlId.find(peptide_sp.get()->toAbsoluteString());
              if(peptide_xml_id_it == m_mapPeptideXmlId.end())
                {
                  throw pappso::PappsoException(
                    QObject::tr(
                      "error in MzIdentMl::writeSequenceCollection "
                      "peptide_xml_id_it == m_mapPeptideXmlId.end():\n%1")
                      .arg(peptide_sp.get()->toAbsoluteString()));
                }
              // for each protein


              for(std::size_t protein_index :
                  peptide_candidate.proteinIndexList)
                {
                  //<PeptideEvidence isDecoy="false" post="H" pre="K" end="108"
                  // start="102" peptide_ref="Pep_134_1_1"
                  // dBSequence_ref="DBSeq5157" id="PepEv_134_1_1"/>
                  mpa_xmlWriter->writeStartElement("PeptideEvidence");
                  pappso::Protein protein =
                    fasta_source_list.getProteinByIndex(protein_index);
                  if(protein.getAccession().endsWith(":reversed"))
                    {
                      mpa_xmlWriter->writeAttribute("isDecoy", "true");
                    }
                  else
                    {
                      mpa_xmlWriter->writeAttribute("isDecoy", "false");
                    }
                  QString peptide_id =
                    QString("pep%1").arg(peptide_xml_id_it->second);
                  mpa_xmlWriter->writeAttribute("peptide_ref", peptide_id);
                  mpa_xmlWriter->writeAttribute(
                    "dBSequence_ref", QString("dbseq%1").arg(protein_index));

                  QString peptide_evidence_ref = QString("PepEv%1_%2")
                                                   .arg(oneResult.spectrumIndex)
                                                   .arg(protein_index);

                  QString pep_ev_ref_key = QString("%1_%2")
                                             .arg(oneResult.spectrumIndex)
                                             .arg(peptide_id);
                  m_mapScanNumberPeptideXmlIdAndPeptideEvidenceRef.insert(
                    std::pair<QString, QString>(pep_ev_ref_key,
                                                peptide_evidence_ref));
                  mpa_xmlWriter->writeAttribute("id", peptide_evidence_ref);

                  // start/end positions
                  QString protein_sequence =
                    QString(protein.getSequence()).replace("L", "I");
                  QString peptide_sequence = peptide_sp.get()->getSequenceLi();
                  int position = protein_sequence.indexOf(peptide_sequence);

                  if(position >= 0)
                    {
                      mpa_xmlWriter->writeAttribute(
                        "start", QString("%1").arg(position + 1));
                      mpa_xmlWriter->writeAttribute(
                        "end",
                        QString("%1").arg(position + 1 +
                                          peptide_sequence.size()));
                    }

                  mpa_xmlWriter->writeEndElement();
                }
            }
        }
      qDebug();
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("error in MzIdentMlReporter::writeSequenceCollection :\n%1")
          .arg(error.qwhat()));
    }
  catch(std::exception &error)
    {
      throw pappso::PappsoException(
        QObject::tr("error in MzIdentMlReporter::writeSequenceCollection :\n%1")
          .arg(error.what()));
    }

  qDebug();
}

void
MzIdentMl::writePeptideModifications(const pappso::Aa &aa,
                                     unsigned int position)
{
  // qDebug() << " MzIdentMlReporter::writePeptideModifications begin" ;
  for(const pappso::AaModificationP &p_mod : aa.getModificationList())
    {
      if(p_mod->isInternal())
        continue;
      //<Modification monoisotopicMassDelta="57.0215" location="5">
      if(p_mod->getName().isEmpty())
        {
          // mass delta is not assigned to a PTM
        }
      else
        {
          mpa_xmlWriter->writeStartElement("Modification");
          mpa_xmlWriter->writeAttribute(
            "monoisotopicMassDelta",
            pappso::MzIdentMlWriter::toXmlMass(p_mod->getMass()));
          mpa_xmlWriter->writeAttribute("location",
                                        QString("%1").arg(position));
          //                                    <cvParam accession="UNIMOD:4"
          //                                    cvRef="UNIMOD"
          //                                    name="Carbamidomethyl"/>
          mpa_xmlWriter->writeCvParam(p_mod);
          //                                            </Modification>
          mpa_xmlWriter->writeEndElement();
        }
    }

  // qDebug() << " MzIdentMlReporter::writePeptideModifications end" ;
}


void
MzIdentMl::writeUserParam(const QString &name,
                          const std::vector<std::size_t> &values)
{
  QStringList concat_str;
  for(auto value : values)
    {
      concat_str << QString("%1").arg(value);
    }
  mpa_xmlWriter->writeUserParam(name, concat_str.join(" "));
}
