/**
 * \file output/mzidentmlallcandidates.cpp
 * \date 30/06/2023
 * \author Olivier Langella
 * \brief Write all (not only the best) identification results in mzIdentML
 * format
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2023  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mzidentmlallcandidates.h"

MzIdentMlAllCandidates::MzIdentMlAllCandidates(const QString &mzIdentMlFile)
  : MzIdentMl(mzIdentMlFile)
{
}

MzIdentMlAllCandidates::~MzIdentMlAllCandidates()
{
}

void
MzIdentMlAllCandidates::writePeptideCandidateList(
  const pappso::QualifiedMassSpectrum &experimental_spectrum_int,
  const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
  pappso::DeepProtMatchType match_type)
{

  QMutexLocker locker(&m_mutex);
  if(peptide_candidate_list.size() == 0)
    {
      qDebug();
    }
  else
    {
      m_resultList.push_back(experimentalSpectrumResultsStruct());

      experimentalSpectrumResultsStruct &experimentalSpectrumResult =
        m_resultList.back();

      experimentalSpectrumResult.charge =
        experimental_spectrum_int.getPrecursorCharge();
      experimentalSpectrumResult.title =
        experimental_spectrum_int.getMassSpectrumId().getNativeId();
      experimentalSpectrumResult.spectrumIndex =
        experimental_spectrum_int.getMassSpectrumId().getSpectrumIndex();
      experimentalSpectrumResult.mass =
        experimental_spectrum_int.getPrecursorMass();

      experimentalSpectrumResult.peptideCandidateList.clear();
      // sort peptide_candidate_list on mass delta
      std::vector<OutputPeptideCandidateStruct> peptide_candidate_list_copy(
        peptide_candidate_list);
      std::sort(peptide_candidate_list_copy.begin(),
                peptide_candidate_list_copy.end(),
                [](const OutputPeptideCandidateStruct &a,
                   const OutputPeptideCandidateStruct &b) {
                  if(a.fittedCount > b.fittedCount)
                    return false;
                  return a.massDelta < b.massDelta;
                });


      qDebug();
      for(auto &peptide_candidate : peptide_candidate_list_copy)
        {
          qDebug();
          experimentalSpectrumResult.peptideCandidateList.push_back(
            PeptideCandidateStruct());

          PeptideCandidateStruct &peptide_struct =
            experimentalSpectrumResult.peptideCandidateList.back();
          peptide_struct.proteinIndexList.clear();


          std::size_t peptide_index = peptide_candidate.protoPeptideNum;


          qDebug();
          // for each protein
          std::vector<std::size_t> protein_index_list_b =
            msp_peptideDatabase.get()->getProteinIndexList(peptide_index);

          qDebug();

          for(std::size_t protein_index : protein_index_list_b)
            {
              peptide_struct.proteinIndexList.push_back(protein_index);
            }


          qDebug();
          std::sort(peptide_struct.proteinIndexList.begin(),
                    peptide_struct.proteinIndexList.end());
          auto it = std::unique(peptide_struct.proteinIndexList.begin(),
                                peptide_struct.proteinIndexList.end());
          peptide_struct.proteinIndexList.resize(
            std::distance(peptide_struct.proteinIndexList.begin(), it));

          qDebug();

          peptide_struct.fittedCount   = peptide_candidate.fittedCount;
          peptide_struct.originalCount = peptide_candidate.originalCount;

          peptide_struct.peptideCandidateStatus = peptide_candidate.status;
          peptide_struct.matchType              = match_type;
          peptide_struct.massDelta              = peptide_candidate.massDelta;
          peptide_struct.deltaPositions = peptide_candidate.deltaPositions;

          QStringList str_positions;
          for(auto position : peptide_candidate.deltaPositions)
            {
              str_positions << QString("%1").arg(position);
            }
          qDebug() << str_positions.join(" ");
          // best_peptide_candidate.print(qDebug());

          peptide_struct.refinedPeptide =
            getRefinedPeptideSp(peptide_candidate.peptideModel,
                                peptide_candidate.massDelta,
                                peptide_candidate.deltaPositions);

          qDebug();
        }
    }
}
