/**
 * \file output/odsoutput.cpp
 * \date 01/04/2020
 * \author Olivier Langella
 * \brief dedicated output in ODS file
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "odsoutput.h"
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <pappsomspp/pappsoexception.h>
#include "../utils/utils.h"
#include "../core/fastasourcelist.h"
#include <iostream>

OdsOutput::OdsOutput(const QString &outputFileStr)
{

  qDebug();

  if(QFileInfo(outputFileStr).suffix() == "tsv")
    {
      mpa_calcWriter = new TsvDirectoryWriter(outputFileStr);
    }
  else
    {

      mpa_calcWriter = new OdsDocWriter(outputFileStr);
    }
}

OdsOutput::~OdsOutput()
{
  qDebug();
  delete mpa_calcWriter;
}


void
OdsOutput::setMsRunReaderSPtr(pappso::MsRunReaderSPtr msrun_reader)
{
  msp_msrunReader = msrun_reader;
}


void
OdsOutput::setPeptideDatabaseSPtr(PeptideDatabaseSPtr &p_peptide_database)
{
  qDebug();
  msp_peptideDatabase = p_peptide_database;
}

void
OdsOutput::setDeepProtParams(const DeepProtParams &deepprot_params)
{
  qDebug();
  deepprot_params.save(*mpa_calcWriter);


  mpa_calcWriter->writeLine();
  mpa_calcWriter->writeLine();

  mpa_calcWriter->writeCell("MZ data file");
  if(msp_msrunReader != nullptr)
    {
      mpa_calcWriter->writeLine();
      mpa_calcWriter->writeCell(
        msp_msrunReader.get()->getMsRunId().get()->getFileName());
    }

  mpa_calcWriter->writeLine();
  mpa_calcWriter->writeLine();

  mpa_calcWriter->writeCell("FASTA data files");
  if(msp_peptideDatabase != nullptr)
    {
      for(auto &fasta_file : msp_peptideDatabase.get()
                               ->getFastaSourceList()
                               .get()
                               ->getFastaFileList())
        {

          mpa_calcWriter->writeLine();
          mpa_calcWriter->writeCell(fasta_file.fileName());
        }
    }

  mpa_calcWriter->writeLine();
  mpa_calcWriter->writeLine();

  mpa_calcWriter->writeCell("FASTA decoy data files");
  if(msp_peptideDatabase != nullptr)
    {
      for(auto &fasta_file : msp_peptideDatabase.get()
                               ->getFastaSourceList()
                               .get()
                               ->getDecoyFastaFileList())
        {
          mpa_calcWriter->writeLine();
          mpa_calcWriter->writeCell(fasta_file.fileName());
        }
    }

  writeSpecfitHeader();
}

void
OdsOutput::close()
{
  qDebug();
  mpa_calcWriter->close();
}


void
OdsOutput::writeSpecfitHeader() const
{
  try
    {
      qDebug();
      mpa_calcWriter->writeSheet("specfit");


      OdsTableSettings settings;
      // settings.setHorizontalWindowSplit(4);
      settings.setVerticalSplit(1);
      mpa_calcWriter->setCurrentOdsTableSettings(settings);

      // columns titles
      mpa_calcWriter->writeCell("spectrum ID");
      mpa_calcWriter->writeCell("spectrum index");
      mpa_calcWriter->writeCell("precursor charge");
      mpa_calcWriter->writeCell("precursor mz");
      mpa_calcWriter->writeCell("precursor mass");

      mpa_calcWriter->writeCell("number of candidates");
      mpa_calcWriter->writeCell("category");

      // colum titles for each peptide candidate
      mpa_calcWriter->writeCell("sequence");

      mpa_calcWriter->writeCell("presence in target database");
      mpa_calcWriter->writeCell("presence in decoy database");

      mpa_calcWriter->writeCell("sequence after specfit");
      mpa_calcWriter->writeCell("peptide mass");
      mpa_calcWriter->writeCell("peptide mz");
      mpa_calcWriter->writeCell("score before specfit");
      mpa_calcWriter->writeCell("score after specfit");
      mpa_calcWriter->writeCell("candidate status");
      mpa_calcWriter->writeCell("mass delta");
      mpa_calcWriter->writeCell("mass delta positions");
      mpa_calcWriter->writeCell("proteins");

/*
      for(auto &&matcher : m_spectrumMatcherList)
        {
          matcher->writeOds(p_writer, msp_peptideDatabase.get());
        }
  */  }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR in OdsOutput::writeSpecfitHeader:\n%1")
          .arg(error.qwhat()));
    }
}


void
OdsOutput::writePeptideCandidateList(
  const pappso::QualifiedMassSpectrum &experimental_spectrum_int,
  const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
  pappso::DeepProtMatchType match_type)
{
  QMutexLocker locker(&m_mutex);
  // std::cout << experimental_spectrum_int.title.toStdString() << endl;
  // std::cout.flush();
  // return;
  if(peptide_candidate_list.size() == 0)
    {

      qDebug();
    }
  else
    {
      // std::vector<PeptideCandidate> sorted = m_peptideCandidates;
      // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __FUNCTION__ << "
      // ";
      qDebug();
      /*

                  mpa_calcWriter->writeLine();
                  mpa_calcWriter->writeCell(experimental_spectrum_int.title);
                  mpa_calcWriter->writeCell(experimental_spectrum_int.spectrumIndex);
                  mpa_calcWriter->writeCell(experimental_spectrum_int.charge);
                  return;
        */
      if(experimental_spectrum_int.getPrecursorCharge() == 0)
        {
          throw pappso::PappsoException(
            QObject::tr(
              "ERROR experimental_spectrum_int.charge == 0 in :\n%1 %2 %3")
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }

      if(!m_isOutputAllPsmCandidates)
        {
          const OutputPeptideCandidateStruct &peptide_candidate =
            peptide_candidate_list[getBestPeptideCandidateIndex(
              peptide_candidate_list)];
          mpa_calcWriter->writeLine();
          mpa_calcWriter->writeCell(
            experimental_spectrum_int.getMassSpectrumId().getNativeId());
          mpa_calcWriter->writeCell(
            experimental_spectrum_int.getMassSpectrumId().getSpectrumIndex());
          mpa_calcWriter->writeCell(
            (std::size_t)experimental_spectrum_int.getPrecursorCharge());

          pappso::pappso_double target_mz =
            experimental_spectrum_int.getPrecursorMz();
          mpa_calcWriter->writeCell(target_mz);
          mpa_calcWriter->writeCell(
            experimental_spectrum_int.getPrecursorMass());

          mpa_calcWriter->writeCell((int)peptide_candidate_list.size());
          mpa_calcWriter->writeCell(Utils::EnumToString(match_type));

          writePeptideCandidate(experimental_spectrum_int, peptide_candidate);
        }
      else
        {
          for(auto &&peptide_candidate : peptide_candidate_list)
            {
              mpa_calcWriter->writeLine();
              mpa_calcWriter->writeCell(
                experimental_spectrum_int.getMassSpectrumId().getNativeId());
              mpa_calcWriter->writeCell(
                experimental_spectrum_int.getMassSpectrumId()
                  .getSpectrumIndex());
              mpa_calcWriter->writeCell(
                (std::size_t)experimental_spectrum_int.getPrecursorCharge());

              pappso::pappso_double target_mz =
                experimental_spectrum_int.getPrecursorMz();
              mpa_calcWriter->writeCell(target_mz);
              mpa_calcWriter->writeCell(
                experimental_spectrum_int.getPrecursorMass());

              mpa_calcWriter->writeCell((int)peptide_candidate_list.size());
              mpa_calcWriter->writeCell(Utils::EnumToString(match_type));

              writePeptideCandidate(experimental_spectrum_int,
                                    peptide_candidate);
            }
        }
    }
}


void
OdsOutput::writePeptideCandidate(
  const pappso::QualifiedMassSpectrum &experimental_spectrum_int,
  const OutputPeptideCandidateStruct &peptide_candidate) const
{
  qDebug();
  mpa_calcWriter->writeCell(
    peptide_candidate.originalPeptideString.get()->toString());
  qDebug();
  if(msp_peptideDatabase->isPeptideTarget(peptide_candidate.protoPeptideNum))
    {
      mpa_calcWriter->writeCell("target");
    }
  else
    {
      mpa_calcWriter->writeCell("decoy");
    }

  if(msp_peptideDatabase->isPeptideDecoy(peptide_candidate.protoPeptideNum))
    {
      mpa_calcWriter->writeCell("decoy");
    }
  else
    {
      mpa_calcWriter->writeCell("target");
    }
  qDebug();
  mpa_calcWriter->writeCell(peptide_candidate.peptideModel.get()->toString());
  qDebug();
  mpa_calcWriter->writeCell(peptide_candidate.peptideModel.get()->getMass());
  qDebug();
  mpa_calcWriter->writeCell(peptide_candidate.peptideModel.get()->getMz(
    experimental_spectrum_int.getPrecursorCharge()));
  qDebug();
  // mpa_calcWriter->writeCell(peptide_candidate.getRefinedPeptideSp().get()->toString());
  mpa_calcWriter->writeCell((int)peptide_candidate.originalCount);
  mpa_calcWriter->writeCell((int)peptide_candidate.fittedCount);
  mpa_calcWriter->writeCell(Utils::EnumToString(peptide_candidate.status));


  if(peptide_candidate.peptideModel.get() != nullptr)
    {
      mpa_calcWriter->writeCell(peptide_candidate.massDelta);
    }
  QStringList str_positions;
  for(auto position : peptide_candidate.deltaPositions)
    {
      str_positions << QString("%1").arg(position);
    }
  mpa_calcWriter->writeCell(str_positions.join(" "));

  /*
    QStringList str_protein_index;
    for(auto protein_index : msp_peptideDatabase->getProteinIndexList(
          peptide_candidate.getProtoPeptideNum()))
      {
        str_protein_index << QString("%1").arg(protein_index);
      }
    mpa_calcWriter->writeCell(str_protein_index.join(" "));
  */

  QStringList str_protein_names;
  for(auto protein :
      msp_peptideDatabase->getProteinList(peptide_candidate.protoPeptideNum))
    {
      str_protein_names << protein.get()->getAccession();
    }
  mpa_calcWriter->writeCell(str_protein_names.join(" "));

  qDebug();
}
