/**
 * \file output/outputinterface.h
 * \date 01/04/2020
 * \author Olivier Langella
 * \brief general interface to write DeepProt results
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../core/spectrumint.h"
#include <pappsomspp/msrun/msrunreader.h>
#include "../utils/deepprotparams.h"
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>


struct OutputPeptideCandidateStruct
{
  pappso::PeptideSp originalPeptideString;
  pappso::PeptideSp peptideModel;
  std::size_t protoPeptideNum;
  std::size_t originalCount;
  std::size_t fittedCount;
  pappso::DeepProtPeptideCandidateStatus status;
  double massDelta;
  std::vector<std::size_t> deltaPositions;
};

/**
 * @todo write docs
 */
class OutputInterface
{
  public:
  virtual ~OutputInterface(){};


  virtual void setMsRunReaderSPtr(pappso::MsRunReaderSPtr msrun_reader) = 0;
  virtual void setDeepProtParams(const DeepProtParams &deepprot_params) = 0;

  virtual void
  setPeptideDatabaseSPtr(PeptideDatabaseSPtr &msp_peptide_database) = 0;

  virtual void setOutputAllPsmCandidates(bool is_all_candidates);
  bool isOutputAllPsmCandidates() const;


  virtual void writePeptideCandidateList(
    const pappso::QualifiedMassSpectrum &experimental_spectrum,
    const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
    pappso::DeepProtMatchType match_type) = 0;

  /** @brief definitively close this output (flush, write content if needed)
   */
  virtual void close() = 0;

  protected:
  virtual std::size_t getBestPeptideCandidateIndex(
    const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list)
    const;

  virtual pappso::PeptideSp getRefinedPeptideSp(
    const pappso::PeptideSp &peptide,
    double m_delta,
    const std::vector<std::size_t> &mass_delta_locations) const;

  protected:
  /** @brief write all psm candidates if true
   */
  bool m_isOutputAllPsmCandidates = false;
};
