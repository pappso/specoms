/**
 * \file output/outputinterface.cpp
 * \date 01/04/2020
 * \author Olivier Langella
 * \brief general interface to write DeepProt results
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "outputinterface.h"

#include "../core/spectrum_matcher_legacy/peptidecandidate.h"
#include <pappsomspp/exception/exceptionnotfound.h>
#include <cmath>

std::size_t
OutputInterface::getBestPeptideCandidateIndex(
  const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list) const
{
  std::size_t i     = 0;
  std::size_t besti = 0;
  if(peptide_candidate_list.size() == 0)
    {

      throw pappso::ExceptionNotFound(
        QObject::tr("OutputInterface::getBestPeptideCandidate "
                    "no candidate found"));
    }

  double mass_delta_best = 100000;
  std::size_t max_score  = 0;

  for(auto &&peptide_candidate : peptide_candidate_list)
    {
      // qDebug() << peptide_candidate.getRefinedPeptideSp().get()->toString();
      if(max_score < peptide_candidate.fittedCount)
        {
          max_score       = peptide_candidate.fittedCount;
          mass_delta_best = peptide_candidate.massDelta;
          besti           = i;
        }
      else
        {
          if(mass_delta_best > std::abs(peptide_candidate.massDelta))
            {
              mass_delta_best = peptide_candidate.massDelta;
              besti           = i;
            }
        }
      i++;
    }

  return besti;
}


pappso::PeptideSp
OutputInterface::getRefinedPeptideSp(
  const pappso::PeptideSp &peptide,
  double m_delta,
  const std::vector<std::size_t> &mass_delta_locations) const
{

  qDebug();
  pappso::Peptide new_peptide(*(peptide.get()));

  if(mass_delta_locations.size() == 0)
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstanceCustomizedMod(m_delta);
      new_peptide.addAaModification(modification, 0);
    }
  else
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstanceCustomizedMod(m_delta);
      new_peptide.addAaModification(modification, mass_delta_locations[0]);
    }


  qDebug();
  return new_peptide.makePeptideSp();
}

void
OutputInterface::setOutputAllPsmCandidates(bool is_all_candidates)
{
  m_isOutputAllPsmCandidates = is_all_candidates;
}

bool
OutputInterface::isOutputAllPsmCandidates() const
{
  return m_isOutputAllPsmCandidates;
}
