/**
 * \file output/mzidentmlallcandidates.h
 * \date 30/06/2023
 * \author Olivier Langella
 * \brief Write all (not only the best) identification results in mzIdentML
 * format
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2023  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "mzidentml.h"

/**
 * @todo write docs
 */
class MzIdentMlAllCandidates : public MzIdentMl
{
  public:
  /**
   * Default constructor
   */
  MzIdentMlAllCandidates(const QString &mzIdentMlFile);

  /**
   * Destructor
   */
  virtual ~MzIdentMlAllCandidates();


  virtual void writePeptideCandidateList(

    const pappso::QualifiedMassSpectrum &experimental_spectrum,
    const std::vector<OutputPeptideCandidateStruct> &peptide_candidate_list,
    pappso::DeepProtMatchType match_type) override;
};
