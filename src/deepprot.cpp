
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of deepprot.
 *
 *     deepprot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License,
 *oroneResult.bestPeptideCandidate (at your option) any later version.
 *
 *     deepprot is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with peptider.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "deepprot.h"
#include <QThread>
#include <QThreadPool>
#include <QDebug>
#include <QCommandLineParser>
#include <QDateTime>
#include <QTimer>
#include <QFileInfo>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/precision.h>
#include <odsstream/odsexception.h>
#include <odsstream/odsdocwriter.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include "utils/utils.h"
#include "output/odsspecsimilarities.h"
#include "output/odsoutput.h"
#include "output/mzidentml.h"
#include "output/mzidentmlallcandidates.h"
#include "utils/monitorcli.h"
#include "core/runningdeepprot.h"

DeepProt::DeepProt(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
DeepProt::run()
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  //./src/deepprot -p
  /// gorgone/pappso/versions_logiciels_pappso/deepprot/v0.0.2/PXD001468/PXD001468_params.ods
  //-m
  /// gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/b1906_293T_proteinID_01A_QE3_122212.mzXML
  /// gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
  /// gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
  //-o
  /// gorgone/pappso/versions_logiciels_pappso/deepprot/v0.0.2/PXD001468/results.ods

  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QTextStream outputStream(stdout, QIODevice::WriteOnly);
  MonitorCli monitor(outputStream);

  monitor.startMonitoredOperation(MonitoredOperation::overall);
  OutputInterface *p_output = nullptr;
  try
    {
      qDebug() << "Peptider::run() begin";
      QCommandLineParser parser;

      // throw
      // pappso::PappsoException("test");3369f491cb17ba5ad08f3df0e856a18a160a404a
      parser.setApplicationDescription(
        QString(SOFTWARE_NAME).append(" ").append(VERSION));
      parser.addHelpOption();
      parser.addVersionOption();
      parser.addPositionalArgument(
        "FASTA files",
        QCoreApplication::translate(
          "main", "List of Fasta files to use for database search."));
      QCommandLineOption fastaOption(
        QStringList() << "f"
                      << "fasta",
        QCoreApplication::translate("main", "FASTA file <fasta>."),
        QCoreApplication::translate("main", "fasta"));
      QCommandLineOption decoyOption(
        QStringList() << "d"
                      << "decoy",
        QCoreApplication::translate("main", "FASTA decoy file <decoy>."),
        QCoreApplication::translate("main", "decoy"));
      QCommandLineOption debugScanOption(
        QStringList() << "debug",
        QCoreApplication::translate("main",
                                    "write informations about scans numbers "
                                    "defined in a list (separated by spaces)"),
        QCoreApplication::translate("main", "debug"));

      QCommandLineOption outputFileOption(
        QStringList() << "o"
                      << "output",
        QCoreApplication::translate("main",
                                    "output file <file> (*.ods or *.mzid)."),
        QCoreApplication::translate("main", "file"));

      QCommandLineOption outputAll(
        QStringList() << "a"
                      << "all",
        QCoreApplication::translate(
          "main", "write all peptide candidates in output file"));

      QCommandLineOption paramFileOption(
        QStringList() << "p"
                      << "parameters",
        QCoreApplication::translate(
          "main", "parameters file <parameters> (*.ods *.tsv)."),
        QCoreApplication::translate("main", "parameters"));


      QCommandLineOption mzOption(
        QStringList() << "m"
                      << "mz",
        QCoreApplication::translate("main", "mz file <mz>."),
        QCoreApplication::translate("main", "mz"));

      QCommandLineOption cpusOption(
        QStringList() << "c"
                      << "cpus",
        QCoreApplication::translate("main", "number of CPUs to use <cpus>."),
        QCoreApplication::translate("main", "cpus"));
      parser.addOption(cpusOption);
      parser.addOption(mzOption);
      parser.addOption(fastaOption);
      parser.addOption(decoyOption);
      parser.addOption(debugScanOption);
      parser.addOption(outputFileOption);
      parser.addOption(outputAll);
      parser.addOption(paramFileOption);

      qDebug() << "Peptider::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "Peptider.Run is executing";
      QString paramFileStr            = parser.value(paramFileOption);
      DeepProtParams &deepprot_params = DeepProtParams::getInstance();
      if(paramFileStr.isEmpty())
        {
        }
      else
        {
          QFileInfo param_info(paramFileStr);
          if(param_info.exists())
            {
              deepprot_params.load(paramFileStr);
            }
          else
            {
              OdsDocWriter param_doc_writer(paramFileStr);
              deepprot_params.save(param_doc_writer);
              param_doc_writer.close();
            }
        }

      uint cpu_number = 50;
      QString cpusStr = parser.value(cpusOption);
      if(!cpusStr.isEmpty())
        {
          cpu_number = cpusStr.toUInt();
        }


      qDebug();
      QString mzFileStr = parser.value(mzOption);

      if(!mzFileStr.isEmpty())
        {
          // build a spectrum collection from mz file

          qDebug();
          monitor.message(
            QObject::tr("getting mz data file %1").arg(mzFileStr));

          pappso::MsFileAccessor accessor(mzFileStr, 0);
          accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                              pappso::FileReaderType::tims_ms2);
          pappso::MsRunReaderSPtr msrun_reader =
            accessor.getMsRunReaderSPtrByRunId("", "msrun");

          if(accessor.getFileFormat() == pappso::MsDataFormat::brukerTims)
            {
              qDebug();
              pappso::TimsMsRunReaderMs2 *tims2_reader =
                dynamic_cast<pappso::TimsMsRunReaderMs2 *>(msrun_reader.get());
              if(tims2_reader != nullptr)
                {
                  qDebug();

                  tims2_reader->setMs2FilterCstSPtr(
                    std::make_shared<pappso::FilterSuiteString>(
                      "chargeDeconvolution|0.02dalton mzExclusion|0.01dalton"));
                  qDebug();
                }
              qDebug();
            }

          qDebug();


          std::vector<QFileInfo> fasta_fileinfo_list;
          std::vector<QFileInfo> decoy_fasta_fileinfo_list;

          QStringList fasta_list = parser.positionalArguments();
          for(QString &fasta_file : fasta_list)
            {
              fasta_fileinfo_list.push_back(QFileInfo(fasta_file));
            }

          QString fastaStr = parser.value(fastaOption);
          if(!fastaStr.isEmpty())
            {
              fasta_fileinfo_list.push_back(QFileInfo(fastaStr));
              // outputStream << fastaStr;
            }
          QString decoyStr = parser.value(decoyOption);
          if(!decoyStr.isEmpty())
            {
              decoy_fasta_fileinfo_list.push_back(QFileInfo(decoyStr));
              // outputStream << decoyStr;
            }


          p_output = Utils::newOutputInterface(parser.value(outputFileOption),
                                               parser.isSet(outputAll));

          if(p_output->isOutputAllPsmCandidates())
            {
              monitor.message(QObject::tr("output all PSMs candidates"));
            }

          RunningDeepProt deepprot;

          deepprot.run(deepprot_params,
                       monitor,
                       msrun_reader,
                       fasta_fileinfo_list,
                       decoy_fasta_fileinfo_list,
                       p_output,
                       cpu_number);
          qDebug();
        }
    }
  catch(OdsException &error)
    {

      errorStream << "Oops! an error occurred in SpecOMS. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in SpecOMS. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in SpecOMS. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      exit(1);
      app->exit(1);
    }

  monitor.message(QObject::tr("peptide identification is finished"));
  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
DeepProt::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
DeepProt::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}

int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName(SOFTWARE_NAME);
  QCoreApplication::setApplicationVersion(VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  DeepProt myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
