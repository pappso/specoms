/**
 * \file core/runningdeepprot.h
 * \date 22/04/2020
 * \author Olivier Langella
 * \brief run a deepprot job
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../utils/deepprotparams.h"
#include "../utils/monitorinterface.h"
#include <pappsomspp/msrun/msrunreader.h>
#include <QFileInfo>
#include "../output/outputinterface.h"
/**
 * @todo write docs
 */
class RunningDeepProt
{
  public:
  /**
   * Default constructor
   */
  RunningDeepProt();

  /**
   * Destructor
   */
  ~RunningDeepProt();

  void run(DeepProtParams &deepprot_params,
           MonitorInterface &monitor,
           pappso::MsRunReaderSPtr msrun_reader,
           std::vector<QFileInfo> fasta_fileinfo_list,
           std::vector<QFileInfo> decoy_fasta_fileinfo_list,
           OutputInterface *p_output,
           uint cpu_number = 50);
};
