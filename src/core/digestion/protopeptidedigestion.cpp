/**
 * \file core/digestion/protopeptidedigestion.cpp
 * \date 4/11/2018
 * \author Olivier Langella
 * \brief digestion protein to protopeptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "protopeptidedigestion.h"
#include "../../utils/deepprotparams.h"
#include "../fastasourcelist.h"
#include <QDebug>
#include <pappsomspp/pappsoexception.h>
#include <iostream>

ProtoPeptideDigestion::ProtoPeptideDigestion(const QString &recognition_site)
  : m_trypsin(recognition_site)
{
  qDebug();
  const DeepProtParams &params = DeepProtParams::getInstance();

  qDebug();
  m_maximumNumberOfMissedCleavage =
    params.get(DeepProtParam::PeptideDigestionNumberOfMissedCleavages).toUInt();

  mp_peptideSize2varmod = new pappso::PeptideSizeFilter(
    params.get(DeepProtParam::PeptideDigestionPeptideMinimumSize).toUInt(),
    params.get(DeepProtParam::PeptideDigestionPeptideMaximumSize).toUInt());

  qDebug();
  mp_peptideSize2varmod->setSink(this);

  qDebug() << " trypsin site :" << m_trypsin.getQRegExpRecognitionSite();

  m_trypsin.setMiscleavage(m_maximumNumberOfMissedCleavage);
}


pappso::Enzyme &
ProtoPeptideDigestion::getEnzyme()
{
  return m_trypsin;
}

const pappso::Enzyme &
ProtoPeptideDigestion::getEnzyme() const
{
  return m_trypsin;
}
ProtoPeptideDigestion::~ProtoPeptideDigestion()
{
  if(mp_peptideSize2varmod != nullptr)
    delete mp_peptideSize2varmod;
}

void
ProtoPeptideDigestion::eat(std::size_t protein_index,
                           const pappso::ProteinSp &protein_sp,
                           bool is_decoy,
                           FastaSourceList *fasta_source_list)
{
  mp_fastaSourceList = fasta_source_list;
  m_proteinIndex     = protein_index;

  try
    {
      // qDebug() << "DigestionPipeline::eat _kinase.eat";
      m_trypsin.eat(0, protein_sp, is_decoy, *mp_peptideSize2varmod);
    }
  catch(pappso::PappsoException &error)
    {
      qDebug() << "ProtoPeptideDigestion::eat error " << error.qwhat();
      throw pappso::PappsoException(
        QObject::tr("error in  ProtoPeptideDigestion::eat : %1")
          .arg(error.qwhat()));
    }
  catch(std::exception &error)
    {
      qDebug() << "ProtoPeptideDigestion::eat std error " << error.what();
      throw pappso::PappsoException(
        QObject::tr("std error in  ProtoPeptideDigestion::eat : %1")
          .arg(error.what()));
    }
}

void
ProtoPeptideDigestion::setPeptide(std::int8_t sequence_database_id
                                  [[maybe_unused]],
                                  const pappso::ProteinSp &protein_sp,
                                  bool is_decoy,
                                  const pappso::PeptideStr &peptide,
                                  unsigned int start,
                                  bool is_nter,
                                  unsigned int missed_cleavage_number,
                                  bool semi_enzyme)
{
  mp_fastaSourceList->protoPeptideProduct(m_proteinIndex,
                                          protein_sp,
                                          is_decoy,
                                          peptide,
                                          start,
                                          is_nter,
                                          missed_cleavage_number,
                                          semi_enzyme);
  /*
  if (missed_cleavage_number == 1) {
  //    std::cout << "cool";
  }
  if (peptide.endsWith("FEEELAAEAAR")) {
 std::cout << peptide.toStdString()<< endl;
  }
  */
}
