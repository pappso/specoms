/**
 * \file core/digestion/peptidemodificationpipeline.h
 * \date 5/11/2018
 * \author Olivier Langella
 * \brief modify protopeptides to get peptides with modifications
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "peptidemodificationpipeline.h"
#include "../../utils/deepprotparams.h"
#include "../fastasourcelist.h"
#include <QDebug>
#include <pappsomspp/pappsoexception.h>


PeptideModificationPipeline::PeptideModificationPipeline()
{
  const DeepProtParams &params = DeepProtParams::getInstance();


  m_peptideModificatorPipeline.addFixedModificationString(
    params.get(DeepProtParam::PeptideDigestionFixedModifications).toString());
  // fixed: MOD:00397@C,MOD:00696@[YST]
  //_peptide_modificator_pipeline.addPotentialModificationString("MOD:00719@M,MOD:00696(0-1)@[YST],MOD:00719(1)@M,MOD:00696(1)@[YST]");
  m_peptideModificatorPipeline.addPotentialModificationString(
    params.get(DeepProtParam::PeptideDigestionVariableModifications)
      .toString());


  m_peptideModificatorPipeline.setSink(this);
  qDebug();
}


PeptideModificationPipeline::~PeptideModificationPipeline()
{
}


void
PeptideModificationPipeline::setPeptideSp(std::int8_t sequence_database_id
                                          [[maybe_unused]],
                                          const pappso::ProteinSp &protein_sp,
                                          bool is_decoy,
                                          const pappso::PeptideSp &peptide_sp,
                                          unsigned int start,
                                          bool is_nter,
                                          unsigned int missed_cleavage_number,
                                          bool semi_enzyme)
{
  mp_fastaSourceList->modifiedPeptideProduct(m_proteinIndex,
                                             m_peptideIndex,
                                             protein_sp,
                                             is_decoy,
                                             peptide_sp,
                                             start,
                                             is_nter,
                                             missed_cleavage_number,
                                             semi_enzyme);
}

void
PeptideModificationPipeline::decoratePeptide(
  std::size_t protein_index,
  std::size_t peptide_index,
  const pappso::ProteinSp &protein_sp,
  bool is_decoy,
  const pappso::PeptideStr &peptide,
  unsigned int start,
  bool is_nter,
  unsigned int missed_cleavage_number,
  bool semi_enzyme,
  FastaSourceList *fasta_source_list)
{
  mp_fastaSourceList = fasta_source_list;
  m_proteinIndex     = protein_index;
  m_peptideIndex     = peptide_index;
  m_peptideModificatorPipeline.setPeptide(0,
                                          protein_sp,
                                          is_decoy,
                                          peptide,
                                          start,
                                          is_nter,
                                          missed_cleavage_number,
                                          semi_enzyme);
}
