/**
 * \file core/digestion/protopeptidedigestion.h
 * \date 4/11/2018
 * \author Olivier Langella
 * \brief digestion protein to protopeptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <pappsomspp/protein/enzyme.h>
#include <pappsomspp/protein/enzymeproductinterface.h>
#include <pappsomspp/protein/peptidesizefilter.h>

class FastaSourceList;

class ProtoPeptideDigestion : public pappso::EnzymeProductInterface
{
  public:
  ProtoPeptideDigestion(const QString &recognition_site);
  ~ProtoPeptideDigestion();
  void eat(std::size_t protein_index,
           const pappso::ProteinSp &protein_sp,
           bool is_decoy,
           FastaSourceList *fasta_source_list);

  virtual void setPeptide(std::int8_t sequence_database_id,
                          const pappso::ProteinSp &protein_sp,
                          bool is_decoy,
                          const pappso::PeptideStr &peptide,
                          unsigned int start,
                          bool is_nter,
                          unsigned int missed_cleavage_number,
                          bool semi_enzyme) override;

  pappso::Enzyme &getEnzyme();
  const pappso::Enzyme &getEnzyme() const;


  private:
  pappso::PeptideSizeFilter *mp_peptideSize2varmod;
  pappso::Enzyme m_trypsin;
  unsigned int m_maximumNumberOfMissedCleavage;
  FastaSourceList *mp_fastaSourceList;
  std::size_t m_proteinIndex;
};
