/**
 * \file core/digestion/peptidemodificationpipeline.h
 * \date 5/11/2018
 * \author Olivier Langella
 * \brief modify protopeptides to get peptides with modifications
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <pappsomspp/protein/peptidemodificatorbase.h>
#include <pappsomspp/protein/peptidemodificatorpipeline.h>

class FastaSourceList;

class PeptideModificationPipeline : public pappso::PeptideModificatorInterface
{
  private:
  pappso::PeptideModificatorPipeline m_peptideModificatorPipeline;
  FastaSourceList *mp_fastaSourceList;
  std::size_t m_proteinIndex;
  std::size_t m_peptideIndex;

  public:
  PeptideModificationPipeline();
  ~PeptideModificationPipeline();

  void setPeptideSp(std::int8_t sequence_database_id,
                    const pappso::ProteinSp &protein_sp,
                    bool is_decoy,
                    const pappso::PeptideSp &peptide_sp,
                    unsigned int start,
                    bool is_nter,
                    unsigned int missed_cleavage_number,
                    bool semi_enzyme) override;

  void decoratePeptide(std::size_t protein_index,
                       std::size_t peptide_index,
                       const pappso::ProteinSp &protein_sp,
                       bool is_decoy,
                       const pappso::PeptideStr &peptide,
                       unsigned int start,
                       bool is_nter,
                       unsigned int missed_cleavage_number,
                       bool semi_enzyme,
                       FastaSourceList *fasta_source_list);
};
