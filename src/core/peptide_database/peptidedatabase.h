/**
 * \file core/peptide_database/peptideDatabase.h
 * \date 5/11/2018
 * \author Olivier Langella
 * \brief store information on peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <memory>
#include <map>
#include <unordered_set>
#include <vector>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/peptide/peptide.h>

class PeptideDatabase;
typedef std::shared_ptr<PeptideDatabase> PeptideDatabaseSPtr;

class FastaSourceList;
typedef std::shared_ptr<FastaSourceList> FastaSourceListSPtr;


struct MissedCleavagePeptide
{
  std::size_t m_peptide_idx;
  pappso::PeptideSp m_peptide;
  unsigned int m_miscleavage_number;
};

class PeptideDatabase
{

  public:
  PeptideDatabase(FastaSourceListSPtr &fasta_source_list);
  ~PeptideDatabase();

  /** @brief record a peptide in the database
   * @param protein_index unique protein index
   * @param protein_sp the protein itself
   * @param is_decoy tell if this is a decoy sequence
   * @param peptide string containing the amino acid sequence
   * @param start the start position of this peptide on the protein
   * @param is_nter tell it the peptide is at Nter position
   * @param missed_cleavage_number number of missed cleavage for this peptide
   * @param semi_enzyme tells if this peptide is a semi tryptic one
   * @return std::pair<std::size_t, bool> a unique peptide index and true if
   * this peptide is new in the database
   */
  std::pair<std::size_t, bool>
  registerProtoPeptide(std::size_t protein_index,
                       const pappso::ProteinSp &protein_sp,
                       bool is_decoy,
                       const pappso::PeptideStr &peptide,
                       unsigned int start,
                       bool is_nter,
                       unsigned int missed_cleavage_number,
                       bool semi_enzyme);

  void registerMissedCleavagePeptide(std::size_t protein_index,
                                     std::size_t peptide_index,
                                     const pappso::PeptideSp &peptide,
                                     unsigned int start);

  std::size_t size() const;
  std::size_t getCountAllProtoPeptideSeen() const;
  bool isPeptideDecoy(std::size_t peptide_number) const;
  bool isPeptideTarget(std::size_t peptide_number) const;

  /** @brief get protein index list for one peptide index
   * @param peptide_number peptide index
   * @return list of protein index
   */
  std::vector<std::size_t>
  getProteinIndexList(std::size_t peptide_number) const;


  std::vector<pappso::ProteinSp>
  getProteinList(std::size_t peptide_number) const;

  std::vector<MissedCleavagePeptide>
  getMissedCleavagePeptideListFromProtoPeptide(
    std::size_t proto_peptide_number) const;

  const FastaSourceListSPtr &getFastaSourceList() const;

  private:
  std::size_t m_countAllProtoPeptideSeen = 0;

  std::hash<std::string> m_hash_fn;

  std::map<std::string, std::size_t> m_mapPeptideHash;
  std::map<std::size_t, std::string> m_mapHashPeptide;
  std::multimap<std::size_t, std::size_t> m_mapPeptideProtein;
  std::multimap<std::size_t, std::size_t> m_mapProteinMissedCleavagePeptide;

  std::vector<std::size_t> m_straight_proteins;
  std::unordered_set<std::size_t> m_reverse_peptides;
  std::unordered_set<std::size_t> m_straight_peptides;


  std::map<std::size_t, pappso::PeptideSp> m_mapMissedCleavagePeptide;

  FastaSourceListSPtr msp_fastaSourceList;
};
