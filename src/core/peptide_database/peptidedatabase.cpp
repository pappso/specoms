/**
 * \file core/peptide_database/peptideDatabase.cpp
 * \date 5/11/2018
 * \author Olivier Langella
 * \brief store information on peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "peptidedatabase.h"
#include <pappsomspp/pappsoexception.h>
#include <QDebug>
#include "../fastasourcelist.h"


PeptideDatabase::PeptideDatabase(FastaSourceListSPtr &source_list)
{
  msp_fastaSourceList = source_list;
}
PeptideDatabase::~PeptideDatabase()
{
}

std::size_t
PeptideDatabase::size() const
{
  return m_mapHashPeptide.size();
}

std::size_t
PeptideDatabase::getCountAllProtoPeptideSeen() const
{
  return m_countAllProtoPeptideSeen;
}

std::pair<std::size_t, bool>
PeptideDatabase::registerProtoPeptide(std::size_t protein_index,
                                      const pappso::ProteinSp &protein_sp
                                      [[maybe_unused]],
                                      bool is_decoy,
                                      const pappso::PeptideStr &peptide,
                                      unsigned int start [[maybe_unused]],
                                      bool is_nter [[maybe_unused]],
                                      unsigned int missed_cleavage_number
                                      [[maybe_unused]],
                                      bool semi_enzyme [[maybe_unused]])
{
  m_countAllProtoPeptideSeen++;
  // test fasta Z mais 5a without deduplication :
  // ls -lah /gorgone/pappso/moulon/database/Genome_Z_mays_5a.fasta 49M 136770
  // proteins 12.1Gb memory

  // 5Gb with hash peptide database

  // 34722 protéines => 3.4 Go de mémoire vive
  std::string peptide_str = peptide.toLatin1().toStdString();
  std::size_t number      = m_hash_fn(peptide_str);

  std::pair<std::size_t, bool> return_value(number, false);

  std::pair<std::map<std::size_t, std::string>::iterator, bool> ret_peptide =
    m_mapHashPeptide.insert(
      std::pair<std::size_t, std::string>(number, peptide_str));
  if(ret_peptide.second == false)
    {
      //"element 'z' already existed";
      return_value.second = false;
      if(ret_peptide.first->second.compare(peptide_str) != 0)
        {
          // collision
          throw pappso::PappsoException(
            QObject::tr("ERROR peptide %1 and %2 collision on hash %3")
              .arg(ret_peptide.first->second.c_str())
              .arg(peptide_str.c_str())
              .arg(number));
        }
    }
  else
    {
      // new peptide
      return_value.second = true;
      // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
      //         << peptide_str.c_str();
      std::pair<std::map<std::string, std::size_t>::iterator, bool> ret_hash =
        m_mapPeptideHash.insert(
          std::pair<std::string, std::size_t>(peptide_str, number));
      if(ret_hash.second == false)
        {
          //"element 'z' already existed";
          // return_value.second = false;
        }
    }


  m_mapPeptideProtein.insert(
    std::pair<std::size_t, std::size_t>(number, protein_index));
  if(is_decoy)
    {
      m_reverse_peptides.insert(number);
    }
  else
    {
      m_straight_peptides.insert(number);
      if((m_straight_proteins.begin() != m_straight_proteins.end()) &&
         (m_straight_proteins.back() != protein_index))
        {
          m_straight_proteins.push_back(protein_index);
        }
    }
  return return_value;
}

bool
PeptideDatabase::isPeptideDecoy(std::size_t peptide_number) const
{
  auto it = m_reverse_peptides.find(peptide_number);
  if(it == m_reverse_peptides.end())
    {
      return false;
    }
  return true;
}

bool
PeptideDatabase::isPeptideTarget(std::size_t peptide_number) const
{
  auto it = m_straight_peptides.find(peptide_number);
  if(it == m_straight_peptides.end())
    {
      return false;
    }
  return true;
}

std::vector<std::size_t>
PeptideDatabase::getProteinIndexList(std::size_t peptide_number) const
{
  qDebug();
  std::vector<std::size_t> protein_list;
  try
    {
      auto it = m_mapPeptideProtein.find(peptide_number);
      while(it != m_mapPeptideProtein.end())
        {
          if(it->first != peptide_number)
            {
              break;
            }

          protein_list.push_back(it->second);
          it++;
        }
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR in PeptideDatabase::getProteinIndexList :\n%1")
          .arg(error.qwhat()));
    }
  qDebug();
  return protein_list;
}


std::vector<pappso::ProteinSp>
PeptideDatabase::getProteinList(std::size_t peptide_number) const
{
  qDebug();
  std::vector<pappso::ProteinSp> protein_list;

  try
    {
      std::vector<std::size_t> protein_index_list =
        getProteinIndexList(peptide_number);

      std::sort(protein_index_list.begin(), protein_index_list.end());

      for(auto protein_index : protein_index_list)
        {
          pappso::Protein protein =
            msp_fastaSourceList->getProteinByIndex(protein_index);
          protein_list.push_back(protein.makeProteinSp());
        }
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR in PeptideDatabase::getProteinList for peptide number %1:\n%2")
          .arg(peptide_number)
          .arg(error.qwhat()));
    }
  qDebug() << " ";
  return protein_list;
}


void
PeptideDatabase::registerMissedCleavagePeptide(std::size_t protein_index,
                                               std::size_t peptide_index,
                                               const pappso::PeptideSp &peptide,
                                               unsigned int start
                                               [[maybe_unused]])
{
  qDebug() << " protein_index=" << protein_index
           << " peptide_index=" << peptide_index;


  m_mapProteinMissedCleavagePeptide.insert(
    std::pair<std::size_t, std::size_t>(protein_index, peptide_index));

  m_mapMissedCleavagePeptide.insert(
    std::pair<std::size_t, pappso::PeptideSp>(peptide_index, peptide));
}


std::vector<MissedCleavagePeptide>
PeptideDatabase::getMissedCleavagePeptideListFromProtoPeptide(
  std::size_t proto_peptide_number) const
{
  std::vector<MissedCleavagePeptide> missed_cleavage_peptide_list;
  try
    {
      auto protein_list = getProteinIndexList(proto_peptide_number);

      for(auto protein_idx : protein_list)
        {
          auto find_missed_cleavage_peptide =
            m_mapProteinMissedCleavagePeptide.find(protein_idx);
          while(find_missed_cleavage_peptide !=
                m_mapProteinMissedCleavagePeptide.end())
            {
              if(find_missed_cleavage_peptide->first != protein_idx)
                {
                  break;
                }

              std::size_t peptide_idx = find_missed_cleavage_peptide->second;
              auto find_if_exist =
                std::find_if(missed_cleavage_peptide_list.begin(),
                             missed_cleavage_peptide_list.end(),
                             [peptide_idx](MissedCleavagePeptide &cp) {
                               if(cp.m_peptide_idx == peptide_idx)
                                 {
                                   return true;
                                 }
                               return false;
                             });
              if(find_if_exist == missed_cleavage_peptide_list.end())
                {
                  MissedCleavagePeptide missed_cleavage_peptide;
                  missed_cleavage_peptide.m_miscleavage_number = 1;
                  missed_cleavage_peptide.m_peptide_idx =
                    find_missed_cleavage_peptide->second;
                  auto find_peptide_sp_it = m_mapMissedCleavagePeptide.find(
                    missed_cleavage_peptide.m_peptide_idx);
                  if(find_peptide_sp_it == m_mapMissedCleavagePeptide.end())
                    {
                      throw pappso::PappsoException(
                        QObject::tr(
                          "missed cleavage peptide %1 not available in "
                          "m_mapMissedCleavagePeptide ")
                          .arg(missed_cleavage_peptide.m_peptide_idx));
                    }
                  missed_cleavage_peptide.m_peptide =
                    find_peptide_sp_it->second;
                  missed_cleavage_peptide_list.push_back(
                    missed_cleavage_peptide);
                }
              find_missed_cleavage_peptide++;
            }
        }
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR in "
          "PeptideDatabase::getMissedCleavagePeptideListFromProtoPeptide for "
          "proto peptide "
          "number %1:\n%2")
          .arg(proto_peptide_number)
          .arg(error.qwhat()));
    }
  qDebug() << " ";
  return missed_cleavage_peptide_list;
}

const FastaSourceListSPtr &
PeptideDatabase::getFastaSourceList() const
{
  if(msp_fastaSourceList == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("ERROR in "
                    "PeptideDatabase::getFastaSourceList for "
                    "msp_fastaSourceList == nullptr"));
    }
  else
    {
      return msp_fastaSourceList;
    }
}
