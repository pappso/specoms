/**
 * \file core/fastafile.cpp
 * \date 3/11/2018
 * \author Olivier Langella
 * \brief fasta file to read protein and sequences
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fastafile.h"
#include <pappsomspp/fasta/fastareader.h>
#include "fastasourcelist.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <QRegularExpression>

class InternalFastaSeq : public pappso::FastaHandlerInterface
{
  public:
  const QString &
  getDescription() const
  {
    return description;
  };
  const QString &
  getSequence() const
  {
    return sequence;
  };
  void
  setSequence(const QString &description_in,
              const QString &sequence_in) override
  {
    description = description_in;
    sequence    = sequence_in;
  };

  private:
  QString description;
  QString sequence;
};


InitialReader::InitialReader(FastaFile *p_fastaFile, MonitorInterface &monitor)
  : m_monitor(monitor)
{
  mp_fastaFile = p_fastaFile;
}

void
InitialReader::setSequence(const QString &description, const QString &sequence)
{

  if(m_monitor.shouldIstop())
    {
      throw pappso::PappsoException(QObject::tr("job stopped"));
    }
  mp_fastaFile->initialSequence(description, sequence);
}

FastaFile::FastaFile(const QFileInfo &fasta_file_info, bool add_reverse)
{
  qDebug() << fasta_file_info.absoluteFilePath();
  m_count      = 0;
  m_addReverse = add_reverse;

  if(!fasta_file_info.exists())
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR FASTA file %1 does not exists")
          .arg(fasta_file_info.absoluteFilePath()));
    }

  m_fastaFile.setFileName(fasta_file_info.absoluteFilePath());
  msp_fastaFileIndex =
    pappso::FastaFileIndexer(fasta_file_info).makeFastaFileIndexerSPtr();
  qDebug() << fasta_file_info.absoluteFilePath();

  if(msp_fastaFileIndex == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("ERROR FASTA file msp_fastaFileIndex == nullptr"));
    }
}

FastaFile::~FastaFile()
{
  /*
  throw pappso::PappsoException(
      QObject::tr("ERROR FastaFile::~FastaFile"));
msp_fastaFileIndex = nullptr;
*/
}

FastaFile::FastaFile(const FastaFile &other)
{
  m_xmlId      = other.m_xmlId;
  m_count      = other.m_count;
  m_addReverse = other.m_addReverse;
  m_fastaFile.setFileName(other.m_fastaFile.fileName());
  mp_fastaSourceList = other.mp_fastaSourceList;
  msp_fastaFileIndex = other.msp_fastaFileIndex;
  qDebug() << m_fastaFile.fileName();

  if(msp_fastaFileIndex == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("ERROR FASTA file msp_fastaFileIndex == nullptr"));
    }
}

void
FastaFile::initialSequence(const QString &description, const QString &sequence)
{
  pappso::Protein protein(
    description, QString(sequence).replace(QRegularExpression("\\*"), ""));
  protein.removeTranslationStop();
  mp_fastaSourceList->setSequence(m_count, false, protein);
  m_count++;
  if(m_addReverse)
    {
      protein.reverse();
      mp_fastaSourceList->setSequence(m_count, true, protein);
      m_count++;
    }
}


void
FastaFile::initialRead(FastaSourceList *fasta_source_list,
                       MonitorInterface &monitor)
{
  mp_fastaSourceList = fasta_source_list;
  InitialReader seq(this, monitor);
  pappso::FastaReader reader(seq);
  reader.parse(m_fastaFile);
}


std::size_t
FastaFile::size() const
{
  return m_count;
}

const QString
FastaFile::fileName() const
{
  return m_fastaFile.fileName();
}

bool
FastaFile::isAddReverse() const
{
  return m_addReverse;
}

pappso::Protein
FastaFile::getProteinByIndex(std::size_t protein_index) const
{
  qDebug() << protein_index;
  bool is_reverse        = false;
  std::size_t true_index = 0;
  if(m_addReverse)
    {
      true_index = protein_index / 2;
    }
  else
    {
      true_index = protein_index;
    }
  if(m_addReverse)
    {
      if(protein_index % 2 != 0)
        {
          is_reverse = true;
        }
    }


  qDebug() << "true_index " << true_index;
  InternalFastaSeq seq_reader;
  if(msp_fastaFileIndex == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("ERROR FASTA file msp_fastaFileIndex == nullptr"));
    }
  qDebug();
  try
    {
      msp_fastaFileIndex->getSequenceByIndex(seq_reader, true_index);
    }
  catch(pappso::ExceptionOutOfRange &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR in FastaFile::getProteinByIndex protein_index=%1, "
                    "true_index=%2 :\n%3")
          .arg(protein_index)
          .arg(true_index)
          .arg(error.qwhat()));
    }
  qDebug();
  pappso::Protein protein(
    seq_reader.getDescription(),
    QString(seq_reader.getSequence()).replace(QRegularExpression("\\*"), ""));
  protein.removeTranslationStop();
  if(is_reverse)
    {
      protein.reverse();
      protein.setAccession(QString("%1:reversed").arg(protein.getAccession()));
    }
  return protein;
}

void
FastaFile::setXmlId(const QString &xml_id)
{
  m_xmlId = xml_id;
}

const QString &
FastaFile::getXmlId() const
{
  return m_xmlId;
}

const QFile &
FastaFile::getFastaFile() const
{
  return m_fastaFile;
}
