/**
 * \file core/msspectrumreader.h
 * \date 16/10/2018
 * \author Olivier Langella
 * \brief SpectrumCollectionHandlerInterface implementation to read spectrum
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <pappsomspp/msrun/msrunreader.h>
#include "spectrumintstore.h"
#include "../utils/types.h"
#include "../utils/deepprotparams.h"
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/filters/filterremovec13.h>
#include <pappsomspp/processing/filters/filtercomplementionenhancer.h>
#include <pappsomspp/processing/filters/filterchargedeconvolution.h>
#include "../utils/monitorinterface.h"

/** @brief read each spectrum in mz data and convert it to SpecSpectrum
 */
class MsSpectrumReader : public pappso::SpectrumCollectionHandlerInterface
{

  public:
  MsSpectrumReader(MonitorInterface &monitor,
                   const DeepProtParams &params,
                   SpectrumIntStoreSPtr &spectrum_int_store_sp);
  virtual ~MsSpectrumReader();

  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;
  virtual void loadingEnded() override;

  virtual bool shouldStop() override;


  private:
  pappso::MassSpectrum tandemProcess(const pappso::MassSpectrum &spectrum,
                                     pappso::pappso_double parent_ion_mz,
                                     unsigned int parent_charge) const;


  private:
  MonitorInterface &m_monitor;
  SpectrumIntStoreSPtr msp_spectrumIntStore;

  pappso::PrecisionPtr m_ms2Precision;

  unsigned int m_accuracyFactor;
  unsigned int m_minimalPeptideCharge;
  unsigned int m_maximalPeptideCharge;
  unsigned int m_accuracyValue;
  bool m_isRemoveIsotope;
  bool m_isExcludeParent;

  pappso::pappso_double m_isExcludeParentLowerDalton = 2;
  pappso::pappso_double m_isExcludeParentUpperDalton = 2;
  bool m_isExcludeParentNeutralLoss                  = false;


  // pappso::pappso_double m_minimumMz                          = 150;
  pappso::FilterGreatestY m_filter_greatest_intensities;
  pappso::FilterRemoveC13 *mpa_filterRemoveC13                     = nullptr;
  pappso::FilterGreatestYperWindow *mpa_filterGreatestYperWindow   = nullptr;
  pappso::FilterChargeDeconvolution *mpa_filterChargeDeconvolution = nullptr;
  bool m_isComplementIonEnhancerFilter                             = false;
  // unsigned int m_nMostIntense                        = 100;
  pappso::pappso_double m_dynamicRange            = 100;
  pappso::pappso_double m_neutralLossMass         = pappso::MASSH2O;
  pappso::pappso_double m_neutralLossWindowDalton = 0.5;

  // pappso::pappso_double m_lowerMzLimit;
  pappso::FilterResampleKeepGreater m_filter_keep_greater;
  // pappso::pappso_double m_upperMzLimit;
  pappso::FilterResampleKeepSmaller m_filter_keep_smaller;
  unsigned int m_minimumNumberOfPeaks;
};
