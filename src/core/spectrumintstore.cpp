/**
 * \file core/spectrumintstore.cpp
 * \date 17/10/2018
 * \author Olivier Langella
 * \brief manage a collection of integere spectrum (without intensity)
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "spectrumintstore.h"
#include <QDebug>
#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <pappsomspp/exception/exceptionoutofrange.h>

SpectrumIntStore::SpectrumIntStore(const DeepProtParams &params)
{
  m_accuracyValue  = params.get(DeepProtParam::AccuracyValue).toUInt();
  m_accuracyFactor = params.getAccuracyFactor();
  m_ionTypeList =
    params.getIonTypeList(DeepProtParam::SpectrumModelIonTypeList);
  m_minimalPeptideCharge =
    params.get(DeepProtParam::MinimalPeptideCharge).toUInt();
  m_maximalPeptideCharge =
    params.get(DeepProtParam::MaximalPeptideCharge).toUInt();
  m_lowerMzLimit = params.get(DeepProtParam::SpectrumLowerMzLimit).toDouble();
  m_upperMzLimit = params.get(DeepProtParam::SpectrumUpperMzLimit).toDouble();
}

std::size_t
SpectrumIntStore::newSpectrumIntFromExperimentalSpectrum(
  pappso::QualifiedMassSpectrumSPtr qualified_mass_spectrum_sp,
  std::vector<std::size_t> &integer_spectrum)
{
  // spectrum indices must start at 1 : 0 is reserved in spec trees
  m_spectrumList.push_back(SpectrumInt(
    m_spectrumList.size(), qualified_mass_spectrum_sp, integer_spectrum));
  std::size_t spectrum_id = m_spectrumList.back().getId();
  if(m_firstExperimentalSpectrumIndex == 0)
    {
      m_firstExperimentalSpectrumIndex = spectrum_id;
    }
  return spectrum_id;
}


void
SpectrumIntStore::buildBucketClustering(
  pappso::spectree::BucketClustering &bucket_clustering) const
{

  for(auto &spectrum : m_spectrumList)
    {
      bucket_clustering.addItemCart(spectrum);
    }
}

const SpectrumInt &
SpectrumIntStore::getSpectrumInt(std::size_t spectrum_index) const
{

  if(spectrum_index < m_spectrumList.size())
    {
      return m_spectrumList.at(spectrum_index);
    }
  else
    {
      throw pappso::ExceptionOutOfRange(
        QObject::tr(
          "spectrum_index %1 out of range (spectrum_index max size=%2)")
          .arg(spectrum_index)
          .arg(m_spectrumList.size()));
    }
}


std::size_t
SpectrumIntStore::newSpectrumIntFromPeptide(std::size_t peptide_index,
                                            const pappso::PeptideSp &peptide)
{

  pappso::PeptideRawFragmentMasses raw_fragments(
    *(peptide.get()), pappso::RawFragmentationMode::full);
  std::vector<pappso::pappso_double> mz_list;

  pappso::pappso_double lower_mz = m_lowerMzLimit;
  pappso::pappso_double upper_mz = m_upperMzLimit;
  unsigned int charge            = 1;
  // for(unsigned int charge = m_minimalPeptideCharge;
  //    charge <= m_maximalPeptideCharge;
  //    charge++)
  //  {
  for(pappso::PeptideIon ion_type : m_ionTypeList)
    {
      raw_fragments.pushBackIonMz(mz_list, ion_type, charge);
    }

  mz_list.erase(std::remove_if(mz_list.begin(),
                               mz_list.end(),
                               [lower_mz](pappso::pappso_double mz) {
                                 return mz < lower_mz;
                               }),
                mz_list.end());

  mz_list.erase(std::remove_if(mz_list.begin(),
                               mz_list.end(),
                               [upper_mz](pappso::pappso_double mz) {
                                 return mz > upper_mz;
                               }),
                mz_list.end());
  std::sort(mz_list.begin(), mz_list.end());
  // if(peak.x > m_upperMzLimit)
  /// break;
  m_spectrumList.push_back(SpectrumInt(
    m_spectrumList.size(), peptide_index, mz_list, peptide, m_accuracyFactor));
  //  }

  qDebug() << "peptide=" << peptide.get()->toString() << " "
           << m_spectrumList.back().toString();
  return m_spectrumList.back().getId();
}

std::size_t
SpectrumIntStore::getFirstExperimentalSpectrumIndex() const
{
  if(m_firstExperimentalSpectrumIndex == 0)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR first experimental index is not set"));
    }
  return m_firstExperimentalSpectrumIndex;
}

std::size_t
SpectrumIntStore::getLastExperimentalSpectrumIndex() const
{
  if(m_firstExperimentalSpectrumIndex == 0)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR first experimental index is not set"));
    }
  return m_spectrumList.back().getId();
}


std::size_t
SpectrumIntStore::getFirstTheoreticalSpectrumIndex() const
{
  if(m_firstExperimentalSpectrumIndex == 0)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR first theoretical index is not set"));
    }
  return 0;
}

std::size_t
SpectrumIntStore::getLastTheoreticalSpectrumIndex() const
{
  if(m_firstExperimentalSpectrumIndex == 0)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR first theoretical index is not set"));
    }
  return (m_firstExperimentalSpectrumIndex - 1);
}

std::size_t
SpectrumIntStore::size() const
{
  return m_spectrumList.size();
}

const std::vector<pappso::PeptideIon> &
SpectrumIntStore::getIonTypeList() const
{
  return m_ionTypeList;
}


unsigned int
SpectrumIntStore::getAccuracyFactor() const
{
  return m_accuracyFactor;
}
