/**
 * \file core/spectrumint.cpp
 * \date 17/10/2018
 * \author Olivier Langella
 * \brief integer spectrum (without intensity)
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <pappsomspp/types.h>
#include <pappsomspp/pappsoexception.h>
#include "spectrumint.h"
#include "../utils/utils.h"


SpectrumInt::SpectrumInt(
  std::size_t id,
  pappso::QualifiedMassSpectrumSPtr &qualified_mass_spectrum_sp,
  std::vector<std::size_t> &integer_spectrum)
{
  m_id                      = id;
  msp_qualifiedMassSpectrum = qualified_mass_spectrum_sp;
  /*
  if(msp_qualifiedMassSpectrum == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("qualified_mass_spectrum_sp == nullptr"));
    }
    */
  msp_peptide = nullptr;
  m_itemList  = integer_spectrum;
}


SpectrumInt::SpectrumInt(std::size_t id,
                         std::size_t protopeptide_id,
                         std::vector<double> &peptide_fragments,
                         const pappso::PeptideSp &peptide_sp,
                         unsigned int accuracy_factor)
{
  m_id                      = id;
  msp_qualifiedMassSpectrum = nullptr;
  msp_peptide               = peptide_sp;

  if(msp_peptide == nullptr)
    {
      throw pappso::PappsoException(QObject::tr("msp_peptide == nullptr"));
    }
  m_protoPeptideNum = protopeptide_id;
  // Computation of the required space for masses storage and allocation of the
  // structure
  // std::size_t picCount = (2 * accuracy_value + 1) * peptide_fragments.size();

  m_itemList.reserve(peptide_fragments.size());

  // specint oldPeak = 0;
  for(pappso::pappso_double mz : peptide_fragments)
    {
      std::size_t newPeak =
        Utils::doubleMassRoundToIntegerMass(mz, accuracy_factor);
      m_itemList.push_back(newPeak);
      // oldPeak = newPeak;
    }
  // conversion into a primitive integer array and sorting
  std::sort(m_itemList.begin(), m_itemList.end());

  // keep unique masses
  m_itemList.erase(std::unique(m_itemList.begin(), m_itemList.end()),
                   m_itemList.end());
}

SpectrumInt::SpectrumInt(const SpectrumInt &other) : ItemCart(other)
{

  // m_id                    = other.m_id;
  m_protoPeptideNum         = other.m_protoPeptideNum;
  msp_peptide               = other.msp_peptide;
  msp_qualifiedMassSpectrum = other.msp_qualifiedMassSpectrum;
}


SpectrumInt &
SpectrumInt::operator=(const SpectrumInt &other)
{

  ItemCart::operator=(other);
  // m_id                    = other.m_id;
  m_protoPeptideNum         = other.m_protoPeptideNum;
  msp_peptide               = other.msp_peptide;
  msp_qualifiedMassSpectrum = other.msp_qualifiedMassSpectrum;
  // m_massList              = other.m_massList;
  return *this;
}


std::size_t
SpectrumInt::getProtoPeptideNum() const
{
  return m_protoPeptideNum;
}
const QString
SpectrumInt::toString() const
{
  QString return_str;
  QTextStream out(&return_str);

  if(msp_qualifiedMassSpectrum != nullptr)
    {
      out << msp_qualifiedMassSpectrum.get()->getMassSpectrumId().getNativeId();
    }
  if(msp_peptide != nullptr)
    {
      out << msp_peptide.get()->toProForma();
    }
  // out << m_title << Qt::endl;
  for(std::size_t mass : m_itemList)
    {
      out << mass << " ";
    }
  out << Qt::endl;
  return return_str;
}

const pappso::QualifiedMassSpectrumSPtr &
SpectrumInt::getQualifiedMassSpectrumSPtr() const
{
  return msp_qualifiedMassSpectrum;
}

const pappso::PeptideSp &
SpectrumInt::getPeptideSp() const
{
  return msp_peptide;
}

const QString
SpectrumInt::getDescriptor() const
{
  if(msp_peptide != nullptr)
    {
      return msp_peptide.get()->toString();
    }
  if(msp_qualifiedMassSpectrum != nullptr)
    {
      return msp_qualifiedMassSpectrum.get()->getMassSpectrumId().getNativeId();
    }
  return "";
}

double
SpectrumInt::getMass() const

{
  if(msp_peptide != nullptr)
    {
      return msp_peptide.get()->getMass();
    }
  if(msp_qualifiedMassSpectrum != nullptr)
    {
      return msp_qualifiedMassSpectrum.get()->getPrecursorMass();
    }
  return 0;
}
