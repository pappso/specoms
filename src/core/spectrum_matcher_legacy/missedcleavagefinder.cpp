/**
 * \file core/spectrum_matcher_legacy/missedcleavagefinder.cpp
 * \date 21/10/2019
 * \author Olivier Langella
 * \brief scan missed cleavage peptides for better fit
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "missedcleavagefinder.h"

#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <pappsomspp/peptide/peptidefragmention.h>
#include <pappsomspp/pappsoexception.h>
#include "../../utils/utils.h"

unsigned int MissedCleavageFinder::m_accuracy_factor = 100;

std::vector<pappso::PeptideIon> MissedCleavageFinder::m_ion_type_list = [] {
  std::vector<pappso::PeptideIon> v;
  v.push_back(pappso::PeptideIon::b);
  v.push_back(pappso::PeptideIon::y);
  return v;
}();

void
MissedCleavageFinder::initializeParameters(const DeepProtParams &params)
{


  MissedCleavageFinder::m_accuracy_factor = params.getAccuracyFactor();

  MissedCleavageFinder::m_ion_type_list =
    params.getIonTypeList(DeepProtParam::SpectrumModelIonTypeList);
}


MissedCleavageFinder::MissedCleavageFinder(
  const PeptideDatabase *p_peptideDatabase,
  PeptideCandidate &original_candidate,
  const SpectrumInt *p_experimentalSpectrumInt [[maybe_unused]],
  std::size_t score_threshold)
{
  qDebug();
  std::vector<MissedCleavagePeptide> miss_peptide_list =
    p_peptideDatabase->getMissedCleavagePeptideListFromProtoPeptide(
      original_candidate.getProtoPeptideNum());

  for(MissedCleavagePeptide &cleavage_peptide : miss_peptide_list)
    {
      PeptideCandidate peptide(original_candidate);
      peptide.setStatus(pappso::DeepProtPeptideCandidateStatus::MissedCleavage);
      peptide.setProtoPeptideNum(cleavage_peptide.m_peptide_idx);
      peptide.setNewPeptideSp(
        cleavage_peptide.m_peptide, m_accuracy_factor, m_ion_type_list);
      if(peptide.getFittedCount() >= score_threshold)
        {
          qDebug() << peptide.getPeptideSp().get()->toString() << " "
                   << peptide.getFittedCount();
          if((msp_peptideCandidate == nullptr) ||
             (msp_peptideCandidate.get()->getFittedCount() <
              peptide.getFittedCount()))
            {
              msp_peptideCandidate =
                std::make_shared<PeptideCandidate>(peptide);
            }
        }
    }
  qDebug();
}

MissedCleavageFinder::~MissedCleavageFinder()
{
}


const std::shared_ptr<PeptideCandidate> &
MissedCleavageFinder::getBestPeptideCandidateSptr() const
{
  return msp_peptideCandidate;
}
