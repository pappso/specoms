/**
 * \file core/spectrum_matcher_legacy/peptidespectrumshifter.cpp
 * \date 01/04/2019
 * \author Olivier Langella
 * \brief shift peptide B or Y ions to find maximum correlation on spectrum
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <pappsomspp/peptide/peptidefragmention.h>
#include <pappsomspp/pappsoexception.h>
#include "../../utils/utils.h"

#include "peptidespectrumshifter.h"
#include <cmath>


unsigned int PeptideSpectrumShifter::m_accuracy_factor = 100;
std::size_t PeptideSpectrumShifter::m_max_integer_mass = 0;

unsigned int PeptideSpectrumShifter::m_accuracy_value = 2;

double PeptideSpectrumShifter::m_accuracy_dalton           = 0.02;
std::size_t PeptideSpectrumShifter::m_minimum_peptide_size = 6;

std::vector<pappso::PeptideIon> PeptideSpectrumShifter::m_ion_type_list = [] {
  std::vector<pappso::PeptideIon> v;
  v.push_back(pappso::PeptideIon::b);
  v.push_back(pappso::PeptideIon::y);
  return v;
}();

void
PeptideSpectrumShifter::initializeParameters(const DeepProtParams &params)
{

  PeptideSpectrumShifter::m_max_integer_mass = Utils::doubleMassToIntegerMass(
    params.get(DeepProtParam::SpectrumUpperMzLimit).toDouble(),
    params.getAccuracyFactor());

  PeptideSpectrumShifter::m_accuracy_value =
    params.get(DeepProtParam::AccuracyValue).toUInt();

  PeptideSpectrumShifter::m_accuracy_dalton =
    params.get(DeepProtParam::MsmsPrecision).toDouble();

  PeptideSpectrumShifter::m_accuracy_factor = params.getAccuracyFactor();

  PeptideSpectrumShifter::m_ion_type_list =
    params.getIonTypeList(DeepProtParam::SpectrumModelIonTypeList);
}


PeptideSpectrumShifter::PeptideSpectrumShifter(
  const PeptideCandidate *p_peptide_candidate,
  const SpectrumInt *p_experimental_spectrum_int)
  : m_candidate(*p_peptide_candidate)
{
  qDebug();
  m_cter_mass = p_peptide_candidate->msp_peptide.get()
                  ->getInternalCterModification()
                  ->getMass();
  m_nter_mass = p_peptide_candidate->msp_peptide.get()
                  ->getInternalNterModification()
                  ->getMass();


  // this.massDelta   = delta;
  m_mass_delta = p_peptide_candidate->m_delta;


  m_candidate.computeIonBandYlists(m_accuracy_factor, m_max_integer_mass);

  buildExperimentalSpectrumMap(p_experimental_spectrum_int);

  // buildIonlists(1);
  /*
  auto score = getScore(0, 0);

  if(score < m_candidate.m_originalCount)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
               << m_candidate.m_originalCount << " " << score << " "
               << p_peptide_candidate->msp_peptide.get()->toAbsoluteString();
      exit(0);
    }
*/
  qDebug();
}


PeptideSpectrumShifter::~PeptideSpectrumShifter()
{
}

void
PeptideSpectrumShifter::searchCutSide()
{
  qDebug();
  std::size_t cut_length   = 1;
  std::size_t peptide_size = m_candidate.msp_peptide.get()->size() - 1;

  std::size_t nter_peptide_size = 0;
  std::size_t cter_peptide_size = 0;
  /*
    int remainingDeltaLeft  = massDelta;
    int remainingDeltaRight = massDelta;
    */
  double remainingDeltaLeft  = m_mass_delta - m_nter_mass;
  double remainingDeltaRight = m_mass_delta - m_cter_mass;
  while(peptide_size >= m_minimum_peptide_size)
    {
      qDebug() << remainingDeltaRight << " " << remainingDeltaLeft << " "
               << m_accuracy_dalton;
      remainingDeltaLeft +=
        m_candidate.msp_peptide.get()->getConstAa(cut_length - 1).getMass();
      remainingDeltaRight +=
        m_candidate.msp_peptide.get()->getConstAa(peptide_size).getMass();


      if(std::abs(remainingDeltaLeft) <=
         PeptideSpectrumShifter::m_accuracy_dalton)
        {
          cter_peptide_size = peptide_size;
          m_is_semi_tryptic = true;
          m_candidate.m_status =
            pappso::DeepProtPeptideCandidateStatus::NterRemoval;
        }
      if(std::abs(remainingDeltaRight) <=
         PeptideSpectrumShifter::m_accuracy_dalton)
        {
          nter_peptide_size = peptide_size;
          m_is_semi_tryptic = true;
          m_candidate.m_status =
            pappso::DeepProtPeptideCandidateStatus::CterRemoval;
        }

      if((remainingDeltaLeft > 0) && (remainingDeltaRight > 0))
        {
          break;
        }
      peptide_size--;
      cut_length++;
    }


  qDebug() << remainingDeltaRight << " " << remainingDeltaLeft;
  if(m_candidate.m_status ==
     pappso::DeepProtPeptideCandidateStatus::NterRemoval)
    {
      pappso::Peptide peptide(*(m_candidate.getPeptideSp().get()));
      while(peptide.size() != cter_peptide_size)
        {
          peptide.removeNterAminoAcid();
        }
      m_candidate.setNewPeptideSp(
        peptide.makePeptideSp(), m_accuracy_factor, m_ion_type_list);
      qDebug() << " " << m_candidate.m_originalCount << "  "
               << m_candidate.m_fittedCount;
      //         exit(0);

      // newSeq = csRef.name();
      // newSeq = newSeq.substring(cutPosLeft + 1);
      // CompleteSpectrumModel newCs =
      // CompleteSpectrumModel.createFromSequence(newSeq);
      // newPeaks = newCs.compareTo(this.experimental_spectrum_buffer);
    }
  else if(m_candidate.m_status ==
          pappso::DeepProtPeptideCandidateStatus::CterRemoval)
    {
      pappso::Peptide peptide(*(m_candidate.getPeptideSp().get()));
      while(peptide.size() != nter_peptide_size)
        {
          peptide.removeCterAminoAcid();
        }
      m_candidate.setNewPeptideSp(
        peptide.makePeptideSp(), m_accuracy_factor, m_ion_type_list);
      qDebug();
      // newSeq = csRef.name();
      // newSeq = newSeq.substring(0, cutPosRight);
      // CompleteSpectrumModel newCs =
      //   CompleteSpectrumModel.createFromSequence(newSeq);
      // newPeaks = newCs.compareTo(this.experimental_spectrum_buffer);
    }
  qDebug();
}


bool
PeptideSpectrumShifter::isSemiTrypticCter()
{
  return (m_candidate.m_status ==
          pappso::DeepProtPeptideCandidateStatus::NterRemoval);
}
bool
PeptideSpectrumShifter::isSemiTrypticNter()
{
  return (m_candidate.m_status ==
          pappso::DeepProtPeptideCandidateStatus::CterRemoval);
}
const PeptideCandidate &
PeptideSpectrumShifter::getPeptideCandidate() const
{
  return m_candidate;
}

void
PeptideSpectrumShifter::buildExperimentalSpectrumMap(
  const SpectrumInt *p_experimental_spectrum_int)
{
  unsigned int peak_width  = (m_accuracy_value * 2) + 1;
  unsigned int peak_number = 1;

  int previous_mass = 0;

  for(auto mass : p_experimental_spectrum_int->getItemList())
    {
      if((mass - previous_mass) > peak_width)
        {
          peak_number++;
          previous_mass = mass;
        }
      m_map_masses_to_peak_numbers.insert(
        std::pair<int, unsigned int>(mass, peak_number));
    }

  /*
for(auto pair_peak : m_map_masses_to_peak_numbers)
  {
    qDebug() << " mass=" << pair_peak.first << " number=" << pair_peak.second;
  }
  */
}


void
PeptideSpectrumShifter::buildIonlists(unsigned int charge)
{
  qDebug() << " ";
  m_nter_ion_mass_lists.resize(0);
  m_cter_ion_mass_lists.resize(0);

  pappso::PeptideRawFragmentMasses raw_fragments(
    *(m_candidate.msp_peptide.get()), pappso::RawFragmentationMode::full);
  std::vector<pappso::pappso_double> mz_list;

  for(pappso::PeptideIon ion_type : m_ion_type_list)
    {

      for(unsigned int charge_i = 0; charge_i < charge; charge_i++)
        {
          mz_list.clear();
          raw_fragments.pushBackIonMz(mz_list, ion_type, charge_i + 1);

          std::vector<double> *p_mass_vector = nullptr;
          if(pappso::PeptideFragmentIon::getPeptideIonDirection(ion_type) ==
             pappso::PeptideDirection::Nter)
            {
              m_nter_ion_mass_lists.push_back(std::vector<double>());
              p_mass_vector = &m_nter_ion_mass_lists.back();
            }
          else
            {
              m_cter_ion_mass_lists.push_back(std::vector<double>());
              p_mass_vector = &m_cter_ion_mass_lists.back();
            }
          for(auto mass : mz_list)
            {
              qDebug() << "mz=" << mass;
              p_mass_vector->push_back(mass);
            }
        }
    }
  qDebug();
}

std::size_t
PeptideSpectrumShifter::getScore(unsigned int position) const
{
  qDebug() << " peptide=" << m_candidate.getPeptideSp().get()->toString()
           << " m_integer_mass_delta=" << m_mass_delta
           << " position=" << position
           << " m_nter_ion_mass_lists.size()=" << m_nter_ion_mass_lists.size();
  std::vector<unsigned int> peak_list;
  double min_mass = 0;

  for(auto mass_list : m_nter_ion_mass_lists)
    {
      for(std::size_t i = 0; i < mass_list.size(); i++)
        {
          double mass = mass_list[i];
          if(i >= position)
            {
              mass += m_mass_delta;
            }
          else
            {
              min_mass = mass;
            }
          qDebug() << " ntermass=" << mass << " i=" << i;
          if(mass >= min_mass)
            {
              auto it = m_map_masses_to_peak_numbers.find(
                Utils::doubleMassRoundToIntegerMass(mass, m_accuracy_factor));
              if(it != m_map_masses_to_peak_numbers.end())
                {
                  peak_list.push_back(it->second);
                  qDebug() << " found Nter mass=" << mass << " i=" << i;
                }
            }
        }
    }
  std::size_t peptide_size = m_candidate.msp_peptide.get()->size();
  position                 = peptide_size - position - 1;
  qDebug() << " Cter position=" << position;
  min_mass = 0;
  for(auto mass_list : m_cter_ion_mass_lists)
    {
      for(std::size_t i = 0; i < mass_list.size(); i++)
        {
          double mass = mass_list[i];
          if(i >= position)
            {
              mass += m_mass_delta;
            }
          else
            {
              min_mass = mass;
            }
          qDebug() << " ctermass=" << mass << " i=" << i;
          if(mass >= min_mass)
            {
              auto it = m_map_masses_to_peak_numbers.find(
                Utils::doubleMassRoundToIntegerMass(mass, m_accuracy_factor));
              if(it != m_map_masses_to_peak_numbers.end())
                {
                  peak_list.push_back(it->second);

                  qDebug() << " found Cter mass=" << mass << " i=" << i;
                }
            }
        }
    }

  std::sort(peak_list.begin(), peak_list.end());
  // peak_list.resize( std::distance(peak_list.begin(),std::unique
  // (peak_list.begin(), peak_list.end())) );
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //         << " m_integer_mass_delta=" << m_integer_mass_delta;
  qDebug() << "score="
           << std::distance(peak_list.begin(),
                            std::unique(peak_list.begin(), peak_list.end()));

  return std::distance(peak_list.begin(),
                       std::unique(peak_list.begin(), peak_list.end()));
}


const std::vector<std::size_t> &
PeptideSpectrumShifter::findBestDeltaPosition(unsigned int charge)
{
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //         << " m_integer_mass_delta=" << m_integer_mass_delta;
  buildIonlists(charge);
  std::size_t peptide_size = m_candidate.msp_peptide.get()->size();
  std::vector<std::size_t> best_positions;
  std::size_t best_score = 0;

  for(std::size_t i = 0; i < peptide_size; i++)
    {
      std::size_t score = getScore(i);
      qDebug() << " i=" << i << " score=" << score;
      if(score > best_score)
        {
          // best_position = i;
          best_score = score;
          best_positions.clear();
          best_positions.push_back(i);
        }
      else if(score == best_score)
        {
          best_positions.push_back(i);
        }
    }

  m_candidate.m_fittedCount     = best_score;
  m_candidate.m_delta_positions = best_positions;
  m_candidate.m_status = pappso::DeepProtPeptideCandidateStatus::DeltaPosition;

  /*
  if(best_positions.size() > 0)
    {
      pappso::NoConstPeptideSp old_peptide =
        std::make_shared<pappso::Peptide>(*(m_candidate.msp_peptide.get()));
      old_peptide.get()->addAaModification(
        pappso::AaModification::getInstanceCustomizedMod(
          m_candidate.getMassDelta()),
        best_positions.front());
      m_candidate.setNewPeptideSp(
        old_peptide, m_accuracy_factor, m_ion_type_list);
    }
    */
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  return m_candidate.m_delta_positions;
}

const std::vector<std::vector<double>> &
PeptideSpectrumShifter::getNterIonMassLists() const
{
  return m_nter_ion_mass_lists;
}


const std::vector<std::vector<double>> &
PeptideSpectrumShifter::getCterIonMassLists() const
{
  return m_cter_ion_mass_lists;
}
