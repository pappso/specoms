/**
 * \file core/spectrum_matcher/peptidecandidate.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "peptidecandidate.h"
#include "spectrummatcher.h"
#include <QDebug>
#include <pappsomspp/peptide/peptidestrparser.h>
#include <pappsomspp/amino_acid/aamodification.h>
#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <pappsomspp/pappsoexception.h>
#include "../../utils/utils.h"

PeptideCandidate::PeptideCandidate(SpectrumMatcher *p_spectrum_matcher,
                                   const SpectrumInt &spectrum_int,
                                   std::size_t count)
{
  mp_parentSpectrumMatcher = p_spectrum_matcher;
  m_originalCount          = count;
  m_fittedCount            = count;
  if(spectrum_int.getQualifiedMassSpectrumSPtr().get() != nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("Error spectrum_int is not a peptide spectrum"));
    }
  m_protoPeptideNum    = spectrum_int.getProtoPeptideNum();
  m_spectrumStoreIndex = spectrum_int.getId();

  msp_originalPeptide = spectrum_int.getPeptideSp();

  try
    {
      msp_peptide = spectrum_int.getPeptideSp();
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("Error building peptide %1 from spectrum index %2: %3")
          .arg(msp_originalPeptide.get()->toString())
          .arg(spectrum_int.getId())
          .arg(error.qwhat()));
    }
  m_delta = mp_parentSpectrumMatcher->getMass() - msp_peptide.get()->getMass();
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
  //        << m_originalPeptide;
}


PeptideCandidate::PeptideCandidate(const PeptideCandidate &other)

{
  msp_originalPeptide      = other.msp_originalPeptide;
  mp_parentSpectrumMatcher = other.mp_parentSpectrumMatcher;
  m_originalCount          = other.m_originalCount;
  m_protoPeptideNum        = other.m_protoPeptideNum;
  msp_peptide              = other.msp_peptide;
  m_delta                  = other.m_delta;
  m_fittedCount            = other.m_fittedCount;
  m_spectrumStoreIndex     = other.m_spectrumStoreIndex;
  m_status                 = other.m_status;
  m_delta_positions        = other.m_delta_positions;
}

void
PeptideCandidate::print(QTextStream &output) const
{

  output << "charge=" << mp_parentSpectrumMatcher->getCharge() << " ";
  output << "precursormass=" << mp_parentSpectrumMatcher->getMass() << " ";
  output << "peptidemass=" << msp_peptide.get()->getMass() << " ";
  output << "count=" << m_originalCount << " ";

  output << Qt::endl;
}


pappso::PeptideSp
PeptideCandidate::getPeptideSp() const
{
  return msp_peptide;
}
double
PeptideCandidate::getMassDelta() const
{
  return m_delta;
}


const pappso::PeptideSp &
PeptideCandidate::getOriginalPeptideSp() const
{
  return msp_originalPeptide;
}

std::size_t
PeptideCandidate::getFittedCount() const
{
  return m_fittedCount;
}

std::size_t
PeptideCandidate::getOriginalCount() const
{
  return m_originalCount;
}
const std::vector<std::size_t> &
PeptideCandidate::getDeltaPositions() const
{
  return m_delta_positions;
}


pappso::DeepProtPeptideCandidateStatus
PeptideCandidate::getStatus() const
{
  return m_status;
}
bool
PeptideCandidate::isZeroMassDelta(const pappso::MzRange &mz_range,
                                  unsigned int charge) const
{

  if(mz_range.contains(msp_peptide->getMz(charge)))
    {
      return true;
    }

  return false;
}

void
PeptideCandidate::setNewPeptideSp(pappso::PeptideSp peptide_sp,
                                  unsigned int accuracy_factor,
                                  std::vector<pappso::PeptideIon> ion_type_list)
{
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";

  /* check, not needed
    std::size_t old_score =
      computePeptideScore(msp_peptide.get(), accuracy_factor, ion_type_list);

    if(old_score != m_originalCount)
      {
        throw pappso::PappsoException(
          QString("m_originalCount != old_score %1 != %2")
            .arg(m_originalCount)
            .arg(old_score));
      }
  */

  m_fittedCount =
    computePeptideScore(peptide_sp.get(), accuracy_factor, ion_type_list);
  msp_peptide = peptide_sp;


  m_delta = mp_parentSpectrumMatcher->getMass() - msp_peptide.get()->getMass();
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //          << " score=" << m_originalCount << " recomp=" << old_score
  //          << " m_fittedCount=" << m_fittedCount << " m_delta=" << m_delta;
}

std::size_t
PeptideCandidate::computePeptideScore(
  const pappso::Peptide *p_peptide,
  unsigned int accuracy_factor,
  std::vector<pappso::PeptideIon> ion_type_list) const
{
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  pappso::PeptideRawFragmentMasses raw_fragments(
    *p_peptide, pappso::RawFragmentationMode::full);
  std::vector<pappso::pappso_double> mz_list;

  unsigned int charge = 1;
  for(pappso::PeptideIon ion_type : ion_type_list)
    {
      raw_fragments.pushBackIonMz(mz_list, ion_type, charge);
    }

  std::vector<std::size_t> mass_list, experimental_spectrum, intersection;
  for(pappso::pappso_double mass : mz_list)
    {
      mass_list.push_back(
        Utils::doubleMassRoundToIntegerMass(mass, accuracy_factor));
    }
  std::sort(mass_list.begin(), mass_list.end());
  mass_list.erase(std::unique(mass_list.begin(), mass_list.end()),
                  mass_list.end());
  experimental_spectrum =
    mp_parentSpectrumMatcher->getExperimentalSpectrumInt()->getItemList();

  intersection.resize(mass_list.size());

  intersection.erase(std::set_intersection(experimental_spectrum.begin(),
                                           experimental_spectrum.end(),
                                           mass_list.begin(),
                                           mass_list.end(),
                                           intersection.begin()),
                     intersection.end());
  return intersection.size();
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
  //       << m_fittedCount;
}

void
PeptideCandidate::computeIonBandYlists(unsigned int accuracy_factor,
                                       std::size_t max_integer_mass)
{
  m_b_integer_ions.resize(0);
  m_y_integer_ions.resize(0);

  pappso::PeptideRawFragmentMasses raw_fragments(
    *(msp_peptide.get()), pappso::RawFragmentationMode::full);
  std::vector<pappso::pappso_double> mz_list;
  unsigned int charge = 1;

  raw_fragments.pushBackIonMz(mz_list, pappso::PeptideIon::b, charge);

  for(auto mz_mass : mz_list)
    {
      std::size_t mass_int =
        Utils::doubleMassToIntegerMass(mz_mass, accuracy_factor);
      if(mass_int < max_integer_mass)
        {
          m_b_integer_ions.push_back(mass_int);
        }
    }
  mz_list.resize(0);

  raw_fragments.pushBackIonMz(mz_list, pappso::PeptideIon::y, charge);

  for(auto mz_mass : mz_list)
    {
      std::size_t mass_int =
        Utils::doubleMassToIntegerMass(mz_mass, accuracy_factor);
      if(mass_int < max_integer_mass)
        {
          m_y_integer_ions.push_back(mass_int);
        }
    }
}


const std::vector<std::size_t> &
PeptideCandidate::getIntegerBionList() const
{
  return m_b_integer_ions;
}
const std::vector<std::size_t> &
PeptideCandidate::getIntegerYionList() const
{
  return m_y_integer_ions;
}

std::size_t
PeptideCandidate::getProtoPeptideNum() const
{
  return m_protoPeptideNum;
}

void
PeptideCandidate::setProtoPeptideNum(std::size_t protoPeptideNum)
{
  m_protoPeptideNum = protoPeptideNum;
}

void
PeptideCandidate::setStatus(pappso::DeepProtPeptideCandidateStatus status)
{
  m_status = status;
}
