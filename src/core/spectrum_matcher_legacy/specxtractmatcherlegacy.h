/**
 * \file core/spectrum_matcher_legacy/specxtractmatcherlegacy.h
 * \date 28/03/2019
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once


#include "../../algorithms/fptree/spectreeextractorreporterinterface.h"
#include "../../output/outputinterface.h"
#include "../spectrumintstore.h"
#include "../../utils/monitorinterface.h"
#include <pappsomspp/precision.h>
#include <pappsomspp/msrun/msrunreader.h>
#include "../peptide_database/peptidedatabase.h"
#include "spectrummatcher.h"


class SpecXtractMatcherLegacy : public SpecTreeExtractorReporterInterface
{
  public:
  SpecXtractMatcherLegacy(OutputInterface *p_output,
                          const DeepProtParams &params,
                          const DecisionBox &decisionBox,
                          SpectrumIntStoreSPtr spectrum_int_store,
                          const PeptideDatabaseSPtr peptide_database);
  virtual ~SpecXtractMatcherLegacy();
  void startingExtraction() override;

  virtual void reportSimilarity(std::size_t cart_id_a,
                                std::size_t cart_id_b,
                                std::size_t similarity) override;
  virtual void beginItemCartExtraction(std::size_t spectra1) override;
  virtual void endItemCartExtraction(std::size_t spectra1) override;
  void endingExtraction() override;


  void selectZeroMassDelta(pappso::DeepProtMatchType type,
                           pappso::PrecisionPtr ms1_precision,
                           unsigned int minimum_similarity);


  void generateOdsReport(CalcWriterInterface *p_writer) const;

  const PeptideDatabaseSPtr &getPeptideDatabase() const;


  private:
  SpectrumIntStoreSPtr msp_spectrumIntStore;
  pappso::PrecisionPtr m_ms1_precision;
  unsigned int m_minimum_similarity;
  unsigned int m_minimum_specxtract_similarity;
  double m_minimal_mass_delta;
  unsigned int m_maximum_missed_cleavage = 0;
  const PeptideDatabaseSPtr msp_peptideDatabase;
  const DecisionBox &m_decisionBox;

  OutputInterface *mp_output = nullptr;

  std::map<QThread *, SpectrumMatcherSp> m_mapThread2SpectrumMatcher;

  QMutex m_mutex;
};
