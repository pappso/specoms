/**
 * \file core/spectrum_matcher/specxtractmatcher.cpp
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "specxtractmatcherlegacy.h"
#include <pappsomspp/pappsoexception.h>
#include <QtConcurrent>
#include "../../utils/deepprotparams.h"

SpecXtractMatcherLegacy::SpecXtractMatcherLegacy(
  OutputInterface *p_output,
  const DeepProtParams &params,
  const DecisionBox &decisionBox,
  SpectrumIntStoreSPtr spectrum_int_store,
  const PeptideDatabaseSPtr peptide_database)
  : msp_peptideDatabase(peptide_database), m_decisionBox(decisionBox)
{
  if(p_output == nullptr)
    {
      throw pappso::PappsoException(QObject::tr(
        "ERROR in SpecXtractMatcherLegacy::SpecXtractMatcherLegacy:\n"
        "p_output == nullptr"));
    }
  mp_output            = p_output;
  msp_spectrumIntStore = spectrum_int_store;
  m_ms1_precision      = params.getMsPrecision();
  m_minimum_specxtract_similarity =
    params.get(DeepProtParam::SpecXtractMinimumSimilarity).toUInt();
  m_minimum_similarity =
    params.get(DeepProtParam::SpecFitMinimumSimilarity).toUInt();

  m_minimal_mass_delta =
    params.get(DeepProtParam::SpecFitMinimalMassDelta).toDouble();

  m_maximum_missed_cleavage =
    params.get(DeepProtParam::PeptideDigestionNumberOfMissedCleavages).toUInt();
}
SpecXtractMatcherLegacy::~SpecXtractMatcherLegacy()
{
}

void
SpecXtractMatcherLegacy::startingExtraction()
{
}
void
SpecXtractMatcherLegacy::beginItemCartExtraction(std::size_t spectra1)
{


  // << "
  // ";
  SpectrumMatcherSp sp_matcher = std::make_shared<SpectrumMatcher>(
    SpectrumMatcher(m_decisionBox, m_minimum_similarity));

  sp_matcher->setExperimentalSpectrumInt(
    msp_spectrumIntStore.get()->getSpectrumInt(spectra1));

  m_mutex.lock(); // protect map allocation
  m_mapThread2SpectrumMatcher[QThread::currentThread()] = sp_matcher;
  m_mutex.unlock();
}

void
SpecXtractMatcherLegacy::endItemCartExtraction(std::size_t spectra1
                                               [[maybe_unused]])
{
  m_mapThread2SpectrumMatcher[QThread::currentThread()]
    ->endExperimentalSpectrumAnalysis(mp_output);
}


void
SpecXtractMatcherLegacy::reportSimilarity(std::size_t cart_id_a,
                                          std::size_t spectra2,
                                          std::size_t count)
{
  qDebug() << count;
  m_mapThread2SpectrumMatcher[QThread::currentThread()]->addPeptideCandidate(
    msp_spectrumIntStore.get()->getSpectrumInt(spectra2), count);
}

const PeptideDatabaseSPtr &
SpecXtractMatcherLegacy::getPeptideDatabase() const
{
  return msp_peptideDatabase;
}

void
SpecXtractMatcherLegacy::endingExtraction()
{
  // flushBuffer()
  // mp_currentSpectrumMatcher->endExperimentalSpectrumAnalysis(mp_output);
}
