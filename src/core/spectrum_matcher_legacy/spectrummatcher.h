/**
 * \file core/spectrum_matcher_legacy/spectrummatcher.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../../utils/types.h"
#include "../../utils/decisionbox.h"
#include "../spectrumint.h"
#include "peptidecandidate.h"
#include "../../output/outputinterface.h"


class SpectrumMatcher;

typedef std::shared_ptr<SpectrumMatcher> SpectrumMatcherSp;

class MzIdentMl;
class SpectrumMatcher
{
  friend DecisionBox;
  friend MzIdentMl;

  public:
  SpectrumMatcher(const DecisionBox &decisionBox,
                  unsigned int minimumSimilarity);
  SpectrumMatcher(const SpectrumMatcher &other);
  virtual ~SpectrumMatcher();
  void setExperimentalSpectrumInt(const SpectrumInt &experimental_spectrum_int);
  const SpectrumInt *getExperimentalSpectrumInt() const;
  void addPeptideCandidate(const SpectrumInt &peptide_spectrum_int,
                           std::size_t count);

  /** @brief get precursor mass
   * */
  pappso::pappso_double getMass() const;

  /** @brief get precursor charge
   */
  pappso::pappso_double getCharge() const;
  void endExperimentalSpectrumAnalysis(OutputInterface *p_output) const;
  std::size_t getScanNumber() const;

  pappso::DeepProtMatchType getMatchType() const;
  std::size_t getMatchTypeCount(pappso::DeepProtMatchType type) const;

  const std::vector<PeptideCandidate> &getPeptideCandidates() const;

  protected:
  std::vector<PeptideCandidate> &getPeptideCandidates();
  void setMatchType(pappso::DeepProtMatchType type, std::size_t count);

  private:
  SpectrumInt *mp_experimentalSpectrumInt = nullptr;
  std::vector<PeptideCandidate> m_peptideCandidates;
  std::size_t m_scanNum;

  pappso::DeepProtMatchType m_type = pappso::DeepProtMatchType::uncategorized;
  std::map<pappso::DeepProtMatchType, std::size_t> m_mapMatchTypeCount;
  const DecisionBox &m_decisionBox;

  /** @brief minimum score for zero delta
   * when this minimum score is reached for "no mass delta" candidates, then all
   * raw scores below this value will be discarded
   */
  unsigned int m_minimumSimilarity;
};
