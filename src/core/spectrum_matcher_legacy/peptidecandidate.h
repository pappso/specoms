/**
 * \file core/spectrum_matcher/peptidecandidate.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../spectrumint.h"
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/precision.h>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/massspectrum/massspectrum.h>

class DecisionBox;
class SpectrumMatcher;
class PeptideSpectrumShifter;


class PeptideCandidate
{
  friend DecisionBox;
  friend PeptideSpectrumShifter;

  public:
  PeptideCandidate(SpectrumMatcher *p_spectrum_matcher,
                   const SpectrumInt &spectrum_int,
                   std::size_t count);
  PeptideCandidate(const PeptideCandidate &other);


  void print(QTextStream &output) const;

  /** @brief get current peptide model
   */
  pappso::PeptideSp getPeptideSp() const;

  bool isZeroMassDelta(const pappso::MzRange &mz_range,
                       unsigned int charge) const;

  void setNewPeptideSp(pappso::PeptideSp peptide_sp,
                       unsigned int accuracy_factor,
                       std::vector<pappso::PeptideIon> ion_type_list);

  void setProtoPeptideNum(std::size_t protoPeptideNum);

  void setStatus(pappso::DeepProtPeptideCandidateStatus status);
  pappso::DeepProtPeptideCandidateStatus getStatus() const;


  void computeIonBandYlists(unsigned int accuracy_factor,
                            std::size_t max_integer_mass);

  const std::vector<std::size_t> &getIntegerBionList() const;
  const std::vector<std::size_t> &getIntegerYionList() const;

  double getMassDelta() const;
  std::size_t getFittedCount() const;
  std::size_t getOriginalCount() const;
  const pappso::PeptideSp &getOriginalPeptideSp() const;

  const std::vector<std::size_t> &getDeltaPositions() const;


  std::size_t getProtoPeptideNum() const;

  private:
  std::size_t
  computePeptideScore(const pappso::Peptide *p_peptide,
                      unsigned int accuracy_factor,
                      std::vector<pappso::PeptideIon> ion_type_list) const;

  private:
  SpectrumMatcher *mp_parentSpectrumMatcher;
  std::size_t m_originalCount;
  std::size_t m_fittedCount;
  std::size_t m_spectrumStoreIndex;
  std::vector<std::size_t> m_delta_positions;

  /** @brief hash integer identifying the peptide sequence origin of this
   * peptide (after digestion, before being "decorated")*/
  std::size_t m_protoPeptideNum;
  pappso::PeptideSp msp_originalPeptide;
  pappso::PeptideSp msp_peptide;
  pappso::pappso_double m_delta;
  pappso::DeepProtPeptideCandidateStatus m_status =
    pappso::DeepProtPeptideCandidateStatus::unmodified;

  /** @brief B ions integer mass list
   * sorted in asc order
   */
  std::vector<std::size_t> m_b_integer_ions;

  /** @brief Y ions integer mass list
   * sorted in asc order
   */
  std::vector<std::size_t> m_y_integer_ions;
};
