/**
 * \file core/spectrum_matcher_legacy/peptidespectrumshifter.h
 * \date 01/04/2019
 * \author Olivier Langella
 * \brief shift peptide B or Y ions to find maximum correlation on spectrum
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#pragma once
#include "peptidecandidate.h"
#include "../spectrumint.h"
#include "../../utils/deepprotparams.h"

class PeptideSpectrumShifter
{
  public:
  static void initializeParameters(const DeepProtParams &params);


  public:
  PeptideSpectrumShifter(const PeptideCandidate *p_peptide_candidate,
                         const SpectrumInt *p_experimental_spectrum_int);
  virtual ~PeptideSpectrumShifter();

  void searchCutSide();

  bool isSemiTrypticCter();
  bool isSemiTrypticNter();

  const PeptideCandidate &getPeptideCandidate() const;

  /** @brief find best position of the mass delta on this peptide/charge
   * */
  const std::vector<std::size_t> &findBestDeltaPosition(unsigned int charge);

  const std::vector<std::vector<double>> &getNterIonMassLists() const;
  const std::vector<std::vector<double>> &getCterIonMassLists() const;

  /** @brief get numer of matching peaks for current candidate
   * @param position position of the optional mass delta (0 is the first Nter
   * amino acid)
   */
  std::size_t getScore(unsigned int position) const;


  private:
  void
  buildExperimentalSpectrumMap(const SpectrumInt *p_experimental_spectrum_int);
  void buildIonlists(unsigned int charge);


  private:
  double m_cter_mass     = 0; // Cter moiety
  double m_nter_mass     = 0; // Nter moiety
  bool m_is_semi_tryptic = false;

  static unsigned int m_accuracy_factor;
  static unsigned int m_accuracy_value;
  static std::size_t m_max_integer_mass;
  static double m_accuracy_dalton;
  static std::vector<pappso::PeptideIon> m_ion_type_list;
  static std::size_t m_minimum_peptide_size;

  /** @brief accurate mass delta (not integer)
   */
  double m_mass_delta;

  PeptideCandidate m_candidate;

  /** @brief map integer experimental mass to peak numbers
   */
  std::map<int, unsigned int> m_map_masses_to_peak_numbers;

  /** @brief list of list of Nter ions (different types and charges)
   */
  std::vector<std::vector<double>> m_nter_ion_mass_lists;

  /** @brief list of list of Cter ions (different types and charges)
   */
  std::vector<std::vector<double>> m_cter_ion_mass_lists;
};
