/**
 * \file core/spectrum_matcher_legacy/spectrummatcher.cpp
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "spectrummatcher.h"
#include "missedcleavagefinder.h"
#include <pappsomspp/exception/exceptionnotfound.h>
#include "../../utils/utils.h"
#include "peptidespectrumshifter.h"
#include "../spectrumintstore.h"
#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <cmath>

SpectrumMatcher::SpectrumMatcher(const DecisionBox &decisionBox,
                                 unsigned int minimumSimilarity)
  : m_decisionBox(decisionBox)
{
  m_minimumSimilarity = minimumSimilarity;
  m_peptideCandidates.clear();
}

SpectrumMatcher::SpectrumMatcher(const SpectrumMatcher &other)
  : m_decisionBox(other.m_decisionBox)
{
  m_minimumSimilarity = other.m_minimumSimilarity;
  m_peptideCandidates.clear();
}

SpectrumMatcher::~SpectrumMatcher()
{
  if(mp_experimentalSpectrumInt != nullptr)
    {
      delete mp_experimentalSpectrumInt;
    }
}
void
SpectrumMatcher::addPeptideCandidate(const SpectrumInt &peptide_spectrum_int,
                                     std::size_t count)
{
  unsigned int best_delta_zero_score =
    getMatchTypeCount(pappso::DeepProtMatchType::ZeroMassDelta);
  if(best_delta_zero_score < m_minimumSimilarity)
    {
      // not good enough candidate at this momment
      // spectrum shift is possible, helps to find better candidate
    }
  else
    {
      if(count < best_delta_zero_score)
        {
          // no good : better fit on other candidate with a zero mass delta
          qDebug();
          return;
        }
    }
  PeptideCandidate peptide_candidate(this, peptide_spectrum_int, count);

  m_decisionBox.decideForPeptideCandidate(this, peptide_candidate);
  qDebug();
}

void
SpectrumMatcher::setExperimentalSpectrumInt(
  const SpectrumInt &experimental_spectrum_int)
{
  if(experimental_spectrum_int.getQualifiedMassSpectrumSPtr() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("not an experimental spectrum"));
    }
  mp_experimentalSpectrumInt = new SpectrumInt(experimental_spectrum_int);
  m_scanNum = mp_experimentalSpectrumInt->getQualifiedMassSpectrumSPtr()
                .get()
                ->getMassSpectrumId()
                .getSpectrumIndex();
}
pappso::pappso_double
SpectrumMatcher::getMass() const
{
  return mp_experimentalSpectrumInt->getQualifiedMassSpectrumSPtr()
    .get()
    ->getPrecursorMass();
}


pappso::pappso_double
SpectrumMatcher::getCharge() const
{
  return mp_experimentalSpectrumInt->getQualifiedMassSpectrumSPtr()
    .get()
    ->getPrecursorCharge();
}


std::size_t
SpectrumMatcher::getScanNumber() const
{
  return m_scanNum;
}


void
SpectrumMatcher::endExperimentalSpectrumAnalysis(
  OutputInterface *p_output) const
{
  // std::vector<PeptideCandidate> sorted = m_peptideCandidates;
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  std::vector<OutputPeptideCandidateStruct> petpide_candidate_list;

  for(auto &&candidate : m_peptideCandidates)
    {
      if((m_type == pappso::DeepProtMatchType::ZeroMassDelta) &&
         (candidate.getStatus() !=
          pappso::DeepProtPeptideCandidateStatus::ZeroMassDelta))
        {
          // remove non zero mass delta candidates
          continue;
        }
      petpide_candidate_list.push_back({candidate.getOriginalPeptideSp(),
                                        candidate.getPeptideSp(),
                                        candidate.getProtoPeptideNum(),
                                        candidate.getOriginalCount(),
                                        candidate.getFittedCount(),
                                        candidate.getStatus(),
                                        candidate.getMassDelta(),
                                        candidate.getDeltaPositions()});
      // petpide_candidate_list.back().deltaPositions =
      // candidate.getDeltaPositions();
    }

  p_output->writePeptideCandidateList(
    *mp_experimentalSpectrumInt->getQualifiedMassSpectrumSPtr().get(),
    petpide_candidate_list,
    m_type);
}


pappso::DeepProtMatchType
SpectrumMatcher::getMatchType() const
{
  return m_type;
}


std::size_t
SpectrumMatcher::getMatchTypeCount(pappso::DeepProtMatchType type) const
{
  auto it = m_mapMatchTypeCount.find(type);
  if(it != m_mapMatchTypeCount.end())
    {
      return it->second;
    }
  return 0;
}

const SpectrumInt *
SpectrumMatcher::getExperimentalSpectrumInt() const
{

  return mp_experimentalSpectrumInt;
}

const std::vector<PeptideCandidate> &
SpectrumMatcher::getPeptideCandidates() const
{
  return m_peptideCandidates;
}


std::vector<PeptideCandidate> &
SpectrumMatcher::getPeptideCandidates()
{
  return m_peptideCandidates;
}

void
SpectrumMatcher::setMatchType(pappso::DeepProtMatchType type, std::size_t count)
{
  m_type   = type;
  auto ret = m_mapMatchTypeCount.insert(
    std::pair<pappso::DeepProtMatchType, std::size_t>(type, count));
  if(ret.second == false)
    {
      ret.first->second = count;
    }
}
