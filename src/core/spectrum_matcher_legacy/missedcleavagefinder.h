/**
 * \file core/spectrum_matcher_legacy/missedcleavagefinder.h
 * \date 21/10/2019
 * \author Olivier Langella
 * \brief scan missed cleavage peptides for better fit
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../peptide_database/peptidedatabase.h"
#include "peptidecandidate.h"
#include "../../utils/deepprotparams.h"
#include <memory>


/** @brief check for better peptide fit using missed cleavages in peptide
 * database
 *
 * for a given peptide, looks after possible missed cleavaged parent peptides in
 * peptide dababase (already computed by the peptide digestion routine)
 *
 */
class MissedCleavageFinder
{
  public:
  static void initializeParameters(const DeepProtParams &params);

  public:
  /** @brief compute scores for missed cleavage peptides
   *
   * @param p_peptideDatabase the peptide database registering possible missed
   * cleavage parent peptides
   * @param original_candidate the peptide candidate to test (no missed
   * cleavage)
   * @param p_experimentalSpectrumInt the experimental spectrum to fit to
   * @param score_threshold the minimum score threshold to validate a better fit
   */
  MissedCleavageFinder(const PeptideDatabase *p_peptideDatabase,
                       PeptideCandidate &original_candidate,
                       const SpectrumInt *p_experimentalSpectrumInt,
                       std::size_t score_threshold);

  /**
   * Destructor
   */
  virtual ~MissedCleavageFinder();

  /** @brief get the list of peptide with missed cleavage best fit
   *
   * @return list of shared pointer to peptide candidates
   */
  const std::shared_ptr<PeptideCandidate> &getBestPeptideCandidateSptr() const;

  private:
  static std::vector<pappso::PeptideIon> m_ion_type_list;
  static unsigned int m_accuracy_factor;

  std::shared_ptr<PeptideCandidate> msp_peptideCandidate = nullptr;
};
