/**
 * \file core/fastasourcelist.cpp
 * \date 3/11/2018
 * \author Olivier Langella
 * \brief collection of fasta files
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fastasourcelist.h"
#include <QDebug>
#include <pappsomspp/exception/exceptionoutofrange.h>

FastaSourceList::FastaSourceList(const DeepProtParams &params,
                                 MonitorInterface &monitor)
  : m_monitor(monitor),
    m_protoPeptideDigestion(
      params.get(DeepProtParam::PeptideDigestionEnzymePattern).toString())
{
  qDebug();
  m_count = 0;
  m_isMissedCleavagesInSpectree =
    params.get(DeepProtParam::PeptideMissedCleavageInSpecTree).toBool();
  // m_protoPeptideDigestion.getEnzyme().getQRegExpRecognitionSite().pattern();
  if(params.get(DeepProtParam::PeptideDigestionEnzymePattern).toString() ==
     "([A-Z])([A-Z])")
    {
      // special case for is_peptidomic
      m_protoPeptideDigestion.getEnzyme().setMiscleavage(
        params.get(DeepProtParam::PeptideDigestionPeptideMaximumSize).toUInt());
      m_isPeptidomic = true;
      // missed cleavages are allowed until maximum peptide size for peptidomic
    }
  qDebug();
}

FastaSourceList::FastaSourceList(const FastaSourceList &other)
  : m_monitor(other.m_monitor),
    m_protoPeptideDigestion(other.m_protoPeptideDigestion.getEnzyme()
                              .getQRegExpRecognitionSite()
                              .pattern())
{
  qDebug();
  m_count                       = other.m_count;
  m_isMissedCleavagesInSpectree = other.m_isMissedCleavagesInSpectree;
  m_isPeptidomic                = other.m_isPeptidomic;
  // m_protoPeptideDigestion.getEnzyme().getQRegExpRecognitionSite().pattern();
  if(m_isPeptidomic)
    {
      // special case for is_peptidomic
      m_protoPeptideDigestion.getEnzyme().setMiscleavage(
        other.m_protoPeptideDigestion.getEnzyme().getMiscleavage());
    }
  qDebug();
}
FastaSourceList::~FastaSourceList()
{
}
void
FastaSourceList::add(const QFileInfo &fasta_file_info, bool add_reverse)
{
  m_monitor.message(QObject::tr("adding Fasta file %1")
                      .arg(fasta_file_info.absoluteFilePath()));
  FastaFile fasta_file(fasta_file_info, add_reverse);
  fasta_file.setXmlId(QString("SearchDB%1").arg(m_fastaFileList.size() + 1));
  m_fastaFileList.push_back(fasta_file);
}

void
FastaSourceList::addDecoy(const QFileInfo &fasta_decoy_file)
{
  m_monitor.message(QObject::tr("adding decoy Fasta file %1")
                      .arg(fasta_decoy_file.absoluteFilePath()));
  FastaFile fasta_file(fasta_decoy_file, false);
  fasta_file.setXmlId(QString("SearchDBr%1").arg(m_fastaFileList.size() + 1));
  m_decoyFastaFileList.push_back(fasta_file);
}

void
FastaSourceList::buildTheoreticalSpectrumInt(
  PeptideDatabaseSPtr &peptide_database,
  SpectrumIntStoreSPtr &spectrum_int_store)
{
  qDebug();
  m_monitor.message(QObject::tr("building theoretical spectra"));
  m_monitor.startMonitoredOperation(
    MonitoredOperation::BuildingTheoreticalSpectrum);

  msp_peptideDatabase  = peptide_database;
  msp_spectrumIntStore = spectrum_int_store;
  m_count              = 0;
  m_currentDecoy       = false;
  for(FastaFile &fasta_file : m_fastaFileList)
    {
      if(m_monitor.shouldIstop())
        {
          throw pappso::PappsoException(QObject::tr("job stopped"));
        }
      m_monitor.message(
        QObject::tr("reading FASTA file %1").arg(fasta_file.fileName()));
      fasta_file.initialRead(this, m_monitor);
      m_count += fasta_file.size();
    }
  m_currentDecoy = true;
  for(FastaFile &fasta_file : m_decoyFastaFileList)
    {
      if(m_monitor.shouldIstop())
        {
          throw pappso::PappsoException(QObject::tr("job stopped"));
        }
      m_monitor.message(
        QObject::tr("reading decoy FASTA file %1").arg(fasta_file.fileName()));
      fasta_file.initialRead(this, m_monitor);
      m_count += fasta_file.size();
    }


  m_monitor.message(
    QObject::tr("%1 proteins, %2 digested protopeptides, %3 protopeptides "
                "registered, %4 spectrum modelized from peptides")
      .arg(m_count)
      .arg(peptide_database->getCountAllProtoPeptideSeen())
      .arg(peptide_database->size())
      .arg(spectrum_int_store->size()));

  qDebug();
  m_monitor.finishedMonitoredOperation(
    MonitoredOperation::BuildingTheoreticalSpectrum);
  // m_monitor.message(QObject::tr("%1 before decorations %2 after decorations")
  //                    .arg(m_countBeforeDecoration)
  //                    .arg(m_countAfterDecoration));
}

void
FastaSourceList::setSequence(std::size_t protein_index,
                             bool reverted,
                             const pappso::Protein &protein)
{
  // digestion to protopeptide :

  bool is_decoy = reverted;

  if(m_currentDecoy)
    {
      is_decoy = true;
    }
  qDebug() << protein.getAccession();
  m_protoPeptideDigestion.eat(
    protein_index, protein.makeProteinSp(), is_decoy, this);
}

void
FastaSourceList::protoPeptideProduct(std::size_t protein_index,
                                     const pappso::ProteinSp &protein_sp,
                                     bool is_decoy,
                                     const pappso::PeptideStr &peptide,
                                     unsigned int start,
                                     bool is_nter,
                                     unsigned int missed_cleavage_number,
                                     bool semi_enzyme)
{
  // qDebug() << protein_sp.get()->getAccession() << " " << peptide << " "
  //        << protein_index;

  protein_index = protein_index + m_count;

  QString peptide_li(peptide);
  peptide_li = peptide_li.replace("L", "I");

  std::pair<std::size_t, bool> ret =
    msp_peptideDatabase->registerProtoPeptide(protein_index,
                                              protein_sp,
                                              is_decoy,
                                              peptide_li,
                                              start,
                                              is_nter,
                                              missed_cleavage_number,
                                              semi_enzyme);


  // SIPAVQEDIHR
  // SIPSGPITEIGMIK

  if(ret.second == true)
    {
      // this is a new peptide
      // m_countBeforeDecoration++;
      m_peptideModificationPipeline.decoratePeptide(protein_index,
                                                    ret.first,
                                                    protein_sp,
                                                    is_decoy,
                                                    peptide_li,
                                                    start,
                                                    is_nter,
                                                    missed_cleavage_number,
                                                    semi_enzyme,
                                                    this);
    }
}

void
FastaSourceList::modifiedPeptideProduct(std::size_t protein_index,
                                        std::size_t peptide_index,
                                        const pappso::ProteinSp &protein_sp
                                        [[maybe_unused]],
                                        bool is_decoy [[maybe_unused]],
                                        const pappso::PeptideSp &peptide,
                                        unsigned int start,
                                        bool is_nter [[maybe_unused]],
                                        unsigned int missed_cleavage_number,
                                        bool semi_enzyme [[maybe_unused]])
{
  // m_countAfterDecoration++;

  if(!m_isPeptidomic)
    {
      if(missed_cleavage_number > 0)
        {
          if(!m_isMissedCleavagesInSpectree)
            {
              msp_peptideDatabase->registerMissedCleavagePeptide(
                protein_index, peptide_index, peptide, start);

              qDebug() << "*************** missed cleavage **************"
                       << peptide.get()->toString() << " "
                       << missed_cleavage_number;
              return; // do not build spectree with missed cleavage peptides
            }
        }
    }

  if(msp_spectrumIntStore.get()->size() < m_maxSpectrumIntNumber)
    {
      msp_spectrumIntStore->newSpectrumIntFromPeptide(peptide_index, peptide);

      // qDebug() << peptide.get()->toString() << " " << missed_cleavage_number;
      // qDebug() << msp_spectrumIntStore.get()
      //            ->getSpectrumInt(peptide_spectrum_idx)
      //          .toString();
    }
}

const FastaFile &
FastaSourceList::getFastaFileByProteinIndex(std::size_t protein_index) const
{


  qDebug() << protein_index;
  std::size_t count = 0;


  try
    {
      for(const FastaFile &fasta_file : m_fastaFileList)
        {
          count += fasta_file.size();
          if(protein_index < count)
            {
              return fasta_file;
            }
        }
      for(const FastaFile &fasta_file : m_decoyFastaFileList)
        {
          count += fasta_file.size();
          if(protein_index < count)
            {
              return fasta_file;
            }
        }
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR in FastaSourceList::getFastaFileByProteinIndex "
                    "protein_index=%1, "
                    "count=%2 total=%3:\n%4")
          .arg(protein_index)
          .arg(count)
          .arg(m_count)
          .arg(error.qwhat()));
    }

  throw pappso::ExceptionOutOfRange(
    QObject::tr(
      "ERROR in FastaSourceList::getFastaFileByProteinIndex :\nprotein index "
      "%1 out of range")
      .arg(protein_index));
}

pappso::Protein
FastaSourceList::getProteinByIndex(std::size_t protein_index) const
{
  qDebug() << protein_index;
  std::size_t count      = 0;
  std::size_t prev_count = 0;


  try
    {
      for(const FastaFile &fasta_file : m_fastaFileList)
        {
          count += fasta_file.size();
          if(protein_index < count)
            {
              qDebug() << protein_index << " " << (protein_index - prev_count)
                       << " " << fasta_file.isAddReverse();
              return fasta_file.getProteinByIndex(protein_index - prev_count);
            }
          prev_count = count;
        }
      for(const FastaFile &fasta_file : m_decoyFastaFileList)
        {
          count += fasta_file.size();
          if(protein_index < count)
            {
              return fasta_file.getProteinByIndex(protein_index - prev_count);
            }
          prev_count = count;
        }
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR in FastaSourceList::getProteinByIndex protein_index=%1, "
          "count=%2 total=%3:\n%4")
          .arg(protein_index)
          .arg(count)
          .arg(m_count)
          .arg(error.qwhat()));
    }

  throw pappso::ExceptionOutOfRange(
    QObject::tr("ERROR in FastaSourceList::getProteinByIndex :\nprotein index "
                "%1 out of range")
      .arg(protein_index));
}


const QString &
FastaSourceList::getFastaFileXmlIdByProteinIndex(
  std::size_t protein_index) const
{
  std::size_t count = 0;


  try
    {
      for(const FastaFile &fasta_file : m_fastaFileList)
        {
          count += fasta_file.size();
          if(protein_index < count)
            {
              return fasta_file.getXmlId();
            }
        }
      for(const FastaFile &fasta_file : m_decoyFastaFileList)
        {
          count += fasta_file.size();
          if(protein_index < count)
            {
              return fasta_file.getXmlId();
            }
        }
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR in FastaSourceList::getFastaFileXmlIdByProteinIndex "
                    "protein_index=%1, "
                    "count=%2 total=%3:\n%4")
          .arg(protein_index)
          .arg(count)
          .arg(m_count)
          .arg(error.qwhat()));
    }

  throw pappso::ExceptionOutOfRange(
    QObject::tr("ERROR in FastaSourceList::getFastaFileXmlIdByProteinIndex "
                ":\nprotein index "
                "%1 out of range")
      .arg(protein_index));
}

const std::vector<FastaFile> &
FastaSourceList::getFastaFileList() const
{
  return m_fastaFileList;
}
const std::vector<FastaFile> &
FastaSourceList::getDecoyFastaFileList() const
{
  return m_decoyFastaFileList;
}
