/**
 * \file core/spectrumint.h
 * \date 17/10/2018
 * \author Olivier Langella
 * \brief integer spectrum (without intensity)
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <vector>
#include <QTextStream>
#include <QString>
#include "../utils/types.h"
#include <pappsomspp/processing/spectree/itemcart.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/peptide/peptide.h>

class SpectrumIntStore;


class SpectrumInt : public pappso::spectree::ItemCart
{
  friend SpectrumIntStore;


  public:
  SpectrumInt(const SpectrumInt &other);

  SpectrumInt &operator=(const SpectrumInt &other);
  const QString toString() const;
  std::size_t getProtoPeptideNum() const;
  const pappso::QualifiedMassSpectrumSPtr &getQualifiedMassSpectrumSPtr() const;

  const pappso::PeptideSp &getPeptideSp() const;


  const QString getDescriptor() const;
  double getMass() const;

  // const std::vector<std::size_t> &getMassList() const;

  protected:
  /**
   * Creates an experimental spectrum from a data read.
   * This spectrum model uses a scaled integer masses representation.
   * The final spectrum exhibits an array sorted by increasing mass values with
   * no dinstinction between B and Y ions. The input data was filtered to keep
   * only a given number of the most intense peaks with no two peaks into a
   * given exclusion window.
   * @param id unique identifier within the spectrum store
   * @param integer_spectrum the list of masses fed in input
   * @param title the experimental spectrum identifier
   * @param accuracy the accuracy used for duplication in the discretisation
   * process
   * @param precursor_mz the total mass of the precursor
   * @param precursor_charge the charge associated with the spectrum
   */
  SpectrumInt(std::size_t id,
              pappso::QualifiedMassSpectrumSPtr &qualified_mass_spectrum_sp,
              std::vector<std::size_t> &integer_spectrum);

  SpectrumInt(std::size_t id,
              std::size_t protopeptide_id,
              std::vector<double> &peptide_fragments,
              const pappso::PeptideSp &peptide_sp,
              unsigned int accuracy_factor);


  private:
  // specint_indice m_id;
  std::size_t m_protoPeptideNum = 0;
  // std::vector<specint> m_massList;
  pappso::QualifiedMassSpectrumSPtr msp_qualifiedMassSpectrum = nullptr;
  pappso::PeptideSp msp_peptide                               = nullptr;
};
