/**
 * \file core/runningdeepprot.cpp
 * \date 22/04/2020
 * \author Olivier Langella
 * \brief run a deepprot job
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2020  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "runningdeepprot.h"
#include <QThread>
#include <QThreadPool>
#include <odsstream/odsexception.h>
#include <pappsomspp/pappsoexception.h>
#include "spectrum_matcher_legacy/peptidespectrumshifter.h"
#include "spectrum_matcher_legacy/missedcleavagefinder.h"
#include "spectrum_matcher_legacy/specxtractmatcherlegacy.h"
#include "fastasourcelist.h"
#include "msspectrumreader.h"
#include "../algorithms/fptree/fptree.h"
#include "../algorithms/fptree/threadedfptree.h"

RunningDeepProt::RunningDeepProt()
{
}

RunningDeepProt::~RunningDeepProt()
{
}


void
RunningDeepProt::run(DeepProtParams &deepprot_params,
                     MonitorInterface &monitor,
                     pappso::MsRunReaderSPtr msrun_reader,
                     std::vector<QFileInfo> fasta_fileinfo_list,
                     std::vector<QFileInfo> decoy_fasta_fileinfo_list,
                     OutputInterface *p_output,
                     uint cpu_number)
{

  try
    {

      monitor.startMonitoredOperation(MonitoredOperation::overall);
      qDebug() << "begin";


      PeptideSpectrumShifter::initializeParameters(deepprot_params);
      MissedCleavageFinder::initializeParameters(deepprot_params);

      uint ideal_number_of_thread = (uint)QThread::idealThreadCount();
      // QThreadPool::globalInstance()->setMaxThreadCount(1);
      if(cpu_number > ideal_number_of_thread)
        {
          cpu_number = ideal_number_of_thread;
        }
      if(cpu_number > 1)
        {
          QThreadPool::globalInstance()->setMaxThreadCount(cpu_number - 1);
        }
      monitor.message(QObject::tr("using up to %1 threads")
                        .arg(QThreadPool::globalInstance()->maxThreadCount()));

      qDebug();


      bool compute_reverse_sequence =
        deepprot_params.get(DeepProtParam::ProteinReverse).toBool();

      qDebug();
      FastaSourceListSPtr fasta_sources = std::make_shared<FastaSourceList>(
        FastaSourceList(deepprot_params, monitor));
      qDebug();
      for(QFileInfo &fasta_file : fasta_fileinfo_list)
        {
          fasta_sources.get()->add(fasta_file, compute_reverse_sequence);
        }
      for(QFileInfo &decoy_fasta_file : decoy_fasta_fileinfo_list)
        {
          fasta_sources.get()->addDecoy(decoy_fasta_file);
        }

      qDebug();
      // reading and converting mass Spectrum from mz data :
      SpectrumIntStoreSPtr spectrum_int_store =
        std::make_shared<SpectrumIntStore>(deepprot_params);
      qDebug();
      PeptideDatabaseSPtr peptide_database =
        std::make_shared<PeptideDatabase>(fasta_sources);

      // read Fasta fasta sources
      fasta_sources.get()->buildTheoreticalSpectrumInt(peptide_database,
                                                       spectrum_int_store);
      qDebug();
      // read mz data
      MsSpectrumReader read(monitor, deepprot_params, spectrum_int_store);

      read.setNeedMsLevelPeakList(1, false);
      read.setReadAhead(true);
      monitor.message(
        QObject::tr("reading mz data from %1")
          .arg(QFileInfo(msrun_reader.get()->getMsRunId().get()->getFileName())
                 .absoluteFilePath()));
      msrun_reader.get()->readSpectrumCollection(read);


      if(monitor.shouldIstop())
        {
          throw pappso::PappsoException(QObject::tr("job stopped"));
        }

      monitor.message(
        QObject::tr("%1 experimental spectrum used")
          .arg(spectrum_int_store->size() -
               spectrum_int_store->getFirstExperimentalSpectrumIndex() + 1));

      monitor.message(QObject::tr("bucket_clustering"));
      pappso::spectree::BucketClustering bucket_clustering;
      spectrum_int_store->buildBucketClustering(bucket_clustering);

      monitor.message(
        QObject::tr("bucket_clustering done, number of buckets (mass) is : %1")
          .arg(bucket_clustering.size()));

      monitor.message(
        QObject::tr("removing from bucket clustering all experimental spectrum "
                    "only if they are not connected to theoretical spectrum"));

      bucket_clustering.removeBucketsWithinCartIdRange(
        spectrum_int_store->getFirstExperimentalSpectrumIndex(),
        spectrum_int_store->getLastExperimentalSpectrumIndex());
      monitor.message(
        QObject::tr("size after experimental spectrum cleaning : %1")
          .arg(bucket_clustering.size()));


      monitor.message(
        QObject::tr("removing from bucket clustering all theoretical spectrum "
                    "only if they are not connected to experimental spectrum"));

      bucket_clustering.removeBucketsWithinCartIdRange(
        spectrum_int_store->getFirstTheoreticalSpectrumIndex(),
        spectrum_int_store->getLastTheoreticalSpectrumIndex());
      monitor.message(
        QObject::tr("size after theoretical spectrum cleaning : %1")
          .arg(bucket_clustering.size()));

      if(monitor.shouldIstop())
        {
          throw pappso::PappsoException(QObject::tr("job stopped"));
        }
      monitor.message(QObject::tr("spec_trees"));


      p_output->setMsRunReaderSPtr(msrun_reader);
      p_output->setPeptideDatabaseSPtr(peptide_database);
      p_output->setDeepProtParams(deepprot_params);

      deepprot_params.getDecisionBox().setPeptideDatabaseSPtr(peptide_database);
      SpecXtractMatcherLegacy spec_fit(p_output,
                                       deepprot_params,
                                       deepprot_params.getDecisionBox(),
                                       spectrum_int_store,
                                       peptide_database);

      FPTree *p_fptree = nullptr;
      if(cpu_number == 1)
        {
          monitor.message(QObject::tr("Spectree extraction using 1 thread"));
          p_fptree = new FPTree(bucket_clustering);
          // p_fptree = new ThreadedFptree(bucket_clustering);
        }
      else
        {
          monitor.message(QObject::tr("Spectree extraction using %1 threads")
                            .arg(cpu_number));
          p_fptree = new ThreadedFptree(bucket_clustering);
        }
      // FPTree spec_trees(bucket_clustering);
      // spec_trees.writeTree(p_ods_writer);
      p_fptree->extractor(
        monitor,
        &spec_fit,
        DeepProtParams::getInstance()
          .get(DeepProtParam::SpecXtractMinimumSimilarity)
          .toUInt(),
        spectrum_int_store->getFirstExperimentalSpectrumIndex());

      delete p_fptree;

      p_output->close();

      monitor.message(QObject::tr("this is the end"));
      monitor.finishedMonitoredOperation(MonitoredOperation::overall);
      qDebug();
    }
  catch(OdsException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR running DeepProt in %1 writing output file :\n %2 ")
          .arg(QString(__FUNCTION__))
          .arg(error.qwhat()));
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR running DeepProt in %1 :\n %2 ")
          .arg(QString(__FUNCTION__))
          .arg(error.qwhat()));
    }

  catch(std::exception &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR running DeepProt in %1 :\n %2 ")
          .arg(QString(__FUNCTION__))
          .arg(error.what()));
    }
}
