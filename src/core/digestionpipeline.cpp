/**
 * \file core/digestionpipeline.h
 * \date 4/11/2018
 * \author Olivier Langella
 * \brief protein digestion pipeline
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of peptider.
 *
 *     peptider is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     peptider is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with peptider.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "digestionpipeline.h"
#include <QDebug>
#include "../utils/deepprotparams.h"

DigestionPipelineBase::DigestionPipelineBase(
  pappso::PeptideModificatorInterface *digestion_sink):_trypsin( DeepProtParams::getInstance().get(DeepProtParam::PeptideDigestionEnzymePattern).toString())
{

    qDebug() << " " << _trypsin.getQRegExpRecognitionSite();
  const DeepProtParams &params = DeepProtParams::getInstance();

  // potential Nter mod +42.01056@[
  // fixed mod mass 57.02146@C
  // 15.99491@M,79.96633@Y
  // 79.96633:-97.9769@[ST!]
  _maximum_number_of_missed_cleavage =
    params.get(DeepProtParam::PeptideDigestionNumberOfMissedCleavages).toUInt();
  _potention_protein_acetylation =
    params.get(DeepProtParam::ProteinDigestionPotentialNterAcetylation)
      .toBool();
  _potential_peptide_nter_cyclisation =
    params.get(DeepProtParam::PeptideDigestionPotentialNterCyclisation)
      .toBool();
  _potential_methionin_removal =
    params.get(DeepProtParam::ProteinDigestionPotentialMethioninRemoval)
      .toBool();


  _p_peptide_size2varmod = new pappso::PeptideSizeFilter(
    params.get(DeepProtParam::PeptideDigestionPeptideMinimumSize).toUInt(),
    params.get(DeepProtParam::PeptideDigestionPeptideMaximumSize).toUInt());

    //test
  //_peptide_modificator_pipeline.addFixedCterModificationString("MOD:00719@P");

  _peptide_modificator_pipeline.addFixedModificationString(
    params.get(DeepProtParam::PeptideDigestionFixedModifications).toString());
  // fixed: MOD:00397@C,MOD:00696@[YST]
  //_peptide_modificator_pipeline.addPotentialModificationString("MOD:00719@M,MOD:00696(0-1)@[YST],MOD:00719(1)@M,MOD:00696(1)@[YST]");
  _peptide_modificator_pipeline.addPotentialModificationString(
    params.get(DeepProtParam::PeptideDigestionVariableModifications)
      .toString());
  // var: MOD:00719@M
  // MOD:00429@^K
  //_peptide_modificator_pipeline.addLabeledModificationString("MOD:00429@(^.|K)");
  // MOD:00552@^K
  //_peptide_modificator_pipeline.addLabeledModificationString("MOD:00552@(^.|K)");
  // MOD:00638@^K
  //_peptide_modificator_pipeline.addLabeledModificationString("MOD:00638@(^.|K)");
  

  if(_potention_protein_acetylation)
    {
      _p_potential_nter_acetylation =
        new pappso::PeptideVariableModificationBuilder(
          pappso::AaModification::getInstance("MOD:00394"));
      _p_potential_nter_acetylation->setProtCter(false);
      _p_potential_nter_acetylation->setProtElse(false);
      _p_potential_nter_acetylation->setProtNter(true);
      QString pattern_str("^.");
      _p_potential_nter_acetylation->setModificationPattern(pattern_str);
    }


  if(_potential_methionin_removal)
    {
      _p_methionin_removal = new pappso::PeptideMethioninRemove(true);
      _p_peptide_size2varmod->setSink(_p_methionin_removal);
      _p_methionin_removal->setSink(&_peptide_modificator_pipeline);
    }
  else
    {
      _p_peptide_size2varmod->setSink(&_peptide_modificator_pipeline);
    }

  pappso::PeptideVariableModificationBuilder *_p_last_modificator = nullptr;
  if(_p_potential_nter_acetylation != nullptr)
    { // acetylation Nter
      _peptide_modificator_pipeline.setSink(_p_potential_nter_acetylation);

      _p_potential_nter_acetylation->setSink(digestion_sink);
      _p_last_modificator = _p_potential_nter_acetylation;
    }
  else
    {
      _peptide_modificator_pipeline.setSink(digestion_sink);
    }

  // peptide cyclization :
  // MOD:00420 2-pyrrolidone-5-carboxylic acid (Glu) Nter E
  // MOD:00040 2-pyrrolidone-5-carboxylic acid (Gln) Nter Q
  // MOD:00397@C => MOD:00419 C Nter
  if(_potential_peptide_nter_cyclisation)
    {
      _p_potential_Ccarba_cyclisation =
        new pappso::PeptideVariableModificationReplacement(
          pappso::AaModification::getInstance("MOD:00397"),
          pappso::AaModification::getInstance("MOD:00419"));
      QString pattern_str("^C");
      _p_potential_Ccarba_cyclisation->setModificationPattern(pattern_str);

      _p_potential_E_cyclisation =
        new pappso::PeptideVariableModificationBuilder(
          pappso::AaModification::getInstance("MOD:00420"));
      pattern_str = "^E";
      _p_potential_E_cyclisation->setModificationPattern(pattern_str);
      _p_potential_Q_cyclisation =
        new pappso::PeptideVariableModificationBuilder(
          pappso::AaModification::getInstance("MOD:00040"));
      pattern_str = "^Q";
      _p_potential_Q_cyclisation->setModificationPattern(pattern_str);

      if(_p_last_modificator == nullptr)
        {
          _peptide_modificator_pipeline.setSink(_p_potential_E_cyclisation);
        }
      else
        {
          _p_last_modificator->setSink(_p_potential_E_cyclisation);
        }
      _p_potential_E_cyclisation->setSink(_p_potential_Q_cyclisation);
      _p_potential_Q_cyclisation->setSink(_p_potential_Ccarba_cyclisation);
      _p_potential_Ccarba_cyclisation->setSink(digestion_sink);
    }


  _trypsin.setMiscleavage(_maximum_number_of_missed_cleavage);

  //_trypsin.setTakeOnlyFirstWildcard(true);
}

DigestionPipelineBase::~DigestionPipelineBase()
{
  // qDebug() << "DigestionPipelineBase::~DigestionPipelineBase begin";
  if(_p_peptide_size2varmod != nullptr)
    delete _p_peptide_size2varmod;
  // qDebug() << "DigestionPipelineBase::~DigestionPipelineBase 1";
  if(_p_potential_nter_acetylation != nullptr)
    delete _p_potential_nter_acetylation;
  //  qDebug() << "DigestionPipelineBase::~DigestionPipelineBase 2";
  if(_p_methionin_removal != nullptr)
    delete _p_methionin_removal;
  // qDebug() << "DigestionPipelineBase::~DigestionPipelineBase 3";
  if(_p_potential_E_cyclisation != nullptr)
    delete _p_potential_E_cyclisation;
  // qDebug() << "DigestionPipelineBase::~DigestionPipelineBase 4";
  if(_p_potential_Q_cyclisation != nullptr)
    delete _p_potential_Q_cyclisation;
  // qDebug() << "DigestionPipelineBase::~DigestionPipelineBase end";
}


DigestionPipeline::DigestionPipeline(
  pappso::PeptideModificatorInterface *digestion_sink)
  : DigestionPipelineBase(digestion_sink)
{
}

DigestionPipeline::~DigestionPipeline()
{
  qDebug() << "DigestionPipeline::~DigestionPipeline begin";
}

void
DigestionPipeline::eat(std::int8_t sequence_database_id,
                       const pappso::ProteinSp &protein_sp,
                       bool is_decoy)
{
  // qDebug() << "DigestionPipeline::eat begin";

  try
    {
      // qDebug() << "DigestionPipeline::eat _kinase.eat";
      _trypsin.eat(
        sequence_database_id, protein_sp, is_decoy, *_p_peptide_size2varmod);
    }
  catch(pappso::PappsoException &error)
    {
      qDebug() << "DigestionPipeline::eat error " << error.qwhat();
      throw pappso::PappsoException(
        QObject::tr("error in  DigestionPipeline::eat : %1")
          .arg(error.qwhat()));
    }
  catch(std::exception &error)
    {
      qDebug() << "DigestionPipeline::eat std error " << error.what();
      throw pappso::PappsoException(
        QObject::tr("std error in  DigestionPipeline::eat : %1")
          .arg(error.what()));
    }

  // qDebug() << "DigestionPipeline::eat end";
}

void
DigestionPipeline::writeMzIdentMlModificationParams(
  QXmlStreamWriter *output_stream[[maybe_unused]]) const
{
  //_output_stream->writeStartElement("ModificationParams");
  /*
  <SearchModification residues="C" massDelta="57.0215" fixedMod="true">
                               <cvParam accession="UNIMOD:4" cvRef="UNIMOD"
  name="Carbamidomethyl"/>
                                       </SearchModification>
                                       */
  /*
                                       <SearchModification residues="M"
     massDelta="15.9949" fixedMod="false"> <cvParam accession="UNIMOD:35"
     cvRef="UNIMOD" name="Oxidation"/>
                                                       </SearchModification>
                                                       */
  /*
                                                       <SearchModification
     residues="M" massDelta="15.9949" fixedMod="false"> <cvParam
     accession="UNIMOD:35" cvRef="UNIMOD" name="Oxidation"/>
                                                               */
  //</ModificationParams>
  //_output_stream->writeEndElement();
}


void
DigestionPipeline::writeMzIdentMlEnzymes(QXmlStreamWriter *output_stream) const
{
  //<Enzymes>
  output_stream->writeStartElement("Enzymes");
  //                                       <Enzyme missedCleavages="1000"
  //                                       semiSpecific="false" id="Tryp">
  output_stream->writeStartElement("Enzyme");
  output_stream->writeAttribute(
    "missedCleavages", QString("%1").arg(_maximum_number_of_missed_cleavage));
  output_stream->writeAttribute("semiSpecific", "false");
  output_stream->writeAttribute("id", "Tryp");
  //<EnzymeName>
  output_stream->writeStartElement("EnzymeName");
  output_stream->writeStartElement("cvParam");
  //<cvParam accession="MS:1001251" cvRef="PSI-MS" name="Trypsin"/>
  output_stream->writeAttribute("accession", "MS:1001251");
  output_stream->writeAttribute("cvRef", "PSI-MS");
  output_stream->writeAttribute("name", "Trypsin");
  output_stream->writeEndElement();

  // </EnzymeName>
  output_stream->writeEndElement();

  // </Enzyme>*/
  output_stream->writeEndElement();
  //</Enzymes>
  output_stream->writeEndElement();
}

