/**
 * \file core/spectrum_matcher/morpheusscore.cpp
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief compute peptide scores
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "morpheusscore.h"

#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <pappsomspp/psm/morpheus/morpheusscore.h>

MorpheusScore::MorpheusScore(pappso::pappso_double lowerMzLimit,
                             pappso::pappso_double upperMzLimit,
                             unsigned int accuracy_factor,
                             const std::vector<pappso::PeptideIon> &ionTypeList,
                             pappso::PrecisionPtr precision, pappso::pappso_double threshold)
{

  m_lowerMzLimit    = lowerMzLimit;
  m_upperMzLimit    = upperMzLimit;
  m_accuracy_factor = accuracy_factor;
  m_ionTypeList     = ionTypeList;
  m_precision_ms2   = precision;
  m_minimum_morpheus_score = threshold;
}

pappso::pappso_double
MorpheusScore::getThreshold() const
{
    return m_minimum_morpheus_score;
}

std::vector<specint>
MorpheusScore::computeSpectrumIntFromPeptide(const pappso::Peptide *p_peptide,
                                             unsigned int max_charge) const
{

  pappso::PeptideRawFragmentMasses raw_fragments(
    *p_peptide, pappso::RawFragmentationMode::full);
  std::vector<pappso::pappso_double> mz_list;

  for(unsigned int charge = 1; charge <= max_charge; charge++)
    {
      for(pappso::PeptideIon ion_type : m_ionTypeList)
        {
          raw_fragments.pushBackIonMz(mz_list, ion_type, charge);
        }
    }

  pappso::pappso_double lower_mz = m_lowerMzLimit;
  pappso::pappso_double upper_mz = m_upperMzLimit;
  mz_list.erase(std::remove_if(mz_list.begin(),
                               mz_list.end(),
                               [lower_mz](pappso::pappso_double mz) {
                                 return mz < lower_mz;
                               }),
                mz_list.end());

  mz_list.erase(std::remove_if(mz_list.begin(),
                               mz_list.end(),
                               [upper_mz](pappso::pappso_double mz) {
                                 return mz > upper_mz;
                               }),
                mz_list.end());


  std::vector<specint> mass_list;
  for(pappso::pappso_double mz : mz_list)
    {
      specint newPeak = (specint)(mz * m_accuracy_factor);
      mass_list.push_back(newPeak);
      // oldPeak = newPeak;
    }

  // conversion into a primitive integer array and sorting
  std::sort(mass_list.begin(), mass_list.end());

  // keep unique masses
  mass_list.erase(std::unique(mass_list.begin(), mass_list.end()),
                  mass_list.end());
  return mass_list;
}

template <class InputIt1, class InputIt2>
std::size_t
countIntersection(InputIt1 first1,
                  InputIt1 last1,
                  InputIt2 first2,
                  InputIt2 last2)
{
  std::size_t count = 0;
  while(first1 != last1 && first2 != last2)
    {
      if(*first1 < *first2)
        {
          ++first1;
        }
      else
        {
          if(!(*first2 < *first1))
            {
              count++;
            }
          ++first2;
        }
    }
  return count;
}

std::size_t
MorpheusScore::getNumberOfCommonPeaks(
  const std::vector<specint> &experimental_mass_list,
  const pappso::Peptide *p_peptide,
  unsigned int max_charge) const
{
  std::vector<specint> peptide_mass_list =
    computeSpectrumIntFromPeptide(p_peptide, max_charge);


  // QTextStream outputStream(stdout, QIODevice::WriteOnly);
  // outputStream << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  std::size_t count = countIntersection(experimental_mass_list.begin(),
                                        experimental_mass_list.end(),
                                        peptide_mass_list.begin(),
                                        peptide_mass_list.end());
  // outputStream << " " << count << endl;
  return count;
}

pappso::pappso_double
MorpheusScore::computeMorpheusScore(const pappso::MassSpectrum *mass_spectrum,
                                    const pappso::PeptideSp &p_peptide,
                                    unsigned int max_charge) const
{
  pappso::MorpheusScore score(*mass_spectrum,
                              p_peptide,
                              max_charge,
                              m_precision_ms2,
                              m_ionTypeList,
                              pappso::RawFragmentationMode::full);

  return score.getMorpheusScore();
}
