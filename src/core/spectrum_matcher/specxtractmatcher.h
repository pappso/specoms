/**
 * \file core/spectrum_matcher/specxtractmatcher.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once


#include "../../algorithms/fptree/spectreeextractorreporterinterface.h"
#include "../spectrumintstore.h"
#include "spectrummatcher.h"
#include "../../utils/monitorinterface.h"
#include <pappsomspp/precision.h>
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/msrun/msrunreader.h>
#include "../peptide_database/peptidedatabase.h"

class SpecXtractMatcher : public SpecTreeExtractorReporterInterface,
                          pappso::SpectrumCollectionHandlerInterface
{
  private:
  SpectrumIntStoreSPtr msp_spectrumIntStore;
  std::map<specint_indice, SpectrumMatcher> m_mapSpectrumMatcher;

  std::map<std::size_t, SpectrumMatcher *> m_mapScanNumSpectrumMatcher;
  pappso::pappso_double m_minimumMz    = 150;
  unsigned int m_nMostIntense          = 100;
  pappso::pappso_double m_dynamicRange = 100;

  public:
  SpecXtractMatcher(const DeepProtParams &params,
                    SpectrumIntStoreSPtr spectrum_int_store);
  virtual ~SpecXtractMatcher();
  void startingExtraction(specint_indice threshold, specint_indice limit);
  void spectrumSimilarities(specint_indice spectra1,
                            specint_indice spectra2,
                            specint_indice count);

  void computeSpecFit(MonitorInterface &monitor,
                      pappso::PrecisionPtr precision,
                      const MorpheusScore &morpheus_score);
  void computeMorpheusScore(MonitorInterface &monitor,
                            pappso::MsRunReaderSp msrun_reader,
                            const MorpheusScore &morpheus_score);

  void generateOdsReport(CalcWriterInterface *p_writer,
                         const PeptideDatabaseSPtr peptide_database) const;

  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;
};
