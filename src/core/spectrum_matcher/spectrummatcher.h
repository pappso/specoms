/**
 * \file core/spectrum_matcher/spectrummatcher.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../../utils/types.h"
#include "../spectrumint.h"
#include "peptidecandidate.h"
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/spectrum/massspectrum.h>

class SpectrumMatcher
{
  private:
  SpectrumInt *mp_experimentalSpectrumInt = nullptr;
  std::vector<PeptideCandidate> m_peptideCandidates;
  std::size_t m_scanNum;
  pappso::MassSpectrumSPtr m_massSpectrumSp;

  private:
  void removeProtoPeptideRedundancy();

  public:
  SpectrumMatcher();
  virtual ~SpectrumMatcher();
  void setExperimentalSpectrumInt(const SpectrumInt &experimental_spectrum_int);
  void addPeptideCandidate(const SpectrumInt &peptide_spectrum_int,
                           specint_indice count);
  void computeSpecFit(pappso::PrecisionPtr precision_ms1,
                      const MorpheusScore &morpheus_score);
  void computeMorpheusScore(const MorpheusScore &morpheus_score);
  pappso::pappso_double getMass() const;
  pappso::pappso_double getCharge() const;
  void writeOds(CalcWriterInterface *p_writer,
                const PeptideDatabase *peptide_database) const;
  std::size_t getScanNumber() const;
  void setMassSpectrumSPtr(pappso::MassSpectrumSPtr original_spectrum_sp);
};
