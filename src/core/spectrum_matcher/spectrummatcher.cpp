/**
 * \file core/spectrum_matcher/spectrummatcher.cpp
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "spectrummatcher.h"
#include <pappsomspp/mzrange.h>
#include "../../utils/types.h"

SpectrumMatcher::SpectrumMatcher()
{
}

SpectrumMatcher::~SpectrumMatcher()
{
  if(mp_experimentalSpectrumInt != nullptr)
    {
      delete mp_experimentalSpectrumInt;
    }
}
void
SpectrumMatcher::addPeptideCandidate(const SpectrumInt &peptide_spectrum_int,
                                     specint_indice count)
{
  m_peptideCandidates.push_back(
    PeptideCandidate(this, peptide_spectrum_int, count));
}

void
SpectrumMatcher::setExperimentalSpectrumInt(
  const SpectrumInt &experimental_spectrum_int)
{
  mp_experimentalSpectrumInt = new SpectrumInt(experimental_spectrum_int);
  m_scanNum = mp_experimentalSpectrumInt->getScanOrProtoPeptideNum();
}
pappso::pappso_double
SpectrumMatcher::getMass() const
{
  return mp_experimentalSpectrumInt->getMass();
}


pappso::pappso_double
SpectrumMatcher::getCharge() const
{
  return mp_experimentalSpectrumInt->getCharge();
}


std::size_t
SpectrumMatcher::getScanNumber() const
{
  return m_scanNum;
}

void
SpectrumMatcher::removeProtoPeptideRedundancy()
{
  std::sort(m_peptideCandidates.begin(),
            m_peptideCandidates.end(),
            [](const PeptideCandidate &a, const PeptideCandidate &b) {
              return std::tie(a.m_protoPeptideNum, a.m_specFitType) <
                     std::tie(b.m_protoPeptideNum, b.m_specFitType);
            });
  m_peptideCandidates.erase(
    std::unique(m_peptideCandidates.begin(),
                m_peptideCandidates.end(),
                [](const PeptideCandidate &a, const PeptideCandidate &b) {
                  if(a.m_specFitType == SpecFitType::noDirectFit)
                    return false;
                  return (a.m_protoPeptideNum == b.m_protoPeptideNum);
                }),
    m_peptideCandidates.end());
}

void
SpectrumMatcher::computeSpecFit(pappso::PrecisionPtr precision_ms1,
                                const MorpheusScore &morpheus_score)
{
  // QTextStream outputStream(stdout, QIODevice::WriteOnly);
  /*outputStream << endl
               << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " scan_num=" << m_scanNum
               << " size=" << m_peptideCandidates.size() << endl;
*/
  pappso::pappso_double charge = mp_experimentalSpectrumInt->getCharge();
  pappso::pappso_double target_mz =
    (mp_experimentalSpectrumInt->getMass() + (charge * pappso::MHPLUS)) /
    charge;
  pappso::MzRange target_mz_range(target_mz, precision_ms1);
  for(auto &&peptide_candidate : m_peptideCandidates)
    {
      peptide_candidate.checkDirectFit(target_mz_range);
    }

  /*
    std::sort(m_peptideCandidates.begin(),
              m_peptideCandidates.end(),
              [](const PeptideCandidate &a, const PeptideCandidate &b) {
                return std::tie(a.m_protoPeptideNum, a.m_specFitType) <
                       std::tie(b.m_protoPeptideNum, b.m_specFitType);
              });
    for(auto &&peptide_candidate : m_peptideCandidates)
      {
        peptide_candidate.print(outputStream);
      }
  */
  removeProtoPeptideRedundancy();


  for(auto &&peptide_candidate : m_peptideCandidates)
    {
      peptide_candidate.computeScore(morpheus_score,
                                     mp_experimentalSpectrumInt->getMassList());
    }
  /*
    outputStream << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                 << " after remove redundancy scan_num=" << m_scanNum
                 << " size=" << m_peptideCandidates.size() << endl;
    */
  /*
for(auto &&peptide_candidate : m_peptideCandidates)
{
peptide_candidate.print(outputStream);
}*/
}


void
SpectrumMatcher::writeOds(CalcWriterInterface *p_writer,
                          const PeptideDatabase *peptide_database) const
{
  std::vector<PeptideCandidate> sorted = m_peptideCandidates;

  std::sort(sorted.begin(),
            sorted.end(),
            [](const PeptideCandidate &a, const PeptideCandidate &b) {
              if(a.m_specFitType == b.m_specFitType)
                {
                  return (a.m_morpheusScore > b.m_morpheusScore);
                }
              else
                {
                  return (a.m_specFitType < b.m_specFitType);
                }
            });
  if(m_peptideCandidates.size() > 0)
    {
      if(m_peptideCandidates.at(0).m_specFitType == SpecFitType::noDirectFit)
        {
        }
    }

  for(auto &&peptide_candidate : sorted)
    {

      pappso::pappso_double charge = mp_experimentalSpectrumInt->getCharge();
      p_writer->writeLine();
      p_writer->writeCell((int)m_scanNum);
      p_writer->writeCell(mp_experimentalSpectrumInt->getCharge());
      pappso::pappso_double target_mz =
        (mp_experimentalSpectrumInt->getMass() + (charge * pappso::MHPLUS)) /
        charge;
      p_writer->writeCell(target_mz);
      p_writer->writeCell(mp_experimentalSpectrumInt->getMass());

      p_writer->writeCell((int)m_peptideCandidates.size());
      peptide_candidate.writeOds(p_writer, peptide_database);

      break;
    }
}


void
SpectrumMatcher::setMassSpectrumSPtr(
  pappso::MassSpectrumSPtr original_spectrum_sp)
{
  m_massSpectrumSp = original_spectrum_sp;
}


void
SpectrumMatcher::computeMorpheusScore(const MorpheusScore &morpheus_score)
{

  for(auto &&peptide_candidate : m_peptideCandidates)
    {
      peptide_candidate.computeMorpheusScore(morpheus_score,
                                             m_massSpectrumSp.get());
    }

  pappso::pappso_double minimum_morpheus_score = morpheus_score.getThreshold();

  m_peptideCandidates.erase(
    std::remove_if(m_peptideCandidates.begin(),
                   m_peptideCandidates.end(),
                   [minimum_morpheus_score](const PeptideCandidate &a) {
                     if(a.m_morpheusScore < minimum_morpheus_score)
                       return true;
                     return false;
                   }),
    m_peptideCandidates.end());
}
