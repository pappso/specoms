/**
 * \file core/spectrum_matcher/peptidecandidate.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../spectrumint.h"
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/precision.h>
#include <pappsomspp/mzrange.h>
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/spectrum/massspectrum.h>
#include "morpheusscore.h"
#include "../peptide_database/peptidedatabase.h"

class SpectrumMatcher;

class PeptideCandidate
{
  friend SpectrumMatcher;

  private:
  bool removeNterFit(const pappso::MzRange &target_mass_range);
  bool removeCterFit(const pappso::MzRange &target_mass_range);

  protected:
  SpectrumMatcher *mp_parentSpectrumMatcher;
  std::size_t m_originalCount;

  /** @brief hash integer identifying the pepride sequence origin of this
   * peptide (after digestion, before being "decorated")*/
  std::size_t m_protoPeptideNum;
  QString m_originalPeptide;
  pappso::PeptideSp msp_peptide;
  SpecFitType m_specFitType;
  std::size_t m_fitCount                = 0;
  pappso::pappso_double m_morpheusScore = 0;
  pappso::pappso_double m_delta;

  public:
  PeptideCandidate(SpectrumMatcher *p_spectrum_matcher,
                   const SpectrumInt &spectrum_int,
                   specint_indice count);
  PeptideCandidate(const PeptideCandidate &other);
  void checkDirectFit(const pappso::MzRange &target_mass_range);
  void findBetterFit(const pappso::MzRange &target_mz_range);

  void print(QTextStream &output) const;
  void writeOds(CalcWriterInterface *p_writer,
                const PeptideDatabase *peptide_database) const;
  pappso::PeptideSp getPeptideSp() const;
  void computeScore(const MorpheusScore &morpheus_score,
                    const std::vector<specint> &experimental_mass_list);
  void computeMorpheusScore(const MorpheusScore &morpheus_score,
                            const pappso::MassSpectrum *mass_spectrum);
};
