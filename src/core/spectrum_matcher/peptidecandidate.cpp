/**
 * \file core/spectrum_matcher/peptidecandidate.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "peptidecandidate.h"
#include "spectrummatcher.h"
#include <QDebug>
#include <pappsomspp/peptide/peptidestrparser.h>
#include <pappsomspp/psm/morpheus/morpheusscore.h>
#include <pappsomspp/amino_acid/aamodification.h>

PeptideCandidate::PeptideCandidate(SpectrumMatcher *p_spectrum_matcher,
                                   const SpectrumInt &spectrum_int,
                                   specint_indice count)
{
  mp_parentSpectrumMatcher = p_spectrum_matcher;
  m_originalCount          = count;
  m_protoPeptideNum        = spectrum_int.getScanOrProtoPeptideNum();
  m_originalPeptide        = spectrum_int.getTitle();
  m_specFitType            = SpecFitType::notChecked;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << m_originalPeptide;
}


PeptideCandidate::PeptideCandidate(const PeptideCandidate &other)
{
  mp_parentSpectrumMatcher = other.mp_parentSpectrumMatcher;
  m_originalCount          = other.m_originalCount;
  m_protoPeptideNum        = other.m_protoPeptideNum;
  m_originalPeptide        = other.m_originalPeptide;
  msp_peptide              = other.msp_peptide;
  m_specFitType            = other.m_specFitType;
  m_fitCount               = other.m_fitCount;
  m_morpheusScore          = other.m_morpheusScore;
  m_delta                  = other.m_delta;
}

void
PeptideCandidate::print(QTextStream &output) const
{

  output << m_originalPeptide << " ";
  switch(m_specFitType)
    {
      case SpecFitType::PerfectFit:
        output << "perfect fit ";
        break;

      case SpecFitType::C13Fit:
        output << "oneC13 ";
        break;
      case SpecFitType::C13C13Fit:
        output << "twoC13 ";
        break;
      default:
        output << "outfit ";
        break;
    }
  output << "charge=" << mp_parentSpectrumMatcher->getCharge() << " ";
  output << "precursormass=" << mp_parentSpectrumMatcher->getMass() << " ";
  output << "peptidemass=" << msp_peptide.get()->getMass() << " ";
  output << "count=" << m_originalCount << " ";

  output << endl;
}

void
PeptideCandidate::checkDirectFit(const pappso::MzRange &target_mz_range)
{
  if(m_specFitType != SpecFitType::notChecked)
    return;
  msp_peptide = pappso::PeptideStrParser::parseString(m_originalPeptide);
  pappso::pappso_double charge = mp_parentSpectrumMatcher->getCharge();
  pappso::pappso_double peptide_mz =
    (msp_peptide.get()->getMass() + (charge * pappso::MHPLUS)) / charge;

  m_specFitType = SpecFitType::noDirectFit;

  if(target_mz_range.contains(peptide_mz))
    {
      // perfect FIT
      m_specFitType = SpecFitType::PerfectFit;
      return;
    }

  if(false)
    { // look for C13
      pappso::pappso_double peptide_mz_c13 =
        peptide_mz + (pappso::DIFFC12C13 / charge);

      pappso::pappso_double peptide_mz_c13c13 =
        peptide_mz_c13 + (pappso::DIFFC12C13 / charge);
      // QTextStream outputStream(stdout, QIODevice::WriteOnly);
      if(target_mz_range.contains(peptide_mz_c13))
        {
          // perfect FIT + one C13 error
          m_specFitType = SpecFitType::C13Fit;
          // outputStream << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
          //             << " C13 error " <<
          //             mp_parentSpectrumMatcher->getCharge()
          //             << " " << target_mz_range.getMz() << " " << peptide_mz
          //             << "
          //             "
          //             << m_originalPeptide << " " << m_originalCount << endl;
          return;
        }

      if(target_mz_range.contains(peptide_mz_c13c13))
        {
          // perfect FIT + two C13 error
          m_specFitType = SpecFitType::C13C13Fit;
          // outputStream << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
          //             << " two C13 error " <<
          //             mp_parentSpectrumMatcher->getCharge()
          //             << " " << target_mz_range.getMz() << " " << peptide_mz
          //             << "
          //             "
          //             << m_originalPeptide << " " << m_originalCount << endl;
          return;
        }
    }
}

void
PeptideCandidate::findBetterFit(const pappso::MzRange &target_mz_range)
{
  pappso::pappso_double charge = mp_parentSpectrumMatcher->getCharge();
  pappso::pappso_double peptide_mz =
    (msp_peptide.get()->getMass() + (charge * pappso::MHPLUS)) / charge;

  pappso::pappso_double peptide_mz_minimum_amino_acid_loss =
    peptide_mz - (57.02 / charge); // glycine is the lightest amino acid


  if(target_mz_range.upper() < peptide_mz_minimum_amino_acid_loss)
    {
      /*   outputStream << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ <<
         " "
                      << mp_parentSpectrumMatcher->getCharge() << " "
                      << target_mz_range.getMz() << " " << peptide_mz << " "
                      << m_originalPeptide << " " << m_originalCount << endl;
   */
      if(removeNterFit(target_mz_range))
        {
          /*        outputStream << __FILE__ << " " << __FUNCTION__ << " " <<
             __LINE__
                               << " NterFit" <<
             mp_parentSpectrumMatcher->getCharge()
                               << " " << target_mz_range.getMz() << " "
                               << msp_peptide.get()->getMz(charge) << " "
                               << msp_peptide.get()->toString() << " "
                               << m_originalCount << endl;
                               */
        }
      else
        {
          if(removeCterFit(target_mz_range))
            {

              /*        outputStream << __FILE__ << " " << __FUNCTION__ << " "
                 << __LINE__
                                   << " CterFit"
                                   << mp_parentSpectrumMatcher->getCharge() << "
                 "
                                   << target_mz_range.getMz() << " "
                                   << msp_peptide.get()->getMz(charge) << " "
                                   << msp_peptide.get()->toString() << " "
                                   << m_originalCount << endl;
                                   */
            }
        }
    }

  m_delta = mp_parentSpectrumMatcher->getMass() - msp_peptide.get()->getMass();
}

bool
PeptideCandidate::removeNterFit(const pappso::MzRange &target_mz_range)
{
  pappso::Peptide new_peptide(*(msp_peptide.get()));
  unsigned int charge = mp_parentSpectrumMatcher->getCharge();

  new_peptide.removeNterAminoAcid();

  while(new_peptide.getMz(charge) > target_mz_range.upper())
    {
      new_peptide.removeNterAminoAcid();
    }

  if(target_mz_range.contains(new_peptide.getMz(charge)))
    {
      m_specFitType = SpecFitType::NterFit;
      msp_peptide   = new_peptide.makePeptideSp();

      return true;
    }
  return false;
}


bool
PeptideCandidate::removeCterFit(const pappso::MzRange &target_mz_range)
{
  pappso::Peptide new_peptide(*(msp_peptide.get()));
  unsigned int charge = mp_parentSpectrumMatcher->getCharge();

  new_peptide.removeCterAminoAcid();

  while(new_peptide.getMz(charge) > target_mz_range.upper())
    {
      new_peptide.removeCterAminoAcid();
    }
  if(target_mz_range.contains(new_peptide.getMz(charge)))
    {
      m_specFitType = SpecFitType::CterFit;
      msp_peptide   = new_peptide.makePeptideSp();
      return true;
    }
  return false;
}


pappso::PeptideSp
PeptideCandidate::getPeptideSp() const
{
  return msp_peptide;
}

void
PeptideCandidate::writeOds(CalcWriterInterface *p_writer,
                           const PeptideDatabase *peptide_database) const
{

  p_writer->writeCell(m_originalPeptide);
  p_writer->writeCell(peptide_database->isPeptideDecoy(m_protoPeptideNum));
  switch(m_specFitType)
    {
      case SpecFitType::PerfectFit:
        p_writer->writeCell("perfect fit");
        break;

      case SpecFitType::C13Fit:
        p_writer->writeCell("oneC13");
        break;
      case SpecFitType::C13C13Fit:
        p_writer->writeCell("twoC13");
        break;
      case SpecFitType::NterFit:
        p_writer->writeCell("NterFit");
        break;
      case SpecFitType::CterFit:
        p_writer->writeCell("CterFit");
        break;
      default:
        p_writer->writeCell("outfit");
        break;
    }
  p_writer->writeCell(msp_peptide.get()->toString());
  p_writer->writeCell(msp_peptide.get()->getMass());
  p_writer->writeCell(
    msp_peptide.get()->getMz(mp_parentSpectrumMatcher->getCharge()));
  p_writer->writeCell((int)m_originalCount);
  p_writer->writeCell((int)m_fitCount);
  p_writer->writeCell(m_morpheusScore);


  if(msp_peptide.get() != nullptr)
    {
      p_writer->writeCell(m_delta);
    }
}


void
PeptideCandidate::computeScore(
  const MorpheusScore &morpheus_score,
  const std::vector<specint> &experimental_mass_list)
{
  // QTextStream outputStream(stdout, QIODevice::WriteOnly);
  // outputStream << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  m_fitCount = morpheus_score.getNumberOfCommonPeaks(
    experimental_mass_list,
    msp_peptide.get(),
    (unsigned int)mp_parentSpectrumMatcher->getCharge());


  // outputStream << " " << m_morpheusScore << endl;
}

void
PeptideCandidate::computeMorpheusScore(
  const MorpheusScore &morpheus_score,
  const pappso::MassSpectrum *mass_spectrum)
{
  m_morpheusScore = morpheus_score.computeMorpheusScore(
    mass_spectrum,
    msp_peptide,
    (unsigned int)mp_parentSpectrumMatcher->getCharge());


  if(m_specFitType == SpecFitType::noDirectFit)
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstanceCustomizedMod(m_delta);
      std::size_t peptide_size = msp_peptide.get()->size();

      pappso::PeptideSp keep_peptide = msp_peptide;
      for(std::size_t i = 0; i < peptide_size; i++)
        {
          pappso::Peptide test_peptide(*(keep_peptide.get()));
          test_peptide.addAaModification(modification, i);
          pappso::PeptideSp petpide_ptr = test_peptide.makePeptideSp();

          pappso::pappso_double score = morpheus_score.computeMorpheusScore(
            mass_spectrum,
            petpide_ptr,
            (unsigned int)mp_parentSpectrumMatcher->getCharge());

          if(score > m_morpheusScore)
            {
              m_morpheusScore = score;
              msp_peptide     = petpide_ptr;
            }
        }
    }
}


pappso::PeptideSp
PeptideCandidate::getRefinedPeptideSp() const
{
  pappso::PeptideSp new_peptide_sp(*(msp_peptide.get()));

  if(m_specFitType == SpecFitType::noDirectFit)
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstanceCustomizedMod(m_delta);
    }
  return new_peptide_sp;
}
