/**
 * \file core/spectrum_matcher/morpheusscore.h
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief compute peptide scores
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/spectrum/massspectrum.h>
#include "../../utils/types.h"

class MorpheusScore
{
  private:
  pappso::pappso_double m_lowerMzLimit;
  pappso::pappso_double m_upperMzLimit;
  unsigned int m_accuracy_factor = 100;
  std::vector<pappso::PeptideIon> m_ionTypeList;
  pappso::PrecisionPtr m_precision_ms2 = nullptr;
  pappso::pappso_double m_minimum_morpheus_score;

  private:
  std::vector<specint>
  computeSpectrumIntFromPeptide(const pappso::Peptide *p_peptide,
                                unsigned int max_charge) const;


  public:
  MorpheusScore(pappso::pappso_double lowerMzLimit,
                pappso::pappso_double upperMzLimit,
                unsigned int accuracy_factor,
                const std::vector<pappso::PeptideIon> &ionTypeList,
                pappso::PrecisionPtr precision, pappso::pappso_double threshold);
  std::size_t
  getNumberOfCommonPeaks(const std::vector<specint> &experimental_mass_list,
                         const pappso::Peptide *p_peptide,
                         unsigned int max_charge) const;

  pappso::pappso_double
  computeMorpheusScore(const pappso::MassSpectrum *mass_spectrum,
                       const pappso::PeptideSp &p_peptide,
                       unsigned int max_charge) const;
  pappso::pappso_double getThreshold() const;
};
