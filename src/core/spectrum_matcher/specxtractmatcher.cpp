/**
 * \file core/spectrum_matcher/specxtractmatcher.cpp
 * \date 23/11/2018
 * \author Olivier Langella
 * \brief help to assign mass spectrum to peptides
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "specxtractmatcher.h"
#include <pappsomspp/pappsoexception.h>
#include "../../utils/deepprotparams.h"


SpecXtractMatcher::SpecXtractMatcher(const DeepProtParams &params,
                                     SpectrumIntStoreSPtr spectrum_int_store)
{
  msp_spectrumIntStore = spectrum_int_store;
  m_minimumMz =
    params.get(DeepProtParam::SpectrumLowerMzLimit).toDouble(); // 150;
  m_nMostIntense =
    params.get(DeepProtParam::SpectrumNmostIntense).toUInt(); // 100;
  m_dynamicRange =
    params.get(DeepProtParam::SpectrumDynamicRange).toUInt(); // 100;
}
SpecXtractMatcher::~SpecXtractMatcher()
{
}


void
SpecXtractMatcher::startingExtraction(specint_indice threshold,
                                      specint_indice limit)
{
}

void
SpecXtractMatcher::spectrumSimilarities(specint_indice spectra1,
                                        specint_indice spectra2,
                                        specint_indice count)
{
  try
    {
      if(spectra1 > spectra2)
        {
          std::swap(spectra1, spectra2);
        }
      std::pair<std::map<specint_indice, SpectrumMatcher>::iterator, bool>
        ret_spectrum_matcher = m_mapSpectrumMatcher.insert(
          std::pair<specint_indice, SpectrumMatcher>(spectra2,
                                                     SpectrumMatcher()));

      if(ret_spectrum_matcher.second == true)
        {
          // new
          ret_spectrum_matcher.first->second.setExperimentalSpectrumInt(
            msp_spectrumIntStore.get()->getSpectrumInt(spectra2));

          m_mapScanNumSpectrumMatcher.insert(
            std::pair<std::size_t, SpectrumMatcher *>(
              ret_spectrum_matcher.first->second.getScanNumber(),
              &ret_spectrum_matcher.first->second));
        }

      ret_spectrum_matcher.first->second.addPeptideCandidate(
        msp_spectrumIntStore.get()->getSpectrumInt(spectra1), count);
      // SpectrumInt spectrum_int_experimental =
      // msp_spectrumIntStore.get()->getSpectrumInt(spectra2); SpectrumInt
      // spectrum_int_peptide =
      // msp_spectrumIntStore.get()->getSpectrumInt(spectra1);
    }

  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR in SpecXtractMatcher::spectrumSimilarities")
          .arg(error.qwhat()));
    }
}

void
SpecXtractMatcher::computeSpecFit(MonitorInterface &monitor,
                                  pappso::PrecisionPtr precision,
                                  const MorpheusScore &morpheus_score)
{
  monitor.startMonitoredOperation(MonitoredOperation::SpecFit);
  monitor.message(
    QObject::tr("starting %1 operation")
      .arg(monitor.getOperationName(MonitoredOperation::SpecFit)));

  monitor.setProgressMaximumValue(m_mapSpectrumMatcher.size());
  std::size_t i = 0;
  for(auto &&pair_matcher : m_mapSpectrumMatcher)
    {
      pair_matcher.second.computeSpecFit(precision, morpheus_score);
      monitor.messagePercent("", i);
      i++;
    }

  monitor.finishedMonitoredOperation(MonitoredOperation::SpecFit);
}


void
SpecXtractMatcher::generateOdsReport(
  CalcWriterInterface *p_writer,
  const PeptideDatabaseSPtr peptide_database) const
{
  p_writer->writeSheet("specfit");

  for(auto &&pair_matcher : m_mapSpectrumMatcher)
    {
      pair_matcher.second.writeOds(p_writer, peptide_database.get());
    }
}


void
SpecXtractMatcher::computeMorpheusScore(MonitorInterface &monitor,
                                        pappso::MsRunReaderSp msrun_reader,
                                        const MorpheusScore &morpheus_score)
{

  monitor.startMonitoredOperation(MonitoredOperation::Scoring);
  monitor.message(
    QObject::tr("starting %1 operation")
      .arg(monitor.getOperationName(MonitoredOperation::Scoring)));

  msrun_reader.get()->readSpectrumCollection(*this);

  monitor.setProgressMaximumValue(m_mapSpectrumMatcher.size());
  std::size_t i = 0;
  for(auto &&pair_matcher : m_mapSpectrumMatcher)
    {
      pair_matcher.second.computeMorpheusScore(morpheus_score);
      monitor.messagePercent("", i);
      i++;
    }

  monitor.finishedMonitoredOperation(MonitoredOperation::Scoring);
}


bool
SpecXtractMatcher::needPeakList() const
{
  return true;
}
void
SpecXtractMatcher::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{
  auto it = m_mapScanNumSpectrumMatcher.find(
    qspectrum.getMassSpectrumId().getScanNum());
  if(it != m_mapScanNumSpectrumMatcher.end())
    {
      pappso::MassSpectrum spectrum_process(
        qspectrum.getMassSpectrumSPtr().get()->highPassFilter(m_minimumMz));
      spectrum_process = spectrum_process.nGreatestY(m_nMostIntense)
                           .rescaleY(m_dynamicRange)
                           .highPassFilter(1);
      it->second->setMassSpectrumSPtr(spectrum_process.makeMassSpectrumSPtr());
    }
}
