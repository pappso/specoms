/**
 * \file core/fastasourcelist.h
 * \date 3/11/2018
 * \author Olivier Langella
 * \brief collection of fasta files
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <pappsomspp/fasta/fastahandlerinterface.h>
#include <pappsomspp/protein/protein.h>
#include "fastafile.h"
#include "spectrumintstore.h"
#include "digestion/peptidemodificationpipeline.h"
#include "digestion/protopeptidedigestion.h"
#include "../utils/monitorinterface.h"

class FastaSourceList;
typedef std::shared_ptr<FastaSourceList> FastaSourceListSPtr;

class FastaSourceList
{
  friend FastaFile;
  friend ProtoPeptideDigestion;
  friend PeptideModificationPipeline;


  public:
  FastaSourceList(const DeepProtParams &params, MonitorInterface &monitor);
  FastaSourceList(const FastaSourceList &other);
  virtual ~FastaSourceList();

  void add(const QFileInfo &fasta_file, bool add_reverse);

  void addDecoy(const QFileInfo &fasta_decoy_file);

  void buildTheoreticalSpectrumInt(PeptideDatabaseSPtr &peptide_database,
                                   SpectrumIntStoreSPtr &spectrum_int_store);

  pappso::Protein getProteinByIndex(std::size_t protein_index) const;
  const QString &
  getFastaFileXmlIdByProteinIndex(std::size_t protein_index) const;

  const std::vector<FastaFile> &getFastaFileList() const;
  const std::vector<FastaFile> &getDecoyFastaFileList() const;


  const FastaFile &getFastaFileByProteinIndex(std::size_t protein_index) const;

  protected:
  void
  setSequence(std::size_t index, bool reverted, const pappso::Protein &protein);

  void protoPeptideProduct(std::size_t protein_index,
                           const pappso::ProteinSp &protein_sp,
                           bool is_decoy,
                           const pappso::PeptideStr &peptide,
                           unsigned int start,
                           bool is_nter,
                           unsigned int missed_cleavage_number,
                           bool semi_enzyme);

  void modifiedPeptideProduct(std::size_t protein_index,
                              std::size_t peptide_index,
                              const pappso::ProteinSp &protein_sp,
                              bool is_decoy,
                              const pappso::PeptideSp &peptide,
                              unsigned int start,
                              bool is_nter,
                              unsigned int missed_cleavage_number,
                              bool semi_enzyme);


  private:
  MonitorInterface &m_monitor;
  std::vector<FastaFile> m_fastaFileList;
  std::vector<FastaFile> m_decoyFastaFileList;
  std::size_t m_count;
  bool m_currentDecoy;
  ProtoPeptideDigestion m_protoPeptideDigestion;
  PeptideModificationPipeline m_peptideModificationPipeline;
  SpectrumIntStoreSPtr msp_spectrumIntStore;
  PeptideDatabaseSPtr msp_peptideDatabase;

  // unsigned int m_countBeforeDecoration = 0;
  // unsigned int m_countAfterDecoration = 0;

  std::size_t m_maxSpectrumIntNumber = std::numeric_limits<std::size_t>::max();
  // std::size_t m_maxSpectrumIntNumber = 10000;

  bool m_isPeptidomic                = false;
  bool m_isMissedCleavagesInSpectree = false;
};
