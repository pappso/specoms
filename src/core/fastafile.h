/**
 * \file core/fastafile.h
 * \date 3/11/2018
 * \author Olivier Langella
 * \brief fasta file to read protein and sequences
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QFileInfo>
#include <pappsomspp/fasta/fastahandlerinterface.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/fasta/fastafileindexer.h>
#include "../utils/monitorinterface.h"

class FastaSourceList;
class FastaFile;

class InitialReader : public pappso::FastaHandlerInterface
{
  friend FastaFile;

  protected:
  InitialReader(FastaFile *p_fastaFile, MonitorInterface &monitor);
  void setSequence(const QString &description,
                   const QString &sequence) override;


  private:
  MonitorInterface &m_monitor;
  FastaFile *mp_fastaFile;
};

class FastaFile
{
  friend InitialReader;

  public:
  FastaFile(const QFileInfo &fasta_file_info, bool add_reverse);
  FastaFile(const FastaFile &other);
  virtual ~FastaFile();

  void initialRead(FastaSourceList *fasta_source_list,
                   MonitorInterface &monitor);
  std::size_t size() const;
  const QString fileName() const;
  bool isAddReverse() const;

  pappso::Protein getProteinByIndex(std::size_t protein_index) const;

  void setXmlId(const QString &xml_id);
  const QString &getXmlId() const;

  const QFile &getFastaFile() const;

  protected:
  void initialSequence(const QString &description, const QString &sequence);


  private:
  QString m_xmlId;
  std::size_t m_count;
  bool m_addReverse;
  QFile m_fastaFile;
  FastaSourceList *mp_fastaSourceList;
  bool m_reverse;

  pappso::FastaFileIndexerSPtr msp_fastaFileIndex = nullptr;
};
