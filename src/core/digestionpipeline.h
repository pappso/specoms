/**
 * \file core/digestionpipeline.h
 * \date 4/11/2018
 * \author Olivier Langella
 * \brief protein digestion pipeline
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of peptider.
 *
 *     peptider is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     peptider is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with peptider.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <pappsomspp/protein/enzyme.h>
#include <pappsomspp/protein/peptidemodificatorpipeline.h>
#include <pappsomspp/protein/peptidesizefilter.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/protein/peptidevariablemodificationbuilder.h>
#include <pappsomspp/protein/peptidevariablemodificationreplacement.h>
#include <pappsomspp/protein/peptidemethioninremove.h>

#include <QXmlStreamWriter>


class DigestionPipelineBase
{
  public:
  DigestionPipelineBase(pappso::PeptideModificatorInterface *digestion_sink);
  virtual ~DigestionPipelineBase();
  virtual void eat(std::int8_t sequence_database_id,
                   const pappso::ProteinSp &protein_sp,
                   bool is_decoy) = 0;

  protected:
  unsigned int _maximum_number_of_missed_cleavage = 1;
  bool _potention_protein_acetylation             = true;
  bool _potential_methionin_removal               = false;
  bool _potential_peptide_nter_cyclisation        = true;

  pappso::PeptideModificatorPipeline _peptide_modificator_pipeline;
  pappso::PeptideSizeFilter *_p_peptide_size2varmod;
  pappso::Enzyme _trypsin;
  /** \*brief potential acetylated residue MOD:00394 for Nterminal protein */
  pappso::PeptideVariableModificationBuilder *_p_potential_nter_acetylation =
    nullptr;
  pappso::PeptideVariableModificationBuilder *_p_potential_E_cyclisation =
    nullptr;
  pappso::PeptideVariableModificationBuilder *_p_potential_Q_cyclisation =
    nullptr;
  pappso::PeptideVariableModificationReplacement
    *_p_potential_Ccarba_cyclisation = nullptr;

  pappso::PeptideMethioninRemove *_p_methionin_removal = nullptr;
};

class DigestionPipeline : public DigestionPipelineBase
{
  public:
  DigestionPipeline(pappso::PeptideModificatorInterface *digestion_sink);
  ~DigestionPipeline();
  void eat(std::int8_t sequence_database_id,
           const pappso::ProteinSp &protein_sp,
           bool is_decoy) override;
  void writeMzIdentMlModificationParams(QXmlStreamWriter *output_stream) const;
  void writeMzIdentMlEnzymes(QXmlStreamWriter *output_stream) const;


  private:
};
