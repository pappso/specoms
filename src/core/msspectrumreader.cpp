/**
 * \file core/msspectrumreader.cpp
 * \date 16/10/2018
 * \author Olivier Langella
 * \brief SpectrumCollectionHandlerInterface implementation to read spectrum
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QDebug>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/processing/filters/filtertandemremovec13.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include "msspectrumreader.h"
#include "../utils/utils.h"

MsSpectrumReader::MsSpectrumReader(MonitorInterface &monitor,
                                   const DeepProtParams &params,
                                   SpectrumIntStoreSPtr &spectrum_int_store_sp)
  : m_monitor(monitor),
    m_filter_greatest_intensities(100),
    m_filter_keep_greater(0),
    m_filter_keep_smaller(2000)
{
  msp_spectrumIntStore = spectrum_int_store_sp;
  if(msp_spectrumIntStore == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("msp_spectrumIntStore == nullptr"));
    }

  m_accuracyFactor = params.getAccuracyFactor();
  m_accuracyValue  = params.get(DeepProtParam::AccuracyValue).toUInt();
  m_minimalPeptideCharge =
    params.get(DeepProtParam::MinimalPeptideCharge).toUInt();
  m_maximalPeptideCharge =
    params.get(DeepProtParam::MaximalPeptideCharge).toUInt();
  m_isRemoveIsotope = params.get(DeepProtParam::SpectrumRemoveIsotope).toBool();

  m_isComplementIonEnhancerFilter =
    params.get(DeepProtParam::SpectrumComplementIonEnhancer).toBool();
  m_isExcludeParent =
    params.get(DeepProtParam::SpectrumExcludeParentIon).toBool();
  m_isExcludeParentLowerDalton =
    params.get(DeepProtParam::SpectrumExcludeParentLowerDalton)
      .toDouble(); // 2;
  m_isExcludeParentUpperDalton =
    params.get(DeepProtParam::SpectrumExcludeParentUpperDalton)
      .toDouble(); // 2;

  m_filter_greatest_intensities = pappso::FilterGreatestY(
    params.get(DeepProtParam::SpectrumNmostIntense).toUInt()); // 100;
  m_dynamicRange =
    params.get(DeepProtParam::SpectrumDynamicRange).toUInt(); // 100;
  m_isExcludeParentNeutralLoss =
    params.get(DeepProtParam::SpectrumExcludeParentIonNeutralLoss)
      .toBool(); // false;
  m_neutralLossMass = params.get(DeepProtParam::SpectrumNeutralLossMass)
                        .toDouble(); // pappso::MASSH2O;
  m_neutralLossWindowDalton =
    params.get(DeepProtParam::SpectrumNeutralLossMassWindowDalton)
      .toDouble(); // 0.5;

  m_filter_keep_greater = pappso::FilterResampleKeepGreater(
    params.get(DeepProtParam::SpectrumLowerMzLimit).toDouble()); // 150;
  // m_lowerMzLimit =
  // params.get(DeepProtParam::SpectrumLowerMzLimit).toDouble();

  m_filter_keep_smaller = pappso::FilterResampleKeepSmaller(
    params.get(DeepProtParam::SpectrumUpperMzLimit).toDouble()); // 150;
  // m_upperMzLimit =
  // params.get(DeepProtParam::SpectrumUpperMzLimit).toDouble();
  m_minimumNumberOfPeaks =
    params.get(DeepProtParam::ExperimentalSpectrumModelMinimumNumberOfPeaks)
      .toUInt();

  m_ms2Precision = pappso::PrecisionFactory::getDaltonInstance(0.02);

  if(params.get(DeepProtParam::ParentIonMassToleranceUnit).toString() == "ppm")
    {
      m_ms2Precision = pappso::PrecisionFactory::getPpmInstance(
        params.get(DeepProtParam::ParentIonMassTolerancePrecision).toDouble());
    }
  else
    {
      m_ms2Precision = pappso::PrecisionFactory::getDaltonInstance(
        params.get(DeepProtParam::ParentIonMassTolerancePrecision).toDouble());
    }
  if(m_isRemoveIsotope)
    {
      mpa_filterRemoveC13 = new pappso::FilterRemoveC13(m_ms2Precision);
    }

  if((params.get(DeepProtParam::SpectrumWindowFilterWidth).toDouble() > 0) &&
     (params.get(DeepProtParam::SpectrumWindowFilterMax).toUInt() > 0))
    {
      mpa_filterGreatestYperWindow = new pappso::FilterGreatestYperWindow(
        params.get(DeepProtParam::SpectrumWindowFilterWidth).toDouble(),
        params.get(DeepProtParam::SpectrumWindowFilterMax).toUInt());
    }


  if(params.get(DeepProtParam::SpectrumFilterChargeDeconvolution).toBool())
    {
      mpa_filterChargeDeconvolution =
        new pappso::FilterChargeDeconvolution(m_ms2Precision);
    }
}

MsSpectrumReader::~MsSpectrumReader()
{
  if(mpa_filterRemoveC13 != nullptr)
    {
      delete mpa_filterRemoveC13;
    }


  if(mpa_filterGreatestYperWindow != nullptr)
    {
      delete mpa_filterGreatestYperWindow;
    }


  if(mpa_filterChargeDeconvolution != nullptr)
    {
      delete mpa_filterChargeDeconvolution;
    }
}


bool
MsSpectrumReader::shouldStop()
{
  return m_monitor.shouldIstop();
}

bool
MsSpectrumReader::needPeakList() const
{
  m_monitor.startMonitoredOperation(MonitoredOperation::readingMzData);
  return true;
}
void
MsSpectrumReader::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{
  qDebug();
  if(qspectrum.getMsLevel() == 1)
    return;
  qDebug() << qspectrum.getMsLevel() << " " << qspectrum.getPrecursorCharge();
  if(qspectrum.getPrecursorCharge() > m_maximalPeptideCharge)
    return;
  if(qspectrum.getPrecursorCharge() < m_minimalPeptideCharge)
    return;
  qDebug() << "spectrum native id = "
           << qspectrum.getMassSpectrumId().getNativeId();
  // Reading the peaks for each spectrum
  /*
  for (Map.Entry<Double, Double> entry : peakMap.entrySet()) {
      mz = entry.getKey();
      intensity = entry.getValue();
      if(intensity > 0.0) {
          peaks.add(Peak.buildPeak(mz, intensity, Param.accuracyFactor()));
      }
  }
  */
  if(qspectrum.getMassSpectrumCstSPtr().get() != nullptr)
    {
      pappso::MassSpectrum mass_spectrum;
      mass_spectrum = *(qspectrum.getMassSpectrumCstSPtr().get());

      qDebug() << mass_spectrum.size();

      if(mpa_filterRemoveC13 != nullptr)
        {
          mass_spectrum.filter(*mpa_filterRemoveC13);
        }
      if(m_isComplementIonEnhancerFilter)
        {
          pappso::FilterComplementIonEnhancer(qspectrum, m_ms2Precision)
            .filter(mass_spectrum);
        }

      if(m_dynamicRange > 0)
        {
          mass_spectrum.filter(pappso::FilterRescaleY(m_dynamicRange));
          mass_spectrum.filter(pappso::FilterHighPass(1));
        }

      if(mpa_filterGreatestYperWindow != nullptr)
        {
          mass_spectrum.filter(*mpa_filterGreatestYperWindow);
        }

      mass_spectrum.filter(m_filter_keep_greater);

      mass_spectrum.filter(m_filter_keep_smaller);

      mass_spectrum.filter(m_filter_greatest_intensities);

      qDebug() << mass_spectrum.size();


      std::vector<std::size_t> integer_spectrum =
        Utils::TraceToIntegerSpectrum(mass_spectrum, m_accuracyFactor);

      qDebug() << qspectrum.getMassSpectrumId().getNativeId() << " "
               << Utils::IntegerSpectrumToString(integer_spectrum);
      /*
        QStringList peak_list;
        for(auto peak : integer_spectrum)
          {
            peak_list << QString("%1").arg(peak);
          }

        qDebug() << peak_list.join(" ");
      */

      if(m_minimumNumberOfPeaks > integer_spectrum.size())
        {
          // too few peaks to take into account 11962 vs 11928 (10) 11816 (20)
        }
      else
        {
          Utils::expandIntegerSpectrum(integer_spectrum, m_accuracyValue);
          pappso::QualifiedMassSpectrumSPtr qspectrum_sp =
            qspectrum.makeQualifiedMassSpectrumSPtr();
          qspectrum_sp.get()->setMassSpectrumSPtr(nullptr);
          std::size_t spectrum_idx = msp_spectrumIntStore.get()->newSpectrumIntFromExperimentalSpectrum(
            qspectrum_sp, integer_spectrum);


          qDebug() << qspectrum.getMassSpectrumId().getNativeId() << " "
                   << msp_spectrumIntStore.get()
                        ->getSpectrumInt(spectrum_idx)
                        .toString();
        }
    }
  else
    {
      qDebug() << qspectrum.getMassSpectrumId().getNativeId() << " nullptr";
    }
}


pappso::MassSpectrum
MsSpectrumReader::tandemProcess(const pappso::MassSpectrum &spectrum,
                                pappso::pappso_double parent_ion_mz,
                                unsigned int parent_charge) const
{
  qDebug();

  // 1) clean isotopes
  pappso::MassSpectrum spectrum_process(spectrum);

  if(mpa_filterChargeDeconvolution != nullptr)
    {
      mpa_filterChargeDeconvolution->filter(spectrum_process);
    }

  if(m_isRemoveIsotope)
    {
      spectrum_process.massSpectrumFilter(pappso::FilterTandemDeisotope());
    }

  // 2) remove parent ion mass
  if(m_isExcludeParent)
    {
      spectrum_process.massSpectrumFilter(
        pappso::MassSpectrumFilterResampleRemoveMzRange(
          pappso::MzRange(parent_ion_mz,
                          pappso::PrecisionFactory::getDaltonInstance(
                            m_isExcludeParentLowerDalton / parent_charge),
                          pappso::PrecisionFactory::getDaltonInstance(
                            m_isExcludeParentUpperDalton / parent_charge))));
    }
  // 3) remove low masses
  // 4) normalization

  spectrum_process.filter(m_filter_keep_greater);
  spectrum_process.filter(m_filter_keep_smaller);


  if(m_dynamicRange > 0)
    {
      spectrum_process.filter(pappso::FilterRescaleY(m_dynamicRange));
      spectrum_process.filter(pappso::FilterHighPass(1));
    }
  // 5) remove neutral loss
  if(m_isExcludeParentNeutralLoss)
    {
      pappso::pappso_double parent_ion_mhplus =
        ((parent_ion_mz - (pappso::MHPLUS * parent_charge)) * parent_charge) +
        pappso::MHPLUS;
      spectrum_process.massSpectrumFilter(
        pappso::MassSpectrumFilterResampleRemoveMzRange(
          pappso::MzRange(parent_ion_mhplus - m_neutralLossMass,
                          pappso::PrecisionFactory::getDaltonInstance(
                            m_neutralLossWindowDalton))));
    }
  // 6) clean isotopes
  // 7) keep n most intense peaks
  // return spectrum_process.xtandemRemoveC13().nGreatestY(m_nMostIntense);
  return spectrum_process.filter(m_filter_greatest_intensities);
}

void
MsSpectrumReader::loadingEnded()
{
  m_monitor.finishedMonitoredOperation(MonitoredOperation::readingMzData);
}
