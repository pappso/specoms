/**
 * \file core/spectrumintstore.h
 * \date 17/10/2018
 * \author Olivier Langella
 * \brief manage a collection of integer spectrum (without intensity)
 * it is valid within a spectree entity
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <memory>
#include <pappsomspp/types.h>
#include <pappsomspp/peptide/peptide.h>
#include "../utils/types.h"
#include "spectrumint.h"
#include <pappsomspp/processing/spectree/bucketclustering.h>
#include "../utils/deepprotparams.h"
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>

class SpectrumIntStore;
typedef std::shared_ptr<SpectrumIntStore> SpectrumIntStoreSPtr;

class SpectrumIntStore
{
  public:
  SpectrumIntStore(const DeepProtParams &params);

  std::size_t newSpectrumIntFromExperimentalSpectrum(
    pappso::QualifiedMassSpectrumSPtr qualified_mass_spectrum_sp,
    std::vector<std::size_t> &integer_spectrum);
  std::size_t newSpectrumIntFromPeptide(std::size_t peptide_index,
                                        const pappso::PeptideSp &peptide);

  void buildBucketClustering(
    pappso::spectree::BucketClustering &bucket_clustering) const;

  const SpectrumInt &getSpectrumInt(std::size_t spectrum_index) const;
  std::size_t getFirstExperimentalSpectrumIndex() const;
  std::size_t getLastExperimentalSpectrumIndex() const;

  std::size_t getFirstTheoreticalSpectrumIndex() const;
  std::size_t getLastTheoreticalSpectrumIndex() const;

  std::size_t size() const;

  const std::vector<pappso::PeptideIon> &getIonTypeList() const;
  unsigned int getAccuracyFactor() const;

  private:
  /** @param m_accuracyValue the accuracy used for duplication in the
   * discretisation process
   */
  unsigned int m_accuracyValue;
  unsigned int m_accuracyFactor;
  std::vector<pappso::PeptideIon> m_ionTypeList;

  unsigned int m_minimalPeptideCharge;
  unsigned int m_maximalPeptideCharge;
  pappso::pappso_double m_lowerMzLimit;
  pappso::pappso_double m_upperMzLimit;

  std::vector<SpectrumInt> m_spectrumList;

  std::size_t m_firstExperimentalSpectrumIndex = 0;
};
