message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

find_package(Qt6 COMPONENTS Core Gui Xml Gui Widgets Sql Concurrent REQUIRED )

find_package(ZLIB REQUIRED)
find_package(QuaZip-Qt6 REQUIRED)

find_package(OdsStream REQUIRED)

find_package(PappsoMSpp COMPONENTS Core Widget REQUIRED)

#find_package( Boost COMPONENTS iostreams thread filesystem chrono REQUIRED )
#find_package( Boost COMPONENTS thread filesystem iostreams REQUIRED ) 


