# File:///home/langella/developpement/git/pappsomspp/CMakeStuff/toolchains/mxe-toolchain.cmake# 
# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..
# export PATH=/backup2/mxeqt6/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/langella/.dotnet/tools
# /backup2/mxeqt6/usr/bin/x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1  ..

message("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE=1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.

#langella@themis:/win64/qcustomplot-2.1.0+dfsg1/wbuild$ x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 .. -DUSE_QT_VERSION=5


SET(TARGET i2masschroq)

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/include)

if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)

find_package( Qt6 COMPONENTS Core Gui Svg Xml PrintSupport Sql Network Concurrent REQUIRED )


find_package(ZLIB REQUIRED)

find_package(SQLite3 REQUIRED)


#install MXE qtsvg package
#langella@piccolo:/media/langella/pappso/mxe$ make qtsvg



set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIR "/home/langella/developpement/git/qcustomplot-qt6")
set(QCustomPlotQt6_LIBRARIES "/win64/mxeqt6_dll/libQCustomPlotQt6.dll")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIR}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


	set(QuaZip_FOUND 1)
    set(QUAZIP_INCLUDE_DIR "/backup2/win64qt6/libquazip1-qt6-1.4/quazip")
    set(QUAZIP_LIBRARIES "/backup2/win64qt6/libquazip1-qt6-1.4/build/quazip/libquazip1-qt6.dll")
	if(NOT TARGET QuaZip::QuaZip)
		add_library(QuaZip::QuaZip UNKNOWN IMPORTED)
		set_target_properties(QuaZip::QuaZip PROPERTIES
			IMPORTED_LOCATION             "${QUAZIP_LIBRARIES}"
			INTERFACE_INCLUDE_DIRECTORIES "${QUAZIP_INCLUDE_DIR}")
	endif()
	
	
set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIRS "/home/langella/developpement/git/libodsstream/src")
set(OdsStream_LIBRARY "/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll")

if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION "${OdsStream_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIRS}"
		)
endif()





set(PappsoMSpp_INCLUDE_DIR /home/langella/developpement/git/pappsomspp/src/)
mark_as_advanced(PappsoMSpp_INCLUDE_DIR)
set(PappsoMSpp_INCLUDE_DIRS ${PappsoMSpp_INCLUDE_DIR})
# Look for the necessary library
set(PappsoMSpp_LIBRARY /home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll)
mark_as_advanced(PappsoMSpp_LIBRARY)
# Mark the lib as found
set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_LIBRARIES ${PappsoMSpp_LIBRARY})
if(NOT TARGET PappsoMSpp::Core)
    add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Core PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSpp_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()
set(PappsoMSppWidget_LIBRARY /home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll)
mark_as_advanced(PappsoMSppWidget_LIBRARY)  
message(STATUS "~~~~~~~~~~~~~ ${PappsoMSppWidget_LIBRARY} ~~~~~~~~~~~~~~~")
set(PappsoMSppWidget_FOUND TRUE)
if(NOT TARGET PappsoMSpp::Widget)
    add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Widget PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()

