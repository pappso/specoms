message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)


if (NOT OdsStream_FOUND)
    find_package( OdsStream REQUIRED )
endif (NOT OdsStream_FOUND)


set(PappsoMSpp_INCLUDE_DIR /home/langella/developpement/git/pappsomspp/src)
mark_as_advanced(PappsoMSpp_INCLUDE_DIR)
set(PappsoMSpp_INCLUDE_DIRS ${PappsoMSpp_INCLUDE_DIR})
# Look for the necessary library
set(PappsoMSpp_LIBRARY /home/langella/developpement/git/pappsomspp/build/src/libpappsomspp.so)
mark_as_advanced(PappsoMSpp_LIBRARY)
# Mark the lib as found
set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_LIBRARIES ${PappsoMSpp_LIBRARY})
if(NOT TARGET PappsoMSpp::Core)
    add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Core PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSpp_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()
set(PappsoMSppWidget_LIBRARY /home/langella/developpement/git/pappsomspp/build/src/pappsomspp/widget/libpappsomspp-widget.so)
mark_as_advanced(PappsoMSppWidget_LIBRARY)  
message(STATUS "~~~~~~~~~~~~~ ${PappsoMSppWidget_LIBRARY} ~~~~~~~~~~~~~~~")
set(PappsoMSppWidget_FOUND TRUE)
if(NOT TARGET PappsoMSpp::Widget)
    add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Widget PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()




set(Alglib_DIR ${CMAKE_MODULE_PATH})
find_package(Alglib REQUIRED)


find_package( ZLIB REQUIRED )

set(Zstd_DIR ${CMAKE_MODULE_PATH})
find_package(Zstd REQUIRED)
