
# test on d7090afdcf197eb3aef148bfcd1110c47dae14df

./src/deepprot -p ../test/cpp/cmd/extraction_method/eme_params.csv  -m /gorgone/pappso/data_extraction_pappso/mzML/20120906_balliau_extract_1_A01_urnb-1.mzML /gorgone/pappso/moulon/database/Genome_Z_mays_5a.fasta /gorgone/pappso/moulon/database/contaminants_standarts.fasta -o /gorgone/pappso/versions_logiciels_pappso/deepprot/v0.0.9/eme/20120906_balliau_extract_1_A01_urnb-1.mzid



# test on a single spectrum
msconvert /gorgone/pappso/data_extraction_pappso/mzML/20120906_balliau_extract_1_A01_urnb-1.mzML --filter "index 15780" --mgf --outfile /home/langella/data1/deepprot/20120906_balliau_extract_1_A01_urnb-1_15780.mgf


langella@piccolo:~/developpement/git/deepprot/cbuild$ ./src/deepprot -p ../test/cpp/cmd/extraction_method/eme_params.csv  -m /gorgone/pappso/versions_logiciels_pappso/deepprot/v0.0.9/test/20120906_balliau_extract_1_A01_urnb-1_15780.mgf /gorgone/pappso/moulon/database/Genome_Z_mays_5a.fasta /gorgone/pappso/moulon/database/contaminants_standarts.fasta -o /gorgone/pappso/versions_logiciels_pappso/deepprot/v0.0.9/test/15780.mzid

problem :
<userParam name="DeepProt:match_type" value="delta_position"/>
                        <userParam name="DeepProt:status" value="delta_position"/>

it should not because :
userParam name="DeepProt:mass_delta" value="-0.004521472715"/>

mass delta is under 10ppm


new test on #b6e25cc0a3e0df2a75d101824913e99554f9651b
