


19/08/2020
execution on a7cf76fd0acfc44de4819bb6447e68b9eda9cc84


langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD004732/PXD004732_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 68 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 77 seconds
55456 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 154122
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 74182
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 43272
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 408 seconds
this is the end
overall process took 597 seconds
peptide identification is finished

real	9m59,271s
user	158m4,573s
sys	0m50,318s
langella@themis:~/developpement/git/deepprot/cbuild$ Rscript ../test/cpp/R/count.R /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv/specfit.tsv 
[1] "total: 27931"
[1] "decoy: 5815"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]    12     0    24  0.00000000
 [2,]    37     0    23  0.00000000
 [3,]    76     0    22  0.00000000
 [4,]   139     0    21  0.00000000
 [5,]   231     0    20  0.00000000
 [6,]   373     0    19  0.00000000
 [7,]   598     0    18  0.00000000
 [8,]  1046     1    17  0.09560229
 [9,]  1938     5    16  0.25799794
[10,]  3428     5    15  0.14585764
[11,]  5327    15    14  0.28158438
[12,]  7599    51    13  0.67114094
[13,] 10016   152    12  1.51757188
[14,] 12582   416    11  3.30631060
[15,] 15633  1013    10  6.47988230
[16,] 19312  2108     9 10.91549296
[17,] 23360  3770     8 16.13869863
[18,] 27931  5815     7 20.81916151
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    24        NaN
 [2,]     0     0    23        NaN
 [3,]    11     0    22  0.0000000
 [4,]    32     0    21  0.0000000
 [5,]    56     0    20  0.0000000
 [6,]    99     0    19  0.0000000
 [7,]   161     0    18  0.0000000
 [8,]   278     1    17  0.3597122
 [9,]   537     4    16  0.7448790
[10,]  1041     4    15  0.3842459
[11,]  1891    12    14  0.6345849
[12,]  3031    42    13  1.3856813
[13,]  4441   122    12  2.7471290
[14,]  6037   334    11  5.5325493
[15,]  8087   785    10  9.7069371
[16,] 10623  1635     9 15.3911324
[17,] 13573  2877     8 21.1964930
[18,] 16335  4135     7 25.3137435
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]    10     0    24 0.00000000
 [2,]    30     0    23 0.00000000
 [3,]    52     0    22 0.00000000
 [4,]    79     0    21 0.00000000
 [5,]   131     0    20 0.00000000
 [6,]   205     0    19 0.00000000
 [7,]   317     0    18 0.00000000
 [8,]   535     0    17 0.00000000
 [9,]   968     0    16 0.00000000
[10,]  1629     0    15 0.00000000
[11,]  2264     0    14 0.00000000
[12,]  2863     0    13 0.00000000
[13,]  3329     1    12 0.03003905
[14,]  3756     5    11 0.13312034
[15,]  4107    21    10 0.51132213
[16,]  4465    57     9 1.27659574
[17,]  4465    57     8 1.27659574
[18,]  4465    57     7 1.27659574
[1] " delta > 0"
      Total Decoy peaks        FDR
 [1,]     2     0    24  0.0000000
 [2,]     7     0    23  0.0000000
 [3,]    13     0    22  0.0000000
 [4,]    28     0    21  0.0000000
 [5,]    44     0    20  0.0000000
 [6,]    69     0    19  0.0000000
 [7,]   120     0    18  0.0000000
 [8,]   233     0    17  0.0000000
 [9,]   433     1    16  0.2309469
[10,]   758     1    15  0.1319261
[11,]  1172     3    14  0.2559727
[12,]  1705     9    13  0.5278592
[13,]  2246    29    12  1.2911843
[14,]  2789    77    11  2.7608462
[15,]  3439   207    10  6.0191916
[16,]  4224   416     9  9.8484848
[17,]  5322   836     8 15.7083803
[18,]  7131  1623     7 22.7597812



21/09/2020
execution on 356e527fcd7a036b98fc8bb1ae5e20419e0069fa


langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD004732/PXD004732_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
cond	96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 67 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 77 seconds
55456 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 154207
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 74182
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 43214
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 404 seconds
this is the end
overall process took 594 seconds
peptide identification is finished

real	11m10,636s
user	156m39,895s
sys	0m48,017s
langella@themis:~/developpement/git/deepprot/cbuild$ Rscript ../test/cpp/R/count.R /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv/specfit.tsv
[1] "total: 27585"
[1] "decoy: 5864"
[1] " tout"
   Total Decoy peaks         FDR
1     12     0    24  0.00000000
2     36     0    23  0.00000000
3     76     0    22  0.00000000
4    138     0    21  0.00000000
5    229     0    20  0.00000000
6    372     0    19  0.00000000
7    600     0    18  0.00000000
8   1045     1    17  0.09569378
9   1925     5    16  0.25974026
10  3414     5    15  0.14645577
11  5323    18    14  0.33815518
12  7601    58    13  0.76305749
13 10040   172    12  1.71314741
14 12624   450    11  3.56463878
15 15660  1036    10  6.61558110
16 19262  2170     9 11.26570450
17 23181  3832     8 16.53077952
18 27585  5864     7 21.25793003
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks        FDR
1      0     0    24        NaN
2      0     0    23        NaN
3     11     0    22  0.0000000
4     32     0    21  0.0000000
5     57     0    20  0.0000000
6     99     0    19  0.0000000
7    163     0    18  0.0000000
8    279     1    17  0.3584229
9    532     4    16  0.7518797
10  1037     4    15  0.3857281
11  1892    15    14  0.7928118
12  3036    48    13  1.5810277
13  4461   140    12  3.1383098
14  6087   367    11  6.0292426
15  8159   814    10  9.9767128
16 10667  1676     9 15.7120090
17 13513  2889     8 21.3794124
18 16209  4154     7 25.6277377
   Total Decoy peaks       FDR
11  1892    15    14 0.7928118
[1] " delta == 0"
   Total Decoy peaks        FDR
1     10     0    24 0.00000000
2     29     0    23 0.00000000
3     51     0    22 0.00000000
4     78     0    21 0.00000000
5    129     0    20 0.00000000
6    203     0    19 0.00000000
7    316     0    18 0.00000000
8    532     0    17 0.00000000
9    963     0    16 0.00000000
10  1617     0    15 0.00000000
11  2252     0    14 0.00000000
12  2852     0    13 0.00000000
13  3307     1    12 0.03023889
14  3732     6    11 0.16077170
15  4064    25    10 0.61515748
16  4387    73     9 1.66400729
17  4387    73     8 1.66400729
18  4387    73     7 1.66400729
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks       FDR
15  4064    25    10 0.6151575
[1] " delta > 0"
   Total Decoy peaks        FDR
1      2     0    24  0.0000000
2      7     0    23  0.0000000
3     14     0    22  0.0000000
4     28     0    21  0.0000000
5     43     0    20  0.0000000
6     70     0    19  0.0000000
7    121     0    18  0.0000000
8    234     0    17  0.0000000
9    430     1    16  0.2325581
10   760     1    15  0.1315789
11  1179     3    14  0.2544529
12  1713    10    13  0.5837712
13  2272    31    12  1.3644366
14  2805    77    11  2.7450980
15  3437   197    10  5.7317428
16  4208   421     9 10.0047529
17  5281   870     8 16.4741526
18  6989  1637     7 23.4225211
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
12  1713    10    13 0.5837712
[1] "Total 1% = 7669"
[1] "Decoy 1% = 50"
[1] "Final FDR = 0.651975485721737"
[1] "assignment % = 27.8013413086823"
