

19/08/2020
execution on 182a2243d9aaa9664e3ad549df34ca22df56d140



langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD004732/PXD004732_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 68 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 11 seconds
55456 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 152659
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 74182
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 42927
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 395 seconds
this is the end
overall process took 520 seconds
peptide identification is finished

real	9m54,958s
user	155m15,860s
sys	0m44,858s



top:
17240 langella  20   0 8372164   7,2g  22624 R  2346  11,5 130:25.09 deepprot


langella@themis:~/developpement/git/deepprot/cbuild$ Rscript ../test/cpp/R/count.R /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv/specfit.tsv 
[1] "total: 27787"
[1] "decoy: 6064"
[1] " tout"
      Total Decoy peaks        FDR
 [1,]     5     0    24  0.0000000
 [2,]    17     0    23  0.0000000
 [3,]    43     0    22  0.0000000
 [4,]    88     0    21  0.0000000
 [5,]   144     0    20  0.0000000
 [6,]   225     0    19  0.0000000
 [7,]   372     0    18  0.0000000
 [8,]   673     0    17  0.0000000
 [9,]  1335     3    16  0.2247191
[10,]  2624     4    15  0.1524390
[11,]  4466     8    14  0.1791312
[12,]  6652    39    13  0.5862898
[13,]  9010   128    12  1.4206437
[14,] 11568   373    11  3.2244122
[15,] 14676   916    10  6.2414827
[16,] 18476  2081     9 11.2632604
[17,] 22628  3752     8 16.5812268
[18,] 27787  6064     7 21.8231547
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    24        NaN
 [2,]     0     0    23        NaN
 [3,]     5     0    22  0.0000000
 [4,]    20     0    21  0.0000000
 [5,]    36     0    20  0.0000000
 [6,]    56     0    19  0.0000000
 [7,]    95     0    18  0.0000000
 [8,]   179     0    17  0.0000000
 [9,]   381     3    16  0.7874016
[10,]   762     3    15  0.3937008
[11,]  1504     6    14  0.3989362
[12,]  2596    29    13  1.1171032
[13,]  3916    98    12  2.5025536
[14,]  5472   296    11  5.4093567
[15,]  7542   719    10  9.5332803
[16,] 10143  1602     9 15.7941437
[17,] 13156  2849     8 21.6555184
[18,] 16390  4309     7 26.2904210
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     5     0    24 0.00000000
 [2,]    13     0    23 0.00000000
 [3,]    27     0    22 0.00000000
 [4,]    49     0    21 0.00000000
 [5,]    82     0    20 0.00000000
 [6,]   129     0    19 0.00000000
 [7,]   203     0    18 0.00000000
 [8,]   340     0    17 0.00000000
 [9,]   672     0    16 0.00000000
[10,]  1299     0    15 0.00000000
[11,]  1996     0    14 0.00000000
[12,]  2601     0    13 0.00000000
[13,]  3097     0    12 0.00000000
[14,]  3548     2    11 0.05636979
[15,]  3946    14    10 0.35478966
[16,]  4347    59     9 1.35725788
[17,]  4347    59     8 1.35725788
[18,]  4347    59     7 1.35725788
[1] " delta > 0"
      Total Decoy peaks        FDR
 [1,]     0     0    24        NaN
 [2,]     4     0    23  0.0000000
 [3,]    11     0    22  0.0000000
 [4,]    19     0    21  0.0000000
 [5,]    26     0    20  0.0000000
 [6,]    40     0    19  0.0000000
 [7,]    74     0    18  0.0000000
 [8,]   154     0    17  0.0000000
 [9,]   282     0    16  0.0000000
[10,]   563     1    15  0.1776199
[11,]   966     2    14  0.2070393
[12,]  1455    10    13  0.6872852
[13,]  1997    30    12  1.5022534
[14,]  2548    75    11  2.9434851
[15,]  3188   183    10  5.7402760
[16,]  3986   420     9 10.5368791
[17,]  5125   844     8 16.4682927
[18,]  7050  1696     7 24.0567376


control on previous commit 9b862f7a4c8db54bcf02cf9677c9e7065a27e35c :

langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD004732/PXD004732_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 69 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 11 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 405 seconds
this is the end
overall process took 532 seconds
peptide identification is finished

real	10m7,907s
user	152m53,664s
sys	0m46,867s



langella@themis:~/developpement/git/deepprot/cbuild$ Rscript ../test/cpp/R/count.R /tmp/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.tsv/specfit.tsv 
[1] "total: 27787"
[1] "decoy: 6064"
[1] " tout"
      Total Decoy peaks        FDR
 [1,]     5     0    24  0.0000000
 [2,]    17     0    23  0.0000000
 [3,]    43     0    22  0.0000000
 [4,]    88     0    21  0.0000000
 [5,]   144     0    20  0.0000000
 [6,]   225     0    19  0.0000000
 [7,]   372     0    18  0.0000000
 [8,]   673     0    17  0.0000000
 [9,]  1335     3    16  0.2247191
[10,]  2624     4    15  0.1524390
[11,]  4466     8    14  0.1791312
[12,]  6652    39    13  0.5862898
[13,]  9010   128    12  1.4206437
[14,] 11568   373    11  3.2244122
[15,] 14676   916    10  6.2414827
[16,] 18476  2081     9 11.2632604
[17,] 22628  3752     8 16.5812268
[18,] 27787  6064     7 21.8231547
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    24        NaN
 [2,]     0     0    23        NaN
 [3,]     5     0    22  0.0000000
 [4,]    20     0    21  0.0000000
 [5,]    36     0    20  0.0000000
 [6,]    56     0    19  0.0000000
 [7,]    95     0    18  0.0000000
 [8,]   179     0    17  0.0000000
 [9,]   381     3    16  0.7874016
[10,]   762     3    15  0.3937008
[11,]  1504     6    14  0.3989362
[12,]  2596    29    13  1.1171032
[13,]  3916    98    12  2.5025536
[14,]  5472   296    11  5.4093567
[15,]  7542   719    10  9.5332803
[16,] 10143  1602     9 15.7941437
[17,] 13156  2849     8 21.6555184
[18,] 16390  4309     7 26.2904210
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     5     0    24 0.00000000
 [2,]    13     0    23 0.00000000
 [3,]    27     0    22 0.00000000
 [4,]    49     0    21 0.00000000
 [5,]    82     0    20 0.00000000
 [6,]   129     0    19 0.00000000
 [7,]   203     0    18 0.00000000
 [8,]   340     0    17 0.00000000
 [9,]   672     0    16 0.00000000
[10,]  1299     0    15 0.00000000
[11,]  1996     0    14 0.00000000
[12,]  2601     0    13 0.00000000
[13,]  3097     0    12 0.00000000
[14,]  3548     2    11 0.05636979
[15,]  3946    14    10 0.35478966
[16,]  4347    59     9 1.35725788
[17,]  4347    59     8 1.35725788
[18,]  4347    59     7 1.35725788
[1] " delta > 0"
      Total Decoy peaks        FDR
 [1,]     0     0    24        NaN
 [2,]     4     0    23  0.0000000
 [3,]    11     0    22  0.0000000
 [4,]    19     0    21  0.0000000
 [5,]    26     0    20  0.0000000
 [6,]    40     0    19  0.0000000
 [7,]    74     0    18  0.0000000
 [8,]   154     0    17  0.0000000
 [9,]   282     0    16  0.0000000
[10,]   563     1    15  0.1776199
[11,]   966     2    14  0.2070393
[12,]  1455    10    13  0.6872852
[13,]  1997    30    12  1.5022534
[14,]  2548    75    11  2.9434851
[15,]  3188   183    10  5.7402760
[16,]  3986   420     9 10.5368791
[17,]  5125   844     8 16.4682927
[18,]  7050  1696     7 24.0567376
