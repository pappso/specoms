/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf



28/08/2020
execution on 2e31387f3537f7c1429dfb2e27e9efed0a236c7f

langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/low_mass_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_low_mass.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 102 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 10 seconds
43117 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 160616
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77310
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 57041
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 478 seconds
this is the end
overall process took 644 seconds
peptide identification is finished

real	11m0,115s
user	185m19,760s
sys	0m55,944s
langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_low_mass.tsv/specfit.tsv 
[1] "total: 41066"
[1] "decoy: 8217"
[1] " tout"
   Total Decoy peaks         FDR
1      1     0    29  0.00000000
2      3     0    28  0.00000000
3      9     0    27  0.00000000
4     21     0    26  0.00000000
5     29     0    25  0.00000000
6     51     0    24  0.00000000
7     88     0    23  0.00000000
8    144     0    22  0.00000000
9    251     0    21  0.00000000
10   416     0    20  0.00000000
11   651     0    19  0.00000000
12  1017     0    18  0.00000000
13  1586     0    17  0.00000000
14  2430     0    16  0.00000000
15  3665     1    15  0.02728513
16  5253     2    14  0.03807348
17  7361     5    13  0.06792555
18 10194    29    12  0.28448107
19 13633   109    11  0.79953055
20 18087   467    10  2.58196495
21 24447  1777     9  7.26878554
22 32171  4426     8 13.75773212
23 38345  7015     7 18.29443213
24 41066  8217     6 20.00925340
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks       FDR
17   658     5    13 0.7598784
[1] " delta == 0"
   Total Decoy peaks        FDR
1      1     0    29 0.00000000
2      2     0    28 0.00000000
3      2     0    27 0.00000000
4      6     0    26 0.00000000
5      8     0    25 0.00000000
6     16     0    24 0.00000000
7     27     0    23 0.00000000
8     46     0    22 0.00000000
9     86     0    21 0.00000000
10   145     0    20 0.00000000
11   234     0    19 0.00000000
12   392     0    18 0.00000000
13   622     0    17 0.00000000
14  1008     0    16 0.00000000
15  1533     0    15 0.00000000
16  2267     0    14 0.00000000
17  3288     0    13 0.00000000
18  4613     0    12 0.00000000
19  6135     1    11 0.01629992
20  7923     2    10 0.02524296
21  9756     9     9 0.09225092
22  9756     9     8 0.09225092
23  9756     9     7 0.09225092
24  9756     9     6 0.09225092
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks        FDR
24  9756     9     6 0.09225092
[1] " delta > 0"
   Total Decoy peaks        FDR
1      0     0    29        NaN
2      1     0    28  0.0000000
3      7     0    27  0.0000000
4     15     0    26  0.0000000
5     21     0    25  0.0000000
6     31     0    24  0.0000000
7     56     0    23  0.0000000
8     89     0    22  0.0000000
9    146     0    21  0.0000000
10   247     0    20  0.0000000
11   373     0    19  0.0000000
12   558     0    18  0.0000000
13   842     0    17  0.0000000
14  1251     0    16  0.0000000
15  1836     0    15  0.0000000
16  2537     0    14  0.0000000
17  3415     0    13  0.0000000
18  4624     7    12  0.1513841
19  5967    23    11  0.3854533
20  7502    66    10  0.8797654
21  9203   240     9  2.6078453
22 11631   655     8  5.6315020
23 13393  1212     7  9.0495035
24 14439  1617     6 11.1988365
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
20  7502    66    10 0.8797654
[1] "Total 1% = 17916"
[1] "Decoy 1% = 80"
[1] "Final FDR = 0.446528242911364"
[1] "assignment % = 43.6273316125262"
