/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf



26/08/2020
execution on 7962f9494eb2740a95a7de947881a2edc578bc49




langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/ion_enhancer_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_ion_enhancer.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 103 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 14 seconds
43121 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 162947
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 56697
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 533 seconds
this is the end
overall process took 704 seconds
peptide identification is finished

real	11m47,321s
user	201m42,840s
sys	0m54,244s
langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_ion_enhancer.tsv/specfit.tsv 
[1] "total: 39187"
[1] "decoy: 6275"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]     1     0    33  0.00000000
 [2,]     4     0    32  0.00000000
 [3,]     7     0    31  0.00000000
 [4,]    10     0    30  0.00000000
 [5,]    12     0    29  0.00000000
 [6,]    20     0    28  0.00000000
 [7,]    35     0    27  0.00000000
 [8,]    58     0    26  0.00000000
 [9,]    94     0    25  0.00000000
[10,]   154     0    24  0.00000000
[11,]   250     0    23  0.00000000
[12,]   397     0    22  0.00000000
[13,]   622     0    21  0.00000000
[14,]   957     0    20  0.00000000
[15,]  1434     0    19  0.00000000
[16,]  2073     0    18  0.00000000
[17,]  2961     0    17  0.00000000
[18,]  4253     1    16  0.02351281
[19,]  5819     4    15  0.06874033
[20,]  7937     9    14  0.11339297
[21,] 10678    25    13  0.23412624
[22,] 13969    94    12  0.67291861
[23,] 17494   250    11  1.42906139
[24,] 21961   755    10  3.43791266
[25,] 27575  1901     9  6.89392566
[26,] 33701  3987     8 11.83050948
[27,] 39187  6275     7 16.01296348
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    33        NaN
 [2,]     0     0    32        NaN
 [3,]     0     0    31        NaN
 [4,]     0     0    30        NaN
 [5,]     0     0    29        NaN
 [6,]     1     0    28  0.0000000
 [7,]     2     0    27  0.0000000
 [8,]     2     0    26  0.0000000
 [9,]     4     0    25  0.0000000
[10,]     7     0    24  0.0000000
[11,]    12     0    23  0.0000000
[12,]    25     0    22  0.0000000
[13,]    42     0    21  0.0000000
[14,]    60     0    20  0.0000000
[15,]    95     0    19  0.0000000
[16,]   147     0    18  0.0000000
[17,]   211     0    17  0.0000000
[18,]   331     1    16  0.3021148
[19,]   477     4    15  0.8385744
[20,]   689     8    14  1.1611030
[21,]   982    21    13  2.1384929
[22,]  1423    73    12  5.1300070
[23,]  2059   206    11 10.0048567
[24,]  3378   617    10 18.2652457
[25,]  5820  1534     9 26.3573883
[26,]  9690  3131     8 32.3116615
[27,] 12979  4584     7 35.3185916
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     1     0    33 0.00000000
 [2,]     2     0    32 0.00000000
 [3,]     3     0    31 0.00000000
 [4,]     4     0    30 0.00000000
 [5,]     4     0    29 0.00000000
 [6,]     7     0    28 0.00000000
 [7,]    13     0    27 0.00000000
 [8,]    23     0    26 0.00000000
 [9,]    32     0    25 0.00000000
[10,]    63     0    24 0.00000000
[11,]   109     0    23 0.00000000
[12,]   169     0    22 0.00000000
[13,]   277     0    21 0.00000000
[14,]   431     0    20 0.00000000
[15,]   653     0    19 0.00000000
[16,]   985     0    18 0.00000000
[17,]  1406     0    17 0.00000000
[18,]  2054     0    16 0.00000000
[19,]  2838     0    15 0.00000000
[20,]  3915     0    14 0.00000000
[21,]  5308     0    13 0.00000000
[22,]  6916     1    12 0.01445922
[23,]  8522     3    11 0.03520300
[24,] 10204    13    10 0.12740102
[25,] 11811    39     9 0.33020066
[26,] 11811    39     8 0.33020066
[27,] 11811    39     7 0.33020066
[1] " delta > 0"
      Total Decoy peaks        FDR
 [1,]     0     0    33        NaN
 [2,]     2     0    32  0.0000000
 [3,]     4     0    31  0.0000000
 [4,]     6     0    30  0.0000000
 [5,]     8     0    29  0.0000000
 [6,]    12     0    28  0.0000000
 [7,]    20     0    27  0.0000000
 [8,]    33     0    26  0.0000000
 [9,]    58     0    25  0.0000000
[10,]    84     0    24  0.0000000
[11,]   129     0    23  0.0000000
[12,]   203     0    22  0.0000000
[13,]   303     0    21  0.0000000
[14,]   466     0    20  0.0000000
[15,]   686     0    19  0.0000000
[16,]   941     0    18  0.0000000
[17,]  1344     0    17  0.0000000
[18,]  1868     0    16  0.0000000
[19,]  2504     0    15  0.0000000
[20,]  3333     1    14  0.0300030
[21,]  4388     4    13  0.0911577
[22,]  5630    20    12  0.3552398
[23,]  6913    41    11  0.5930855
[24,]  8379   125    10  1.4918248
[25,]  9944   328     9  3.2984714
[26,] 12200   817     8  6.6967213
[27,] 14397  1652     7 11.4746128



testing ion enhancer filter + isotope filter :
77064d9683d4873b417aaeff925e42bca3e404af


langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/ion_enhancer_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_ion_enhancer.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 104 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 20 seconds
43120 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 164784
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 57364
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 550 seconds
this is the end
overall process took 730 seconds
peptide identification is finished

real	12m26,061s
user	212m18,637s
sys	0m57,552s
langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_ion_enhancer.tsv/specfit.tsv 
[1] "total: 39387"
[1] "decoy: 5786"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]     2     0    33  0.00000000
 [2,]     2     0    32  0.00000000
 [3,]     3     0    31  0.00000000
 [4,]     6     0    30  0.00000000
 [5,]    11     0    29  0.00000000
 [6,]    24     0    28  0.00000000
 [7,]    42     0    27  0.00000000
 [8,]    72     0    26  0.00000000
 [9,]   110     0    25  0.00000000
[10,]   194     0    24  0.00000000
[11,]   307     0    23  0.00000000
[12,]   460     0    22  0.00000000
[13,]   732     0    21  0.00000000
[14,]  1101     0    20  0.00000000
[15,]  1560     0    19  0.00000000
[16,]  2241     0    18  0.00000000
[17,]  3186     0    17  0.00000000
[18,]  4545     0    16  0.00000000
[19,]  6239     1    15  0.01602821
[20,]  8464     7    14  0.08270321
[21,] 11211    22    13  0.19623584
[22,] 14540    75    12  0.51581843
[23,] 18155   224    11  1.23381988
[24,] 22669   672    10  2.96440072
[25,] 28199  1698     9  6.02149012
[26,] 34201  3657     8 10.69266980
[27,] 39387  5786     7 14.69012618
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    33        NaN
 [2,]     0     0    32        NaN
 [3,]     0     0    31        NaN
 [4,]     0     0    30        NaN
 [5,]     1     0    29  0.0000000
 [6,]     1     0    28  0.0000000
 [7,]     2     0    27  0.0000000
 [8,]     3     0    26  0.0000000
 [9,]     4     0    25  0.0000000
[10,]     9     0    24  0.0000000
[11,]    19     0    23  0.0000000
[12,]    28     0    22  0.0000000
[13,]    48     0    21  0.0000000
[14,]    71     0    20  0.0000000
[15,]    96     0    19  0.0000000
[16,]   152     0    18  0.0000000
[17,]   220     0    17  0.0000000
[18,]   323     0    16  0.0000000
[19,]   463     1    15  0.2159827
[20,]   681     6    14  0.8810573
[21,]   951    16    13  1.6824395
[22,]  1401    62    12  4.4254104
[23,]  2040   188    11  9.2156863
[24,]  3329   556    10 16.7017122
[25,]  5652  1362     9 24.0976645
[26,]  9386  2866     8 30.5348391
[27,] 12419  4208     7 33.8835655
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     2     0    33 0.00000000
 [2,]     2     0    32 0.00000000
 [3,]     3     0    31 0.00000000
 [4,]     4     0    30 0.00000000
 [5,]     4     0    29 0.00000000
 [6,]     9     0    28 0.00000000
 [7,]    16     0    27 0.00000000
 [8,]    30     0    26 0.00000000
 [9,]    50     0    25 0.00000000
[10,]    93     0    24 0.00000000
[11,]   152     0    23 0.00000000
[12,]   230     0    22 0.00000000
[13,]   357     0    21 0.00000000
[14,]   552     0    20 0.00000000
[15,]   780     0    19 0.00000000
[16,]  1144     0    18 0.00000000
[17,]  1594     0    17 0.00000000
[18,]  2299     0    16 0.00000000
[19,]  3173     0    15 0.00000000
[20,]  4307     0    14 0.00000000
[21,]  5706     0    13 0.00000000
[22,]  7284     2    12 0.02745744
[23,]  8870     3    11 0.03382187
[24,] 10560    11    10 0.10416667
[25,] 12127    45     9 0.37107281
[26,] 12127    45     8 0.37107281
[27,] 12127    45     7 0.37107281
[1] " delta > 0"
      Total Decoy peaks        FDR
 [1,]     0     0    33        NaN
 [2,]     0     0    32        NaN
 [3,]     0     0    31        NaN
 [4,]     2     0    30  0.0000000
 [5,]     6     0    29  0.0000000
 [6,]    14     0    28  0.0000000
 [7,]    24     0    27  0.0000000
 [8,]    39     0    26  0.0000000
 [9,]    56     0    25  0.0000000
[10,]    92     0    24  0.0000000
[11,]   136     0    23  0.0000000
[12,]   202     0    22  0.0000000
[13,]   327     0    21  0.0000000
[14,]   478     0    20  0.0000000
[15,]   684     0    19  0.0000000
[16,]   945     0    18  0.0000000
[17,]  1372     0    17  0.0000000
[18,]  1923     0    16  0.0000000
[19,]  2603     0    15  0.0000000
[20,]  3476     1    14  0.0287687
[21,]  4554     6    13  0.1317523
[22,]  5855    11    12  0.1878736
[23,]  7245    33    11  0.4554865
[24,]  8780   105    10  1.1958998
[25,] 10420   291     9  2.7927063
[26,] 12688   746     8  5.8795712
[27,] 14841  1533     7 10.3294926


testing ion enhancer filter + isotope filter, score >=6 :
f08e01dc170d1c9fa0869b7fa7bd75dfdba7cb34

langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/ion_enhancer_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_ion_enhancer.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 103 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 21 seconds
43120 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 164784
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 57364
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 554 seconds
this is the end
overall process took 740 seconds
peptide identification is finished

real	12m36,749s
user	214m17,216s
sys	1m14,426s


langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_ion_enhancer.tsv/specfit.tsv 
[1] "total: 41635"
[1] "decoy: 6947"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]     2     0    33  0.00000000
 [2,]     2     0    32  0.00000000
 [3,]     3     0    31  0.00000000
 [4,]     6     0    30  0.00000000
 [5,]    11     0    29  0.00000000
 [6,]    24     0    28  0.00000000
 [7,]    42     0    27  0.00000000
 [8,]    72     0    26  0.00000000
 [9,]   110     0    25  0.00000000
[10,]   193     0    24  0.00000000
[11,]   306     0    23  0.00000000
[12,]   459     0    22  0.00000000
[13,]   731     0    21  0.00000000
[14,]  1098     0    20  0.00000000
[15,]  1554     0    19  0.00000000
[16,]  2234     0    18  0.00000000
[17,]  3179     0    17  0.00000000
[18,]  4541     0    16  0.00000000
[19,]  6247     1    15  0.01600768
[20,]  8482     7    14  0.08252771
[21,] 11264    25    13  0.22194602
[22,] 14672    96    12  0.65430752
[23,] 18451   290    11  1.57173053
[24,] 23257   864    10  3.71501053
[25,] 29291  2158     9  7.36745075
[26,] 35510  4389     8 12.35989862
[27,] 39867  6223     7 15.60940126
[28,] 41635  6947     6 16.68548097
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    33        NaN
 [2,]     0     0    32        NaN
 [3,]     0     0    31        NaN
 [4,]     0     0    30        NaN
 [5,]     1     0    29  0.0000000
 [6,]     1     0    28  0.0000000
 [7,]     2     0    27  0.0000000
 [8,]     3     0    26  0.0000000
 [9,]     4     0    25  0.0000000
[10,]     9     0    24  0.0000000
[11,]    19     0    23  0.0000000
[12,]    28     0    22  0.0000000
[13,]    48     0    21  0.0000000
[14,]    71     0    20  0.0000000
[15,]    94     0    19  0.0000000
[16,]   150     0    18  0.0000000
[17,]   218     0    17  0.0000000
[18,]   320     0    16  0.0000000
[19,]   459     1    15  0.2178649
[20,]   683     6    14  0.8784773
[21,]   960    15    13  1.5625000
[22,]  1447    76    12  5.2522460
[23,]  2187   243    11 11.1111111
[24,]  3703   723    10 19.5247097
[25,]  6488  1788     9 27.5585697
[26,] 10723  3607     8 33.6379744
[27,] 13772  4993     7 36.2547197
[28,] 14790  5451     6 36.8559838
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     2     0    33 0.00000000
 [2,]     2     0    32 0.00000000
 [3,]     3     0    31 0.00000000
 [4,]     4     0    30 0.00000000
 [5,]     4     0    29 0.00000000
 [6,]     9     0    28 0.00000000
 [7,]    16     0    27 0.00000000
 [8,]    30     0    26 0.00000000
 [9,]    50     0    25 0.00000000
[10,]    93     0    24 0.00000000
[11,]   152     0    23 0.00000000
[12,]   230     0    22 0.00000000
[13,]   357     0    21 0.00000000
[14,]   552     0    20 0.00000000
[15,]   780     0    19 0.00000000
[16,]  1144     0    18 0.00000000
[17,]  1594     0    17 0.00000000
[18,]  2299     0    16 0.00000000
[19,]  3173     0    15 0.00000000
[20,]  4307     0    14 0.00000000
[21,]  5706     0    13 0.00000000
[22,]  7284     2    12 0.02745744
[23,]  8870     3    11 0.03382187
[24,] 10560    11    10 0.10416667
[25,] 12127    45     9 0.37107281
[26,] 12127    45     8 0.37107281
[27,] 12127    45     7 0.37107281
[28,] 12127    45     6 0.37107281
[1] " delta > 0"
      Total Decoy peaks        FDR
 [1,]     0     0    33        NaN
 [2,]     0     0    32        NaN
 [3,]     0     0    31        NaN
 [4,]     2     0    30 0.00000000
 [5,]     6     0    29 0.00000000
 [6,]    14     0    28 0.00000000
 [7,]    24     0    27 0.00000000
 [8,]    39     0    26 0.00000000
 [9,]    56     0    25 0.00000000
[10,]    91     0    24 0.00000000
[11,]   135     0    23 0.00000000
[12,]   201     0    22 0.00000000
[13,]   326     0    21 0.00000000
[14,]   475     0    20 0.00000000
[15,]   680     0    19 0.00000000
[16,]   940     0    18 0.00000000
[17,]  1367     0    17 0.00000000
[18,]  1922     0    16 0.00000000
[19,]  2615     0    15 0.00000000
[20,]  3492     1    14 0.02863688
[21,]  4598    10    13 0.21748586
[22,]  5941    18    12 0.30297930
[23,]  7394    44    11 0.59507709
[24,]  8994   130    10 1.44540805
[25,] 10676   325     9 3.04421132
[26,] 12660   737     8 5.82148499
[27,] 13968  1185     7 8.48367698
[28,] 14718  1451     6 9.85867645


new R script :
74780a0e93d6ea86c43ef9ede712d0b28996553f

langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_ion_enhancer.tsv/specfit.tsv 
[1] "total: 41635"
[1] "decoy: 6947"
[1] " tout"
   Total Decoy peaks         FDR
1      2     0    33  0.00000000
2      2     0    32  0.00000000
3      3     0    31  0.00000000
4      6     0    30  0.00000000
5     11     0    29  0.00000000
6     24     0    28  0.00000000
7     42     0    27  0.00000000
8     72     0    26  0.00000000
9    110     0    25  0.00000000
10   193     0    24  0.00000000
11   306     0    23  0.00000000
12   459     0    22  0.00000000
13   731     0    21  0.00000000
14  1098     0    20  0.00000000
15  1554     0    19  0.00000000
16  2234     0    18  0.00000000
17  3179     0    17  0.00000000
18  4541     0    16  0.00000000
19  6247     1    15  0.01600768
20  8482     7    14  0.08252771
21 11264    25    13  0.22194602
22 14672    96    12  0.65430752
23 18451   290    11  1.57173053
24 23257   864    10  3.71501053
25 29291  2158     9  7.36745075
26 35510  4389     8 12.35989862
27 39867  6223     7 15.60940126
28 41635  6947     6 16.68548097
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks       FDR
20   683     6    14 0.8784773
[1] " delta == 0"
   Total Decoy peaks        FDR
1      2     0    33 0.00000000
2      2     0    32 0.00000000
3      3     0    31 0.00000000
4      4     0    30 0.00000000
5      4     0    29 0.00000000
6      9     0    28 0.00000000
7     16     0    27 0.00000000
8     30     0    26 0.00000000
9     50     0    25 0.00000000
10    93     0    24 0.00000000
11   152     0    23 0.00000000
12   230     0    22 0.00000000
13   357     0    21 0.00000000
14   552     0    20 0.00000000
15   780     0    19 0.00000000
16  1144     0    18 0.00000000
17  1594     0    17 0.00000000
18  2299     0    16 0.00000000
19  3173     0    15 0.00000000
20  4307     0    14 0.00000000
21  5706     0    13 0.00000000
22  7284     2    12 0.02745744
23  8870     3    11 0.03382187
24 10560    11    10 0.10416667
25 12127    45     9 0.37107281
26 12127    45     8 0.37107281
27 12127    45     7 0.37107281
28 12127    45     6 0.37107281
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks       FDR
28 12127    45     6 0.3710728
[1] " delta > 0"
   Total Decoy peaks        FDR
1      0     0    33        NaN
2      0     0    32        NaN
3      0     0    31        NaN
4      2     0    30 0.00000000
5      6     0    29 0.00000000
6     14     0    28 0.00000000
7     24     0    27 0.00000000
8     39     0    26 0.00000000
9     56     0    25 0.00000000
10    91     0    24 0.00000000
11   135     0    23 0.00000000
12   201     0    22 0.00000000
13   326     0    21 0.00000000
14   475     0    20 0.00000000
15   680     0    19 0.00000000
16   940     0    18 0.00000000
17  1367     0    17 0.00000000
18  1922     0    16 0.00000000
19  2615     0    15 0.00000000
20  3492     1    14 0.02863688
21  4598    10    13 0.21748586
22  5941    18    12 0.30297930
23  7394    44    11 0.59507709
24  8994   130    10 1.44540805
25 10676   325     9 3.04421132
26 12660   737     8 5.82148499
27 13968  1185     7 8.48367698
28 14718  1451     6 9.85867645
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
23  7394    44    11 0.5950771
[1] "Total 1% = 20204"
[1] "Decoy 1% = 95"
[1] "Final FDR = 0.470203920015838"
[1] "assignment % = 48.5264801248949"
