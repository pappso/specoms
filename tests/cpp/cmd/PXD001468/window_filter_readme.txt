/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf



26/08/2020
execution on 7962f9494eb2740a95a7de947881a2edc578bc49



langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/window_filter_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_window_filter.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 104 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 10 seconds
43121 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 163004
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 56470
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 532 seconds
this is the end
overall process took 706 seconds
peptide identification is finished

real	12m10,171s
user	205m54,985s
sys	0m50,876s


langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_window_filter.tsv/specfit.tsv 
[1] "total: 39151"
[1] "decoy: 6795"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]     1     0    30  0.00000000
 [2,]     1     0    29  0.00000000
 [3,]     4     0    28  0.00000000
 [4,]    11     0    27  0.00000000
 [5,]    24     0    26  0.00000000
 [6,]    37     0    25  0.00000000
 [7,]    56     0    24  0.00000000
 [8,]   103     0    23  0.00000000
 [9,]   183     0    22  0.00000000
[10,]   297     0    21  0.00000000
[11,]   488     0    20  0.00000000
[12,]   754     0    19  0.00000000
[13,]  1188     0    18  0.00000000
[14,]  1870     0    17  0.00000000
[15,]  2863     1    16  0.03492840
[16,]  4208     3    15  0.07129278
[17,]  6007     4    14  0.06658898
[18,]  8341    11    13  0.13187867
[19,] 11387    33    12  0.28980416
[20,] 14909   114    11  0.76463881
[21,] 19319   449    10  2.32413686
[22,] 25416  1652     9  6.49984262
[23,] 32729  4103     8 12.53628281
[24,] 39151  6795     7 17.35587852
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    30        NaN
 [2,]     0     0    29        NaN
 [3,]     0     0    28        NaN
 [4,]     0     0    27        NaN
 [5,]     1     0    26  0.0000000
 [6,]     3     0    25  0.0000000
 [7,]     5     0    24  0.0000000
 [8,]     7     0    23  0.0000000
 [9,]    13     0    22  0.0000000
[10,]    23     0    21  0.0000000
[11,]    32     0    20  0.0000000
[12,]    54     0    19  0.0000000
[13,]    95     0    18  0.0000000
[14,]   148     0    17  0.0000000
[15,]   235     1    16  0.4255319
[16,]   352     3    15  0.8522727
[17,]   540     4    14  0.7407407
[18,]   795    10    13  1.2578616
[19,]  1176    24    12  2.0408163
[20,]  1699    84    11  4.9440848
[21,]  2788   361    10 12.9483501
[22,]  5359  1350     9 25.1912670
[23,] 10039  3260     8 32.4733539
[24,] 13971  5020     7 35.9315725
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     1     0    30 0.00000000
 [2,]     1     0    29 0.00000000
 [3,]     2     0    28 0.00000000
 [4,]     3     0    27 0.00000000
 [5,]     7     0    26 0.00000000
 [6,]    10     0    25 0.00000000
 [7,]    20     0    24 0.00000000
 [8,]    35     0    23 0.00000000
 [9,]    63     0    22 0.00000000
[10,]   108     0    21 0.00000000
[11,]   182     0    20 0.00000000
[12,]   268     0    19 0.00000000
[13,]   475     0    18 0.00000000
[14,]   755     0    17 0.00000000
[15,]  1204     0    16 0.00000000
[16,]  1796     0    15 0.00000000
[17,]  2641     0    14 0.00000000
[18,]  3766     0    13 0.00000000
[19,]  5214     0    12 0.00000000
[20,]  6898     2    11 0.02899391
[21,]  8688     7    10 0.08057090
[22,] 10485    22     9 0.20982356
[23,] 10485    22     8 0.20982356
[24,] 10485    22     7 0.20982356
[1] " delta > 0"
      Total Decoy peaks         FDR
 [1,]     0     0    30         NaN
 [2,]     0     0    29         NaN
 [3,]     2     0    28  0.00000000
 [4,]     8     0    27  0.00000000
 [5,]    16     0    26  0.00000000
 [6,]    24     0    25  0.00000000
 [7,]    31     0    24  0.00000000
 [8,]    61     0    23  0.00000000
 [9,]   107     0    22  0.00000000
[10,]   166     0    21  0.00000000
[11,]   274     0    20  0.00000000
[12,]   432     0    19  0.00000000
[13,]   618     0    18  0.00000000
[14,]   967     0    17  0.00000000
[15,]  1424     0    16  0.00000000
[16,]  2060     0    15  0.00000000
[17,]  2826     0    14  0.00000000
[18,]  3780     1    13  0.02645503
[19,]  4997     9    12  0.18010806
[20,]  6312    28    11  0.44359949
[21,]  7843    81    10  1.03276807
[22,]  9572   280     9  2.92519850
[23,] 12205   821     8  6.72675133
[24,] 14695  1753     7 11.92922763


Params not taken into account. Fixing
retrying => 337fc33e6f1735fc31cb6dcc84fa74ea92b30c44
=> segmentation fault, problem using window filter

this was a bug in pappsomspp filter, fixed in pappsomspp library e075554e22df296a593ca9291e0c4ca7a5bb7f6f

retrying on 0b7863292ecde74db0261b562f01b9bea759e72f :

langella@themis:~/developpement/git/deepprot/build$ ./src/deepprot -p ../test/cpp/cmd/PXD001468/window_filter_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_window_filter.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 103 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 9 seconds
43117 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 166248
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 60881
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 365 seconds
this is the end
overall process took 532 seconds
peptide identification is finished
langella@themis:~/developpement/git/deepprot/build$ ../test/cpp/R/count.R /tmp/PXD001468_window_filter.tsv/specfit.tsv 
[1] "total: 32006"
[1] "decoy: 4004"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]     1     0    30  0.00000000
 [2,]     1     0    29  0.00000000
 [3,]     3     0    28  0.00000000
 [4,]     5     0    27  0.00000000
 [5,]    15     0    26  0.00000000
 [6,]    34     0    25  0.00000000
 [7,]    71     0    24  0.00000000
 [8,]   110     0    23  0.00000000
 [9,]   173     0    22  0.00000000
[10,]   293     0    21  0.00000000
[11,]   465     0    20  0.00000000
[12,]   715     0    19  0.00000000
[13,]  1121     0    18  0.00000000
[14,]  1641     0    17  0.00000000
[15,]  2413     0    16  0.00000000
[16,]  3429     0    15  0.00000000
[17,]  4857     1    14  0.02058884
[18,]  6739     6    13  0.08903398
[19,]  9115    20    12  0.21941854
[20,] 12033    63    11  0.52356021
[21,] 15300   175    10  1.14379085
[22,] 19427   528     9  2.71786689
[23,] 24482  1521     8  6.21272772
[24,] 32006  4004     7 12.51015435
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    30        NaN
 [2,]     0     0    29        NaN
 [3,]     0     0    28        NaN
 [4,]     0     0    27        NaN
 [5,]     1     0    26  0.0000000
 [6,]     2     0    25  0.0000000
 [7,]     4     0    24  0.0000000
 [8,]     6     0    23  0.0000000
 [9,]    11     0    22  0.0000000
[10,]    21     0    21  0.0000000
[11,]    27     0    20  0.0000000
[12,]    47     0    19  0.0000000
[13,]    86     0    18  0.0000000
[14,]   115     0    17  0.0000000
[15,]   181     0    16  0.0000000
[16,]   267     0    15  0.0000000
[17,]   401     1    14  0.2493766
[18,]   564     5    13  0.8865248
[19,]   805    13    12  1.6149068
[20,]  1176    48    11  4.0816327
[21,]  1731   136    10  7.8567302
[22,]  2756   403     9 14.6226415
[23,]  5181  1154     8 22.2736923
[24,]  9230  2783     7 30.1516793
[1] " delta == 0"
      Total Decoy peaks       FDR
 [1,]     1     0    30 0.0000000
 [2,]     1     0    29 0.0000000
 [3,]     2     0    28 0.0000000
 [4,]     2     0    27 0.0000000
 [5,]     6     0    26 0.0000000
 [6,]    13     0    25 0.0000000
 [7,]    21     0    24 0.0000000
 [8,]    33     0    23 0.0000000
 [9,]    53     0    22 0.0000000
[10,]   104     0    21 0.0000000
[11,]   174     0    20 0.0000000
[12,]   274     0    19 0.0000000
[13,]   457     0    18 0.0000000
[14,]   687     0    17 0.0000000
[15,]  1032     0    16 0.0000000
[16,]  1450     0    15 0.0000000
[17,]  2109     0    14 0.0000000
[18,]  3030     0    13 0.0000000
[19,]  4176     0    12 0.0000000
[20,]  5545     0    11 0.0000000
[21,]  7081     2    10 0.0282446
[22,]  8772     9     9 0.1025992
[23,]  8772     9     8 0.1025992
[24,]  8772     9     7 0.1025992
[1] " delta > 0"
      Total Decoy peaks       FDR
 [1,]     0     0    30       NaN
 [2,]     0     0    29       NaN
 [3,]     1     0    28 0.0000000
 [4,]     3     0    27 0.0000000
 [5,]     8     0    26 0.0000000
 [6,]    19     0    25 0.0000000
 [7,]    46     0    24 0.0000000
 [8,]    71     0    23 0.0000000
 [9,]   109     0    22 0.0000000
[10,]   168     0    21 0.0000000
[11,]   264     0    20 0.0000000
[12,]   394     0    19 0.0000000
[13,]   578     0    18 0.0000000
[14,]   839     0    17 0.0000000
[15,]  1200     0    16 0.0000000
[16,]  1712     0    15 0.0000000
[17,]  2347     0    14 0.0000000
[18,]  3145     1    13 0.0317965
[19,]  4134     7    12 0.1693275
[20,]  5312    15    11 0.2823795
[21,]  6488    37    10 0.5702836
[22,]  7899   116     9 1.4685403
[23,] 10529   358     8 3.4001330
[24,] 14004  1212     7 8.6546701


