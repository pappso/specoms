[1] "total: 38666"
[1] "decoy: 7150"
[1] " tout"
   Total Decoy peaks         FDR
1      1     0    31  0.00000000
2      1     0    30  0.00000000
3      2     0    29  0.00000000
4      6     0    28  0.00000000
5     16     0    27  0.00000000
6     33     0    26  0.00000000
7     53     0    25  0.00000000
8     88     0    24  0.00000000
9    136     0    23  0.00000000
10   206     0    22  0.00000000
11   317     0    21  0.00000000
12   495     0    20  0.00000000
13   729     0    19  0.00000000
14  1115     0    18  0.00000000
15  1592     0    17  0.00000000
16  2340     0    16  0.00000000
17  3339     0    15  0.00000000
18  4761     1    14  0.02100399
19  6603     2    13  0.03028926
20  8956    14    12  0.15631979
21 11886    62    11  0.52162208
22 15237   208    10  1.36509812
23 19777   692     9  3.49901401
24 25299  2048     8  8.09518163
25 32317  4490     7 13.89361636
26 38666  7150     6 18.49169813
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks      FDR
19   602     1    13 0.166113
[1] " delta == 0"
   Total Decoy peaks        FDR
1      1     0    31 0.00000000
2      1     0    30 0.00000000
3      2     0    29 0.00000000
4      5     0    28 0.00000000
5     11     0    27 0.00000000
6     23     0    26 0.00000000
7     39     0    25 0.00000000
8     58     0    24 0.00000000
9     84     0    23 0.00000000
10   130     0    22 0.00000000
11   206     0    21 0.00000000
12   309     0    20 0.00000000
13   442     0    19 0.00000000
14   692     0    18 0.00000000
15   975     0    17 0.00000000
16  1421     0    16 0.00000000
17  2003     0    15 0.00000000
18  2787     0    14 0.00000000
19  3837     0    13 0.00000000
20  5131     0    12 0.00000000
21  6623     0    11 0.00000000
22  8280     1    10 0.01207729
23 10107     8     9 0.07915306
24 10107     8     8 0.07915306
25 10107     8     7 0.07915306
26 10107     8     6 0.07915306
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks        FDR
26 10107     8     6 0.07915306
[1] " delta > 0"
   Total Decoy peaks         FDR
1      0     0    31         NaN
2      0     0    30         NaN
3      0     0    29         NaN
4      1     0    28  0.00000000
5      4     0    27  0.00000000
6      9     0    26  0.00000000
7     12     0    25  0.00000000
8     25     0    24  0.00000000
9     42     0    23  0.00000000
10    61     0    22  0.00000000
11    83     0    21  0.00000000
12   147     0    20  0.00000000
13   226     0    19  0.00000000
14   327     0    18  0.00000000
15   483     0    17  0.00000000
16   720     0    16  0.00000000
17  1044     0    15  0.00000000
18  1553     0    14  0.00000000
19  2164     1    13  0.04621072
20  2981     5    12  0.16772895
21  4005    13    11  0.32459426
22  5072    41    10  0.80835962
23  6473   140     9  2.16283022
24  8919   401     8  4.49601973
25 11324   897     7  7.92122925
26 13616  1710     6 12.55875441
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
22  5072    41    10 0.8083596
[1] "Total 1% = 15781"
[1] "Decoy 1% = 50"
[1] "Final FDR = 0.316836702363602"
[1] "assignment % = 40.81363471784"
