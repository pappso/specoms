/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf


no filter but maximum number of peaks set to 100
executed on d847698788d8bfd5c7f6d5fefb32d396d5c26c9b

memory used : 10Go

langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/no_filter_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_no_filter.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 103 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 10 seconds
43121 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 169786
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 59764
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 786 seconds
this is the end
overall process took 956 seconds
peptide identification is finished

real	16m11,549s
user	293m57,383s
sys	2m52,711s

langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_no_filter.tsv/specfit.tsv 
[1] "total: 41791"
[1] "decoy: 7431"
[1] " tout"
   Total Decoy peaks         FDR
1      1     0    34  0.00000000
2      1     0    33  0.00000000
3      1     0    32  0.00000000
4      1     0    31  0.00000000
5      6     0    30  0.00000000
6     19     0    29  0.00000000
7     33     0    28  0.00000000
8     50     0    27  0.00000000
9     93     0    26  0.00000000
10   138     0    25  0.00000000
11   224     0    24  0.00000000
12   343     0    23  0.00000000
13   523     0    22  0.00000000
14   779     0    21  0.00000000
15  1161     0    20  0.00000000
16  1680     0    19  0.00000000
17  2447     0    18  0.00000000
18  3463     1    17  0.02887670
19  4864     2    16  0.04111842
20  6752     4    15  0.05924171
21  9086     9    14  0.09905349
22 12028    44    13  0.36581310
23 15776   178    12  1.12829615
24 20200   595    11  2.94554455
25 26415  1898    10  7.18531138
26 34301  4385     9 12.78388385
27 38860  6260     8 16.10910962
28 40629  6983     7 17.18723080
29 41791  7431     6 17.78134048
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks       FDR
21   902     7    14 0.7760532
[1] " delta == 0"
   Total Decoy peaks        FDR
1      1     0    34 0.00000000
2      1     0    33 0.00000000
3      1     0    32 0.00000000
4      1     0    31 0.00000000
5      1     0    30 0.00000000
6      2     0    29 0.00000000
7      7     0    28 0.00000000
8     13     0    27 0.00000000
9     31     0    26 0.00000000
10    47     0    25 0.00000000
11    77     0    24 0.00000000
12   130     0    23 0.00000000
13   206     0    22 0.00000000
14   290     0    21 0.00000000
15   453     0    20 0.00000000
16   671     0    19 0.00000000
17   997     0    18 0.00000000
18  1442     0    17 0.00000000
19  2087     0    16 0.00000000
20  2979     0    15 0.00000000
21  4060     0    14 0.00000000
22  5420     1    13 0.01845018
23  7048     1    12 0.01418842
24  8695     2    11 0.02300173
25 10457    16    10 0.15300755
26 12126    71     9 0.58551872
27 12126    71     8 0.58551872
28 12126    71     7 0.58551872
29 12126    71     6 0.58551872
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks       FDR
29 12126    71     6 0.5855187
[1] " delta > 0"
   Total Decoy peaks        FDR
1      0     0    34        NaN
2      0     0    33        NaN
3      0     0    32        NaN
4      0     0    31        NaN
5      5     0    30 0.00000000
6     17     0    29 0.00000000
7     25     0    28 0.00000000
8     35     0    27 0.00000000
9     57     0    26 0.00000000
10    83     0    25 0.00000000
11   135     0    24 0.00000000
12   192     0    23 0.00000000
13   281     0    22 0.00000000
14   429     0    21 0.00000000
15   616     0    20 0.00000000
16   871     0    19 0.00000000
17  1236     0    18 0.00000000
18  1729     0    17 0.00000000
19  2352     0    16 0.00000000
20  3150     0    15 0.00000000
21  4124     2    14 0.04849661
22  5302    10    13 0.18860807
23  6737    35    12 0.51951907
24  8222    78    11 0.94867429
25  9860   237    10 2.40365112
26 11412   593     9 5.19628461
27 12564   908     8 7.22699777
28 13187  1094     7 8.29604914
29 13754  1288     6 9.36454849
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
24  8222    78    11 0.9486743
[1] "Total 1% = 21250"
[1] "Decoy 1% = 156"
[1] "Final FDR = 0.734117647058824"
[1] "assignment % = 50.8482687660022"
