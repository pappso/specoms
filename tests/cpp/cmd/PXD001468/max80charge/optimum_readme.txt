[1] "total: 41093"
[1] "decoy: 7110"
[1] " tout"
   Total Decoy peaks         FDR
1      1     0    37  0.00000000
2      1     0    36  0.00000000
3      1     0    35  0.00000000
4      1     0    34  0.00000000
5      3     0    33  0.00000000
6      9     0    32  0.00000000
7     11     0    31  0.00000000
8     17     0    30  0.00000000
9     27     0    29  0.00000000
10    49     0    28  0.00000000
11    88     0    27  0.00000000
12   138     0    26  0.00000000
13   192     0    25  0.00000000
14   284     0    24  0.00000000
15   420     0    23  0.00000000
16   622     0    22  0.00000000
17   905     0    21  0.00000000
18  1293     0    20  0.00000000
19  1793     0    19  0.00000000
20  2481     0    18  0.00000000
21  3433     0    17  0.00000000
22  4772     0    16  0.00000000
23  6448     1    15  0.01550868
24  8670    10    14  0.11534025
25 11268    34    13  0.30173944
26 14610    99    12  0.67761807
27 18541   346    11  1.86613451
28 23517  1071    10  4.55415232
29 30345  2853     9  9.40187840
30 36106  5087     8 14.08907107
31 39419  6443     7 16.34490982
32 41093  7110     6 17.30221692
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks        FDR
1      0     0    37        NaN
2      0     0    36        NaN
3      0     0    35        NaN
4      0     0    34        NaN
5      0     0    33        NaN
6      0     0    32        NaN
7      0     0    31        NaN
8      0     0    30        NaN
9      1     0    29  0.0000000
10     1     0    28  0.0000000
11     3     0    27  0.0000000
12     6     0    26  0.0000000
13    10     0    25  0.0000000
14    16     0    24  0.0000000
15    28     0    23  0.0000000
16    41     0    22  0.0000000
17    59     0    21  0.0000000
18    87     0    20  0.0000000
19   119     0    19  0.0000000
20   178     0    18  0.0000000
21   265     0    17  0.0000000
22   372     0    16  0.0000000
23   518     1    15  0.1930502
24   744    10    14  1.3440860
25  1040    30    13  2.8846154
26  1555    85    12  5.4662379
27  2477   303    11 12.2325394
28  4356   935    10 21.4646465
29  7931  2474     9 31.1940487
30 12150  4347     8 35.7777778
31 14472  5403     7 37.3341625
32 15356  5802     6 37.7832769
   Total Decoy peaks       FDR
23   518     1    15 0.1930502
[1] " delta == 0"
   Total Decoy peaks         FDR
1      1     0    37 0.000000000
2      1     0    36 0.000000000
3      1     0    35 0.000000000
4      1     0    34 0.000000000
5      3     0    33 0.000000000
6      7     0    32 0.000000000
7      9     0    31 0.000000000
8     14     0    30 0.000000000
9     21     0    29 0.000000000
10    39     0    28 0.000000000
11    66     0    27 0.000000000
12    97     0    26 0.000000000
13   130     0    25 0.000000000
14   191     0    24 0.000000000
15   286     0    23 0.000000000
16   418     0    22 0.000000000
17   595     0    21 0.000000000
18   828     0    20 0.000000000
19  1164     0    19 0.000000000
20  1607     0    18 0.000000000
21  2190     0    17 0.000000000
22  2994     0    16 0.000000000
23  3993     0    15 0.000000000
24  5260     0    14 0.000000000
25  6671     0    13 0.000000000
26  8312     0    12 0.000000000
27 10049     1    11 0.009951239
28 11746     9    10 0.076621829
29 13438    39     9 0.290221759
30 13438    39     8 0.290221759
31 13438    39     7 0.290221759
32 13438    39     6 0.290221759
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks       FDR
32 13438    39     6 0.2902218
[1] " delta > 0"
   Total Decoy peaks        FDR
1      0     0    37        NaN
2      0     0    36        NaN
3      0     0    35        NaN
4      0     0    34        NaN
5      0     0    33        NaN
6      2     0    32  0.0000000
7      2     0    31  0.0000000
8      3     0    30  0.0000000
9      5     0    29  0.0000000
10     9     0    28  0.0000000
11    19     0    27  0.0000000
12    35     0    26  0.0000000
13    52     0    25  0.0000000
14    77     0    24  0.0000000
15   106     0    23  0.0000000
16   163     0    22  0.0000000
17   251     0    21  0.0000000
18   378     0    20  0.0000000
19   510     0    19  0.0000000
20   696     0    18  0.0000000
21   978     0    17  0.0000000
22  1406     0    16  0.0000000
23  1937     0    15  0.0000000
24  2666     0    14  0.0000000
25  3557     4    13  0.1124543
26  4743    14    12  0.2951718
27  6015    42    11  0.6982544
28  7415   127    10  1.7127444
29  8976   340     9  3.7878788
30 10518   701     8  6.6647652
31 11509  1001     7  8.6975411
32 12299  1269     6 10.3179120
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
27  6015    42    11 0.6982544
[1] "Total 1% = 19971"
[1] "Decoy 1% = 82"
[1] "Final FDR = 0.410595363276751"
[1] "assignment % = 48.599518166111"
[1] "effectif = 41093"
