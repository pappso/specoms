/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf


maximum number of peaks set to 80
executed on d847698788d8bfd5c7f6d5fefb32d396d5c26c9b

langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/max80_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_max80.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf

using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 106 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 10 seconds
43121 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 167389
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 58600
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 653 seconds
this is the end
overall process took 825 seconds
peptide identification is finished

real	14m1,427s
user	241m57,680s
sys	1m58,631s
langella@themis:~/developpement/git/deepprot/cbuild$ 
langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_max80.tsv/specfit.tsv 
[1] "total: 41770"
[1] "decoy: 7610"
[1] " tout"
   Total Decoy peaks         FDR
1      1     0    31  0.00000000
2      1     0    30  0.00000000
3      4     0    29  0.00000000
4     18     0    28  0.00000000
5     32     0    27  0.00000000
6     52     0    26  0.00000000
7     85     0    25  0.00000000
8    145     0    24  0.00000000
9    240     0    23  0.00000000
10   358     0    22  0.00000000
11   559     0    21  0.00000000
12   855     0    20  0.00000000
13  1275     0    19  0.00000000
14  1921     0    18  0.00000000
15  2812     1    17  0.03556188
16  4053     2    16  0.04934616
17  5723     4    15  0.06989341
18  7853     7    14  0.08913791
19 10578    23    13  0.21743241
20 14046    83    12  0.59091556
21 18112   334    11  1.84408127
22 23585  1184    10  5.02013992
23 31306  3331     9 10.64013288
24 37775  5934     8 15.70880212
25 40506  7113     7 17.56036143
26 41770  7610     6 18.21881733
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks      FDR
18   755     6    14 0.794702
[1] " delta == 0"
   Total Decoy peaks        FDR
1      1     0    31 0.00000000
2      1     0    30 0.00000000
3      1     0    29 0.00000000
4      3     0    28 0.00000000
5      7     0    27 0.00000000
6     14     0    26 0.00000000
7     29     0    25 0.00000000
8     51     0    24 0.00000000
9     86     0    23 0.00000000
10   141     0    22 0.00000000
11   211     0    21 0.00000000
12   322     0    20 0.00000000
13   501     0    19 0.00000000
14   784     0    18 0.00000000
15  1159     0    17 0.00000000
16  1708     0    16 0.00000000
17  2469     0    15 0.00000000
18  3476     0    14 0.00000000
19  4766     0    13 0.00000000
20  6325     1    12 0.01581028
21  7977     2    11 0.02507208
22  9743    11    10 0.11290157
23 11495    43     9 0.37407569
24 11495    43     8 0.37407569
25 11495    43     7 0.37407569
26 11495    43     6 0.37407569
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks       FDR
26 11495    43     6 0.3740757
[1] " delta > 0"
   Total Decoy peaks         FDR
1      0     0    31         NaN
2      0     0    30         NaN
3      3     0    29  0.00000000
4     15     0    28  0.00000000
5     24     0    27  0.00000000
6     34     0    26  0.00000000
7     51     0    25  0.00000000
8     86     0    24  0.00000000
9    138     0    23  0.00000000
10   194     0    22  0.00000000
11   309     0    21  0.00000000
12   468     0    20  0.00000000
13   676     0    19  0.00000000
14   974     0    18  0.00000000
15  1418     0    17  0.00000000
16  1998     0    16  0.00000000
17  2749     0    15  0.00000000
18  3622     1    14  0.02760906
19  4723     6    13  0.12703790
20  6114    18    12  0.29440628
21  7612    52    11  0.68313190
22  9234   173    10  1.87351094
23 10947   488     9  4.45784233
24 12658   948     8  7.48933481
25 13435  1196     7  8.90212132
26 14034  1406     6 10.01852644
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
21  7612    52    11 0.6831319
[1] "Total 1% = 19862"
[1] "Decoy 1% = 101"
[1] "Final FDR = 0.508508710099688"
[1] "assignment % = 47.5508738328944"
