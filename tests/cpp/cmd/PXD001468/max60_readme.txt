/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf


maximum number of peaks set to 60
executed on d847698788d8bfd5c7f6d5fefb32d396d5c26c9b

langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/max60_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468_max60.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 111 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 10 seconds
43121 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 163004
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 56470
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 551 seconds
this is the end
overall process took 735 seconds
peptide identification is finished

real	12m30,840s
user	200m47,230s
sys	1m28,439s

langella@themis:~/developpement/git/deepprot/cbuild$ ../test/cpp/R/count.R /tmp/PXD001468_max60.tsv/specfit.tsv
[1] "total: 41676"
[1] "decoy: 8097"
[1] " tout"
   Total Decoy peaks         FDR
1      1     0    30  0.00000000
2      1     0    29  0.00000000
3      4     0    28  0.00000000
4     11     0    27  0.00000000
5     24     0    26  0.00000000
6     37     0    25  0.00000000
7     56     0    24  0.00000000
8    103     0    23  0.00000000
9    183     0    22  0.00000000
10   297     0    21  0.00000000
11   488     0    20  0.00000000
12   753     0    19  0.00000000
13  1190     0    18  0.00000000
14  1875     0    17  0.00000000
15  2869     1    16  0.03485535
16  4228     3    15  0.07095553
17  6038     4    14  0.06624710
18  8392    11    13  0.13107722
19 11502    41    12  0.35645975
20 15164   147    11  0.96940121
21 19849   593    10  2.98755605
22 26596  2080     9  7.82072492
23 34299  4928     8 14.36776582
24 39720  7274     7 18.31319235
25 41676  8097     6 19.42844803
[1] " delta < 0"
[1] "delta < 0, with FDR < 1%"
   Total Decoy peaks       FDR
17   544     4    14 0.7352941
[1] " delta == 0"
   Total Decoy peaks        FDR
1      1     0    30 0.00000000
2      1     0    29 0.00000000
3      2     0    28 0.00000000
4      3     0    27 0.00000000
5      7     0    26 0.00000000
6     10     0    25 0.00000000
7     20     0    24 0.00000000
8     35     0    23 0.00000000
9     63     0    22 0.00000000
10   108     0    21 0.00000000
11   182     0    20 0.00000000
12   268     0    19 0.00000000
13   475     0    18 0.00000000
14   755     0    17 0.00000000
15  1204     0    16 0.00000000
16  1796     0    15 0.00000000
17  2641     0    14 0.00000000
18  3766     0    13 0.00000000
19  5214     0    12 0.00000000
20  6898     2    11 0.02899391
21  8688     7    10 0.08057090
22 10485    22     9 0.20982356
23 10485    22     8 0.20982356
24 10485    22     7 0.20982356
25 10485    22     6 0.20982356
[1] "delta == 0, with FDR < 1%"
   Total Decoy peaks       FDR
25 10485    22     6 0.2098236
[1] " delta > 0"
   Total Decoy peaks         FDR
1      0     0    30         NaN
2      0     0    29         NaN
3      2     0    28  0.00000000
4      8     0    27  0.00000000
5     16     0    26  0.00000000
6     24     0    25  0.00000000
7     31     0    24  0.00000000
8     61     0    23  0.00000000
9    107     0    22  0.00000000
10   166     0    21  0.00000000
11   274     0    20  0.00000000
12   431     0    19  0.00000000
13   620     0    18  0.00000000
14   973     0    17  0.00000000
15  1431     0    16  0.00000000
16  2079     0    15  0.00000000
17  2853     0    14  0.00000000
18  3819     1    13  0.02618487
19  5081    10    12  0.19681165
20  6455    31    11  0.48024787
21  8058    94    10  1.16654257
22  9858   304     9  3.08378982
23 12189   826     8  6.77660185
24 13681  1345     7  9.83115269
25 14477  1647     6 11.37666644
[1] "delta > 0, with FDR < 1%"
   Total Decoy peaks       FDR
20  6455    31    11 0.4802479
[1] "Total 1% = 17484"
[1] "Decoy 1% = 57"
[1] "Final FDR = 0.326012354152368"
[1] "assignment % = 41.9522027065937"
