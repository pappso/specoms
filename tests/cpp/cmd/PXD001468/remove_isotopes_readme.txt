/media/langella/pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
/media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf



19/08/2020
execution on 1e86061802cb86a3b7fe2bee757c3a5db110a059



langella@themis:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/cpp/cmd/PXD001468/PXD001468_params.csv -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta -o /tmp/PXD001468.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/crapPeptidesRef.fasta
174100 proteins, 9351494 digested protopeptides, 3120322 protopeptides registered, 3120322 spectrum modelized from peptides
Building theoretical spectrum took 102 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf
Building experimental spectrum took 18 seconds
43120 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 174233
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 77339
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 57216
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 553 seconds
this is the end
overall process took 728 seconds
peptide identification is finished

real	12m11,109s
user	213m28,133s
sys	0m55,903s

langella@themis:~/developpement/git/deepprot/cbuild$ Rscript ../test/cpp/R/count.R /tmp/PXD001468.tsv/specfit.tsv 
[1] "total: 39355"
[1] "decoy: 6199"
[1] " tout"
      Total Decoy peaks         FDR
 [1,]     1     0    31  0.00000000
 [2,]     1     0    30  0.00000000
 [3,]     5     0    29  0.00000000
 [4,]    18     0    28  0.00000000
 [5,]    31     0    27  0.00000000
 [6,]    52     0    26  0.00000000
 [7,]    80     0    25  0.00000000
 [8,]   135     0    24  0.00000000
 [9,]   216     0    23  0.00000000
[10,]   321     0    22  0.00000000
[11,]   494     0    21  0.00000000
[12,]   751     0    20  0.00000000
[13,]  1121     0    19  0.00000000
[14,]  1665     0    18  0.00000000
[15,]  2454     0    17  0.00000000
[16,]  3540     0    16  0.00000000
[17,]  4984     0    15  0.00000000
[18,]  6942     2    14  0.02881014
[19,]  9414    10    13  0.10622477
[20,] 12568    37    12  0.29439847
[21,] 16188   137    11  0.84630591
[22,] 20623   468    10  2.26931096
[23,] 26679  1551     9  5.81356123
[24,] 33467  3775     8 11.27976813
[25,] 39355  6199     7 15.75149282
[1] " delta < 0"
      Total Decoy peaks        FDR
 [1,]     0     0    31        NaN
 [2,]     0     0    30        NaN
 [3,]     0     0    29        NaN
 [4,]     0     0    28        NaN
 [5,]     1     0    27  0.0000000
 [6,]     3     0    26  0.0000000
 [7,]     3     0    25  0.0000000
 [8,]     5     0    24  0.0000000
 [9,]    14     0    23  0.0000000
[10,]    20     0    22  0.0000000
[11,]    31     0    21  0.0000000
[12,]    45     0    20  0.0000000
[13,]    70     0    19  0.0000000
[14,]   117     0    18  0.0000000
[15,]   174     0    17  0.0000000
[16,]   262     0    16  0.0000000
[17,]   379     0    15  0.0000000
[18,]   572     2    14  0.3496503
[19,]   823     8    13  0.9720535
[20,]  1232    30    12  2.4350649
[21,]  1793   112    11  6.2465142
[22,]  2934   386    10 13.1561009
[23,]  5452  1266     9 23.2208364
[24,]  9745  3008     8 30.8671113
[25,] 13283  4577     7 34.4575774
[1] " delta == 0"
      Total Decoy peaks        FDR
 [1,]     1     0    31 0.00000000
 [2,]     1     0    30 0.00000000
 [3,]     1     0    29 0.00000000
 [4,]     3     0    28 0.00000000
 [5,]     6     0    27 0.00000000
 [6,]    12     0    26 0.00000000
 [7,]    25     0    25 0.00000000
 [8,]    44     0    24 0.00000000
 [9,]    75     0    23 0.00000000
[10,]   122     0    22 0.00000000
[11,]   187     0    21 0.00000000
[12,]   301     0    20 0.00000000
[13,]   460     0    19 0.00000000
[14,]   707     0    18 0.00000000
[15,]  1060     0    17 0.00000000
[16,]  1552     0    16 0.00000000
[17,]  2237     0    15 0.00000000
[18,]  3182     0    14 0.00000000
[19,]  4422     0    13 0.00000000
[20,]  5919     1    12 0.01689475
[21,]  7572     2    11 0.02641310
[22,]  9313     5    10 0.05368839
[23,] 11086    28     9 0.25257081
[24,] 11086    28     8 0.25257081
[25,] 11086    28     7 0.25257081
[1] " delta > 0"
      Total Decoy peaks         FDR
 [1,]     0     0    31         NaN
 [2,]     0     0    30         NaN
 [3,]     4     0    29  0.00000000
 [4,]    15     0    28  0.00000000
 [5,]    24     0    27  0.00000000
 [6,]    37     0    26  0.00000000
 [7,]    52     0    25  0.00000000
 [8,]    86     0    24  0.00000000
 [9,]   127     0    23  0.00000000
[10,]   179     0    22  0.00000000
[11,]   276     0    21  0.00000000
[12,]   405     0    20  0.00000000
[13,]   591     0    19  0.00000000
[14,]   841     0    18  0.00000000
[15,]  1220     0    17  0.00000000
[16,]  1726     0    16  0.00000000
[17,]  2368     0    15  0.00000000
[18,]  3188     0    14  0.00000000
[19,]  4169     2    13  0.04797314
[20,]  5417     6    12  0.11076241
[21,]  6823    23    11  0.33709512
[22,]  8376    77    10  0.91929322
[23,] 10141   257     9  2.53426684
[24,] 12636   739     8  5.84836974
[25,] 14986  1594     7 10.63659415
