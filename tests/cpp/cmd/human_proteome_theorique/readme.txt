cd cbuild

cmake .. -DCMAKE_BUILD_TYPE=Release

make

execute on ccd9842ac935bfa88aeb1d30b828f87c986d0adb

test vs arabidopsis fasta file :

langella@themis:~/developpement/git/deepprot/cbuild$ ./src/deepprot -p ../test/cpp/cmd/human_proteome_theorique/human_vs_arabidopsis_fasta_params.csv  -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/quart_human_proteomeGRCh37_61.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/Araport11_genes.201606.pep.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/crapPeptidesRef_Pool65.fasta -o /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/results.tsv
getting mz data file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/quart_human_proteomeGRCh37_61.mgf
using up to 23 threads
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/Araport11_genes.201606.pep.fasta
adding Fasta file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/crapPeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/Araport11_genes.201606.pep.fasta
reading FASTA file /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/crapPeptidesRef_Pool65.fasta
48487 proteins, 972999 digested protopeptides, 495005 protopeptides registered, 495005 spectrum modelized from peptides
Building theoretical spectrum took 13 seconds
reading mz data from /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/quart_human_proteomeGRCh37_61.mgf
Building experimental spectrum took 17 seconds
350000 experimental spectrum used
bucket_clustering
bucket_clustering done, number of buckets (mass) is : 75237
removing from bucket clustering all experimental spectrum only if they are not connected to theoretical spectrum
size after experimental spectrum cleaning : 67467
removing from bucket clustering all theoretical spectrum only if they are not connected to experimental spectrum
size after theoretical spectrum cleaning : 63673
spec_trees
Spectree extraction using 24 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 567 seconds
this is the end
overall process took 617 seconds
peptide identification is finished



test on human fasta file :

./src/deepprot -p ../test/cpp/cmd/human_proteome_theorique/human_vs_arabidopsis_fasta_params.csv  -m /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/quart_human_proteomeGRCh37_61.mgf /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD001468/Homo_sapiens_GRCh37_61.fasta /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/PXD004732/crapPeptidesRef_Pool65.fasta -o /gorgone/pappso/versions_logiciels_pappso/deepprot/donnees/human_proteome_theorique/human_fasta_results.tsv
