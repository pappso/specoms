Params=(
"\nLow=150"
"\nParentFilter_range=0.02\nDynamicRangeFilter_min=1.0\nDynamicRangeFilter_max=100.0"
"\nHighPass=10000"
"\nDeisotoping=1"
"\nLow=150\nWindowFilter_max=1\nWindowFilter_win=50"
)

Folders=(
"Low"
"Parent_plus_Dynamic"
"HighPassFilter_10000"   
"DeisotopingFilter_1"    
"Low_WindowFilter"        
)

for i in "${!Params[@]}"; 
do 
	mkdir "${Folders[$i]}/";
	cd "${Folders[$i]}/";
	echo 2/${Folders[$i]}/ >> ../../process.txt;
	cat ../files_config.ini > files_config.ini
	cat ../execution_parameters.ini > execution_parameters.ini
	echo -e "${Params[$i]}" >> execution_parameters.ini
	java -server -Xmx4g -jar ~/../../mnt/c/Users/Alexandre/Desktop/git_repos/SpecOMS/SpecOMS.jar -c >> log_${Folders[$i]}.txt;
	cd ..
done