/**
 * \file test/test_spectrumint.cpp
 * \date 28/10/2018
 * \author Olivier Langella
 * \brief test spectrum int
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2018  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// ./tests/catch2-only-tests [SpectrumInt] -s


#include <QDebug>
#include <QString>
#include <vector>
#include <iostream>
#include <cstdio>

#include <odsstream/tsvoutputstream.h>

#include "../src/core/spectrumintstore.h"
#include "../src/utils/deepprotparams.h"
#include "../src/algorithms/fptree/threadedfptree.h"
#include "../src/algorithms/fptree/spectreeextractorreporterinterface.h"
#include "../src/utils/monitorinterface.h"
#include "../src/utils/deepprotparams.h"
#include <pappsomspp/pappsoexception.h>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

TEST_CASE("Test spectrum int", "[SpectrumInt]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: SpectrumInt ::..", "[SpectrumInt]")
  {
    std::cout << std::endl << "..:: DeepProt test ::.." << std::endl;
    //   OboPsiMod test;

    std::vector<std::size_t> spectrum0 = {
      10008, 14711, 17111, 28417, 30817, 35521, 45528};
    std::vector<std::size_t> spectrum1 = {
      10008, 17111, 17512, 30817, 31218, 38221, 48329};
    std::vector<std::size_t> spectrum2 = {
      10008, 14711, 17111, 26020, 28420, 33123, 43131};
    std::vector<std::size_t> spectrum3 = {
      14711, 16707, 23511, 28417, 35521, 37219, 51928};
    std::vector<std::size_t> spectrum4 = {
      7104, 14711, 17111, 26512, 28417, 30817, 35521, 41219, 43131, 45528};
    std::vector<std::size_t> spectrum5 = {
      10008, 17111, 18913, 25915, 28417, 30817, 30917, 32619, 39722, 49730};
    std::vector<std::size_t> spectrum6 = {
      7104, 10008, 17111, 18913, 29717, 32416, 34218, 39722, 41321, 51329};

    qDebug();
    DeepProtParams &params = DeepProtParams::getInstance();
    qDebug();
    params.set(DeepProtParam::AccuracyValue, 0);
    // params.load("data/param.ods");
    qDebug();
    SpectrumIntStoreSPtr spectrum_int_store =
      std::make_shared<SpectrumIntStore>(params);
    qDebug();
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum0);
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum1);
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum2);
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum3);
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum4);
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum5);
    spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(nullptr,
                                                                     spectrum6);


    REQUIRE(spectrum_int_store.get()->size() == 7);

    pappso::spectree::BucketClustering bucket_clustering;
    spectrum_int_store->buildBucketClustering(bucket_clustering);

    std::vector<pappso::spectree::Bucket> bucket_list =
      bucket_clustering.asSortedList();

    REQUIRE(bucket_list.size() == 11);


    std::vector<std::size_t> bucket0  = {0, 1, 2, 4, 5, 6};
    std::vector<std::size_t> bucket1  = {0, 1, 2, 5, 6};
    std::vector<std::size_t> bucket2  = {0, 1, 4, 5};
    std::vector<std::size_t> bucket3  = {0, 2, 3, 4};
    std::vector<std::size_t> bucket4  = {0, 3, 4};
    std::vector<std::size_t> bucket5  = {0, 3, 4, 5};
    std::vector<std::size_t> bucket6  = {0, 4};
    std::vector<std::size_t> bucket7  = {2, 4};
    std::vector<std::size_t> bucket8  = {4, 6};
    std::vector<std::size_t> bucket9  = {5, 6};
    std::vector<std::size_t> bucket10 = {5, 6};

    REQUIRE(bucket_list[0].getId() == 17111);
    INFO("mass 17111 is present in spectrum 1, 2, 3, 5, 6, 7");
    REQUIRE_THAT(bucket_list[0].getCartList(),
                 Catch::Matchers::Equals(bucket0));


    REQUIRE(bucket_list[1].getId() == 10008);
    REQUIRE_THAT(bucket_list[1].getCartList(),
                 Catch::Matchers::Equals(bucket1));


    REQUIRE(bucket_list[2].getId() == 30817);
    REQUIRE_THAT(bucket_list[2].getCartList(),
                 Catch::Matchers::Equals(bucket2));

    REQUIRE(bucket_list[3].getId() == 14711);
    REQUIRE_THAT(bucket_list[3].getCartList(),
                 Catch::Matchers::Equals(bucket3));

    REQUIRE(bucket_list[4].getId() == 35521);
    REQUIRE_THAT(bucket_list[4].getCartList(),
                 Catch::Matchers::Equals(bucket4));

    REQUIRE(bucket_list[5].getId() == 28417);
    REQUIRE_THAT(bucket_list[5].getCartList(),
                 Catch::Matchers::Equals(bucket5));


    REQUIRE(bucket_list[6].getId() == 45528);

    INFO("mass 45528 is present in spectrum 1, 5");
    REQUIRE_THAT(bucket_list[6].getCartList(),
                 Catch::Matchers::Equals(bucket6));


    REQUIRE(bucket_list[7].getId() == 43131);
    REQUIRE_THAT(bucket_list[7].getCartList(),
                 Catch::Matchers::Equals(bucket7));

    REQUIRE(bucket_list[8].getId() == 7104);
    REQUIRE_THAT(bucket_list[8].getCartList(),
                 Catch::Matchers::Equals(bucket8));

    REQUIRE(bucket_list[9].getId() == 18913);
    REQUIRE_THAT(bucket_list[9].getCartList(),
                 Catch::Matchers::Equals(bucket9));

    REQUIRE(bucket_list[10].getId() == 39722);
    REQUIRE_THAT(bucket_list[10].getCartList(),
                 Catch::Matchers::Equals(bucket10));


    pappso::spectree::BucketClustering bucket_clustering2;
    spectrum_int_store->buildBucketClustering(bucket_clustering2);
  }
}
