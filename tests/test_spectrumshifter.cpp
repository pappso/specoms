
/**
 * \file test/test_spectrumshifter.cpp
 * \date 14/04/2019
 * \author Olivier Langella
 * \brief test spectrum shifter
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2019  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// make test ARGS="-V -I 2,2"
// ./tests/catch2-only-tests [SpectrumShifter] -s


#include <QDebug>
#include <QString>
#include <vector>
#include <iostream>
#include <cstdio>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/peptide/peptidestrparser.h>
#include "../src/utils/deepprotparams.h"
#include "../src/utils/utils.h"
#include "../src/core/spectrum_matcher_legacy/peptidespectrumshifter.h"
#include "../src/core/spectrum_matcher_legacy/peptidecandidate.h"
#include "../src/core/spectrum_matcher_legacy/spectrummatcher.h"
#include "../src/core/spectrumintstore.h"
#include "config.h"


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

TEST_CASE("Test spectrum shifter", "[SpectrumShifter]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: SpectrumShifter ::..", "[SpectrumShifter]")
  {
    std::cout << std::endl
              << "..:: DeepProt Spectrum Shifter test ::.." << std::endl;


    int mass_int = Utils::doubleMassToIntegerMass(500.254, 100);
    REQUIRE(mass_int == 50025);
    qDebug();
    DeepProtParams &params = DeepProtParams::getInstance();
    qDebug();
    std::vector<std::size_t> spectrum6 = {4452,
                                          8806,
                                          8004,
                                          15258,
                                          15355,
                                          20208,
                                          20912,
                                          25765,
                                          25862,
                                          32314,
                                          33117,
                                          36668};

    Utils::expandIntegerSpectrum(spectrum6, 2);

    REQUIRE_THAT(
      spectrum6,
      Catch::Matchers::Equals(std::vector<std::size_t>(
        {4450,  4451,  4452,  4453,  4454,  8002,  8003,  8004,  8005,
         8006,  8804,  8805,  8806,  8807,  8808,  15256, 15257, 15258,
         15259, 15260, 15353, 15354, 15355, 15356, 15357, 20206, 20207,
         20208, 20209, 20210, 20910, 20911, 20912, 20913, 20914, 25763,
         25764, 25765, 25766, 25767, 25860, 25861, 25862, 25863, 25864,
         32312, 32313, 32314, 32315, 32316, 33115, 33116, 33117, 33118,
         33119, 36666, 36667, 36668, 36669, 36670})));

    //     7104, 10008, 17111, 18913,29011, 29717, 32416, 34218, 39722, 41321,
    //     51329};

    // SAMPLER z=2 402,207638328489

    /* B
    44,523290669014
80,041847561369
145,562090017864
194,088471942289
250,630503930854
315,151800474839

Y
358,691624126354
323,173067233999
257,652824777504
209,126442853079
152,584410864514
88,063114320529

*/

    // SAM(MOD:00719)PLER z=2 410,205095638269
    /* B
     * 44,523290669014
    80,041847561369
    153,559547327644
    202,085929252069
    258,627961240634
    323,149257784619

    Y
    366,689081436134
    331,170524543779
    257,652824777504
    209,126442853079
    152,584410864514
    88,063114320529
    */

    qDebug();
    SpectrumIntStoreSPtr spectrum_int_store =
      std::make_shared<SpectrumIntStore>(params);


    pappso::PrecursorIonData precursor;
    precursor.charge = 2;
    precursor.mz     = 410.20509563826;
    pappso::QualifiedMassSpectrum qspectrum;
    qspectrum.getMassSpectrumId().setNativeId("SAMPLER");
    qspectrum.appendPrecursorIonData(precursor);

    std::size_t indice =
      spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(
        qspectrum.makeQualifiedMassSpectrumSPtr(), spectrum6);
    SpectrumInt spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);

    indice = spectrum_int_store.get()->newSpectrumIntFromPeptide(
      0, pappso::PeptideStrParser::parseString("SAMPLER"));
    SpectrumInt peptide_spectrum_int =
      spectrum_int_store.get()->getSpectrumInt(indice);

    params.set(DeepProtParam::AccuracyValue, 2);
    qDebug();
    params.set(DeepProtParam::ParentIonMassTolerancePrecision, 10);
    qDebug();
    params.set(DeepProtParam::ParentIonMassToleranceUnit, QString("ppm"));
    params.set(DeepProtParam::SpecXtractMinimumSimilarity, 8);
    params.set(DeepProtParam::SpecFitMinimumSimilarity, 8);
    params.set(DeepProtParam::SpecFitMinimalMassDelta, -500);
    params.set(DeepProtParam::PeptideDigestionNumberOfMissedCleavages, 0);


    SpectrumMatcher spectrum_matcher(params.getDecisionBox(), 7);
    spectrum_matcher.setExperimentalSpectrumInt(spectrum_int);

    // params.load("data/param.ods");
    qDebug();
    PeptideCandidate candidate(&spectrum_matcher, peptide_spectrum_int, 6);
    qDebug();

    PeptideSpectrumShifter shifter(&candidate, &spectrum_int);
    qDebug();

    for(auto position : shifter.findBestDeltaPosition(2))
      {
        std::cout << position << std::endl;
        /*
        if(position != 2)
          {
            std::cerr << " position != 2 ERROR" << std::endl;
            return 1;
          }
          */
      }

    std::cout << std::endl
              << "..:: DeepProt Spectrum Shifter on 59741 ::.." << std::endl;

    qDebug();
    pappso::MsFileAccessor accessor(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/"
                "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59741.mgf"),
      0);
    qDebug();
    pappso::MsRunReaderSPtr reader =
      accessor.getMsRunReaderSPtrByRunId("", "msrun");
    qDebug();
    std::cout << reader->spectrumListSize() << std::endl;
    pappso::QualifiedMassSpectrum spectrum_sp =
      reader->qualifiedMassSpectrum(0);
    std::cout << spectrum_sp.getMassSpectrumCstSPtr().get()->size()
              << std::endl;

    // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
    // spectrum_sp.get()->debugPrintValues();
    qDebug();
    // spectrum_sp.getMassSpectrumCstSPtr().get()->sortMz();

    std::vector<std::size_t> integer_spectrum = Utils::TraceToIntegerSpectrum(
      *(spectrum_sp.getMassSpectrumSPtr().get()), 100);

    for(auto val : integer_spectrum)
      {
        qDebug() << " mz=" << val;
      }
    // spectrum_sp.getPrecursorMz()

    Utils::expandIntegerSpectrum(integer_spectrum, 2);
    indice = spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(
      spectrum_sp.makeQualifiedMassSpectrumSPtr(), integer_spectrum);
    spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);

    //  VIEFIASSFEVSR

    indice = spectrum_int_store.get()->newSpectrumIntFromPeptide(
      0, pappso::PeptideStrParser::parseString("VIEFIASSFEVSR"));
    peptide_spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);


    SpectrumMatcher spectrum_matcher_b(params.getDecisionBox(), 7);
    spectrum_matcher_b.setExperimentalSpectrumInt(spectrum_int);

    // params.load("data/param.ods");
    qDebug();
    PeptideCandidate candidate_b(&spectrum_matcher_b, peptide_spectrum_int, 6);
    qDebug() << " candidate_b.getMassDelta=" << candidate_b.getMassDelta();

    PeptideSpectrumShifter shifter_b(&candidate_b, &spectrum_int);
    qDebug();

    shifter_b.findBestDeltaPosition(2);

    REQUIRE(shifter_b.getScore(1) == 14);

    REQUIRE_THAT(shifter_b.getPeptideCandidate().getDeltaPositions(),
                 Catch::Matchers::Equals(std::vector<std::size_t>({0, 1})));


    // it does not work beacause mass delta is cumulated with mass precursor
    // error : need specglob
    // REQUIRE(shifter_b.getPeptideCandidate().getFittedCount() == 14);
    /*
        for(auto position : shifter_b.getPeptideCandidate().getDeltaPositions())
          {
            std::cout << position << std::endl;


            REQUIRE_FALSE(position > 1);
          }
    */

    std::cout << std::endl
              << "..:: DeepProt Spectrum Shifter on 59747 ::.." << std::endl;


    qDebug();
    pappso::MsFileAccessor accessor_59747(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/"
                "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59747.mgf"),
      0);

    qDebug();
    reader = accessor_59747.getMsRunReaderSPtrByRunId("", "msrun");

    qDebug();
    std::cout << reader->spectrumListSize() << std::endl;
    spectrum_sp = reader->qualifiedMassSpectrum(0);
    std::cout << spectrum_sp.getMassSpectrumSPtr().get()->size() << std::endl;

    // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
    // spectrum_sp.get()->debugPrintValues();
    qDebug();
    // spectrum_sp.get()->sortMz();
    spectrum_sp.getMassSpectrumSPtr().get()->filter(
      pappso::FilterGreatestY(60));
    // spectrum_sp.get()->filter( pappso::FilterHighPassPercentage(0.01));

    integer_spectrum = Utils::TraceToIntegerSpectrum(
      *(spectrum_sp.getMassSpectrumSPtr().get()), 100);

    for(auto val : integer_spectrum)
      {
        qDebug() << " mz=" << val;
      }

    indice = spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(
      spectrum_sp.makeQualifiedMassSpectrumSPtr(), integer_spectrum);
    spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);


    SpectrumMatcher spectrum_matcher_c(params.getDecisionBox(), 7);
    spectrum_matcher_c.setExperimentalSpectrumInt(spectrum_int);

    indice = spectrum_int_store.get()->newSpectrumIntFromPeptide(
      0, pappso::PeptideStrParser::parseString("AIGSIIAYITDAK"));
    peptide_spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);
    // params.load("data/param.ods");
    qDebug();
    PeptideCandidate candidate_c(&spectrum_matcher_c, peptide_spectrum_int, 9);
    qDebug() << " candidate_b.getMassDelta=" << candidate_b.getMassDelta();

    PeptideSpectrumShifter shifter_c(&candidate_c, &spectrum_int);
    qDebug();

    shifter_c.findBestDeltaPosition(2);
    REQUIRE_FALSE(shifter_c.getPeptideCandidate().getOriginalCount() != 9);
    REQUIRE_FALSE(shifter_c.getPeptideCandidate().getFittedCount() != 8);

    for(auto position : shifter_c.getPeptideCandidate().getDeltaPositions())
      {
        std::cout << position << std::endl;


        REQUIRE_FALSE(position > 6);
      }


    std::cout << std::endl
              << "..:: DeepProt Spectrum Shifter on 59469 ::.." << std::endl;
    // SITIIQAVIPQIK 01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.raw.59469
    //


    qDebug();
    pappso::MsFileAccessor accessor_59469(

      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/"
                "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59469.mgf"),
      0);
    qDebug();
    reader = accessor_59469.getMsRunReaderSPtrByRunId("", "msrun");
    qDebug();
    std::cout << reader->spectrumListSize() << std::endl;
    spectrum_sp = reader->massSpectrumSPtr(0);
    std::cout << spectrum_sp.getMassSpectrumSPtr().get()->size() << std::endl;

    // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
    // spectrum_sp.get()->debugPrintValues();
    qDebug();
    // spectrum_sp.get()->sortMz();
    spectrum_sp.getMassSpectrumSPtr().get()->filter(
      pappso::FilterGreatestY(60));
    // spectrum_sp.get()->filter( pappso::FilterHighPassPercentage(0.01));

    integer_spectrum = Utils::TraceToIntegerSpectrum(
      *(spectrum_sp.getMassSpectrumSPtr().get()), 100);

    for(auto val : integer_spectrum)
      {
        qDebug() << " mz=" << val;
      }

    indice = spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(
      spectrum_sp.makeQualifiedMassSpectrumSPtr(), integer_spectrum);
    spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);


    SpectrumMatcher spectrum_matcher_d(params.getDecisionBox(), 7);
    spectrum_matcher_d.setExperimentalSpectrumInt(spectrum_int);

    // params.load("data/param.ods");

    indice = spectrum_int_store.get()->newSpectrumIntFromPeptide(
      0, pappso::PeptideStrParser::parseString("SITIIQAVIPQIK"));
    peptide_spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);
    qDebug();
    PeptideCandidate candidate_d(&spectrum_matcher_d, peptide_spectrum_int, 9);
    qDebug() << " candidate_b.getMassDelta=" << candidate_b.getMassDelta();

    PeptideSpectrumShifter shifter_d(&candidate_d, &spectrum_int);
    qDebug();
    std::cout << "spectrum_int.getTitle()="
              << spectrum_int.getDescriptor().toStdString().c_str()
              << std::endl;
    std::cout << "shifter_d.getPeptideCandidate().getOriginalCount()="
              << shifter_d.getPeptideCandidate().getOriginalCount()
              << std::endl;
    shifter_d.findBestDeltaPosition(2);
    std::cout << "shifter_d.getPeptideCandidate().getFittedCount()="
              << shifter_d.getPeptideCandidate().getFittedCount() << std::endl;
    REQUIRE(shifter_d.getPeptideCandidate().getOriginalCount() == 9);
    REQUIRE(shifter_d.getPeptideCandidate().getFittedCount() == 10);

    REQUIRE_THAT(shifter_d.getPeptideCandidate().getDeltaPositions(),
                 Catch::Matchers::Equals(std::vector<std::size_t>({3, 4})));


    std::cout << std::endl
              << "..:: DeepProt Spectrum Shifter on 59739 ::.." << std::endl;


    qDebug();
    pappso::MsFileAccessor accessor_59739(

      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/"
                "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59739.mgf"),
      0);
    qDebug();
    reader = accessor_59739.getMsRunReaderSPtrByRunId("", "msrun");
    qDebug();
    std::cout << reader->spectrumListSize() << std::endl;
    spectrum_sp = reader->qualifiedMassSpectrum(0);
    std::cout << spectrum_sp.getMassSpectrumSPtr().get()->size() << std::endl;

    // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
    // spectrum_sp.get()->debugPrintValues();
    qDebug();
    // spectrum_sp.get()->sortMz();
    spectrum_sp.getMassSpectrumSPtr().get()->filter(
      pappso::FilterGreatestY(60));
    // spectrum_sp.get()->filter( pappso::FilterHighPassPercentage(0.01));

    integer_spectrum = Utils::TraceToIntegerSpectrum(
      *(spectrum_sp.getMassSpectrumSPtr().get()), 100);

    for(auto val : integer_spectrum)
      {
        qDebug() << " mz=" << val;
      }

    Utils::expandIntegerSpectrum(integer_spectrum, 2);
    indice = spectrum_int_store.get()->newSpectrumIntFromExperimentalSpectrum(
      spectrum_sp.makeQualifiedMassSpectrumSPtr(), integer_spectrum);
    spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);


    SpectrumMatcher spectrum_matcher_e(params.getDecisionBox(), 7);
    spectrum_matcher_e.setExperimentalSpectrumInt(spectrum_int);


    indice = spectrum_int_store.get()->newSpectrumIntFromPeptide(
      0, pappso::PeptideStrParser::parseString("SIPSGPITEIGMIK"));
    peptide_spectrum_int = spectrum_int_store.get()->getSpectrumInt(indice);
    // params.load("data/param.ods");
    qDebug();
    PeptideCandidate candidate_e(&spectrum_matcher_e, peptide_spectrum_int, 7);
    qDebug() << " candidate_e.getMassDelta=" << candidate_e.getMassDelta();

    PeptideSpectrumShifter shifter_e(&candidate_e, &spectrum_int);
    qDebug();
    std::cout << "spectrum_int.getTitle()="
              << spectrum_int.getDescriptor().toStdString().c_str()
              << std::endl;
    std::cout << "shifter_e.getPeptideCandidate().getOriginalCount()="
              << shifter_e.getPeptideCandidate().getOriginalCount()
              << std::endl;
    std::vector<std::size_t> position_list = shifter_e.findBestDeltaPosition(1);
    std::cout << "shifter_e.getPeptideCandidate().getFittedCount()="
              << shifter_e.getPeptideCandidate().getFittedCount() << std::endl;
    std::cout << "shifter_e.getPeptideCandidate().getFittedCount() "
                 "position_list.size="
              << position_list.size() << std::endl;
    std::cout << std::endl
              << "shifter_e.getNterIntegerIonMassLists()" << std::endl;

    std::cout << std::endl;

    REQUIRE(shifter_e.getPeptideCandidate().getOriginalCount() == 7);
    REQUIRE(shifter_e.getPeptideCandidate().getFittedCount() == 7);

    REQUIRE_THAT(shifter_e.getPeptideCandidate().getDeltaPositions(),
                 Catch::Matchers::Equals(std::vector<std::size_t>({10, 11})));
  }
}
