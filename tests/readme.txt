
testing for issue https://forgemia.inra.fr/anr-deepprot/deepprot/issues/2

PXD004732 scan 59737 difference between deepprot and specoms

./src/deepprot -p ../tests/data/PXD004732_params.ods -m ../tests/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59737.mgf ../tests/data/fasta_59737.fasta  -o test.ods

PXD004732 scan 59737 spectrum int :
11208 12008 12910 13008 14711 15809 15909 16009 16407 17312 17511 17908 18314 18512 19507 19608 19712 20112 21114 21713 22911 24311 24411 24416 24512 25315 26019 26115 26816 26912 27217 28713 31215 33319 34414 34614 37222 37318 37328 40022 41523 46921 51330 55030 55826 57628 68036 71635 73035 75436 77194 77237 77249 77294 80043 98551 116360 130073 130166 134968

TGQIGITDFVMIDIIK :
373.28 OK
260.19 OK
147.11 OK
159.0764 15909 OK
287.13 OK
400.21906 40022 OK
671.3722 NO

peptide spectrum int TGQIGITDFVMIDIIK in deepprot
10205 14711 15907 26019 28713 37328 40021 45724 48830 57032 60139 67137 73243 78639 83150 93346 97856 103253 109359 116357 119464 127666 130772 136474 139168 147783 150477 160589 161785 166291




PXD004732 scan 59739 difference between deepprot and specoms

./src/deepprot -p ../test/data/PXD004732_params.ods -m ../test/data/composite_59739.mgf ../test/data/fasta_59739.fasta  -o test.ods

PXD004732 scan 59739 spectrum int in deepprot :
11007 12008 12910 13008 14711 17511 17908 18314 18516 18608 19508 19712 20112 21114 21117 21316 22515 22717 23917 24311 24411 24416 24512 25318 25322 26019 27610 28820 29817 29917 34018 34117 34124 35518 36923 39325 46830 48324 49828 54026 64635 65738 70237 72434 72538 74191 74239 74244 81139 83245 88243 88343 90350 90450 96450 99553 101659 111565 124569 125968

PXD004732 scan 59739 spectrum int in specoms :
11007 12008 12910 13008 14711 17511 17908 18314 18516 18608 19508 19712 20112 21114 21117 21316 22515 22717 23917 24311 24411 24416 24512 25318 25322 26019 27610 28820 29817 29917 34018 34117 34124 35518 36923 39325 46830 48324 49828 54026 64635 65738 70237 72434 72538 74191 74239 74244 81139 83245 88243 88343 90350 90450 96450 99553 101659 111565 124569 125968


test of missed cleavages on MKPAIFNVIC(MOD:00397)EIK Protein_chimera6
./src/deepprot -p ../test/data/PXD004732_params_mc1.csv -m ../test/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-49434.mgf ../test/data/fasta_49434.fasta  -o test.ods


https://forgemia.inra.fr/anr-deepprot/deepprot/issues/3

./src/deepprot -p ../test/data/PXD004732_params.csv -m ../test/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59735.mgf ../test/data/fasta_59735.fasta  -o test.ods
PFGQPYDEIAIIGDGFK
9806 14711 24513 29418 30215 35120 43021 46623 52325 52726 63634 69032 74942 80535 82046 93354 93439 104748 106258 111852 117761 123160 134067 134468 140170 143773 151673 156578 157375 162281 172082 176987


X!Tandem identifies E(-18.02)EVLLDFLSLAHK from Protein_chimera1 in scan 59561
./src/deepprot -p ../tests/data/PXD004732_params.csv -m ../tests/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59561.mgf ../tests/data/fasta_59561.fasta -o test.ods
EEVIIDFISIAHK
13005 14711 25909 28417 35521 35816 46829 47124 55532 58433 66841 69936 81548 84642 93050 95951 104359 104654 115667 115962 123066 125574 136772 138478
9 ions Y in peptideviewer
1043.588 => 104359 OK
930.504 => 93050 OK
815.477 => 81548 OK
668.408 => 66841 
555.324 => 55532 OK
468.292 => 46829
355.208 => 35521
284.171 => 28417 OK
147.112 => 14711 OK
spectrum int :
11007 12008 12910 14110 14711 16909 17511 18314 19917 20112 21114 22717 22911 23316 24013 24113 24214 24311 24411 24416 24516 25814 26115 28412 28417 28514 29418 29514 32422 33920 34015 34220 35725 39822 40319 42523 43119 45323 47026 55532 58443 58543 66034 74842 74893 74941 79455 79555 79655 81547 93050 93151 94058 94160 94263 94363 95249 98049 104357 105467
60 first peaks don't include important peaks => deepprot is unable to identify it
but deeprot finds it if minimum score is set to 6 : after specfit: 8	delta_position	-18,010959936998	0 1 2
perhaps deepprot could rerun specxtract on unresolved scans with lesser minimum score


msconvert /media/langella/pappso/deepprot/donnees/PXD001468/sender/b1922_293T_proteinID_02A_QE3_122212.mgf --mgf --filter "scanNumber 53083"
EEEEEIEEEKEEK


./src/specoms -p ../tests/data/PXD004732_params.ods -m ../tests/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59737.mgf ../tests/data/fasta_59737.fasta  -o test.ods



./src/specoms -p ../tests/data/PXD004732_params.ods -m ../tests/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59678.mgf ../tests/data/fasta_59678.fasta  -o test.mzid
 ./src/specoms -p ../tests/v20200324/PXD004732_params.csv -m ../tests/data/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59678.mgf ../tests/data/fasta_59678.fasta  -o test.ods

 spectrum ID	spectrum index	precursor charge	precursor mz	precursor mass	number of candidates	category	sequence	presence in target database	presence in decoy database	sequence after specfit	peptide mass	peptide mz	score before specfit	score after specfit	candidate status	mass delta	mass delta positions	proteins
index=0	0	2	799,400634765625	1596,78671659749	1	no_delta_position	IINVTIATIDTSK	target	target	IINVTIATIDTSK	1387,79224559703	694,903399265394	7	7	no_delta_position	208,994471000463		AT1G07460.1 AT1G07460.2
