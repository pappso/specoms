run(){
	mkdir $1/$3 
	cd $1/$3

	# copier les configs (accès aux donés) standard (base de donnée et contaminants)
	cat ../../files_config.ini > files_config.ini

	# ajouter le nom du fichier de spectres
	printf "\n../$2" >> files_config.ini
	# ajouter le nom du fichier de résultat (c'est le nom du dossier qui nous donne le nom de a manip...)
	printf "\nresults.csv" >> files_config.ini

	# parametres d'exe standard
	cat ../../execution_parameters.ini > execution_parameters.ini
	# ajouter les autres params éventuels		
	printf $4 >> execution_parameters.ini

	# lancer SpecOMS
	java -server -Xmx6g -jar ~/../../mnt/c/Users/Alexandre/Desktop/git_repos/SpecOMS/SpecOMS.jar -c

	cd ../..
}


# arguments : 
#	$1 : juste le nom du dossier correspondant au mgf étudié (ici fichier n°2 donc dossier n°2)
#	$2 : fichier contenant les spectres (ici : b1922_293T_proteinID_02A_QE3_122212.mgf TEST_DEISOTOPING_1_0)
#	$3 : nom de la manip (et donc nom du sous dossier ici : TEST_DEISOTOPING_1_0)
#	$4 : paramètres éventuels (la valeur du paramètre de déisotoping : "\nDeisotoping=1.0")
run 2 b1922_293T_proteinID_02A_QE3_122212.mgf TEST_DEISOTOPING_1_0 "\nDeisotoping=1.0"