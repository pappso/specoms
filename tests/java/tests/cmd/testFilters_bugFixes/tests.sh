Folders=(
"ParentFilter"        
"DynamicRangeFilter"  
"HighPassFilter"   
"DeisotopingFilter"   
"UpFilter"            
"WindowFilter"        
)

for i in "${!Folders[@]}"; 
do 
	cd "${Folders[$i]}/";
	java -server -Xmx4g -jar ~/../../mnt/c/Users/Alexandre/Desktop/git_repos/SpecOMS/SpecOMS_testFilters.jar -c >> log_${Folders[$i]}.txt;
	cd ..
	cp ../../../../../java/log1.txt ${Folders[$i]}/log_normal.txt
	cp ../../../../../java/log2.txt ${Folders[$i]}/log_filtered.txt
done