import sys

def test_presence(tab1, tab2):
	missmatches = 0
	for i in tab1:
		if i not in tab2:
			missmatches = missmatches + 1
	return missmatches

# give in entry the file where you want to count :
f = open(sys.argv[1], "r")

masses_tab = []

for line in f:
	if "INFOS: " in line:
		masses = line[8:(len(line)-2)]
		mass = ""
		masses_line = []
		for c in masses:
			if c == ",":
				masses_line.append(int(mass))
				mass = ""
			elif c == " ":
				pass
			else:
				mass += c
				
		masses_tab.append(masses_line)
				
			
print("lost : ")			
print(test_presence(masses_tab[0], masses_tab[1]))		
print("added: ")
print(test_presence(masses_tab[1], masses_tab[0]))		