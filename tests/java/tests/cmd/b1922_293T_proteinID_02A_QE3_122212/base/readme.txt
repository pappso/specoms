cd test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base

choosing logging_prod.properties to avoid debug messages


test on 6869dfc4257dd0a75a269a6ef32dcd41950a66c5
time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
116 database proteins added...
Adding contaminants...
86934 contaminants proteins added...
Nombre de peptides:2950099
Constructing theoretical spectra...
2950099 theoretical spectra added.
43137 experimental spectra added.
Clustering 2993236 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 6429 seconds.

real	107m11,303s
user	107m21,598s
sys	30m14,379s


langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$  Rscript --vanilla ../../../../R/count.R results.csv 
[1] "total: 41734"
[1] "decoy: 8132"
      Total Decoy peaks         FDR
 [1,]     1     0    32  0.00000000
 [2,]     1     0    31  0.00000000
 [3,]     1     0    30  0.00000000
 [4,]     1     0    29  0.00000000
 [5,]     7     0    28  0.00000000
 [6,]    16     0    27  0.00000000
 [7,]    27     0    26  0.00000000
 [8,]    42     0    25  0.00000000
 [9,]    70     0    24  0.00000000
[10,]   130     0    23  0.00000000
[11,]   215     0    22  0.00000000
[12,]   347     0    21  0.00000000
[13,]   568     0    20  0.00000000
[14,]   865     0    19  0.00000000
[15,]  1362     0    18  0.00000000
[16,]  2094     1    17  0.04775549
[17,]  3241     2    16  0.06170935
[18,]  4732     8    15  0.16906171
[19,]  6720    21    14  0.31250000
[20,]  9348    69    13  0.73812580
[21,] 12882   230    12  1.78543704
[22,] 17372   692    11  3.98342160
[23,] 23629  1864    10  7.88861145
[24,] 31802  4279     9 13.45512861
[25,] 38011  6652     8 17.50019731
[26,] 40777  7747     7 18.99845501
[27,] 41734  8132     6 19.48531174


parameters modified (threshold, 7)
test on a294430272165225d878a002e89220fe9cf334a6
time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
116 database proteins added...
Adding contaminants...
86934 contaminants proteins added...
Nombre de peptides:2950099
Constructing theoretical spectra...
2950099 theoretical spectra added.
43137 experimental spectra added.
Clustering 2993236 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 6429 seconds.

real	107m11,303s
user	107m21,598s
sys	30m14,379s


langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ Rscript --vanilla ../../../../R/count.R results.csv
[1] "total: 39299"
[1] "decoy: 7030"
      Total Decoy peaks         FDR
 [1,]     1     0    32  0.00000000
 [2,]     1     0    31  0.00000000
 [3,]     1     0    30  0.00000000
 [4,]     1     0    29  0.00000000
 [5,]     7     0    28  0.00000000
 [6,]    18     0    27  0.00000000
 [7,]    29     0    26  0.00000000
 [8,]    46     0    25  0.00000000
 [9,]    78     0    24  0.00000000
[10,]   156     0    23  0.00000000
[11,]   260     0    22  0.00000000
[12,]   414     0    21  0.00000000
[13,]   668     0    20  0.00000000
[14,]  1054     0    19  0.00000000
[15,]  1639     0    18  0.00000000
[16,]  2540     1    17  0.03937008
[17,]  3805     4    16  0.10512484
[18,]  5415    11    15  0.20313943
[19,]  7556    40    14  0.52938062
[20,] 10437   140    13  1.34138162
[21,] 14186   409    12  2.88312421
[22,] 18950  1056    11  5.57255937
[23,] 25166  2410    10  9.57641262
[24,] 31708  4281     9 13.50132459
[25,] 36495  5907     8 16.18577887
[26,] 39299  7030     7 17.88849589




test on bea646bf6c237d4b8f6106ab8d983d68b3d437ae


langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:2950099
Constructing theoretical spectra...
2950099 theoretical spectra added.
43137 experimental spectra added.
Clustering 2993236 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 3876 seconds.

real	64m36,890s
user	104m25,216s
sys	0m10,925s

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ Rscript --vanilla ../../../../R/count.R results.csv
[1] "total: 39299"
[1] "decoy: 6506"
      Total Decoy peaks         FDR
 [1,]     1     0    32  0.00000000
 [2,]     1     0    31  0.00000000
 [3,]     1     0    30  0.00000000
 [4,]     1     0    29  0.00000000
 [5,]     7     0    28  0.00000000
 [6,]    15     0    27  0.00000000
 [7,]    27     0    26  0.00000000
 [8,]    41     0    25  0.00000000
 [9,]    65     0    24  0.00000000
[10,]   125     0    23  0.00000000
[11,]   209     0    22  0.00000000
[12,]   336     0    21  0.00000000
[13,]   547     0    20  0.00000000
[14,]   842     0    19  0.00000000
[15,]  1312     0    18  0.00000000
[16,]  2037     1    17  0.04909180
[17,]  3142     2    16  0.06365372
[18,]  4604     6    15  0.13032146
[19,]  6527    13    14  0.19917267
[20,]  9070    42    13  0.46306505
[21,] 12375   120    12  0.96969697
[22,] 16379   381    11  2.32614934
[23,] 21676  1093    10  5.04244326
[24,] 28554  2713     9  9.50129579
[25,] 35074  4856     8 13.84501340
[26,] 39299  6506     7 16.55512863


test on da447250bc931cbb20020f92df9397f63ccca447
langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:2950099
Constructing theoretical spectra...
2950099 theoretical spectra added.
43137 experimental spectra added.
Clustering 2993236 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 3917 seconds.

real	65m18,162s
user	74m17,478s
sys	0m28,727s

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/base$ Rscript --vanilla ../../../../R/count.R results.csv
[1] "total: 39299"
[1] "decoy: 6506"
      Total Decoy peaks         FDR
 [1,]     1     0    32  0.00000000
 [2,]     1     0    31  0.00000000
 [3,]     1     0    30  0.00000000
 [4,]     1     0    29  0.00000000
 [5,]     7     0    28  0.00000000
 [6,]    15     0    27  0.00000000
 [7,]    27     0    26  0.00000000
 [8,]    41     0    25  0.00000000
 [9,]    65     0    24  0.00000000
[10,]   125     0    23  0.00000000
[11,]   209     0    22  0.00000000
[12,]   336     0    21  0.00000000
[13,]   547     0    20  0.00000000
[14,]   842     0    19  0.00000000
[15,]  1312     0    18  0.00000000
[16,]  2037     1    17  0.04909180
[17,]  3142     2    16  0.06365372
[18,]  4604     6    15  0.13032146
[19,]  6527    13    14  0.19917267
[20,]  9070    42    13  0.46306505
[21,] 12375   120    12  0.96969697
[22,] 16379   381    11  2.32614934
[23,] 21676  1093    10  5.04244326
[24,] 28554  2713     9  9.50129579
[25,] 35074  4856     8 13.84501340
[26,] 39299  6506     7 16.55512863

