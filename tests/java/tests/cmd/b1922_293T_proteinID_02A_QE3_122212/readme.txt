cd test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/

choosing logging_prod.properties to avoid debug messages


test on 7d7385cbaf72f6835be9a01a2ed497068f4c4422
time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
116 database proteins added...
Adding contaminants...
86934 contaminants proteins added...
Nombre de peptides:528268
Constructing theoretical spectra...
528268 theoretical spectra added.
43137 experimental spectra added.
Clustering 571405 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 716 seconds.

real	11m56,825s
user	20m52,187s
sys	0m3,936s


=> 34085 identified peptides

test on 46a5a0535d1d73166869dd639a3546c3addd65e8
new parameters for test (50 peak, decoy db)

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
116 database proteins added...
Adding contaminants...
86934 contaminants proteins added...
Nombre de peptides:1055951
Constructing theoretical spectra...
1055951 theoretical spectra added.
43137 experimental spectra added.
Clustering 1099088 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 1073 seconds.

real	17m54,113s
user	19m12,704s
sys	0m2,613s

=> 22873 identified peptides (decoy + target)

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$ Rscript --vanilla ../../../R/count.R results.csv 
[1] "total: 22873"
[1] "decoy: 1906"
      Total Decoy peaks       FDR
 [1,]     2     0    28 0.0000000
 [2,]    13     0    27 0.0000000
 [3,]    27     0    26 0.0000000
 [4,]    44     0    25 0.0000000
 [5,]    74     0    24 0.0000000
 [6,]   137     0    23 0.0000000
 [7,]   269     0    22 0.0000000
 [8,]   495     3    21 0.6060606
 [9,]   851     5    20 0.5875441
[10,]  1381    15    19 1.0861694
[11,]  2139    20    18 0.9350164
[12,]  3208    48    17 1.4962594
[13,]  4593    84    16 1.8288700
[14,]  6363   159    15 2.4988213
[15,]  8476   260    14 3.0674847
[16,] 10922   423    13 3.8729170
[17,] 13517   601    12 4.4462529
[18,] 16089   816    11 5.0717882
[19,] 18563  1076    10 5.7964769
[20,] 20754  1400     9 6.7456876
[21,] 22873  1906     8 8.3329690


new test on 9a09a5700cd55ccfc85329134a7d9f1d46516e01 (gold parameters)

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:1120832
Constructing theoretical spectra...
1120832 theoretical spectra added.
43137 experimental spectra added.
Clustering 1163969 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 1267 seconds.

real	21m7,638s
user	32m10,271s
sys	0m4,722s
langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$ Rscript --vanilla ../../../R/count.R results.csv
[1] "total: 36907"
[1] "decoy: 7495"
      Total Decoy peaks        FDR
 [1,]     1     0    30  0.0000000
 [2,]     1     0    29  0.0000000
 [3,]     7     0    28  0.0000000
 [4,]    17     0    27  0.0000000
 [5,]    25     0    26  0.0000000
 [6,]    41     0    25  0.0000000
 [7,]    68     0    24  0.0000000
 [8,]   134     0    23  0.0000000
 [9,]   211     0    22  0.0000000
[10,]   356     0    21  0.0000000
[11,]   585     0    20  0.0000000
[12,]   920     0    19  0.0000000
[13,]  1466     0    18  0.0000000
[14,]  2265     3    17  0.1324503
[15,]  3328     7    16  0.2103365
[16,]  4757    22    15  0.4624764
[17,]  6713    72    14  1.0725458
[18,]  9239   187    13  2.0240286
[19,] 12515   476    12  3.8034359
[20,] 16677  1142    11  6.8477544
[21,] 21971  2358    10 10.7323290
[22,] 27816  4044     9 14.5383952
[23,] 33001  5846     8 17.7146147
[24,] 36907  7495     7 20.3078007


new test on 70e98fc600e6c655c6c1a4b8defd01a4eb12381f (master bug fix)

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:1120832
Constructing theoretical spectra...
1120832 theoretical spectra added.
43137 experimental spectra added.
Clustering 1163969 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 1298 seconds.

real	21m39,194s
user	37m8,795s
sys	0m6,277s

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212$  Rscript --vanilla ../../../R/count.R results.csv
[1] "total: 36907"
[1] "decoy: 7016"
      Total Decoy peaks         FDR
 [1,]     1     0    30  0.00000000
 [2,]     1     0    29  0.00000000
 [3,]     6     0    28  0.00000000
 [4,]    13     0    27  0.00000000
 [5,]    22     0    26  0.00000000
 [6,]    32     0    25  0.00000000
 [7,]    50     0    24  0.00000000
 [8,]   103     0    23  0.00000000
 [9,]   167     0    22  0.00000000
[10,]   268     0    21  0.00000000
[11,]   435     0    20  0.00000000
[12,]   665     0    19  0.00000000
[13,]  1060     0    18  0.00000000
[14,]  1664     1    17  0.06009615
[15,]  2629     2    16  0.07607455
[16,]  3838     4    15  0.10422095
[17,]  5491    19    14  0.34602076
[18,]  7694    53    13  0.68884845
[19,] 10494   125    12  1.19115685
[20,] 13835   335    11  2.42139501
[21,] 18292   938    10  5.12792478
[22,] 24055  2278     9  9.46996466
[23,] 30864  4509     8 14.60925350
[24,] 36907  7016     7 19.00994391
