cd test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/deisotoping



test on b51e7873525c5174d4c5adcf84892374a5ce01de
time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c

langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/deisotoping$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:1120832
Constructing theoretical spectra...
1120832 theoretical spectra added.
43137 experimental spectra added.
Clustering 1163969 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 1181 seconds.

real	19m41,707s
user	20m38,919s
sys	0m5,881s
langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/deisotoping$ Rscript --vanilla ../../../../R/count.R results.csv
[1] "total: 29091"
[1] "decoy: 4298"
      Total Decoy peaks        FDR
 [1,]     1     0    29  0.0000000
 [2,]     3     0    28  0.0000000
 [3,]     5     0    27  0.0000000
 [4,]    12     0    26  0.0000000
 [5,]    23     0    25  0.0000000
 [6,]    45     0    24  0.0000000
 [7,]    76     0    23  0.0000000
 [8,]   124     0    22  0.0000000
 [9,]   204     0    21  0.0000000
[10,]   317     0    20  0.0000000
[11,]   512     0    19  0.0000000
[12,]   806     0    18  0.0000000
[13,]  1220     0    17  0.0000000
[14,]  1790     3    16  0.1675978
[15,]  2604     4    15  0.1536098
[16,]  3801    12    14  0.3157064
[17,]  5460    29    13  0.5311355
[18,]  7517    60    12  0.7981908
[19,] 10062   137    11  1.3615583
[20,] 13217   355    10  2.6859348
[21,] 17179   815     9  4.7441644
[22,] 22304  1946     8  8.7248924
[23,] 29091  4298     7 14.7743288


test on a8142e6410fc431d1318d2d1c6c68db330b88457
langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/deisotoping$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:2950099
Constructing theoretical spectra...
2950099 theoretical spectra added.
43137 experimental spectra added.
Clustering 2993236 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[^C        ]
real	13m10,980s
user	14m23,131s
sys	0m10,089s
no start after 14'

test on 9f8608b5001ade6648507febef1bfbecf63f959f
langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/deisotoping$ time java -server -Xmx8g -jar /home/langella/developpement/git/deepprot/cbuild/bin/SpecOMS.jar -c
Reading configuration
Parsing proteins...
86934 database proteins added...
Adding contaminants...
116 contaminants proteins added...
Nombre de peptides:2950099
Constructing theoretical spectra...
2950099 theoretical spectra added.
43137 experimental spectra added.
Clustering 2993236 spectra...
Building SpecTrees...
c'est extractor
Running SpecXtract and SpecShift...
[**********]
Done.


Dataset analyzed in 3401 seconds.

real	56m42,141s
user	89m36,570s
sys	0m9,020s
langella@piccolo:~/developpement/git/deepprot/test/java/tests/cmd/b1922_293T_proteinID_02A_QE3_122212/deisotoping$ Rscript --vanilla ../../../../R/count.R results.csv
[1] "total: 37044"
[1] "decoy: 6661"
      Total Decoy peaks        FDR
 [1,]     1     0    30  0.0000000
 [2,]     1     0    29  0.0000000
 [3,]     3     0    28  0.0000000
 [4,]     9     0    27  0.0000000
 [5,]    22     0    26  0.0000000
 [6,]    34     0    25  0.0000000
 [7,]    56     0    24  0.0000000
 [8,]    95     0    23  0.0000000
 [9,]   152     0    22  0.0000000
[10,]   251     0    21  0.0000000
[11,]   422     0    20  0.0000000
[12,]   666     0    19  0.0000000
[13,]  1036     0    18  0.0000000
[14,]  1613     0    17  0.0000000
[15,]  2433     3    16  0.1233046
[16,]  3587     6    15  0.1672707
[17,]  5136    12    14  0.2336449
[18,]  7097    20    13  0.2818092
[19,]  9720    54    12  0.5555556
[20,] 13058   198    11  1.5163118
[21,] 17426   709    10  4.0686331
[22,] 23478  1965     9  8.3695374
[23,] 30594  4109     8 13.4307381
[24,] 37044  6661     7 17.9813195
