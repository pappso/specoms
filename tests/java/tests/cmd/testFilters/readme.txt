This test is for testing the new filters once implemented in SpecOMS

-----------------------------------------------------------------------------------------------------

There is two .sh files that coordinate the rest of the program

-old.sh

just launching the regular SpecOMS program on all files that you want to test

-test.sh

same but with the new version and test all different filter one by one (no combinatorics yet)

----------------------------------------------------------------------------------------------------

the test was done on 12 files, in 1/ you can find the basis of how it is done.

-execution_parameters : a default parameter file

-file_config : the pathway to the files used as databases

-test.sh the script that launch, for a specific mgf file all of the possible filters. 
Note that it will add one or twoparameter lines to the exec_parameters inside the Filter folder.