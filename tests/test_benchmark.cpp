
/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// cmake .. -DCMAKE_BUILD_TYPE=Debug  -DMAKE_TESTS=1  -DUSEPAPPSOTREE=1

//./tests/catch2-only-tests [benchmark] -s
//./tests/catch2-only-tests [benchmark] -s --benchmark-samples 5

#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

#include <QString>
#include <pappsomspp/processing/spectree/spectree.h>
#include "../src/core/fastasourcelist.h"
#include "../src/utils/monitorcli.h"
#include "../src/utils/utils.h"
#include "../src/core/msspectrumreader.h"
#include <pappsomspp/msfile/msfileaccessor.h>
#include "config.h"


TEST_CASE("Test spectree benchmark", "[benchmark]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: benchmark ::..", "[benchmark]")
  {

    QTextStream outputStream(stdout, QIODevice::WriteOnly);
    MonitorCli monitor(outputStream);
    DeepProtParams &deepprot_params   = DeepProtParams::getInstance();
    FastaSourceListSPtr fasta_sources = std::make_shared<FastaSourceList>(
      FastaSourceList(deepprot_params, monitor));
    qDebug();
    fasta_sources.get()->add(
      QFileInfo(QString("%1/tests/testAlignment/testAlignment1_database.fasta")
                  .arg(CMAKE_SOURCE_DIR)),
      true);

    qDebug();
    // reading and converting mass Spectrum from mz data :
    SpectrumIntStoreSPtr spectrum_int_store =
      std::make_shared<SpectrumIntStore>(deepprot_params);
    qDebug();
    PeptideDatabaseSPtr peptide_database =
      std::make_shared<PeptideDatabase>(fasta_sources);

    // read Fasta fasta sources
    fasta_sources.get()->buildTheoreticalSpectrumInt(peptide_database,
                                                     spectrum_int_store);
    qDebug();
    // read mz data
    MsSpectrumReader read(monitor, deepprot_params, spectrum_int_store);

    read.setNeedMsLevelPeakList(1, false);
    read.setReadAhead(true);

    pappso::MsFileAccessor accessor(
      QString("%1/tests/testAlignment/testAlignment1.mgf")
        .arg(CMAKE_SOURCE_DIR),
      0);
    accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                        pappso::FileReaderType::tims_ms2);
    pappso::MsRunReaderSPtr msrun_reader =
      accessor.getMsRunReaderSPtrByRunId("", "msrun");
    msrun_reader.get()->readSpectrumCollection(read);

    pappso::spectree::BucketClustering bucket_clustering;
    spectrum_int_store->buildBucketClustering(bucket_clustering);

    std::map<std::size_t, std::size_t> map_count;
    BENCHMARK("benchmark spectree building")
    {
      pappso::spectree::SpecTree spec_tree(bucket_clustering);
    };
  }
}
