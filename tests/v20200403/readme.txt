
time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep.fasta /media/langella/pappso/deepprot/donnees/PXD004732/crapPeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.ods


langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.ods
using up to 8 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 55 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 7 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 2174 seconds
this is the end
overall process took 2278 seconds
peptide identification is finished

real	38m10,372s
user	37m29,450s
sys	0m31,977s

langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt.ods
using up to 8 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 56 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 6 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 2554 seconds
this is the end
overall process took 2667 seconds
peptide identification is finished

real	44m28,026s
user	43m56,539s
sys	0m24,744s

 
langella@piccolo:~/developpement/git/deepprot/cbuild$  time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt2.ods
using up to 8 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 56 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 7 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 2220 seconds
this is the end
overall process took 2329 seconds
peptide identification is finished

real	38m50,110s
user	38m38,225s
sys	0m7,301s

langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_normal.ods
using up to 8 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 56 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 6 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 2193 seconds
this is the end
overall process took 2302 seconds
peptide identification is finished

real	38m23,327s
user	38m14,623s
sys	0m5,988s


time ./src/deepprot -p ../test/v20200403/PXD004732_nomc_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt_nomc.ods



langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt4.ods -c 4
using up to 4 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 56 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 6 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 4 threads
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 1427 seconds
this is the end
overall process took 1532 seconds
peptide identification is finished

real	25m32,923s
user	103m57,018s
sys	13m28,020s






langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot.ods -c 1
using up to 1 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 54 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 6 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 1 thread
starting SpecXtract operation
***************************************************************************************************
SpecXtract took 2124 seconds
this is the end
overall process took 2230 seconds
peptide identification is finished

real	37m11,563s
user	36m55,540s
sys	0m11,475s



langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt4.ods -c 4
using up to 4 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 51 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 6 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 4 threads
starting SpecXtract operation
****************************************************************************************************
SpecXtract took 1141 seconds
this is the end
overall process took 1236 seconds
peptide identification is finished

real	20m36,811s
user	95m34,103s
sys	0m24,728s


langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt2.ods -c 2
using up to 2 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 51 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 6 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 2 threads
starting SpecXtract operation
****************************************************************************************************
SpecXtract took 1185 seconds
this is the end
overall process took 1281 seconds
peptide identification is finished

real	21m21,969s
user	59m39,425s
sys	0m34,147s

langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt4.ods -c 4
using up to 4 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 53 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 7 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 4 threads
starting SpecXtract operation
****************************************************************************************************
SpecXtract took 1137 seconds
this is the end
overall process took 1238 seconds
peptide identification is finished

real	20m39,186s
user	95m30,151s
sys	0m40,693s


langella@piccolo:~/developpement/git/deepprot/cbuild$ time ./src/deepprot -p ../test/v20200403/PXD004732_params.csv -m /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta -o /media/langella/pappso/deepprot/donnees/PXD004732/v20200403/PXD004732_SENSIBLE_T7S9_MIN-500_deepprot_mt4.mzid -c 2
using up to 2 threads
getting mz data file /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
adding Fasta file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
building theoretical spectra
reading FASTA file /media/langella/pappso/deepprot/donnees/PXD004732/Araport11_genes.201606.pep-Crap_PeptidesRef_Pool65.fasta
96972 proteins, 4986838 digested protopeptides, 2549348 protopeptides registered, 2549348 spectrum modelized from peptides
Building theoretical spectrum took 54 seconds
reading mz data from /media/langella/pappso/deepprot/donnees/PXD004732/01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2.mzXML
Building experimental spectrum took 7 seconds
55456 experimental spectrum used
bucket_clustering
spec_trees
Spectree extraction using 2 threads
starting SpecXtract operation
****************************************************************************************************
SpecXtract took 1792 seconds
this is the end
overall process took 1920 seconds
peptide identification is finished

real	32m0,489s
user	89m54,718s
sys	1m6,789s

