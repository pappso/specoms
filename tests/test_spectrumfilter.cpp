
/**
 * \file test/test_spectrumfilter.cpp
 * \date 6/5/2019
 * \author Olivier Langella
 * \brief test spectrum filter
 */


/*
 * DeepProt, Open Modification Search engine for proteomics
 * Copyright (C) 2019  Olivier Langella <olivier.langella@u-psud.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// ./tests/catch2-only-tests [SpectrumFilter] -s


#include <QDebug>
#include <QString>
#include <iostream>
#include <cstdio>
#include <QFile>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>

#include <pappsomspp/msrun/output/mgfoutput.h>
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/filters/filtermorpho.h>

#include "config.h"

#include <catch2/catch_test_macros.hpp>

pappso::QualifiedMassSpectrum
readQualifiedMassSpectrumMgf(const QString &filename)
{
  try
    {
      qDebug();
      pappso::MsFileAccessor accessor(filename, "msrun");
      qDebug();
      pappso::MsRunReaderSPtr reader =
        accessor.getMsRunReaderSPtrByRunId("", "msrun");
      qDebug() << accessor.getMsRunIds().front().get()->getXmlId();
      std::cout << reader->spectrumListSize() << std::endl;
      pappso::QualifiedMassSpectrum spectrum_sp =
        reader->qualifiedMassSpectrum(0, true);
      return spectrum_sp;
    }
  catch(pappso::PappsoException &error)
    {
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << QString("ERROR reading file %1 : %2")
                     .arg(filename)
                     .arg(error.qwhat())
                     .toStdString()
                     .c_str();
      throw error;
    }
}


TEST_CASE("Test spectrum filters", "[SpectrumFilter]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: SpectrumFilter ::..", "[SpectrumFilter]")
  {
    std::cout << std::endl
              << "..:: DeepProt Spectrum Filters test ::.." << std::endl;


    pappso::QualifiedMassSpectrum spectrum_scan_15968 =
      readQualifiedMassSpectrumMgf(
        QString(CMAKE_SOURCE_DIR)
          .append("/tests/data/"
                  "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-49434.mgf"));

    spectrum_scan_15968.getMassSpectrumSPtr().get()->filter(
      pappso::FilterHighPassPercentage(0.01));

    QFile file("01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-49434_1pc.mgf");
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
      {
        pappso::MgfOutput mgf_output(&file);
        mgf_output.write(spectrum_scan_15968);
        mgf_output.close();
        file.close();
      }
  }
}
