[Setup]
AppName=${SOFTWARE_NAME}

#define public winerootdir "z:"
; Set version number below
#define public version "${VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/langella/developpement/git/specoms/"
#define cmake_build_dir "z:/home/langella/developpement/git/specoms/wbuild"

; Set version number below
AppVerName=${SOFTWARE_NAME} version {#version}
DefaultDirName={pf}\specoms
DefaultGroupName=specoms
OutputDir="{#cmake_build_dir}"

; Set version number below
OutputBaseFilename=specoms-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=specoms-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2020- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="SpecOMS, ANR DeepProt"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "z:/win64/mxeqt6_dll/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/qt6/plugins/*"; DestDir: {app}/plugins; Flags: ignoreversion recursesubdirs
Source: "z:/home/langella/developpement/git/cutelee/wbuild/templates/lib/libCuteleeQt6Templates.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/cutelee/wbuild/cutelee-qt6/6.1/*"; DestDir: {app}/cutelee-qt6/6.1; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

;Source: "{#cmake_build_dir}\src\libspecoms.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

; Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: specomsComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: specomsComp
; Source: "{#sourceDir}\win64\xtandempipeline_icon.ico"; DestDir: {app}; Components: specomsComp

Source: "{#cmake_build_dir}\src\specoms.exe"; DestDir: {app}; Components: specomsComp 
Source: "{#cmake_build_dir}\src\specoms-gui.exe"; DestDir: {app}; Components: specomsComp 

[Icons]
Name: "{group}\specoms"; Filename: "{app}\specoms-gui.exe"; WorkingDir: "{app}";IconFilename: "{app}\xtandempipeline_icon.ico"
Name: "{group}\Uninstall specoms"; Filename: "{uninstallexe}"

[Types]
Name: "specomsType"; Description: "Full installation"

[Components]
Name: "specomsComp"; Description: "SpecOMS files"; Types: specomsType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\specoms-gui.exe"; Description: "Launch SpecOMS"; Flags: postinstall nowait unchecked
