
# cd build
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/pappsomspp_devel.cmake ..

set(USEQT5 1)
set(MAKE_TEST 0)

SET (PAPPSOMSPP_QT5_FOUND  "1")
SET (PAPPSOMSPP_DIR  "/home/langella/developpement/git/pappsomspp")
SET (PAPPSOMSPP_INCLUDE_DIR "${PAPPSOMSPP_DIR}/src")
SET (PAPPSOMSPP_QT5_LIBRARY "${PAPPSOMSPP_DIR}/cbuild/src/libpappsomspp-qt5.so")
# SET (PAPPSOMSPP_QT4_LIBRARY "${PAPPSOMSPP_DIR}/build/src/libpappsomspp-qt4.so")
