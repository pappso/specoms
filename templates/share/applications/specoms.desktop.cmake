#https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
[Desktop Entry]
Version=1.0
Name=SpecOMS
Categories=Science;Biology;Education;Qt;
Comment=Open Mass Search peptide identification
Exec=${CMAKE_INSTALL_PREFIX}/bin/specoms-gui %f
Icon=${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/scalable/apps/deepprot.svg
Terminal=false
Type=Application
StartupNotify=true
Keywords=Mass spectrometry;Protein inference;Peptide;
MimeType=application/x-specoms;
